﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllCities
Comments		: 26-03-2021 | Amit Khanna | This procedure is used to get All cities.

Test Execution	: EXEC app.GetAllCities
					@SubscriberId = 1,
					@StateId = NULL,
					@CountryId = NULL,
					@Status = NULL,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@IpAddress = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [app].[GetAllCities]
(
	@SubscriberId INT,
	@StateId INT,
	@CountryId INT,
	@Status TINYINT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [app].[GetAllCities]','
										@SubscriberId =',@SubscriberId,',
										@StateId =',@StateId,',
										@CountryId =',@CountryId,',
										@Status = ',@Status,',
										@Start = ',@Start,',
										@Length = ',@Length,',
										@SearchKeyword = ','',@SearchKeyword,'',',
										@SortExpression = ','',@SortExpression,'',',
										@IpAddress = ','',@IpAddress,'',',
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[app].[GetAllCities]',
			@ExecutionTime = GETDATE()	
			
   CREATE TABLE #TempTable(Id INT); 
    BEGIN TRY
		
		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[app].[Cities] AS us
		WHERE 
			us.[Status] = ISNULL(@Status,us.[Status])
			AND us.EndDate IS NULL
			AND us.StateId = ISNULL(@StateId,us.StateId)
			AND us.CountryId = ISNULL(@CountryId,us.CountryId)
			AND 
			(
					us.[Name] LIKE '%' + ISNULL(@SearchKeyword,us.[Name]) +'%'
			);

		SELECT @TotalRecords =COUNT(Id)  FROM #TempTable 

		SELECT
			us.Id,
			us.[Name],
			st.[Name] AS [State],
			ct.[Name] AS Country,
			us.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN [app].Cities us ON tmp.Id = us.Id
			INNER JOIN [app].States AS st ON us.StateId = st.Id
			INNER JOIN [app].Countries AS ct ON us.CountryId = ct.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.CreatedDate END ASC,
			CASE WHEN @SortExpression = 'Name asc' THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'Name desc' THEN us.[Name] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
