﻿CREATE TABLE [app].[Cities] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [CountryId]   INT           NOT NULL,
    [StateId]     INT           NOT NULL,
    [Name]        VARCHAR (100) NOT NULL,
    [Status]      TINYINT       CONSTRAINT [DF__Cities__Status__24285DB4] DEFAULT ((1)) NOT NULL,
    [EndDate]     DATETIME      NULL,
    [CreatedBy]   INT           NULL,
    [CreatedDate] SMALLDATETIME CONSTRAINT [DF__Cities__CreatedD__251C81ED] DEFAULT (getdate()) NOT NULL,
    [UpdatedBy]   INT           NULL,
    [UpdatedDate] SMALLDATETIME NULL,
    CONSTRAINT [PK__Cities__3214EC07D0D25ED7] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Cities__CountryI__22401542] FOREIGN KEY ([CountryId]) REFERENCES [app].[Countries] ([Id]),
    CONSTRAINT [FK__Cities__StateId__2334397B] FOREIGN KEY ([StateId]) REFERENCES [app].[States] ([Id])
);

