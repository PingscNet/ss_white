﻿CREATE TABLE [Logging].[Requests] (
    [Id]           INT              IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT              NULL,
    [RequestId]    UNIQUEIDENTIFIER NULL,
    [UserId]       INT              NULL,
    [Data]         VARCHAR (MAX)    NULL,
    [FileName]     VARCHAR (MAX)    NULL,
    [Method]       VARCHAR (6)      NULL,
    [IpAddress]    VARCHAR (45)     NULL,
    [Stamp]        SMALLDATETIME    NULL,
    CONSTRAINT [PK__Requests__3214EC0726241FF3] PRIMARY KEY CLUSTERED ([Id] ASC)
);

