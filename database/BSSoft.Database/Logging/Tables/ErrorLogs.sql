﻿CREATE TABLE [Logging].[ErrorLogs] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId]     INT           NULL,
    [ErrorNumber]      VARCHAR (MAX) NULL,
    [ErrorSeverity]    VARCHAR (MAX) NULL,
    [ErrorLine]        INT           NULL,
    [ProcedureName]    VARCHAR (200) NULL,
    [ExecutionCommand] VARCHAR (MAX) NULL,
    [Message]          VARCHAR (MAX) NULL,
    [ErrorState]       INT           NULL,
    [IpAddress]        VARCHAR (45)  NULL,
    [Stamp]            SMALLDATETIME NULL,
    CONSTRAINT [PK__ErrorLog__3214EC077C5F662D] PRIMARY KEY CLUSTERED ([Id] ASC)
);

