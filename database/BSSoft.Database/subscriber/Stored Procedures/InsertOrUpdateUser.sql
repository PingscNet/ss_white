﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateUser
Comments		: 28-02-2021 | Amit Khanna | This procedure is used to insert user or update user by Id.

Test Execution	: EXEC subscriber.InsertOrUpdateUser
						@SubscriberId = 1,
						@Id = NULL,
						@UserTypeActive = 2,
						@FirstName = 'Amit',
						@LastName 'Khanna',
						@UserName 'amitkhanna',
						@Password = 'M2MrYCbl2TkXcE1Gow7ADrRN7Ds1MVkY9qQY+8lnJWc=',
						@MobileNumber  '9727466371',
						@EmailAddress = 'amit.khanna2201@gmail.com',
						@Address = 'Ahmedabad',
						@ProfilePicPath = NULL,
						@IpAddress = '',
						@CreatedBy = 1,
						@CreatedDate = '2021-02-28',
						@StatusTypeInActive = 2
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[InsertOrUpdateUser]
(
	@SubscriberId INT,
	@Id INT,
	@UserType TINYINT,
	@FirstName VARCHAR(100),
	@LastName VARCHAR(100),
	@UserName VARCHAR(100),
	@Password VARCHAR(MAX),
	@MobileNumber VARCHAR(20),
	@EmailAddress VARCHAR(100),
	@Address VARCHAR(200),
	@ProfilePicPath VARCHAR(MAX),
	@EntityIds [common].IntType READONLY,
	@RoleIds [common].IntType READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeInActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[InsertOrUpdateUser]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@UserType =',@UserType,',
										@FirstName = ','',@FirstName,'','
										@LastName = ','',@LastName,'','
										@UserName = ','',@UserName,'','
										@Password = ','',@Password,'','
										@MobileNumber = ','',@MobileNumber,'','
										@EmailAddress = ','',@EmailAddress,'','
										@ProfilePicPath = ','',@ProfilePicPath,'','
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,'
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeInActive = ',@StatusTypeInActive
									  ),
			@ProcedureName = '[subscriber].[InsertOrUpdateUser]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@UserId INT;
	
	SELECT
		et.Item
	INTO
		#TempEntityIds
	FROM
		@EntityIds AS et;

	SELECT
		rids.Item
	INTO
		#TempRoleIds
	FROM
		@RoleIds AS rids;

    BEGIN TRY
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM subscriber.Users WHERE UserName = @UserName AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO subscriber.Users
				(
					SubscriberId,
					[Type],
					UserName,
					[Password],
					FirstName,
					LastName,
					EmailAddress,
					MobileNumber,
					[Address],
					ProfilePic,
					PasswordExpiry,
					[Status],
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@UserType,
					@UserName,
					@Password,
					@FirstName,
					@LastName,
					@EmailAddress,
					@MobileNumber,
					@Address,
					@ProfilePicPath,
					DATEADD(MONTH,6,@CreatedDate),
					@StatusTypeInActive,
					@CreatedBy,
					@CreatedDate
				);

				SET @UserId = SCOPE_IDENTITY();

				INSERT INTO subscriber.UserRoles
				(
					UserId,
					RoleId,
					CreatedBy,
					CreatedDate,
					IpAddress
				)
				SELECT
					@UserId,
					rids.Item,
					@CreatedBy,
					@CreatedDate,
					@IpAddress
				FROM
					#TempRoleIds As rids;

				INSERT INTO subscriber.UserEntities
				(
					UserId,
					EntityId,
					CreatedBy,
					CreatedDate,
					IpAddress
				)
				SELECT
					@UserId,
					eids.Item,
					@CreatedBy,
					@CreatedDate,
					@IpAddress
				FROM
					#TempEntityIds As eids;
					
				INSERT INTO subscriber.UserPasswordHistory
				(
					UserId,
					[Password],
					CreatedDate
				)
				VALUES
				(
					@UserId,
					@Password,
					@CreatedDate
				);

				COMMIT TRAN;
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM subscriber.Users WHERE UserName = @UserName AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN

				UPDATE
					subscriber.Users
				SET
					[Type] = @UserType,
					UserName = @UserName,
					FirstName = @FirstName,
					LastName = @LastName,
					EmailAddress = @EmailAddress,
					MobileNumber = @MobileNumber,
					[Address] = @Address,
					ProfilePic = ISNULL(@ProfilePicPath,ProfilePic),
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id
					AND SubscriberId = @SubscriberId;

				DELETE FROM
					subscriber.UserRoles
				WHERE
					UserId = @Id;

				DELETE FROM
					subscriber.UserEntities
				WHERE
					UserId = @Id;

				INSERT INTO subscriber.UserRoles
				(
					UserId,
					RoleId,
					CreatedBy,
					CreatedDate,
					IpAddress
				)
				SELECT
					@Id,
					rids.Item,
					@CreatedBy,
					@CreatedDate,
					@IpAddress
				FROM
					#TempRoleIds As rids;

				INSERT INTO subscriber.UserEntities
				(
					UserId,
					EntityId,
					CreatedBy,
					CreatedDate,
					IpAddress
				)
				SELECT
					@Id,
					eids.Item,
					@CreatedBy,
					@CreatedDate,
					@IpAddress
				FROM
					#TempEntityIds As eids;
				
				COMMIT TRAN;
			END
		END

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
		RETURN @ReturnValue;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
		RETURN @ReturnValue;
	END CATCH;
