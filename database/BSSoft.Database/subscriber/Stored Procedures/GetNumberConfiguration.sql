﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetNumberConfiguration
Comments		: 30-03-2021 | Amit Khanna | This procedure is used to get all number configuration.

Test Execution	: EXEC [subscriber].[GetNumberConfiguration]
						@SubscriberId =1,
						@EntityId = 1,
						@FinancialYear = 201920,
						@DocumentType = 1,
						@ModuleType = NULL,
						@Status = NULL,
						@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[GetNumberConfiguration]
(
	@SubscriberId INT,
	@EntityId INT,
	@FinancialYear INT,
	@DocumentType TINYINT,
	@ModuleType TINYINT,
	@Status TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[GetNumberConfiguration]','
										@SubscriberId =',@SubscriberId,',
										@EntityId = ',@EntityId,',
										@FinancialYear = ',@FinancialYear,',
										@DocumentType = ',@DocumentType,',
										@ModuleType = ',@ModuleType,',
										@Status = ',@Status,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[subscriber].[GetNumberConfiguration]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT); 
    BEGIN TRY
		SELECT
			dc.Id,
			dc.EntityId,
			dc.DocumentType,
			dc.ModuleType,
			dc.FinancialYear,
			(dc.LastNumber + 1) AS Number,
			dc.Prefix,
			dc.Suffix,
			dc.[Status]
		FROM
			subscriber.NumberConfigurations AS dc
		WHERE
			dc.SubscriberId = @SubscriberId
			AND ISNULL(dc.EntityId,'') = ISNULL(@EntityId,ISNULL(dc.EntityId,''))
			AND ISNULL(dc.FinancialYear,'') = ISNULL(@FinancialYear,ISNULL(dc.FinancialYear,''))
			AND ISNULL(dc.DocumentType,'') = ISNULL(@DocumentType,ISNULL(dc.DocumentType,''))
			AND ISNULL(dc.ModuleType,'') = ISNULL(@ModuleType,ISNULL(dc.ModuleType,''))
			AND dc.[Status] = ISNULL(@Status,dc.[Status]);

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
