﻿INSERT INTO subscriber.Companies
(
	[SubscriberId]  ,
	[Name]          ,
	[OwnerName]		,
	[Gstin]         ,
	[Pan]           ,
	[Address]       ,
	[StateId]       ,
	[CityId]        ,
	[EmailAddress]  ,
	[Status]        ,
	[EndDate]       ,
	[IpAddress]     ,
	[CreatedBy]     ,
	[CreatedDate]   ,
	[UpdatedBy]     ,
	[UpdatedDate]   
)
VALUES
(
	1,
	'Malani Constructions',
	'Malani Constructions',
	'24ABCDE123451ZH',
	'ABCDE12345',
	'Address',
	11,
	331,
	'malaniconstructions@gmail.com',
	1,
	NULL,
	NULL,
	1,
	GETDATE(),
	NULL,
	NULL
)