﻿DECLARE @SubscriberId INT;
INSERT INTO [subscriber].[Subscribers]
           ([FirstName]
           ,[LastName]
           ,[EmailAddress]
           ,[SubscriptionType]
           ,[SubscriptionStartDate]
           ,[SubscriptionEndDate]
           ,[Status]
           ,[EndDate])
     VALUES
           (
		   'Malani'
           ,'Constructions'
           ,'Malani Constructions Erp'
           ,4
           ,GETDATE()
           ,DATEADD(YEAR,1,GETDATE())
           ,1
           ,NULL)
SET @SubscriberId = SCOPE_IDENTITY();
INSERT INTO [subscriber].[Users]
           ([SubscriberId]
           ,[Type]
           ,[UserName]
           ,[Password]
           ,[FirstName]
           ,[LastName]
           ,[EmailAddress]
           ,[MobileNumber]
           ,[Address]
           ,[ProfilePic]
           ,[IsPasswordReset]
           ,[IsApproved]
           ,[ApprovedDate]
           ,[PasswordExpiry]
           ,[Status]
           ,[EndDate]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[UpdatedBy]
           ,[UpdatedDate])
     VALUES
           (
			@SubscriberId
           ,2
           ,'Admin'
           ,''
           ,'Admin'
           ,'Admin'
           ,'admin@admin.com'
           ,'972746637XX'
           ,'Demo Address'
           ,NULL
           ,1
           ,0
           ,NULL
           ,DATEADD(MONTH,6,GETDATE())
           ,1
           ,NULL
           ,1
           ,GETDATE()
           ,NULL
           ,NULL)
