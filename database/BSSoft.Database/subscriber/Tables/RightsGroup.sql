﻿CREATE TABLE [subscriber].[RightsGroup] (
    [Id]           INT           NOT NULL,
    [DisplayOrder] SMALLINT      NOT NULL,
    [Name]         VARCHAR (100) NOT NULL,
    [Acronym]      VARCHAR (50)  NOT NULL,
    [Status]       TINYINT       NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__RightsGr__3214EC0764C6F7C0] PRIMARY KEY CLUSTERED ([Id] ASC)
);

