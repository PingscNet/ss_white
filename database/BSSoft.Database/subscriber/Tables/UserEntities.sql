﻿CREATE TABLE [subscriber].[UserEntities] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [UserId]      INT           NOT NULL,
    [EntityId]    INT           NOT NULL,
    [CreatedBy]   INT           NOT NULL,
    [CreatedDate] SMALLDATETIME NOT NULL,
    [IpAddress]   VARCHAR (45)  NULL,
    [UpdatedBy]   INT           NULL,
    [UpdatedDate] SMALLDATETIME NULL,
    CONSTRAINT [PK__UserEnti__3214EC0751FD8F6D] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__UserEntit__Crate__7993056A] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__UserEntit__Entit__789EE131] FOREIGN KEY ([EntityId]) REFERENCES [master].[Companies] ([Id]),
    CONSTRAINT [FK__UserEntit__Updat__7A8729A3] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__UserEntit__UserI__77AABCF8] FOREIGN KEY ([UserId]) REFERENCES [subscriber].[Users] ([Id])
);

