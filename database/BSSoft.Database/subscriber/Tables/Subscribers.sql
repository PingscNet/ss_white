﻿CREATE TABLE [subscriber].[Subscribers] (
    [Id]                    INT           IDENTITY (1, 1) NOT NULL,
    [FirstName]             VARCHAR (100) NOT NULL,
    [LastName]              VARCHAR (100) NOT NULL,
    [EmailAddress]          VARCHAR (100) NOT NULL,
    [SubscriptionType]      TINYINT       NOT NULL,
    [SubscriptionStartDate] SMALLDATETIME NOT NULL,
    [SubscriptionEndDate]   SMALLDATETIME NOT NULL,
    [Status]                TINYINT       NOT NULL,
    [EndDate]               SMALLDATETIME NULL,
    CONSTRAINT [PK__Subscrib__3214EC0714E8D80A] PRIMARY KEY CLUSTERED ([Id] ASC)
);

