﻿CREATE TABLE [document].[AllocatedLotNumbers] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [SubscriberId]       INT             NULL,
    [PurchaseDocumentId] INT             NULL,
    [SalesDocumentId]    INT             NULL,
    [DocumentType]       TINYINT         NOT NULL,
    [LotNumber]          INT             NOT NULL,
    [Quantity]           DECIMAL (18, 2) NOT NULL,
    [Weight]             DECIMAL (18, 2) NULL,
    CONSTRAINT [PK__Allocate__3214EC0758951AC5] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Allocated__Purch__23A93AC7] FOREIGN KEY ([PurchaseDocumentId]) REFERENCES [document].[PurchaseDocuments] ([Id]),
    CONSTRAINT [FK__Allocated__Sales__249D5F00] FOREIGN KEY ([SalesDocumentId]) REFERENCES [document].[SalesDocuments] ([Id]),
    CONSTRAINT [FK_AllocatedLotNumbers_AllocatedLotNumbers_SubscriberId] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id])
);

