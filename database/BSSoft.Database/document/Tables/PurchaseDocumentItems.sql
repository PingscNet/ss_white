﻿CREATE TABLE [document].[PurchaseDocumentItems] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [PurchaseDocumentId] INT             NOT NULL,
    [ItemId]             INT             NOT NULL,
    [UnitId]             INT             NOT NULL,
    [CrateId]            INT             NOT NULL,
    [Description]        VARCHAR (150)   NULL,
    [Quantity]           DECIMAL (18, 2) NOT NULL,
    [Weight]             DECIMAL (18, 2) NULL,
    [FreeQuantity]       DECIMAL (18, 2) NULL,
    [Rate]               DECIMAL (18, 2) NULL,
    [Amount]             DECIMAL (18, 2) NULL,
    [LotNumber]          INT             NOT NULL,
    [GroupId]            INT             NULL,
    CONSTRAINT [PK__Purchase__3214EC075BE10028] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__PurchaseD__Crate__62458BBE] FOREIGN KEY ([CrateId]) REFERENCES [master].[Crates] ([Id]),
    CONSTRAINT [FK__PurchaseD__ItemI__605D434C] FOREIGN KEY ([ItemId]) REFERENCES [master].[Items] ([Id]),
    CONSTRAINT [FK__PurchaseD__Purch__5F691F13] FOREIGN KEY ([PurchaseDocumentId]) REFERENCES [document].[PurchaseDocuments] ([Id]),
    CONSTRAINT [FK__PurchaseD__UnitI__61516785] FOREIGN KEY ([UnitId]) REFERENCES [master].[UQCS] ([Id])
);

