﻿/*------------------------------------------------------------------------------------------------------------
Name			: DeletePurchaseDocument
Comments		: 23-04-2021 | Amit Khanna | This procedure is used to delete purchase documents if its respective lot number is not allocated.

Test Execution	: EXEC [document].[DeletePurchaseDocument]
						@SubscriberId =  1,
						@Id =  5,
						@IpAddress =  1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[DeletePurchaseDocument]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[DeletePurchaseDocument]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].[DeletePurchaseDocument]',
			@ExecutionTime = GETDATE()	
	
	DECLARE @ReturnValue INT = 1;
    BEGIN TRY
		BEGIN TRAN

		IF EXISTS
		(
			SELECT 
				aln.Id
			FROM 
				document.AllocatedLotNumbers AS aln
				INNER JOIN document.AllocatedLotNumbers AS alni ON aln.LotNumber = alni.LotNumber AND alni.SalesDocumentId IS NOT NULL
			WHERE
				aln.SubscriberId = @SubscriberId
				AND aln.PurchaseDocumentId = @Id
		)
		BEGIN
			SET @ReturnValue = -1;
		END
		ELSE 
		BEGIN
			DELETE FROM
				document.PurchaseDocumentItems
			WHERE
				PurchaseDocumentId = @Id;

			DELETE FROM
				document.AllocatedLotNumbers
			WHERE
				PurchaseDocumentId = @Id;

			DELETE FROM
				document.PurchaseDocumentHeaders
			WHERE
				PurchaseDocumentId = @Id;

			DELETE FROM
				document.PurchaseDocuments
			WHERE
				Id = @Id
				AND SubscriberId = @SubscriberId;	
		END
			
		COMMIT TRAN;
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		ROLLBACK TRAN;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
