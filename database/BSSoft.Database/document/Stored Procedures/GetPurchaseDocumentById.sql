﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetPurchaseDocumentById
Comments		: 09-04-2021 | Amit Khanna | This procedure is used to get purchase document by Id.

Test Execution	: EXEC [document].GetPurchaseDocumentById
					@SubscriberId =  1,
					@Id =  10,
					@IpAddress =  NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetPurchaseDocumentById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].GetPurchaseDocumentById','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].GetPurchaseDocumentById',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			pd.Id,
			pd.CompanyId,
			pd.ClientId,
			ct.[CompanyName] AS ClientName,
			pd.VoucherNumber,
			pd.[Date],
			pd.VoucherType,
			pd.ChallanNumber,
			pd.ChallanDate,
			pd.VehicleNumber,
			pd.MarkNumber,
			pd.[Month],
			pd.FinancialYear,
			pd.ReceivingPerson,
			pd.TotalAmount,
			pd.Remarks
		FROM
			[document].PurchaseDocuments AS pd
			INNER JOIN [master].Clients AS ct ON pd.ClientId = ct.Id
		WHERE
			pd.Id = @Id;

		SELECT
			pdi.ItemId,
			it.[Name],
			pdi.UnitId,
			uqc.[Name] AS Unit,
			pdi.CrateId,
			crt.[Name] AS CrateName,
			pdi.[Description],
			pdi.Quantity,
			pdi.[Weight],
			pdi.FreeQuantity,
			aln.Quantity AS CurrentQuantity,
			aln.[Weight] AS CurrentWeight,
			pdi.Rate,
			pdi.Amount,
			pdi.LotNumber
		FROM
			[document].PurchaseDocumentItems AS pdi
			INNER JOIN document.AllocatedLotNumbers AS aln ON pdi.PurchaseDocumentId = aln.PurchaseDocumentId AND pdi.LOtNumber = aln.LotNumber
			INNER JOIN [master].Items AS it ON pdi.Itemid = it.Id
			INNER JOIN [master].Crates AS crt ON pdi.CrateId= crt.Id
			INNER JOIN [master].Uqcs AS uqc ON pdi.UnitId = uqc.Id
		WHERE
			pdi.PurchaseDocumentId = @Id;

		SELECT
			phdr.HeaderId,
			hdr.[Name] AS [Name],
			phdr.CalculationType,
			phdr.[Percentage],
			phdr.[Value]
		FROM
			[document].PurchaseDocumentHeaders phdr
			INNER JOIN [master].Headers AS hdr ON phdr.HeaderId = hdr.Id
		WHERE
			PurchaseDocumentId = @Id;


		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
