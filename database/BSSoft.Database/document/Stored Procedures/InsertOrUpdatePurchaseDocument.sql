﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdatePurchaseDocument
Comments		: 09-04-2021 | Amit Khanna | This procedure is used to insert or update purchase document.

Test Execution	: EXEC document.InsertOrUpdatePurchaseDocument
						@SubscriberId = 1,
						@FinancialYear = 2,
						@Id = 1
						@ClientId = 1,
						@VoucherNumber = 1,
						@Date = '2022-01-01',
						@VoucherType = 1,
						@Month = 4,
						@ChallanNumber = NULL,
						@ChallanDate = NULL,
						@MarkNumber = NULL,
						@ReceivingPerson = NULL,
						@TotalAmount = 410.05,
						@Remarks = NULL,
						@Prefix = NULL,
						@Suffix = NULL,
						@IpAddress = NULL,
						@CreatedBy = 1,
						@CreatedDate  = '2021-01-01',
						@StatusTypeActive = 1,
						@ModuleTypeLotNumber = 6,
						@DocumentTypePurchase = 2;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[InsertOrUpdatePurchaseDocument]
(
	@SubscriberId INT,
	@FinancialYear INT,
	@Id INT,
	@CompanyId INT,
	@ClientId INT,
	@VoucherNumber VARCHAR(MAX),
	@Date DATE,
	@VoucherType TINYINT,
	@Month TINYINT,
	@ChallanNumber VARCHAR(MAX),
	@ChallanDate DATE,
	@VehicleNumber VARCHAR(20),
	@MarkNumber VARCHAR(100),
	@ReceivingPerson VARCHAR(50),
	@TotalAmount DECIMAL(18,2),
	@Remarks VARCHAR(100),
	@Prefix VARCHAR(MAX),
	@Suffix VARCHAR(MAX),
	@DocumentItems [document].PurchaseDocumentItemType READONLY,
	@DocumentHeaders [document].[DocumentHeaderTableType] READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT,
	@ModuleTypeLotNumber TINYINT,
	@DocumentTypePurchase TINYINT,
	@ReturnValue SMALLINT OUTPUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC document.InsertOrUpdatePurchaseDocument','
										@SubscriberId =',@SubscriberId,',
										@FinancialYear =',@FinancialYear,',
										@Id =',@Id,',
										@CompanyId =',@CompanyId,',
										@ClientId =',@ClientId,',
										@VoucherNumber =''',@VoucherNumber,''',
										@Date =',@Date,',
										@VoucherType =',@VoucherType,',
										@Month =',@Month,',
										@ChallanNumber =''',@ChallanNumber,''',
										@ChallanDate =',@ChallanDate,',
										@VehicleNumber =''',@VehicleNumber,''',
										@MarkNumber =''',@MarkNumber,''',
										@ReceivingPerson =''',@ReceivingPerson,''',
										@TotalAmount =',@TotalAmount,',
										@Remarks =''',@Remarks,''',
										@Prefix = ','',@Prefix,'','
										@Suffix = ','',@Suffix,'','
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive,',
										@ModuleTypeLotNumber =',@ModuleTypeLotNumber,',
										@DocumentTypePurchase =',@DocumentTypePurchase,',
										@ReturnValue =',@ReturnValue
									  ),

			@ProcedureName = 'document.InsertOrUpdatePurchaseDocument',
			@ExecutionTime = GETDATE()	
			
	DECLARE @PurchaseDocumentId INT,@LastNumber INT,@LastLotNumber INT;
	SET @ReturnValue = 1;
    BEGIN TRY
		EXEC @LastNumber = [subscriber].GetLastNumber
									@SubscriberId = @SubscriberId,
									@EntityId = @CompanyId,
									@FinancialYear = @FinancialYear,
									@DocumentType = @DocumentTypePurchase,
									@ModuleType = NULL,
									@Status = @StatusTypeActive,
									@IpAddress = @IpAddress;

			EXEC @LastLotNumber = [subscriber].GetLastNumber
									@SubscriberId = @SubscriberId,
									@EntityId = @CompanyId,
									@FinancialYear = NULL,
									@DocumentType = NULL,
									@ModuleType = @ModuleTypeLotNumber,
									@Status = @StatusTypeActive,
									@IpAddress = @IpAddress;
		
		SET @VoucherNumber = CONCAT(@Prefix,@LastNumber,@Suffix);
		SET @LastLotNumber = @LastLotNumber - 1;
		CREATE TABLE #TempPurchaseDocumentItems
		(
			ItemId			INT					NOT NULL, 
			CrateId			INT					NOT NULL,
			UnitId			INT					NOT NULL,
			[Description]	VARCHAR(150)		NULL,
			Quantity		DECIMAL(18,2)		NOT NULL,
			[Weight]		DECIMAL(18,2)		NULL,
			FreeQuantity	DECIMAL(18,2)		NULL,
			Rate			DECIMAL(18,2)		NULL,
			TotalAmount		DECIMAL(18,2)		NULL,
			LotNumber		INT					NOT NULL,
			IsLotNumberAlreadyPresent BIT NOT NULL DEFAULT 0 
		);

		CREATE TABLE #TempErrorLotNumbers
		(
			Id INT IDENTITY(1,1),
			LotNumber INT NOT NULL,
			Quantity DECIMAL(18,2),
			[Weight] DECIMAL(18,2)
		);

		CREATE TABLE #TempAllocatedLotNumbers
		(
			LotNumber INT NOT NULL,
			Quantity DECIMAL(18,2),
			[Weight] DECIMAL(18,2)
		)

		IF(@Id IS NULL)
		BEGIN
			INSERT INTO #TempPurchaseDocumentItems
			(
				ItemId,
				CrateId,
				UnitId,
				[Description],
				Quantity,
				[Weight],
				FreeQuantity,
				Rate,
				TotalAmount,
				LotNumber
			)
			SELECT
				di.ItemId,
				di.CrateId,
				di.UnitId,
				di.[Description],
				di.Quantity,
				di.[Weight],
				di.FreeQuantity,
				di.Rate,
				di.TotalAmount,
				ROW_NUMBER()  OVER(ORDER BY LotNumber) + @LastLotNumber   AS LotNumber
			FROM
				@DocumentItems AS di;

			SELECT
				TOP 1
				@LastLotNumber = tpi.LotNumber
			FROM
				#TempPurchaseDocumentItems AS tpi
			ORDER BY
				tpi.LotNumber DESC;
		END
		ELSE
		BEGIN
			INSERT INTO #TempPurchaseDocumentItems
			(
				ItemId,
				CrateId,
				UnitId,
				[Description],
				Quantity,
				[Weight],
				FreeQuantity,
				Rate,
				TotalAmount,
				LotNumber,
				IsLotNumberAlreadyPresent
			)
			SELECT
				di.ItemId,
				di.CrateId,
				di.UnitId,
				di.[Description],
				di.Quantity,
				di.[Weight],
				di.FreeQuantity,
				di.Rate,
				di.TotalAmount,
				di.LotNumber,
				1
			FROM
				@DocumentItems AS di
			WHERE
				di.LotNumber IS NOT NULL;

			INSERT INTO #TempPurchaseDocumentItems
			(
				ItemId,
				CrateId,
				UnitId,
				[Description],
				Quantity,
				[Weight],
				FreeQuantity,
				Rate,
				TotalAmount,
				LotNumber
			)
			SELECT
				di.ItemId,
				di.CrateId,
				di.UnitId,
				di.[Description],
				di.Quantity,
				di.[Weight],
				di.FreeQuantity,
				di.Rate,
				di.TotalAmount,
				ROW_NUMBER()  OVER(ORDER BY LotNumber) + @LastLotNumber   AS LotNumber
			FROM
				@DocumentItems AS di
			WHERE
				di.LotNumber IS NULL;

			SELECT
				TOP 1
				@LastLotNumber = tpi.LotNumber
			FROM
				#TempPurchaseDocumentItems AS tpi
			ORDER BY
				tpi.LotNumber DESC;

			INSERT INTO #TempAllocatedLotNumbers
			(
				LotNumber,
				Quantity,
				[Weight]
			)
			SELECT
				aln.LotNumber,
				SUM(aln.Quantity),
				SUM(aln.[Weight])
			FROM
				document.AllocatedLotNumbers AS aln
				INNER JOIN #TempPurchaseDocumentItems AS pdi ON aln.LotNumber = pdi.LotNumber
			WHERE
				aln.SubscriberId = @SubscriberId
				AND aln.SalesDocumentId IS NOT NULL
			GROUP BY
				aln.LotNumber;

			INSERT INTO #TempErrorLotNumbers
			(
				LotNumber,
				Quantity,
				[Weight]
			)
			SELECT
				aln.LotNumber,
				aln.Quantity,
				aln.[Weight]
			FROM
				#TempAllocatedLotNumbers AS aln
				INNER JOIN #TempPurchaseDocumentItems AS pdi ON aln.LotNumber = pdi.LotNumber
			WHERE
				(
					pdi.Quantity  < aln.Quantity
					OR
					pdi.[Weight] < aln.[Weight]
				)
		END

		SELECT
			dh.HeaderId,
			dh.[Percentage],
			dh.CalculationType,
			dh.[Value]
		INTO
			#DocumentHeaders
		FROM
			@DocumentHeaders AS dh
			
		IF EXISTS(SELECT Id FROM #TempErrorLotNumbers)
		BEGIN
			SET @ReturnValue = -3;
		END
		ELSE
		BEGIN
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [document].PurchaseDocuments WHERE SubscriberId = @SubscriberId AND [VoucherNumber] = @VoucherNumber AND CompanyId = @CompanyId AND FinancialYear = @FinancialYear AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO [document].PurchaseDocuments
				(
					SubscriberId,
					CompanyId,
					ClientId,
					VoucherNumber,
					[Date],
					VoucherType,
					ChallanNumber,
					ChallanDate,
					VehicleNumber,
					MarkNumber,
					[Month],
					FinancialYear,
					ReceivingPerson,
					TotalAmount,
					Remarks,
					ActualDocumentNumber,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@CompanyId,
					@ClientId,
					@VoucherNumber,
					@Date,
					@VoucherType,
					@ChallanNumber,
					@ChallanDate,
					@VehicleNumber,
					@MarkNumber,
					@Month,
					@FinancialYear,
					@ReceivingPerson,
					@TotalAmount,
					@Remarks,
					@LastNumber,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);

				SET @PurchaseDocumentId = SCOPE_IDENTITY();

				INSERT INTO [document].PurchaseDocumentItems
				(
					PurchaseDocumentId,
					ItemId,
					UnitId,
					CrateId,
					[Description],
					Quantity,
					[Weight],
					FreeQuantity,
					Rate,
					Amount,
					LotNumber
				)
				SELECT
					@PurchaseDocumentId,
					di.ItemId,
					di.UnitId,
					di.CrateId,
					di.[Description],
					di.Quantity,
					di.[Weight],
					di.FreeQuantity,
					di.Rate,
					di.TotalAmount,
					di.LotNumber
				FROM
					#TempPurchaseDocumentItems AS di;

				INSERT INTO document.PurchaseDocumentHeaders
				(
					PurchaseDocumentId,
					HeaderId,
					CalculationType,
					[Percentage],
					[Value]
				)
				SELECT
					@PurchaseDocumentId,
					thd.HeaderId,
					thd.CalculationType,
					thd.[Percentage],
					thd.[Value]
				FROM
					#DocumentHeaders AS thd;

				INSERT INTO document.AllocatedLotNumbers
				(
					SubscriberId,
					PurchaseDocumentId,
					DocumentType,
					LotNumber,
					Quantity,
					[Weight]
				)
				SELECT
					@SubscriberId,
					@PurchaseDocumentId,
					@DocumentTypePurchase,
					pdi.LotNumber,
					pdi.Quantity,
					pdi.[Weight]
				FROM
					#TempPurchaseDocumentItems AS pdi;

				UPDATE
					subscriber.NumberConfigurations
				SET
					LastNumber = LastNumber + 1,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					SubscriberId = @SubscriberId
					AND EntityId = @CompanyId
					AND DocumentType = @DocumentTypePurchase
					AND FinancialYear = @FinancialYear;

				UPDATE
					subscriber.NumberConfigurations
				SET
					LastNumber = @LastLotNumber,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					SubscriberId = @SubscriberId
					AND EntityId = @CompanyId
					AND ModuleType = @ModuleTypeLotNumber;

				COMMIT TRAN;
			END
		END
		ELSE
		BEGIN
			INSERT INTO #TempErrorLotNumbers
			(
				LotNumber,
				Quantity,
				[Weight]
			)
			SELECT
				aln.LotNumber,
				aln.Quantity,
				aln.[Weight]
			FROM
				document.AllocatedLotNumbers AS aln
			WHERE
				aln.PurchaseDocumentId = @Id
				AND EXISTS 
				(
					SELECT
						LotNumber
					FROM
						document.AllocatedLotNumbers
					WHERE
						LotNumber = aln.LotNumber
						AND SalesDocumentId IS NOT NULL
				)
				AND NOT EXISTS
				(
					SELECT
						LotNumber
					FROM 
						#TempPurchaseDocumentItems
					WHERE
						IsLotNumberAlreadyPresent = 1
				);

			IF EXISTS(SELECT Id FROM #TempErrorLotNumbers)
			BEGIN
				SET @ReturnValue = -4;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				UPDATE
					[document].Purchasedocuments
				SET
					ClientId = @ClientId,
					[Date] = @Date,
					VoucherType = @VoucherType,
					ChallanNumber = @ChallanNumber,
					ChallanDate = @ChallanDate,
					VehicleNumber = @VehicleNumber,
					MarkNumber = @MarkNumber,
					[Month] = @Month,
					ReceivingPerson = @ReceivingPerson,
					TotalAmount = @TotalAmount,
					Remarks = @Remarks,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;

				DELETE FROM
					[document].PurchaseDocumentItems
				WHERE
					PurchaseDocumentId = @Id;

				DELETE FROM
					[document].PurchaseDocumentHeaders
				WHERE
					PurchaseDocumentId = @Id;
			
				DELETE
					aln
				FROM 
					document.AllocatedLotNumbers AS aln
				WHERE
					NOT EXISTS
					(
						SELECT
							LotNumber
						FROM
							#TempPurchaseDocumentItems
						WHERE
							LotNumber = aln.LotNumber
					)
					AND aln.SubscriberId = @SubscriberId
					AND aln.PurchaseDocumentId = @Id;

			INSERT INTO [document].PurchaseDocumentItems
			(
				PurchaseDocumentId,
				ItemId,
				UnitId,
				CrateId,
				[Description],
				Quantity,
				[Weight],
				FreeQuantity,
				Rate,
				Amount,
				LotNumber
			)
			SELECT
				@Id,
				di.ItemId,
				di.UnitId,
				di.CrateId,
				di.[Description],
				di.Quantity,
				di.[Weight],
				di.FreeQuantity,
				di.Rate,
				di.TotalAmount,
				di.LotNumber
			FROM
				#TempPurchaseDocumentItems AS di;

			INSERT INTO document.PurchaseDocumentHeaders
			(
				PurchaseDocumentId,
				HeaderId,
				CalculationType,
				[Percentage],
				[Value]
			)
			SELECT
				@Id,
				thd.HeaderId,
				thd.CalculationType,
				thd.[Percentage],
				thd.[Value]
			FROM
				#DocumentHeaders AS thd;

			UPDATE 
				aln
			SET
				aln.Quantity = ABS(ISNULL(taln.Quantity,0) - tpdi.Quantity),
				aln.[Weight] = ABS(ISNULL(taln.[Weight],0) - tpdi.[Weight])
			FROM
				document.AllocatedLotNumbers AS aln
				INNER JOIN #TempPurchaseDocumentItems AS tpdi ON aln.LotNumber = tpdi.LotNumber
				LEFT JOIN #TempAllocatedLotNumbers AS taln ON aln.LotNumber = taln.LotNumber
			WHERE
				tpdi.IsLotNumberAlreadyPresent = 1
				AND aln.PurchaseDocumentId = @Id;
					
			INSERT INTO document.AllocatedLotNumbers
			(
				SubscriberId,
				PurchaseDocumentId,
				DocumentType,
				LotNumber,
				Quantity,
				[Weight]
			)
			SELECT
				@SubscriberId,
				@Id,
				@DocumentTypePurchase,
				pdi.LotNumber,
				pdi.[Quantity],
				pdi.[Weight]
			FROM
				#TempPurchaseDocumentItems AS pdi
			WHERE
				pdi.IsLotNumberAlreadyPresent = 0;

			IF EXISTS(SELECT ItemId FROM @DocumentItems WHERE LotNumber IS NULL)
			BEGIN
				UPDATE
					subscriber.NumberConfigurations
				SET
					LastNumber = @LastLotNumber,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					SubscriberId = @SubscriberId
					AND EntityId = @CompanyId
					AND ModuleType = @ModuleTypeLotNumber;
			END
				COMMIT TRAN;
			END
		END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	SELECT
		@ReturnValue AS ReturnValue,
		tln.LotNumber,
		tln.Quantity,
		tln.[Weight]
	FROM
		#TempErrorLotNumbers AS tln

	DROP TABLE #TempPurchaseDocumentItems,#DocumentHeaders,#TempAllocatedLotNumbers,#TempErrorLotNumbers;
