﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetItemDetailByLotNumber
Comments		: 20-04-2021 | Amit Khanna | This procedure is used to get item detail by lot number.

Test Execution	: EXEC document.GetItemDetailByLotNumber
					@SubscriberId = 1,
					@LotNumber = 11,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetItemDetailByLotNumber]
(
	@SubscriberId INT,
	@LotNumber INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[GetItemDetailByLotNumber]','
										@SubscriberId =',@SubscriberId,',
										@LotNumber =',@LotNumber,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].[GetItemDetailByLotNumber]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
	SELECT 
		it.Id,
		it.[Name],
		al.Quantity,
		al.[Weight]
	FROM
		document.PurchaseDocumentItems AS doc
		INNER JOIN document.AllocatedLotNumbers AS al ON doc.PurchaseDocumentId = al.PurchaseDocumentId AND doc.LotNumber = al.LotNumber
		INNER JOIN [master].Items AS it ON doc.ItemId = it.Id
	WHERE
		it.SubscriberId = @SubscriberId
		AND al.LotNumber = @LotNumber;
			
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
