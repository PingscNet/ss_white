﻿/*------------------------------------------------------------------------------------------------------------
Name			: DeleteJournalVoucher
Comments		: 11-05-2021 | Amit Khanna | This procedure is used to delete journal voucher by Id.

Test Execution	: EXEC [document].[DeleteJournalVoucher]
						@SubscriberId =  1,
						@Id =  1,
						@IpAddress =  1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[DeleteJournalVoucher]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[DeleteJournalVoucher]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].[DeleteJournalVoucher]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY
		DELETE FROM
			document.JournalVouchers
		WHERE
			SubscriberId = @SubscriberId
			AND Id = @Id;
		
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
