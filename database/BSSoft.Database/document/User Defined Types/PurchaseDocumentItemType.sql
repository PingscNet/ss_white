﻿CREATE TYPE [document].[PurchaseDocumentItemType] AS TABLE (
    [ItemId]       INT             NOT NULL,
    [CrateId]      INT             NOT NULL,
    [UnitId]       INT             NOT NULL,
    [Description]  VARCHAR (150)   NULL,
    [Quantity]     DECIMAL (18, 2) NOT NULL,
    [Weight]       DECIMAL (18, 2) NULL,
    [FreeQuantity] DECIMAL (18, 2) NULL,
    [Rate]         DECIMAL (18, 2) NULL,
    [TotalAmount]  DECIMAL (18, 2) NULL,
    [LotNumber]    INT             NULL);

