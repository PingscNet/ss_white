﻿CREATE TYPE [document].[DocumentHeaderTableType] AS TABLE (
    [HeaderId]        INT             NOT NULL,
    [CalculationType] INT             NOT NULL,
    [Percentage]      DECIMAL (18, 2) NOT NULL,
    [Value]           DECIMAL (18, 2) NOT NULL);

