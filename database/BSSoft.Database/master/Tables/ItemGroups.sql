﻿CREATE TABLE [master].[ItemGroups] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [Name]         VARCHAR (50)  NOT NULL,
    [Status]       TINYINT       NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT           NOT NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT           NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__ItemGrou__3214EC078BE9FF57] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ItemGroup__Creat__2A164134] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__ItemGroup__Subsc__29221CFB] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__ItemGroup__Updat__2B0A656D] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

