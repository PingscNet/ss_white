﻿CREATE TABLE [master].[PackingTypes] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [Name]         VARCHAR (100) NOT NULL,
    [Status]       TINYINT       NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT           NOT NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT           NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__PackingT__3214EC07AC98C464] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__PackingTy__Creat__409A7F30] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__PackingTy__Subsc__3FA65AF7] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__PackingTy__Updat__418EA369] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

