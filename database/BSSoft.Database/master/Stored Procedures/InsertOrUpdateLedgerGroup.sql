﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateLedgerGroup
Comments		: 30-03-2021 | Vikas Patel | This procedure is used to insert ledger group or update ledger group by Id.

Test Execution	: EXEC master.InsertOrUpdateLedgerGroup
						@SubscriberId = 1,
						@Id = NULL,
						@Name = 'demo',
						@Code = '1',						
						@IpAddress = '',
						@CreatedBy = 1,
						@CreatedDate = '2021-02-28',
						@StatusTypeActive = 1,
						@ModuleTypeItem =1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateLedgerGroup]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(100),
	@Code VARCHAR(50),
	@LedgerGroupType TINYINT,
	@AccountType TINYINT,
	@Order TINYINT,
	@Sections VARCHAR(120),
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT	
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateLedgerGroup]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =''',@Name,''',
										@Code =''',@Code,''',
										@LedgerGroupType =',@LedgerGroupType,',
										@AccountType =',@AccountType,',
										@Order =',@Order,',										
										@Sections =',@Sections,',										
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive																				
									  ),

			@ProcedureName = '[master].[InsertOrUpdateLedgerGroup]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1;
    BEGIN TRY
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].LedgerGroups WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO [master].LedgerGroups
				(
					SubscriberId,					
					[Name],
					[Code],					
					[Type],
					[AccountType],
					[Order],	
					Sections,
					[IpAddress],
					[Status],
					[CreatedBy],
					[CreatedDate]										
				)
				VALUES
				(
					@SubscriberId,					
					@Name,
					@Code,
					@LedgerGroupType,
					@AccountType,
					@Order,		
					@Sections,
					@IpAddress,
					@StatusTypeActive,
					@CreatedBy,
					@CreatedDate					
				);
								
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].LedgerGroups WHERE SubscriberId = @SubscriberId  AND [Name] = @Name AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				UPDATE
					[master].LedgerGroups
				SET
					[Name] = @Name,
					[Type] = @LedgerGroupType,
					[AccountType] = @AccountType,
					[Order] = @Order,
					Sections = @Sections,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id
					AND SubscriberId = @SubscriberId;
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
