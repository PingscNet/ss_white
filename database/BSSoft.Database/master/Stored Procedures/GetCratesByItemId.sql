﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetCratesByItemId
Comments		: 19-05-2021 | Amit Khanna | This procedure is used to get All Crates By Item Id for common calls.

Test Execution	: EXEC master.GetCratesByItemId
					@SubscriberId = 1,
					@ItemId = 1,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetCratesByItemId]
(
	@SubscriberId INT,
	@ItemId INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetCratesByItemId]','
										@SubscriberId =',@SubscriberId,',
										@ItemId = ',@ItemId,'
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetCratesByItemId]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
		SELECT
			crt.Id,
			crt.[Name]
		FROM
			[master].ItemCrates AS st
			INNER JOIN [master].Crates AS crt ON st.CrateId = crt.Id
		WHERE
			ItemId = @ItemId
		ORDER BY
			crt.[Name] ASC;
		
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
