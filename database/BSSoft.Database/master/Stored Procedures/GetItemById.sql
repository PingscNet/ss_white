﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetItemById
Comments		: 29-03-2021 | Amit Khanna | This procedure is used to get item by Id.

Test Execution	: EXEC master.GetItemById
					@SubscriberId =  1,
					@Id =  1,
					@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetItemById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetItemById]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetItemById]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			tx.Id,
			tx.[Name],
			tx.Code,
			tx.HsnOrSac,
			tx.ItemGroupId,
			tx.ItemSubGroupId,
			tx.ItemCategoryId,
			tx.ItemSubCategoryId,
			tx.Remarks
		FROM
			[master].Items AS tx
		WHERE
			tx.Id = @Id;

		SELECT
			ic.UnitId,
			ic.Labour,
			cr.[Name] AS Unit
		FROM
			[master].ItemUnits AS ic
			INNER JOIN [master].Uqcs AS cr ON ic.UnitId = cr.Id
		WHERE
			ItemId = @Id;

		SELECT
			ic.CrateId,
			ic.CrateDeposit AS Deposit,
			cr.[Name] AS Crate
		FROM
			[master].ItemCrates AS ic
			INNER JOIN [master].Crates AS cr ON ic.CrateId = cr.Id
		WHERE
			ItemId = @Id;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
