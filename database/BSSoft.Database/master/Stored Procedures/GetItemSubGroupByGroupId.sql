﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetItemSubGroupByGroupId
Comments		: 19-05-2021 | Amit Khanna | This procedure is used to get All items sub groups by group Id for common call.

Test Execution	: EXEC master.GetItemSubGroupByGroupId
					@SubscriberId = 1,
					@GroupId  = 1,
					@Status = NULL,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetItemSubGroupByGroupId]
(
	@SubscriberId INT,
	@GroupId INT,
	@Status TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetItemSubGroupByGroupId]','
										@SubscriberId =',@SubscriberId,',
										@GroupId =',@GroupId,',
										@Status = ',@Status,'
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetItemSubGroupByGroupId]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
		SELECT
			st.Id,
			st.[Name],
			st.[Status]
		FROM
			[master].ItemSubGroups AS st
		WHERE
			st.SubscriberId = @SubscriberId
			AND st.ItemGroupId = ISNULL(@GroupId,st.ItemGroupId)
			AND st.[Status] = ISNULL(@Status,st.[Status])
		ORDER BY
			st.[Name] ASC;
		
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
