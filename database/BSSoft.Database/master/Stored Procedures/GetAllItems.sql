﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllItems
Comments		: 29-03-2021 | Amit Khanna | This procedure is used to get All Items.

Test Execution	: EXEC master.GetAllItems
					@SubscriberId = 1,
					@Status = NULL,
					@ItemGroupId = NULL,
					@ItemCategoryId = NULL,
					@UnitId = NULL,
					@CrateId = NULL,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@IpAddress = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetAllItems]
(
	@SubscriberId INT,
	@Status TINYINT,
	@ItemGroupId INT,
	@ItemCategoryId INT,
	@UnitId INT,
	@CrateId INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@Start INT,
	@Length INT,
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetAllItems]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,',
										@ItemGroupId = ',@ItemGroupId,',
										@ItemCategoryId = ',@ItemCategoryId,',
										@UnitId = ',@UnitId,',
										@CrateId = ',@CrateId,',
										@Start = ',@Start,',
										@Length = ',@Length,',
										@SearchKeyword = ','',@SearchKeyword,'',',
										@SortExpression = ','',@SortExpression,'',',
										@IpAddress = ','',@IpAddress,'',',
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[master].[GetAllItems]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT); 
    BEGIN TRY
		
		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[master].[Items] AS us
		WHERE 
			us.SubscriberId = @SubscriberId
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.ItemGroupId = ISNULL(@ItemGroupId,us.ItemGroupId)
			AND us.ItemCategoryId = ISNULL(@ItemCategoryId,us.ItemCategoryId)
			AND us.EndDate IS NULL
			AND 
			(
					us.[Name] LIKE '%' + ISNULL(@SearchKeyword,us.[Name]) +'%'
					OR us.[Code] LIKE '%' + ISNULL(@SearchKeyword,us.[Code]) +'%'
			);

		SELECT @TotalRecords =COUNT(Id)  FROM #TempTable 

		SELECT
			us.Id,
			us.[Name],
			us.Code,
			ig.[Name] AS [Group],
			tx.[Name] AS Category,
			us.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN [master].Items us ON tmp.Id = us.Id
			INNER JOIN [master].ItemGroups ig ON us.ItemGroupId = ig.Id
			INNER JOIN [master].ItemCategories tx ON us.ItemCategoryId = tx.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.[ActualLastNumber] END ASC,
			CASE WHEN @SortExpression = 'name asc' THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'name desc' THEN us.[Name] END DESC,
			CASE WHEN @SortExpression = 'code asc' THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'code desc' THEN us.[Name] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
