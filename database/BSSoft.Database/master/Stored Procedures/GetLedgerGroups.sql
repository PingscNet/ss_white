﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetLedgerGroups
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to get all ledger groups for common calls.

Test Execution	: EXEC master.GetRegions
					@SubscriberId = 1,
					@Status = NULL,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetLedgerGroups]
(
	@SubscriberId INT,
	@Status TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetLedgerGroups]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,'
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetLedgerGroups]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
		SELECT
			st.Id,
			st.[Name],
			st.Sections,
			st.[Status]
		FROM
			[master].LedgerGroups AS st
		WHERE
			st.[Status] = ISNULL(@Status,st.[Status])
			AND st.[SubscriberId] = @SubscriberId;
		
		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
