﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetCompanyById
Comments		: 29-03-2021 | Amit Khanna | This procedure is used to get company by Id.

Test Execution	: EXEC master.GetCompanyById
					@SubscriberId =  1,
					@Id =  1,
					@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetCompanyById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetCompanyById]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetCompanyById]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			cmp.Id,
			cmp.[Name],
			cmp.Gstin,
			cmp.Pan,
			cmp.[Address],
			cmp.CountryId,
			cmp.StateId,
			cmp.CityId,
			cmp.Pincode,
			cmp.EmailAddress,
			cmp.ContactPerson,
			cmp.ContactNumber,
			cmp.Website,
			cmp.OtherLicense,
			cmp.TagLine,
			cmp.TermsAndConditions,
			cmp.LogoPath
		FROM
			[master].Companies AS cmp
		WHERE
			cmp.Id = @Id;

		SELECT
			bdt.[Name],
			bdt.AccountNumber,
			bdt.Branch,
			bdt.IfscCode,
			bdt.IsDefault
		FROM
			[master].CompanyBankDetails AS bdt
		WHERE
			bdt.CompanyId = @Id;
			

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
