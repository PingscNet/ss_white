﻿/*------------------------------------------------------------------------------------------------------------
Name			: DeleteCrate
Comments		: 29-03-2021 | Amit Khanna | This procedure is used to delete Crate by Id. Status of Crate is set to 3 and enddate is set.

Test Execution	: EXEC master.DeleteCrate
					@SubscriberId =  1,
					@Id =  5,
					@IpAddress =  1,
					@StatusTypeDeleted = 3;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[DeleteCrate]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45),
	@UpdatedBy INT,
	@UpdatedDate SMALLDATETIME,
	@StatusTypeDeleted TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[DeleteCrate]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,'','
										@UpdateBy = ',@UpdatedBy,',
										@UpdateDate = ',@UpdatedDate,',
										@StatusTypeDelete = ',@StatusTypeDeleted
									  ),
			@ProcedureName = '[master].[DeleteCrate]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		UPDATE
			[master].Crates
		SET
			[status]  = @StatusTypeDeleted,
			EndDate = GETDATE(),
			UpdatedBy = @UpdatedBy,
			UpdatedDate = @UpdatedDate,
			IpAddress = @IpAddress
		WHERE
			Id = @Id
			AND SubscriberId = @SubscriberId;

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
