﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetClientById
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to get client by Id.

	Test Execution	: EXEC master.GetClientById
						@SubscriberId =  1,
						@Id =  13,
						@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetClientById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetClientById]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetCompanyById]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			cmp.Id,
			cmp.[Name],
			cmp.CompanyName,
			cmp.Code,
			cmp.Gstin,
			cmp.Pan,
			cmp.[Address],
			cmp.CityId,
			cmp.StateId,
			cmp.CountryId,
			cmp.EmailAddress,
			cmp.MobileNumber,
			cmp.ContactNumber,
			cmp.Website,
			cmp.CreditLimit,
			cmp.OpeningBalance,
			cmp.BalanceType AS TransactionType,
			cmp.LedgerGroupId,
			cmp.RegionId,
			CASE WHEN cmp.[Address] IS NOT NULL OR cmp.CityId IS NOT NULL OR cmp.[StateId] IS NOT NULL OR cmp.CountryId IS NOT NULL 
					OR cmp.Website IS NOT NULL OR cmp.ContactNumber IS NOT NULL OR cmp.EmailAddress IS NOT NULL OR cmp.MobileNumber IS NOT NULL
					THEN 1 ELSE 0 END AS IsAddressDetails,
			CASE WHEN cmp.Gstin IS NOT NULL OR cmp.Pan IS NOT NULL OR cmp.CreditLimit IS NOT NULL OR cmp.RegionId IS NOT NULL
				THEN 1 ELSE 0 END AS IsStatutoryDetails
		FROM
			[master].Clients AS cmp
		WHERE
			cmp.Id = @Id;

		SELECT
			bdt.[Name],
			bdt.AccountNumber,
			bdt.Branch,
			bdt.IfscCode,
			bdt.IsDefault
		FROM
			[master].ClientBankDetails AS bdt
		WHERE
			bdt.ClientId = @Id;

		SELECT
			bdt.[Name],
			bdt.MobileNumber,
			bdt.Phone,
			bdt.EmailAddress
		FROM
			[master].ClientContactDetails AS bdt
		WHERE
			bdt.ClientId = @Id;

		SELECT
			hdr.HeaderId,
			hd.[Name],
			hdr.CalculationType,
			ml.[Name] AS Module,
			hdr.ModuleId,
			bdt.[Value]
		INTO
			#TempHeaderDetails
		FROM
			[master].ClientHeaderDetails AS bdt
			RIGHT JOIN subscriber.Modules AS ml ON bdt.ModuleId = ml.Id
			RIGHT JOIN [master].HeaderDetails AS hdr ON hdr.ModuleId = bdt.ModuleId AND hdr.HeaderId = bdt.HeaderId
			INNER JOIN [master].Headers AS hd ON bdt.HeaderId = hd.Id
		WHERE
			bdt.ClientId = @Id;

		SELECT
			cd.HeaderId,
			cd.[Name],
			cd.[CalculationType],
			cd.Module,
			cd.ModuleId,
			cd.[Value]
		FROM
		(
			SELECT
				thd.HeaderId,
				thd.[Name],
				thd.CalculationType,
				thd.Module,
				thd.ModuleId,
				thd.[Value]
			FROM
				#TempHeaderDetails AS thd

			UNION ALL

			SELECT
				hdt.HeaderId,
				hdr.[Name],
				hdt.CalculationType,
				ml.[Name] AS Module,
				hdt.ModuleId,
				NULL AS [Value]
			FROM
				[master].HeaderDetails AS hdt 
				INNER JOIN subscriber.Modules AS ml ON ml.Id = hdt.ModuleId
				INNER JOIN [master].Headers AS hdr ON hdt.HeaderId = hdr.Id
			WHERE
				NOT EXISTS
				(
					SELECT
						HeaderId,
						ModuleId
					FROM
						#TempHeaderDetails
					WHERE
						hdt.HeaderId = HeaderId
						AND hdt.ModuleId = ModuleId
				)
				AND hdr.SubscriberId = @SubscriberId
				AND hdr.[Status] = 1
		) AS cd;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
