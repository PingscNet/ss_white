﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllUqcs
Comments		: 25-03-2021 | Amit Khanna | This procedure is used to get All Uqcs.

Test Execution	: EXEC master.GetAllUqcs
					@SubscriberId = 1,
					@SubUnitId = NULL,
					@PackingTypeId = NULL,
					@Status = NULL,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@IpAddress = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetAllUqcs]
(
	@SubscriberId INT,
	@SubUnitId INT,
	@PackingTypeId INT,
	@Status TINYINT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetAllUqcs]','
										@SubscriberId =',@SubscriberId,',
										@SubUnitId =',@SubUnitId,',
										@PackingTypeId =',@PackingTypeId,',
										@Status = ',@Status,',
										@Start = ',@Start,',
										@Length = ',@Length,',
										@SearchKeyword = ','',@SearchKeyword,'',',
										@SortExpression = ','',@SortExpression,'',',
										@IpAddress = ','',@IpAddress,'',',
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[master].[GetAllUqcs]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT); 
    BEGIN TRY
		
		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[master].[UQCS] AS us
		WHERE 
			us.SubscriberId = @SubscriberId
			AND ISNULL(us.PackingTypeId,0) = ISNULL(@PackingTypeId,ISNULL(us.PackingTypeId,0))
			AND ISNULL(us.SubUnitId,0) = ISNULL(@SubUnitId,ISNULL(us.SubUnitId,0))
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.EndDate IS NULL
			AND 
			(
					us.[Name] LIKE '%' + ISNULL(@SearchKeyword,us.[Name]) +'%'
					OR us.[Code] LIKE '%' + ISNULL(@SearchKeyword,us.[Code]) +'%'
			);

		SELECT @TotalRecords =COUNT(Id)  FROM #TempTable 

		SELECT
			us.Id,
			us.[Name],
			us.Code,
			us.[Status],
			su.[Name] AS SubUnit,
			pt.[Name] AS PackingType,
			us.ConversionUnit
		FROM 
			#TempTable AS tmp
			INNER JOIN [master].UQCS us ON tmp.Id = us.Id
			LEFT JOIN [master].SubUnits AS su ON su.Id = us.SubUnitId
			LEFT JOIN [master].PackingTypes AS pt ON pt.Id = us.PackingTypeId
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'name asc' THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'name desc' THEN us.[Name] END DESC,
			CASE WHEN @SortExpression = 'code asc' THEN us.[Code] END ASC,
			CASE WHEN @SortExpression = 'code desc' THEN us.[Code] END DESC,
			CASE WHEN @SortExpression = 'subUnit asc' THEN su.[Name] END ASC,
			CASE WHEN @SortExpression = 'subUnit desc' THEN su.[Name] END DESC,
			CASE WHEN @SortExpression = 'packingType asc' THEN pt.[Name] END ASC,
			CASE WHEN @SortExpression = 'packingType desc' THEN pt.[Name] END DESC,
			CASE WHEN @SortExpression = 'conversionUnit asc' THEN us.[ConversionUnit] END ASC,
			CASE WHEN @SortExpression = 'conversionUnit desc' THEN us.[ConversionUnit] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
