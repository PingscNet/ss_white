﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateHeader
Comments		: 30-03-2021 | Amit Khanna | This procedure is used to insert header or update header by Id.

Test Execution	: DECLARE @ReturnValue INT = 1;
					EXEC @ReturnValue = [master].[InsertOrUpdateHeader]
					@SubscriberId =1,
					@Id = NULL,
					@Name ='AK Industries',
					@IpAddress =NULL,
					@CreatedBy = 1,
					@CreatedDate = '2021-03-03',
					@StatusTypeActive =1
					SELECT @ReturnValue;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateHeader]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(100),
	@HeaderDetails [master].HeaderDetailTableType READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateHeader]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =''',@Name,''',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateHeader]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@HeaderId INT;
	
	SELECT
		hdt.[ModuleId],		
		hdt.CalculationType
	INTO
		#TempHeaderDetails
	FROM
		@HeaderDetails AS hdt

		
    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Headers WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO [master].Headers
				(
					SubscriberId,
					[Name],
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@Name,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
				
				SET @HeaderId = SCOPE_IDENTITY();

				INSERT INTO [master].HeaderDetails
				(
					HeaderId,
					[ModuleId],
					CalculationType
				)
				SELECT
					@HeaderId,
					hdt.[ModuleId],
					hdt.[CalculationType]
				FROM
					#TempHeaderDetails AS hdt;
				COMMIT TRAN
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Headers WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				UPDATE
					[master].Headers
				SET
					[Name] = @Name,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;

				DELETE FROM 
					[master].HeaderDetails
				WHERE
					HeaderId = @Id;

				INSERT INTO [master].HeaderDetails
				(
					HeaderId,
					[ModuleId],
					CalculationType
				)
				SELECT
					@Id,
					hdt.[ModuleId],
					hdt.[CalculationType]
				FROM
					#TempHeaderDetails AS hdt;
				COMMIT TRAN
			END
		END

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
		RETURN @ReturnValue;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
		RETURN @ReturnValue;
	END CATCH;

	DROP TABLE #TempHeaderDetails;
