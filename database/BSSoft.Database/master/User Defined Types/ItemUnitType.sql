﻿CREATE TYPE [master].[ItemUnitType] AS TABLE (
    [UnitId] INT             NOT NULL,
    [Labour] DECIMAL (18, 2) NOT NULL);

