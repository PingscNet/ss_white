﻿INSERT INTO app.Cities([Name],StateId,CountryId) VALUES
('Kolhapur', 20,1),
('Port Blair', 1,1),
('Adilabad', 2,1),
('Adoni', 2,1),
('Amadalavalasa', 2,1),
('Amalapuram', 2,1),
('Anakapalle', 2,1),
('Anantapur', 2,1),
('Badepalle', 2,1),
( 'Banganapalle', 2,1),
( 'Bapatla', 2,1),
( 'Bellampalle', 2,1),
( 'Bethamcherla', 2,1),
( 'Bhadrachalam', 2,1),
( 'Bhainsa', 2,1),
( 'Bheemunipatnam', 2,1),
( 'Bhimavaram', 2,1),
( 'Bhongir', 2,1),
( 'Bobbili', 2,1),
( 'Bodhan', 2,1),
( 'Chilakaluripet', 2,1),
( 'Chirala', 2,1),
( 'Chittoor', 2,1),
( 'Cuddapah', 2,1),
( 'Devarakonda', 2,1),
( 'Dharmavaram', 2,1),
( 'Eluru', 2,1),
( 'Farooqnagar', 2,1),
( 'Gadwal', 2,1),
( 'Gooty', 2,1),
( 'Gudivada', 2,1),
( 'Gudur', 2,1),
( 'Guntakal', 2,1),
( 'Guntur', 2,1),
( 'Hanuman Junction', 2,1),
( 'Hindupur', 2,1),
( 'Hyderabad', 35,1),
( 'Ichchapuram', 2,1),
( 'Jaggaiahpet', 2,1),
( 'Jagtial', 2,1),
( 'Jammalamadugu', 2,1),
( 'Jangaon', 2,1),
( 'Kadapa', 2,1),
( 'Kadiri', 2,1),
( 'Kagaznagar', 2,1),
( 'Kakinada', 2,1),
( 'Kalyandurg', 2,1),
( 'Kamareddy', 2,1),
( 'Kandukur', 2,1),
( 'Karimnagar', 35,1),
( 'Kavali', 2,1),
( 'Khammam', 35,1),
( 'Koratla', 2,1),
( 'Kothagudem', 2,1),
( 'Kothapeta', 2,1),
( 'Kovvur', 2,1),
( 'Kurnool', 2,1),
( 'Kyathampalle', 2,1),
( 'Macherla', 2,1),
( 'Machilipatnam', 2,1),
( 'Madanapalle', 2,1),
( 'Mahbubnagar', 2,1),
( 'Mancherial', 2,1),
( 'Mandamarri', 2,1),
( 'Mandapeta', 2,1),
( 'Manuguru', 2,1),
( 'Markapur', 2,1),
( 'Medak', 2,1),
( 'Miryalaguda', 35,1),
( 'Mogalthur', 2,1),
( 'Nagari', 2,1),
( 'Nagarkurnool', 2,1),
( 'Nandyal', 2,1),
( 'Narasapur', 2,1),
( 'Narasaraopet', 2,1),
( 'Narayanpet', 2,1),
( 'Narsipatnam', 2,1),
( 'Nellore', 2,1),
( 'Nidadavole', 2,1),
( 'Nirmal', 2,1),
( 'Nizamabad', 35,1),
( 'Nuzvid', 2,1),
( 'Ongole', 2,1),
( 'Palacole', 2,1),
( 'Palasa Kasibugga', 2,1),
( 'Palwancha', 2,1),
( 'Parvathipuram', 2,1),
( 'Pedana', 2,1),
( 'Peddapuram', 2,1),
( 'Pithapuram', 2,1),
( 'Pondur', 2,1),
( 'Ponnur', 2,1),
( 'Proddatur', 2,1),
( 'Punganur', 2,1),
( 'Puttur', 2,1),
( 'Rajahmundry', 2,1),
( 'Rajam', 2,1),
( 'Ramachandrapuram', 2,1),
( 'Ramagundam', 35,1),
('Rayachoti', 2,1),
('Rayadurg', 2,1),
('Renigunta', 2,1),
('Repalle', 2,1),
('Sadasivpet', 2,1),
('Salur', 2,1),
('Samalkot', 2,1),
('Sangareddy', 2,1),
('Sattenapalle', 2,1),
('Siddipet', 2,1),
('Singapur', 2,1),
('Sircilla', 2,1),
('Srikakulam', 2,1),
('Srikalahasti', 2,1),
('Suryapet', 35,1),
('Tadepalligudem', 2,1),
('Tadpatri', 2,1),
('Tandur', 2,1),
('Tanuku', 2,1),
('Tenali', 2,1),
('Tirupati', 2,1),
('Tuni', 2,1),
('Uravakonda', 2,1),
('Venkatagiri', 2,1),
('Vicarabad', 2,1),
('Vijayawada', 2,1),
('Vinukonda', 2,1),
('Visakhapatnam', 2,1),
('Vizianagaram', 2,1),
('Wanaparthy', 2,1),
('Warangal', 35,1),
('Yellandu', 2,1),
('Yemmiganur', 2,1),
('Yerraguntla', 2,1),
('Zahirabad', 2,1),
('Rajampet', 2,1),
('Along', 3,1),
('Bomdila', 3,1),
('Itanagar', 3,1),
('Naharlagun', 3,1),
('Pasighat', 3,1),
('Abhayapuri', 4,1),
('Amguri', 4,1),
('Anandnagaar', 4,1),
('Barpeta', 4,1),
('Barpeta Road', 4,1),
('Bilasipara', 4,1),
('Bongaigaon', 4,1),
('Dhekiajuli', 4,1),
('Dhubri', 4,1),
('Dibrugarh', 4,1),
('Digboi', 4,1),
('Diphu', 4,1),
('Dispur', 4,1),
('Gauripur', 4,1),
('Goalpara', 4,1),
('Golaghat', 4,1),
('Guwahati', 4,1),
('Haflong', 4,1),
('Hailakandi', 4,1),
('Hojai', 4,1),
('Jorhat', 4,1),
('Karimganj', 4,1),
('Kokrajhar', 4,1),
('Lanka', 4,1),
('Lumding', 4,1),
('Mangaldoi', 4,1),
('Mankachar', 4,1),
('Margherita', 4,1),
('Mariani', 4,1),
('Marigaon', 4,1),
('Nagaon', 4,1),
('Nalbari', 4,1),
('North Lakhimpur', 4,1),
('Rangia', 4,1),
('Sibsagar', 4,1),
('Silapathar', 4,1),
('Silchar', 4,1),
('Tezpur', 4,1),
('Tinsukia', 4,1),
('Amarpur', 5,1),
('Araria', 5,1),
('Areraj', 5,1),
('Arrah', 5,1),
('Asarganj', 5,1),
('Aurangabad', 5,1),
('Bagaha', 5,1),
('Bahadurganj', 5,1),
('Bairgania', 5,1),
('Bakhtiarpur', 5,1),
('Banka', 5,1),
('Banmankhi Bazar', 5,1),
('Barahiya', 5,1),
('Barauli', 5,1),
('Barbigha', 5,1),
('Barh', 5,1),
('Begusarai', 5,1),
('Behea', 5,1),
('Bettiah', 5,1),
('Bhabua', 5,1),
('Bhagalpur', 5,1),
('Bihar Sharif', 5,1),
('Bikramganj', 5,1),
('Bodh Gaya', 5,1),
('Buxar', 5,1),
('Chandan Bara', 5,1),
('Chanpatia', 5,1),
('Chhapra', 5,1),
('Colgong', 5,1),
('Dalsinghsarai', 5,1),
('Darbhanga', 5,1),
('Daudnagar', 5,1),
('Dehri-on-Sone', 5,1),
('Dhaka', 5,1),
('Dighwara', 5,1),
('Dumraon', 5,1),
('Fatwah', 5,1),
('Forbesganj', 5,1),
('Gaya', 5,1),
('Gogri Jamalpur', 5,1),
('Gopalganj', 5,1),
('Hajipur', 5,1),
('Hilsa', 5,1),
('Hisua', 5,1),
('Islampur', 5,1),
('Jagdispur', 5,1),
('Jamalpur', 5,1),
('Jamui', 5,1),
('Jehanabad', 5,1),
('Jhajha', 5,1),
('Jhanjharpur', 5,1),
('Jogabani', 5,1),
('Kanti', 5,1),
('Katihar', 5,1),
('Khagaria', 5,1),
('Kharagpur', 5,1),
('Kishanganj', 5,1),
('Lakhisarai', 5,1),
('Lalganj', 5,1),
('Madhepura', 5,1),
('Madhubani', 5,1),
('Maharajganj', 5,1),
('Mahnar Bazar', 5,1),
('Makhdumpur', 5,1),
('Maner', 5,1),
('Manihari', 5,1),
('Marhaura', 5,1),
('Masaurhi', 5,1),
('Mirganj', 5,1),
('Mokameh', 5,1),
('Motihari', 5,1),
('Motipur', 5,1),
('Munger', 5,1),
('Murliganj', 5,1),
('Muzaffarpur', 5,1),
('Narkatiaganj', 5,1),
('Naugachhia', 5,1),
('Nawada', 5,1),
('Nokha', 5,1),
('Patna', 5,1),
('Piro', 5,1),
('Purnia', 5,1),
('Rafiganj', 5,1),
('Rajgir', 5,1),
('Ramnagar', 5,1),
('Raxaul Bazar', 5,1),
('Revelganj', 5,1),
('Rosera', 5,1),
('Saharsa', 5,1),
('Samastipur', 5,1),
('Sasaram', 5,1),
('Sheikhpura', 5,1),
('Sheohar', 5,1),
('Sherghati', 5,1),
('Silao', 5,1),
('Sitamarhi', 5,1),
('Siwan', 5,1),
('Sonepur', 5,1),
('Sugauli', 5,1),
('Sultanganj', 5,1),
('Supaul', 5,1),
('Warisaliganj', 5,1),
('Ahiwara', 6,1),
('Akaltara', 6,1),
('Ambagarh Chowki', 6,1),
('Ambikapur', 6,1),
('Arang', 6,1),
('Bade Bacheli', 6,1),
('Balod', 6,1),
('Baloda Bazar', 6,1),
('Bemetra', 6,1),
('Bhatapara', 6,1),
('Bilaspur', 6,1),
('Birgaon', 6,1),
('Champa', 6,1),
('Chirmiri', 6,1),
('Dalli-Rajhara', 6,1),
('Dhamtari', 6,1),
('Dipka', 6,1),
('Dongargarh', 6,1),
('Durg-Bhilai Nagar', 6,1),
('Gobranawapara', 6,1),
('Jagdalpur', 6,1),
('Janjgir', 6,1),
('Jashpurnagar', 6,1),
('Kanker', 6,1),
('Kawardha', 6,1),
('Kondagaon', 6,1),
('Korba', 6,1),
('Mahasamund', 6,1),
('Mahendragarh', 6,1),
('Mungeli', 6,1),
('Naila Janjgir', 6,1),
('Raigarh', 6,1),
('Raipur', 6,1),
('Rajnandgaon', 6,1),
('Sakti', 6,1),
('Tilda Newra', 6,1),
('Amli', 9,1),
('Silvassa', 9,1),
('Daman and Diu', 8,1),
('Daman and Diu', 8,1),
('Asola', 36,1),
('Delhi',36,1),
('Aldona',10,1),
('Curchorem Cacora',10,1),
('Madgaon',10,1),
('Mapusa',10,1),
('Margao',10,1),
('Marmagao',10,1),
('Panaji',10,1),
('Ahmedabad', 11,1),
('Amreli', 11,1),
('Anand', 11,1),
('Ankleshwar', 11,1),
('Bharuch', 11,1),
('Bhavnagar', 11,1),
('Bhuj', 11,1),
('Cambay', 11,1),
('Dahod', 11,1),
('Deesa', 11,1),
('Dholka', 11,1),
('Gandhinagar', 11,1),
('Godhra', 11,1),
('Himatnagar', 11,1),
('Idar', 11,1),
('Jamnagar', 11,1),
('Junagadh', 11,1),
('Kadi', 11,1),
('Kalavad', 11,1),
('Kalol', 11,1),
('Kapadvanj', 11,1),
('Karjan', 11,1),
('Keshod', 11,1),
('Khambhalia', 11,1),
('Khambhat', 11,1),
('Kheda', 11,1),
('Khedbrahma', 11,1),
('Kheralu', 11,1),
('Kodinar', 11,1),
('Lathi', 11,1),
('Limbdi', 11,1),
('Lunawada', 11,1),
('Mahesana', 11,1),
('Mahuva', 11,1),
('Manavadar', 11,1),
('Mandvi', 11,1),
('Mangrol', 11,1),
('Mansa', 11,1),
('Mehmedabad', 11,1),
('Modasa', 11,1),
('Morvi', 11,1),
('Nadiad', 11,1),
('Navsari', 11,1),
('Padra', 11,1),
('Palanpur', 11,1),
('Palitana', 11,1),
('Pardi', 11,1),
('Patan', 11,1),
('Petlad', 11,1),
('Porbandar', 11,1),
('Radhanpur', 11,1),
('Rajkot', 11,1),
('Rajpipla', 11,1),
('Rajula', 11,1),
('Ranavav', 11,1),
('Rapar', 11,1),
('Salaya', 11,1),
('Sanand', 11,1),
('Savarkundla', 11,1),
('Sidhpur', 11,1),
('Sihor', 11,1),
('Songadh', 11,1),
('Surat', 11,1),
('Talaja', 11,1),
('Thangadh', 11,1),
('Tharad', 11,1),
('Umbergaon', 11,1),
('Umreth', 11,1),
('Una', 11,1),
('Unjha', 11,1),
('Upleta', 11,1),
('Vadnagar', 11,1),
('Vadodara', 11,1),
('Valsad', 11,1),
('Vapi', 11,1),
('Vapi', 11,1),
('Veraval', 11,1),
('Vijapur', 11,1),
('Viramgam', 11,1),
('Visnagar', 11,1),
('Vyara', 11,1),
('Wadhwan', 11,1),
('Wankaner', 11,1),
('Adalaj', 11,1),
('Adityana', 11,1),
('Alang', 11,1),
('Ambaji', 11,1),
('Ambaliyasan', 11,1),
('Andada', 11,1),
('Anjar', 11,1),
('Anklav', 11,1),
('Antaliya', 11,1),
('Arambhada', 11,1),
('Atul', 11,1),
('Ballabhgarh', 13,1),
('Ambala', 13,1),
('Ambala', 13,1),
('Asankhurd', 13,1),
('Assandh', 13,1),
('Ateli', 13,1),
('Babiyal', 13,1),
('Bahadurgarh', 13,1),
('Barwala', 13,1),
('Bhiwani', 13,1),
('Charkhi Dadri', 13,1),
('Cheeka', 13,1),
('Ellenabad 2', 13,1),
('Faridabad', 13,1),
('Fatehabad', 13,1),
('Ganaur', 13,1),
('Gharaunda', 13,1),
('Gohana', 13,1),
('Gurgaon', 13,1),
('Haibat(Yamuna Nagar)', 13,1),
('Hansi', 13,1),
('Hisar', 13,1),
('Hodal', 13,1),
('Jhajjar', 13,1),
('Jind', 13,1),
('Kaithal', 13,1),
('Kalan Wali', 13,1),
('Kalka', 13,1),
('Karnal', 13,1),
('Ladwa', 13,1),
('Mahendragarh', 13,1),
('Mandi Dabwali', 13,1),
('Narnaul', 13,1),
('Narwana', 13,1),
('Palwal', 13,1),
('Panchkula', 13,1),
('Panipat', 13,1),
('Pehowa', 13,1),
('Pinjore', 13,1),
('Rania', 13,1),
('Ratia', 13,1),
('Rewari', 13,1),
('Rohtak', 13,1),
('Safidon', 13,1),
('Samalkha', 13,1),
('Shahbad', 13,1),
('Sirsa', 13,1),
('Sohna', 13,1),
('Sonipat', 13,1),
('Taraori', 13,1),
('Thanesar', 13,1),
('Tohana', 13,1),
('Yamunanagar', 13,1),
('Arki', 12,1),
('Baddi', 12,1),
('Bilaspur', 12,1),
('Chamba', 12,1),
('Dalhousie', 12,1),
('Dharamsala', 12,1),
('Hamirpur', 12,1),
('Mandi', 12,1),
('Nahan', 12,1),
('Shimla', 12,1),
('Solan', 12,1),
('Sundarnagar', 12,1),
('Jammu', 14,1),
('Achabbal', 14,1),
('Akhnoor', 14,1),
('Anantnag', 14,1),
('Arnia', 14,1),
('Awantipora', 14,1),
('Bandipore', 14,1),
('Baramula', 14,1),
('Kathua', 14,1),
('Leh', 14,1),
('Punch', 14,1),
('Rajauri', 14,1),
('Sopore', 14,1),
('Srinagar', 14,1),
('Udhampur', 14,1),
('Amlabad', 15,1),
('Ara', 15,1),
('Barughutu', 15,1),
('Bokaro Steel City', 15,1),
('Chaibasa', 15,1),
('Chakradharpur', 15,1),
('Chandrapura', 15,1),
('Chatra', 15,1),
('Chirkunda', 15,1),
('Churi', 15,1),
('Daltonganj', 15,1),
('Deoghar', 15,1),
('Dhanbad', 15,1),
('Dumka', 15,1),
('Garhwa', 15,1),
('Ghatshila', 15,1),
('Giridih', 15,1),
('Godda', 15,1),
('Gomoh', 15,1),
('Gumia', 15,1),
('Gumla', 15,1),
('Hazaribag', 15,1),
('Hussainabad', 15,1),
('Jamshedpur', 15,1),
('Jamtara', 15,1),
('Jhumri Tilaiya', 15,1),
('Khunti', 15,1),
('Lohardaga', 15,1),
('Madhupur', 15,1),
('Mihijam', 15,1),
('Musabani', 15,1),
('Pakaur', 15,1),
('Patratu', 15,1),
('Phusro', 15,1),
('Ramngarh', 15,1),
('Ranchi', 15,1),
('Sahibganj', 15,1),
('Saunda', 15,1),
('Simdega', 15,1),
('Tenu Dam-cum- Kathhara', 15,1),
('Arasikere', 17,1),
('Bangalore', 17,1),
('Belgaum', 17,1),
( 'Bellary', 17,1),
( 'Chamrajnagar', 17,1),
( 'Chikkaballapur', 17,1),
( 'Chintamani', 17,1),
( 'Chitradurga', 17,1),
( 'Gulbarga', 17,1),
( 'Gundlupet', 17,1),
( 'Hassan', 17,1),
( 'Hospet', 17,1),
( 'Hubli', 17,1),
( 'Karkala', 17,1),
( 'Karwar', 17,1),
( 'Kolar', 17,1),
( 'Kota', 17,1),
( 'Lakshmeshwar', 17,1),
( 'Lingsugur', 17,1),
( 'Maddur', 17,1),
( 'Madhugiri', 17,1),
( 'Madikeri', 17,1),
( 'Magadi', 17,1),
( 'Mahalingpur', 17,1),
( 'Malavalli', 17,1),
( 'Malur', 17,1),
( 'Mandya', 17,1),
( 'Mangalore', 17,1),
( 'Manvi', 17,1),
( 'Mudalgi', 17,1),
( 'Mudbidri', 17,1),
( 'Muddebihal', 17,1),
( 'Mudhol', 17,1),
( 'Mulbagal', 17,1),
( 'Mundargi', 17,1),
( 'Mysore', 17,1),
( 'Nanjangud', 17,1),
( 'Pavagada', 17,1),
( 'Puttur', 17,1),
( 'Rabkavi Banhatti', 17,1),
( 'Raichur', 17,1),
( 'Ramanagaram', 17,1),
( 'Ramdurg', 17,1),
( 'Ranibennur', 17,1),
( 'Robertson Pet', 17,1),
( 'Ron', 17,1),
( 'Sadalgi', 17,1),
( 'Sagar', 17,1),
( 'Sakleshpur', 17,1),
( 'Sandur', 17,1),
( 'Sankeshwar', 17,1),
( 'Saundatti-Yellamma', 17,1),
( 'Savanur', 17,1),
( 'Sedam', 17,1),
( 'Shahabad', 17,1),
( 'Shahpur', 17,1),
( 'Shiggaon', 17,1),
( 'Shikapur', 17,1),
( 'Shimoga', 17,1),
( 'Shorapur', 17,1),
( 'Shrirangapattana', 17,1),
( 'Sidlaghatta', 17,1),
( 'Sindgi', 17,1),
( 'Sindhnur', 17,1),
( 'Sira', 17,1),
( 'Sirsi', 17,1),
( 'Siruguppa', 17,1),
( 'Srinivaspur', 17,1),
( 'Talikota', 17,1),
( 'Tarikere', 17,1),
( 'Tekkalakota', 17,1),
( 'Terdal', 17,1),
( 'Tiptur', 17,1),
( 'Tumkur', 17,1),
( 'Udupi', 17,1),
( 'Vijayapura', 17,1),
( 'Wadi', 17,1),
( 'Yadgir', 17,1),
( 'Adoor', 16,1),
( 'Akathiyoor', 16,1),
( 'Alappuzha', 16,1),
( 'Ancharakandy', 16,1),
( 'Aroor', 16,1),
( 'Ashtamichira', 16,1),
( 'Attingal', 16,1),
( 'Avinissery', 16,1),
( 'Chalakudy', 16,1),
( 'Changanassery', 16,1),
( 'Chendamangalam', 16,1),
( 'Chengannur', 16,1),
( 'Cherthala', 16,1),
( 'Cheruthazham', 16,1),
( 'Chittur-Thathamangalam', 16,1),
( 'Chockli', 16,1),
( 'Erattupetta', 16,1),
( 'Guruvayoor', 16,1),
( 'Irinjalakuda', 16,1),
( 'Kadirur', 16,1),
( 'Kalliasseri', 16,1),
( 'Kalpetta', 16,1),
( 'Kanhangad', 16,1),
( 'Kanjikkuzhi', 16,1),
( 'Kannur', 16,1),
( 'Kasaragod', 16,1),
( 'Kayamkulam', 16,1),
( 'Kochi', 16,1),
( 'Kodungallur', 16,1),
( 'Kollam', 16,1),
( 'Koothuparamba', 16,1),
( 'Kothamangalam', 16,1),
( 'Kottayam', 16,1),
( 'Kozhikode', 16,1),
( 'Kunnamkulam', 16,1),
( 'Malappuram', 16,1),
( 'Mattannur', 16,1),
( 'Mavelikkara', 16,1),
( 'Mavoor', 16,1),
( 'Muvattupuzha', 16,1),
( 'Nedumangad', 16,1),
( 'Neyyattinkara', 16,1),
( 'Ottappalam', 16,1),
( 'Palai', 16,1),
( 'Palakkad', 16,1),
( 'Panniyannur', 16,1),
( 'Pappinisseri', 16,1),
( 'Paravoor', 16,1),
( 'Pathanamthitta', 16,1),
( 'Payyannur', 16,1),
( 'Peringathur', 16,1),
( 'Perinthalmanna', 16,1),
( 'Perumbavoor', 16,1),
( 'Ponnani', 16,1),
( 'Punalur', 16,1),
( 'Quilandy', 16,1),
( 'Shoranur', 16,1),
( 'Taliparamba', 16,1),
( 'Thiruvalla', 16,1),
( 'Thiruvananthapuram', 16,1),
( 'Thodupuzha', 16,1),
( 'Thrissur', 16,1),
( 'Tirur', 16,1),
( 'Vadakara', 16,1),
( 'Vaikom', 16,1),
( 'Varkala', 16,1),
( 'Kavaratti', 18,1),
( 'Ashok Nagar', 22,1),
( 'Balaghat', 22,1),
( 'Betul', 22,1),
( 'Bhopal', 22,1),
( 'Burhanpur', 22,1),
( 'Chhatarpur', 22,1),
( 'Dabra', 22,1),
( 'Datia', 22,1),
( 'Dewas', 22,1),
( 'Dhar', 22,1),
( 'Fatehabad', 22,1),
( 'Gwalior', 22,1),
( 'Indore', 22,1),
( 'Itarsi', 22,1),
( 'Jabalpur', 22,1),
( 'Katni', 22,1),
( 'Kotma', 22,1),
( 'Lahar', 22,1),
( 'Lundi', 22,1),
( 'Maharajpur', 22,1),
( 'Mahidpur', 22,1),
( 'Maihar', 22,1),
( 'Malajkhand', 22,1),
( 'Manasa', 22,1),
( 'Manawar', 22,1),
( 'Mandideep', 22,1),
( 'Mandla', 22,1),
( 'Mandsaur', 22,1),
( 'Mauganj', 22,1),
( 'Mhow Cantonment', 22,1),
( 'Mhowgaon', 22,1),
( 'Morena', 22,1),
( 'Multai', 22,1),
( 'Murwara', 22,1),
( 'Nagda', 22,1),
( 'Nainpur', 22,1),
( 'Narsinghgarh', 22,1),
( 'Narsinghgarh', 22,1),
( 'Neemuch', 22,1),
( 'Nepanagar', 22,1),
( 'Niwari', 22,1),
( 'Nowgong', 22,1),
( 'Nowrozabad', 22,1),
( 'Pachore', 22,1),
( 'Pali', 22,1),
( 'Panagar', 22,1),
( 'Pandhurna', 22,1),
( 'Panna', 22,1),
( 'Pasan', 22,1),
( 'Pipariya', 22,1),
( 'Pithampur', 22,1),
( 'Porsa', 22,1),
( 'Prithvipur', 22,1),
( 'Raghogarh-Vijaypur', 22,1),
( 'Rahatgarh', 22,1),
( 'Raisen', 22,1),
( 'Rajgarh', 22,1),
( 'Ratlam', 22,1),
( 'Rau', 22,1),
( 'Rehli', 22,1),
( 'Rewa', 22,1),
( 'Sabalgarh', 22,1),
( 'Sagar', 22,1),
( 'Sanawad', 22,1),
( 'Sarangpur', 22,1),
( 'Sarni', 22,1),
( 'Satna', 22,1),
( 'Sausar', 22,1),
( 'Sehore', 22,1),
( 'Sendhwa', 22,1),
( 'Seoni', 22,1),
( 'Seoni-Malwa', 22,1),
( 'Shahdol', 22,1),
( 'Shajapur', 22,1),
( 'Shamgarh', 22,1),
( 'Sheopur', 22,1),
( 'Shivpuri', 22,1),
( 'Shujalpur', 22,1),
( 'Sidhi', 22,1),
( 'Sihora', 22,1),
( 'Singrauli', 22,1),
( 'Sironj', 22,1),
( 'Sohagpur', 22,1),
( 'Tarana', 22,1),
( 'Tikamgarh', 22,1),
( 'Ujhani', 22,1),
( 'Ujjain', 22,1),
( 'Umaria', 22,1),
( 'Vidisha', 22,1),
( 'Wara Seoni', 22,1),
( 'Ahmednagar', 20,1),
( 'Akola', 20,1),
( 'Amravati', 20,1),
( 'Aurangabad', 20,1),
( 'Baramati', 20,1),
( 'Chalisgaon', 20,1),
( 'Chinchani', 20,1),
( 'Devgarh', 20,1),
( 'Dhule', 20,1),
( 'Dombivli', 20,1),
( 'Durgapur', 20,1),
( 'Ichalkaranji', 20,1),
( 'Jalna', 20,1),
( 'Kalyan', 20,1),
( 'Latur', 20,1),
( 'Loha', 20,1),
( 'Lonar', 20,1),
( 'Lonavla', 20,1),
( 'Mahad', 20,1),
( 'Mahuli', 20,1),
( 'Malegaon', 20,1),
( 'Malkapur', 20,1),
( 'Manchar', 20,1),
( 'Mangalvedhe', 20,1),
( 'Mangrulpir', 20,1),
( 'Manjlegaon', 20,1),
( 'Manmad', 20,1),
( 'Manwath', 20,1),
( 'Mehkar', 20,1),
( 'Mhaswad', 20,1),
( 'Miraj', 20,1),
( 'Morshi', 20,1),
( 'Mukhed', 20,1),
( 'Mul', 20,1),
( 'Mumbai', 20,1),
( 'Murtijapur', 20,1),
( 'Nagpur', 20,1),
( 'Nalasopara', 20,1),
( 'Nanded-Waghala', 20,1),
( 'Nandgaon', 20,1),
( 'Nandura', 20,1),
( 'Nandurbar', 20,1),
( 'Narkhed', 20,1),
( 'Nashik', 20,1),
( 'Navi Mumbai', 20,1),
( 'Nawapur', 20,1),
( 'Nilanga', 20,1),
( 'Osmanabad', 20,1),
( 'Ozar', 20,1),
( 'Pachora', 20,1),
( 'Paithan', 20,1),
( 'Palghar', 20,1),
( 'Pandharkaoda', 20,1),
( 'Pandharpur', 20,1),
( 'Panvel', 20,1),
( 'Parbhani', 20,1),
( 'Parli', 20,1),
( 'Parola', 20,1),
( 'Partur', 20,1),
( 'Pathardi', 20,1),
( 'Pathri', 20,1),
( 'Patur', 20,1),
( 'Pauni', 20,1),
( 'Pen', 20,1),
( 'Phaltan', 20,1),
( 'Pulgaon', 20,1),
( 'Pune', 20,1),
( 'Purna', 20,1),
( 'Pusad', 20,1),
( 'Rahuri', 20,1),
( 'Rajura', 20,1),
( 'Ramtek', 20,1),
( 'Ratnagiri', 20,1),
( 'Raver', 20,1),
( 'Risod', 20,1),
( 'Sailu', 20,1),
( 'Sangamner', 20,1),
( 'Sangli', 20,1),
( 'Sangole', 20,1),
( 'Sasvad', 20,1),
( 'Satana', 20,1),
( 'Satara', 20,1),
( 'Savner', 20,1),
( 'Sawantwadi', 20,1),
( 'Shahade', 20,1),
( 'Shegaon', 20,1),
( 'Shendurjana', 20,1),
( 'Shirdi', 20,1),
( 'Shirpur-Warwade', 20,1),
( 'Shirur', 20,1),
( 'Shrigonda', 20,1),
( 'Shrirampur', 20,1),
( 'Sillod', 20,1),
( 'Sinnar', 20,1),
( 'Solapur', 20,1),
( 'Soyagaon', 20,1),
( 'Talegaon Dabhade', 20,1),
( 'Talode', 20,1),
( 'Tasgaon', 20,1),
( 'Tirora', 20,1),
( 'Tuljapur', 20,1),
( 'Tumsar', 20,1),
( 'Uran', 20,1),
( 'Uran Islampur', 20,1),
( 'Wadgaon Road', 20,1),
( 'Wai', 20,1),
( 'Wani', 20,1),
( 'Wardha', 20,1),
( 'Warora', 20,1),
( 'Warud', 20,1),
( 'Washim', 20,1),
( 'Yevla', 20,1),
( 'Uchgaon', 20,1),
( 'Udgir', 20,1),
( 'Umarga', 20,1),
( 'Umarkhed', 20,1),
( 'Umred', 20,1),
( 'Vadgaon Kasba', 20,1),
( 'Vaijapur', 20,1),
( 'Vasai', 20,1),
( 'Virar', 20,1),
( 'Vita', 20,1),
( 'Yavatmal', 20,1),
( 'Yawal', 20,1),
( 'Imphal', 21,1),
( 'Kakching', 21,1),
( 'Lilong', 21,1),
( 'Mayang Imphal', 21,1),
( 'Thoubal', 21,1),
( 'Jowai', 19,1),
( 'Nongstoin', 19,1),
( 'Shillong', 19,1),
( 'Tura', 19,1),
( 'Aizawl', 23,1),
( 'Champhai', 23,1),
( 'Lunglei', 23,1),
( 'Saiha', 23,1),
( 'Dimapur', 24,1),
( 'Kohima', 24,1),
( 'Mokokchung', 24,1),
( 'Tuensang', 24,1),
( 'Wokha', 24,1),
( 'Zunheboto', 24,1),
( 'Anandapur', 25,1),
( 'Anugul', 25,1),
( 'Asika', 25,1),
( 'Balangir', 25,1),
( 'Balasore', 25,1),
( 'Baleshwar', 25,1),
( 'Bamra', 25,1),
( 'Barbil', 25,1),
( 'Bargarh', 25,1),
( 'Bargarh', 25,1),
( 'Baripada', 25,1),
( 'Basudebpur', 25,1),
( 'Belpahar', 25,1),
( 'Bhadrak', 25,1),
( 'Bhawanipatna', 25,1),
( 'Bhuban', 25,1),
( 'Bhubaneswar', 25,1),
( 'Biramitrapur', 25,1),
( 'Brahmapur', 25,1),
( 'Brajrajnagar', 25,1),
( 'Byasanagar', 25,1),
( 'Cuttack', 25,1),
( 'Debagarh', 25,1),
( 'Dhenkanal', 25,1),
( 'Gunupur', 25,1),
( 'Hinjilicut', 25,1),
( 'Jagatsinghapur', 25,1),
( 'Jajapur', 25,1),
( 'Jaleswar', 25,1),
( 'Jatani', 25,1),
( 'Jeypur', 25,1),
( 'Jharsuguda', 25,1),
( 'Joda', 25,1),
( 'Kantabanji', 25,1),
( 'Karanjia', 25,1),
( 'Kendrapara', 25,1),
( 'Kendujhar', 25,1),
( 'Khordha', 25,1),
( 'Koraput', 25,1),
( 'Malkangiri', 25,1),
( 'Nabarangapur', 25,1),
( 'Paradip', 25,1),
( 'Parlakhemundi', 25,1),
( 'Pattamundai', 25,1),
( 'Phulabani', 25,1),
( 'Puri', 25,1),
( 'Rairangpur', 25,1),
( 'Rajagangapur', 25,1),
( 'Raurkela', 25,1),
( 'Rayagada', 25,1),
( 'Sambalpur', 25,1),
( 'Soro', 25,1),
( 'Sunabeda', 25,1),
( 'Sundargarh', 25,1),
( 'Talcher', 25,1),
( 'Titlagarh', 25,1),
( 'Umarkote', 25,1),
( 'Karaikal', 27,1),
( 'Mahe', 27,1),
( 'Pondicherry', 27,1),
( 'Yanam', 27,1),
( 'Ahmedgarh', 26,1),
( 'Amritsar', 26,1),
( 'Barnala', 26,1),
( 'Batala', 26,1),
( 'Bathinda', 26,1),
( 'Bhagha Purana', 26,1)

INSERT INTO app.Cities([Name],StateId,CountryId) VALUES
( 'Budhlada', 26,1),
( 'Dasua', 26,1),
( 'Dhuri', 26,1),
( 'Dinanagar', 26,1),
( 'Faridkot', 26,1),
( 'Fazilka', 26,1),
( 'Firozpur', 26,1),
( 'Firozpur Cantt.', 26,1),
( 'Giddarbaha', 26,1),
( 'Gobindgarh', 26,1),
( 'Gurdaspur', 26,1),
( 'Hoshiarpur', 26,1),
( 'Jagraon', 26,1),
( 'Jaitu', 26,1),
( 'Jalalabad', 26,1),
( 'Jalandhar', 26,1),
( 'Jalandhar Cantt.', 26,1),
( 'Jandiala', 26,1),
( 'Kapurthala', 26,1),
( 'Karoran', 26,1),
( 'Kartarpur', 26,1),
( 'Khanna', 26,1),
( 'Kharar', 26,1),
( 'Kot Kapura', 26,1),
( 'Kurali', 26,1),
( 'Longowal', 26,1),
( 'Ludhiana', 26,1),
( 'Malerkotla', 26,1),
( 'Malout', 26,1),
( 'Mansa', 26,1),
( 'Maur', 26,1),
( 'Moga', 26,1),
( 'Mohali', 26,1),
( 'Morinda', 26,1),
( 'Mukerian', 26,1),
( 'Muktsar', 26,1),
( 'Nabha', 26,1),
( 'Nakodar', 26,1),
( 'Nangal', 26,1),
( 'Nawanshahr', 26,1),
( 'Pathankot', 26,1),
( 'Patiala', 26,1),
( 'Patran', 26,1),
( 'Patti', 26,1),
( 'Phagwara', 26,1),
( 'Phillaur', 26,1),
( 'Qadian', 26,1),
( 'Raikot', 26,1),
( 'Rajpura', 26,1),
( 'Rampura Phul', 26,1),
( 'Rupnagar', 26,1),
( 'Samana', 26,1),
( 'Sangrur', 26,1),
( 'Sirhind Fatehgarh Sahib', 26,1),
( 'Sujanpur', 26,1),
( 'Sunam', 26,1),
( 'Talwara', 26,1),
( 'Tarn Taran', 26,1),
( 'Urmar Tanda', 26,1),
( 'Zira', 26,1),
( 'Zirakpur', 26,1),
( 'Bali', 28,1),
( 'Banswara', 28,1),
( 'Ajmer', 28,1),
( 'Alwar', 28,1),
( 'Bandikui', 28,1),
( 'Baran', 28,1),
( 'Barmer', 28,1),
( 'Bikaner', 28,1),
( 'Fatehpur', 28,1),
( 'Jaipur', 28,1),
( 'Jaisalmer', 28,1),
( 'Jodhpur', 28,1),
( 'Kota', 28,1),
( 'Lachhmangarh', 28,1),
( 'Ladnu', 28,1),
( 'Lakheri', 28,1),
( 'Lalsot', 28,1),
( 'Losal', 28,1),
( 'Makrana', 28,1),
( 'Malpura', 28,1),
( 'Mandalgarh', 28,1),
( 'Mandawa', 28,1),
( 'Mangrol', 28,1),
( 'Merta City', 28,1),
( 'Mount Abu', 28,1),
( 'Nadbai', 28,1),
( 'Nagar', 28,1),
( 'Nagaur', 28,1),
( 'Nargund', 28,1),
( 'Nasirabad', 28,1),
( 'Nathdwara', 28,1),
( 'Navalgund', 28,1),
( 'Nawalgarh', 28,1),
( 'Neem-Ka-Thana', 28,1),
( 'Nelamangala', 28,1),
( 'Nimbahera', 28,1),
( 'Nipani', 28,1),
( 'Niwai', 28,1),
( 'Nohar', 28,1),
( 'Nokha', 28,1),
( 'Pali', 28,1),
( 'Phalodi', 28,1),
( 'Phulera', 28,1),
( 'Pilani', 28,1),
( 'Pilibanga', 28,1),
( 'Pindwara', 28,1),
( 'Pipar City', 28,1),
( 'Prantij', 28,1),
( 'Pratapgarh', 28,1),
( 'Raisinghnagar', 28,1),
( 'Rajakhera', 28,1),
( 'Rajaldesar', 28,1),
( 'Rajgarh (Alwar)', 28,1),
( 'Rajgarh (Churu', 28,1),
( 'Rajsamand', 28,1),
( 'Ramganj Mandi', 28,1),
( 'Ramngarh', 28,1),
( 'Ratangarh', 28,1),
( 'Rawatbhata', 28,1),
( 'Rawatsar', 28,1),
( 'Reengus', 28,1),
( 'Sadri', 28,1),
( 'Sadulshahar', 28,1),
( 'Sagwara', 28,1),
( 'Sambhar', 28,1),
( 'Sanchore', 28,1),
( 'Sangaria', 28,1),
( 'Sardarshahar', 28,1),
( 'Sawai Madhopur', 28,1),
( 'Shahpura', 28,1),
( 'Shahpura', 28,1),
( 'Sheoganj', 28,1),
( 'Sikar', 28,1),
( 'Sirohi', 28,1),
( 'Sojat', 28,1),
( 'Sri Madhopur', 28,1),
( 'Sujangarh', 28,1),
( 'Sumerpur', 28,1),
( 'Suratgarh', 28,1),
( 'Taranagar', 28,1),
( 'Todabhim', 28,1),
( 'Todaraisingh', 28,1),
( 'Tonk', 28,1),
( 'Udaipur', 28,1),
( 'Udaipurwati', 28,1),
( 'Vijainagar', 28,1),
( 'Gangtok', 29,1),
( 'Calcutta', 34,1),
( 'Arakkonam', 30,1),
( 'Arcot', 30,1),
( 'Aruppukkottai', 30,1),
( 'Bhavani', 30,1),
( 'Chengalpattu', 30,1),
( 'Chennai', 30,1),
( 'Chinna salem', 30,1),
( 'Coimbatore', 30,1),
( 'Coonoor', 30,1),
( 'Cuddalore', 30,1),
( 'Dharmapuri', 30,1),
( 'Dindigul', 30,1),
( 'Erode', 30,1),
( 'Gudalur', 30,1),
( 'Kanchipuram', 30,1),
( 'Karaikudi', 30,1),
( 'Karungal', 30,1),
( 'Karur', 30,1),
( 'Kollankodu', 30,1),
( 'Lalgudi', 30,1),
( 'Madurai', 30,1),
( 'Nagapattinam', 30,1),
( 'Nagercoil', 30,1),
( 'Namagiripettai', 30,1),
( 'Namakkal', 30,1),
( 'Nandivaram-Guduvancheri', 30,1),
( 'Nanjikottai', 30,1),
( 'Natham', 30,1),
( 'Nellikuppam', 30,1),
( 'Neyveli', 30,1),
( 'O'' Valley', 30,1),
( 'Oddanchatram', 30,1),
( 'P.N.Patti', 30,1),
( 'Pacode', 30,1),
( 'Padmanabhapuram', 30,1),
( 'Palani', 30,1),
( 'Palladam', 30,1),
( 'Pallapatti', 30,1),
( 'Pallikonda', 30,1),
( 'Panagudi', 30,1),
( 'Panruti', 30,1),
( 'Paramakudi', 30,1),
( 'Parangipettai', 30,1),
( 'Pattukkottai', 30,1),
( 'Perambalur', 30,1),
( 'Peravurani', 30,1),
( 'Periyakulam', 30,1),
( 'Periyasemur', 30,1),
( 'Pernampattu', 30,1),
( 'Pollachi', 30,1),
( 'Polur', 30,1),
( 'Ponneri', 30,1),
( 'Pudukkottai', 30,1),
( 'Pudupattinam', 30,1),
( 'Puliyankudi', 30,1),
( 'Punjaipugalur', 30,1),
( 'Rajapalayam', 30,1),
( 'Ramanathapuram', 30,1),
( 'Rameshwaram', 30,1),
( 'Rasipuram', 30,1),
( 'Salem', 30,1),
( 'Sankarankoil', 30,1),
( 'Sankari', 30,1),
( 'Sathyamangalam', 30,1),
( 'Sattur', 30,1),
( 'Shenkottai', 30,1),
( 'Sholavandan', 30,1),
( 'Sholingur', 30,1),
( 'Sirkali', 30,1),
( 'Sivaganga', 30,1),
( 'Sivagiri', 30,1),
( 'Sivakasi', 30,1),
( 'Srivilliputhur', 30,1),
( 'Surandai', 30,1),
( 'Suriyampalayam', 30,1),
( 'Tenkasi', 30,1),
( 'Thammampatti', 30,1),
( 'Thanjavur', 30,1),
( 'Tharamangalam', 30,1),
( 'Tharangambadi', 30,1),
( 'Theni Allinagaram', 30,1),
( 'Thirumangalam', 30,1),
( 'Thirunindravur', 30,1),
( 'Thiruparappu', 30,1),
( 'Thirupuvanam', 30,1),
( 'Thiruthuraipoondi', 30,1),
( 'Thiruvallur', 30,1),
( 'Thiruvarur', 30,1),
( 'Thoothukudi', 30,1),
( 'Thuraiyur', 30,1),
( 'Tindivanam', 30,1),
( 'Tiruchendur', 30,1),
( 'Tiruchengode', 30,1),
( 'Tiruchirappalli', 30,1),
( 'Tirukalukundram', 30,1),
( 'Tirukkoyilur', 30,1),
( 'Tirunelveli', 30,1),
( 'Tirupathur', 30,1),
( 'Tirupathur', 30,1),
( 'Tiruppur', 30,1),
( 'Tiruttani', 30,1),
( 'Tiruvannamalai', 30,1),
( 'Tiruvethipuram', 30,1),
( 'Tittakudi', 30,1),
( 'Udhagamandalam', 30,1),
( 'Udumalaipettai', 30,1),
( 'Unnamalaikadai', 30,1),
( 'Usilampatti', 30,1),
( 'Uthamapalayam', 30,1),
( 'Uthiramerur', 30,1),
( 'Vadakkuvalliyur', 30,1),
( 'Vadalur', 30,1),
( 'Vadipatti', 30,1),
( 'Valparai', 30,1),
( 'Vandavasi', 30,1),
( 'Vaniyambadi', 30,1),
( 'Vedaranyam', 30,1),
( 'Vellakoil', 30,1),
( 'Vellore', 30,1),
( 'Vikramasingapuram', 30,1),
( 'Viluppuram', 30,1),
( 'Virudhachalam', 30,1),
( 'Virudhunagar', 30,1),
( 'Viswanatham', 30,1),
( 'Agartala', 31,1),
( 'Badharghat', 31,1),
( 'Dharmanagar', 31,1),
( 'Indranagar', 31,1),
( 'Jogendranagar', 31,1),
( 'Kailasahar', 31,1),
( 'Khowai', 31,1),
( 'Pratapgarh', 31,1),
( 'Udaipur', 31,1),
( 'Achhnera', 33,1),
( 'Adari', 33,1),
( 'Agra', 33,1),
( 'Aligarh', 33,1),
( 'Allahabad', 33,1),
( 'Amroha', 33,1),
( 'Azamgarh', 33,1),
( 'Bahraich', 33,1),
( 'Ballia', 33,1),
( 'Balrampur', 33,1),
( 'Banda', 33,1),
( 'Bareilly', 33,1),
( 'Chandausi', 33,1),
( 'Dadri', 33,1),
( 'Deoria', 33,1),
( 'Etawah', 33,1),
( 'Fatehabad', 33,1),
( 'Fatehpur', 33,1),
( 'Fatehpur', 33,1),
( 'Greater Noida', 33,1),
( 'Hamirpur', 33,1),
( 'Hardoi', 33,1),
( 'Jajmau', 33,1),
( 'Jaunpur', 33,1),
( 'Jhansi', 33,1),
( 'Kalpi', 33,1),
( 'Kanpur', 33,1),
( 'Kota', 33,1),
( 'Laharpur', 33,1),
( 'Lakhimpur', 33,1),
( 'Lal Gopalganj Nindaura', 33,1),
( 'Lalganj', 33,1),
( 'Lalitpur', 33,1),
( 'Lar', 33,1),
( 'Loni', 33,1),
( 'Lucknow', 33,1),
( 'Mathura', 33,1),
( 'Meerut', 33,1),
( 'Modinagar', 33,1),
( 'Muradnagar', 33,1),
( 'Nagina', 33,1),
( 'Najibabad', 33,1),
( 'Nakur', 33,1),
( 'Nanpara', 33,1),
( 'Naraura', 33,1),
( 'Naugawan Sadat', 33,1),
( 'Nautanwa', 33,1),
( 'Nawabganj', 33,1),
( 'Nehtaur', 33,1),
( 'NOIDA', 33,1),
( 'Noorpur', 33,1),
( 'Obra', 33,1),
( 'Orai', 33,1),
( 'Padrauna', 33,1),
( 'Palia Kalan', 33,1),
( 'Parasi', 33,1),
( 'Phulpur', 33,1),
( 'Pihani', 33,1),
( 'Pilibhit', 33,1),
( 'Pilkhuwa', 33,1),
( 'Powayan', 33,1),
( 'Pukhrayan', 33,1),
( 'Puranpur', 33,1),
( 'Purquazi', 33,1),
( 'Purwa', 33,1),
( 'Rae Bareli', 33,1),
( 'Rampur', 33,1),
( 'Rampur Maniharan', 33,1),
( 'Rasra', 33,1),
( 'Rath', 33,1),
( 'Renukoot', 33,1),
( 'Reoti', 33,1),
( 'Robertsganj', 33,1),
( 'Rudauli', 33,1),
( 'Rudrapur', 33,1),
( 'Sadabad', 33,1),
( 'Safipur', 33,1),
( 'Saharanpur', 33,1),
( 'Sahaspur', 33,1),
( 'Sahaswan', 33,1),
( 'Sahawar', 33,1),
( 'Sahjanwa', 33,1),
( 'Sambhal', 33,1),
( 'Samdhan', 33,1),
( 'Samthar', 33,1),
( 'Sandi', 33,1),
( 'Sandila', 33,1),
( 'Sardhana', 33,1),
( 'Seohara', 33,1),
( 'Shahganj', 33,1),
( 'Shahjahanpur', 33,1),
( 'Shamli', 33,1),
( 'Sherkot', 33,1),
( 'Shikohabad', 33,1),
( 'Shishgarh', 33,1),
( 'Siana', 33,1),
( 'Sikanderpur', 33,1),
( 'Sikandra Rao', 33,1),
( 'Sikandrabad', 33,1),
( 'Sirsaganj', 33,1),
( 'Sirsi', 33,1),
( 'Sitapur', 33,1),
( 'Soron', 33,1),
( 'Suar', 33,1),
( 'Sultanpur', 33,1),
( 'Sumerpur', 33,1),
( 'Tanda', 33,1),
( 'Tanda', 33,1),
( 'Tetri Bazar', 33,1),
( 'Thakurdwara', 33,1),
( 'Thana Bhawan', 33,1),
( 'Tilhar', 33,1),
( 'Tirwaganj', 33,1),
( 'Tulsipur', 33,1),
( 'Tundla', 33,1),
( 'Unnao', 33,1),
( 'Utraula', 33,1),
( 'Varanasi', 33,1),
( 'Vrindavan', 33,1),
( 'Warhapur', 33,1),
( 'Zaidpur', 33,1),
( 'Zamania', 33,1),
( 'Almora', 32,1),
( 'Bazpur', 32,1),
( 'Chamba', 32,1),
( 'Dehradun', 32,1),
( 'Haldwani', 32,1),
( 'Haridwar', 32,1),
( 'Jaspur', 32,1),
( 'Kashipur', 32,1),
( 'kichha', 32,1),
( 'Kotdwara', 32,1),
( 'Manglaur', 32,1),
( 'Mussoorie', 32,1),
( 'Nagla', 32,1),
( 'Nainital', 32,1),
( 'Pauri', 32,1),
( 'Pithoragarh', 32,1),
( 'Ramnagar', 32,1),
( 'Rishikesh', 32,1),
( 'Roorkee', 32,1),
( 'Rudrapur', 32,1),
( 'Sitarganj', 32,1),
( 'Tehri', 32,1),
( 'Muzaffarnagar', 33,1),
( 'Alipurduar', 34,1),
( 'Arambagh', 34,1),
( 'Asansol', 34,1),
( 'Baharampur', 34,1),
( 'Bally', 34,1),
( 'Balurghat', 34,1),
( 'Bankura', 34,1),
( 'Barakar', 34,1),
( 'Barasat', 34,1),
( 'Bardhaman', 34,1),
( 'Bidhan Nagar', 34,1),
( 'Chinsura', 34,1),
( 'Contai', 34,1),
( 'Cooch Behar', 34,1),
( 'Darjeeling', 34,1),
( 'Durgapur', 34,1),
( 'Haldia', 34,1),
( 'Howrah', 34,1),
( 'Islampur', 34,1),
( 'Jhargram', 34,1),
( 'Kharagpur', 34,1),
( 'Kolkata', 34,1),
( 'Mainaguri', 34,1),
( 'Mal', 34,1),
( 'Mathabhanga', 34,1),
( 'Medinipur', 34,1),
( 'Memari', 34,1),
( 'Monoharpur', 34,1),
( 'Murshidabad', 34,1),
( 'Nabadwip', 34,1),
( 'Naihati', 34,1),
( 'Panchla', 34,1),
( 'Pandua', 34,1),
( 'Paschim Punropara', 34,1),
( 'Purulia', 34,1),
( 'Raghunathpur', 34,1),
( 'Raiganj', 34,1),
( 'Rampurhat', 34,1),
( 'Ranaghat', 34,1),
( 'Sainthia', 34,1),
( 'Santipur', 34,1),
( 'Siliguri', 34,1),
( 'Sonamukhi', 34,1),
( 'Srirampore', 34,1),
( 'Suri', 34,1),
( 'Taki', 34,1),
( 'Tamluk', 34,1),
( 'Tarakeswar', 34,1),
( 'Chikmagalur', 17,1),
( 'Davanagere', 17,1),
( 'Dharwad', 17,1),
( 'Gadag', 17,1),
( 'Chennai', 30,1),
( 'Coimbatore', 30,1),
('Thane',20,1),
('Gandhidham',11,1),
('Chandigarh',7,1)