﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetRoleById
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to get role by Id.

Test Execution	: EXEC subscriber.GetRoleById
					@SubscriberId =  1,
					@Id =  1,
					@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[GetRoleById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[GetRoleById]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ''',@IpAddress,''
									  ),
			@ProcedureName = '[subscriber].[GetRoleById]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			ct.Id,
			ct.[Name],
			ct.[Description]
		FROM
			subscriber.Roles AS ct
		WHERE
			ct.Id = @Id;

		SELECT
			rl.RightId
		FROM
			subscriber.RoleRights AS rl
		WHERE
			rl.RoleId = @Id;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
