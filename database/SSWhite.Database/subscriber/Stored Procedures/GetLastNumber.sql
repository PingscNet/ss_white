﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetLastNumber
Comments		: 08-04-2021 | Amit Khanna | This procedure is used to get last number from number config.

Test Execution	: EXEC [subscriber].[GetLastNumber]
						@SubscriberId =1,
						@EntityId = 1,
						@FinancialYear = 201920,
						@DocumentType = 1,
						@ModuleType = NULL,
						@Status = NULL,
						@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[GetLastNumber]
(
	@SubscriberId INT,
	@EntityId INT,
	@FinancialYear INT,
	@DocumentType TINYINT,
	@ModuleType TINYINT,
	@Status TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[GetLastNumber]','
										@SubscriberId =',@SubscriberId,',
										@EntityId = ',@EntityId,',
										@FinancialYear = ',@FinancialYear,',
										@DocumentType = ',@DocumentType,',
										@ModuleType = ',@ModuleType,',
										@Status = ',@Status,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[subscriber].[GetLastNumber]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT); 
	DECLARE @LastNumber INT;
    BEGIN TRY
		SELECT
			@LastNumber = dc.LastNumber + 1
		FROM
			subscriber.NumberConfigurations AS dc
		WHERE
			dc.SubscriberId = @SubscriberId
			AND ISNULL(dc.EntityId,'') = ISNULL(@EntityId,ISNULL(dc.EntityId,''))
			AND ISNULL(dc.FinancialYear,'') = ISNULL(@FinancialYear,ISNULL(dc.FinancialYear,''))
			AND ISNULL(dc.DocumentType,'') = ISNULL(@DocumentType,ISNULL(dc.DocumentType,''))
			AND ISNULL(dc.ModuleType,'') = ISNULL(@ModuleType,ISNULL(dc.ModuleType,''))
			AND dc.[Status] = ISNULL(@Status,dc.[Status]);

		RETURN @LastNumber;

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
