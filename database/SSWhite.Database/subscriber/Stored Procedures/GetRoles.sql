﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetRoles
Comments		: 01-04-2021 | Amit Khanna | This procedure is used to get roles for common call.

Test Execution	: EXEC subscriber.GetRoles
					@SubscriberId = 1,
					@Status = NULL,
					@IpAddress = NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[GetRoles]
(
	@SubscriberId INT,
	@Status TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[GetRoles]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[subscriber].[GetRoles]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			st.Id,
			st.[Name],
			st.[Status]
		FROM
			subscriber.Roles AS st
		WHERE
			st.[Status] = ISNULL(@Status,st.[Status])
			AND st.SubscriberId = @SubscriberId;

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
