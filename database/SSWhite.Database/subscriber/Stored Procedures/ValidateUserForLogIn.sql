﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: ValidateUserForLogIn
Comments	: 26-02-2021 | Amit Khanna | This procedure is used to validate user while login into the application.

Test Execution : EXEC subscriber.ValidateUserForLogIn
					@UserName = 'dhavalchavda',
					@Password = '6G94qKPK8LYNjnTllCqm2G3BUM08AzOK7yW30tfjrMc=',
					@IpAddress = NULL,
					@StatusTypeActive = 1,
					@UserTypeAdmin = 2
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[ValidateUserForLogIn]
(
	@UserName VARCHAR(100),
	@Password VARCHAR(MAX),
	@IpAddress VARCHAR(45),
	@StatusTypeActive TINYINT,
	@UserTypeAdmin TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[ValidateUserForLogIn]','
										@UserName =''',@UserName,''',
										@Password = ','',@Password,'','
										@IpAddress = ','',@IpAddress,'','
										@StatusTypeActive =',@StatusTypeActive,',
										@UserTypeAdmin =',@UserTypeAdmin
									  ),
			@ProcedureName = '[subscriber].[ValidateUserForLogIn]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue INT = 1;
	DECLARE @UserId INT,@SubscriberId INT,@UserType SMALLINT;
	DECLARE @SubscriptionEndDate SMALLDATETIME;

    BEGIN TRY
		SELECT
			@SubscriberId = sub.Id,
			@SubscriptionEndDate =  sub.SubscriptionEndDate
		FROM
			subscriber.Subscribers AS sub
		WHERE 
			sub.Id  = (SELECT us.SubscriberId FROM subscriber.Users us WHERE us.UserName = @UserName)

		IF EXISTS(SELECT us.SubscriberId FROM subscriber.Users us WHERE us.UserName = @UserName)
		BEGIN
			IF(@SubscriptionEndDate >= GETDATE())
			BEGIN
				SELECT 
					@UserId = us.Id,
					@UserType = us.[Type]
				FROM 
					subscriber.Users AS us
				WHERE 
					us.UserName = @UserName
					AND us.[Password] = @Password
					AND us.[Status] = @StatusTypeActive
					AND us.EndDate IS NULL;

				SELECT
					us.Id AS UserId,
					us.SubscriberId,
					us.UserName,
					us.FirstName,
					us.LastName,
					us.[Type] AS UserType,
					us.EmailAddress,
					us.IsPasswordReset
				FROM 
					subscriber.Users AS us
				WHERE 
					us.Id = @UserId;

				IF(@UserType = @UserTypeAdmin)
				BEGIN
					SELECT
						DISTINCT
						st.Id,
						st.[Name]
					FROM
						subscriber.UserEntities AS ust
						RIGHT JOIN [master].Companies AS st ON st.Id = ust.EntityId
					WHERE
						st.[Status] = @StatusTypeActive
						AND st.SubscriberId = @SubscriberId;

					SELECT
						DISTINCT
						rt.[Name],
						rt.Acronym,
						rt.RightsGroupId
					FROM
						subscriber.Rights AS rt
						INNER JOIN subscriber.RightsGroup AS rg ON rt.RightsGroupId = rg.Id
						INNER JOIN subscriber.SubscriberRightsGroup AS sgr ON rg.Id = sgr.RightGroupId
					WHERE
						sgr.[Status] = @StatusTypeActive
						AND rg.[Status] = @StatusTypeActive
						AND rt.[Status] = @StatusTypeActive
						AND 
						(
							sgr.TrialExpiry IS NULL 
							OR sgr.TrialExpiry >= GETDATE()
						);
				END
				ELSE
				BEGIN
					SELECT
						st.Id,
						st.[Name]
					FROM
						subscriber.UserEntities AS ust
						INNER JOIN subscriber.Users AS us ON ust.UserId = us.Id
						INNER JOIN [master].Companies AS st ON st.Id = ust.EntityId
					WHERE
						us.Id = @UserId
						AND st.SubscriberId = @SubscriberId;

					SELECT
						DISTINCT
						rt.[Name],
						rt.Acronym,
						rt.RightsGroupId
					FROM
						subscriber.UserRoles AS ur
						INNER JOIN subscriber.Roles AS rl ON ur.RoleId = rl.Id
						INNER JOIN subscriber.RoleRights AS rlrt ON rl.Id = rlrt.RoleId
						INNER JOIN subscriber.Rights AS rt ON rlrt.RightId = rt.Id
						INNER JOIN subscriber.RightsGroup AS rg ON rt.RightsGroupId = rg.Id
						INNER JOIN subscriber.SubscriberRightsGroup AS sgr ON rg.Id = sgr.RightGroupId
					WHERE
						ur.UserId = @UserId
						AND rl.[Status] = @StatusTypeActive
						AND rg.[Status] = @StatusTypeActive
						AND rt.[Status] = @StatusTypeActive
						AND sgr.[Status] = @StatusTypeActive
						AND 
						(
							sgr.TrialExpiry IS NULL 
							OR sgr.TrialExpiry >= GETDATE()
						);
				END;
			END
			ELSE
			BEGIN
				SELECT
				-1 AS SubscriberId
			END
		END
		ELSE
		BEGIN
			SELECT
				us.Id AS UserId,
				us.SubscriberId,
				us.UserName,
				us.FirstName,
				us.LastName,
				us.[Type] AS UserType,
				us.EmailAddress,
				us.IsPasswordReset
			FROM 
				subscriber.Users  AS us
			WHERE 
				us.UserName = @UserName
				AND us.[Password] = @Password
				AND us.[Status] = @StatusTypeActive
				AND us.EndDate IS NULL;

			SELECT
				st.Id,
				st.[Name]
			FROM
				subscriber.UserEntities AS ust
				INNER JOIN subscriber.Users AS us ON ust.UserId = us.Id
				INNER JOIN [master].Companies AS st ON st.Id = ust.EntityId
			WHERE
				us.Id = @UserId
				AND st.SubscriberId = @SubscriberId;

			SELECT
				DISTINCT
				rt.[Name],
				rt.Acronym,
				rt.RightsGroupId
			FROM
				subscriber.UserRoles AS ur
				INNER JOIN subscriber.Roles AS rl ON ur.RoleId = rl.Id
				INNER JOIN subscriber.RoleRights AS rlrt ON rl.Id = rlrt.RoleId
				INNER JOIN subscriber.Rights AS rt ON rlrt.RightId = rt.Id
				INNER JOIN subscriber.RightsGroup AS rg ON rt.RightsGroupId = rg.Id
				INNER JOIN subscriber.SubscriberRightsGroup AS sgr ON rg.Id = sgr.RightGroupId
			WHERE
				ur.UserId = @UserId
				AND sgr.[Status] = @StatusTypeActive
				AND 
				(
					sgr.TrialExpiry IS NULL 
					OR sgr.TrialExpiry >= GETDATE()
				);
		END
			Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH
	;
