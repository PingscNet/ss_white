﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetRightsGroups
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to get rights groups for common call.

Test Execution	: EXEC subscriber.GetRightsGroups
					@SubscriberId = 1,
					@Status = NULL,
					@IpAddress = NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[GetRightsGroups]
(
	@SubscriberId INT,
	@Status TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[GetRightsGroups]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[subscriber].[GetRightsGroups]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			rtg.Id,
			rtg.[Name],
			rtg.[DisplayOrder],
			rtg.Acronym
		FROM
			subscriber.RightsGroup AS rtg
		WHERE
			rtg.[Status] = ISNULL(@Status,rtg.[Status]);

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
