﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllUsers
Comments		: 24-03-2021 | Amit Khanna | This procedure is used to get All users.

Test Execution	: EXEC subscriber.GetAllUsers
					@SubscriberId = 1,
					@UserId =  1,
					@SearchKeyword = NULL,
					@Status = NULL,
					@SortExpression = NULL,
					@Start =  0,
					@Length = 100,
					@IpAddress = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[GetAllUsers]
(
	@SubscriberId INT,
	@UserId INT,
	@SearchKeyword VARCHAR(100),
	@Status TINYINT,
	@SortExpression VARCHAR(50),
	@Start INT,
	@Length INT,
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[GetAllUsers]','
										@SubscriberId =',@SubscriberId,',
										@UserId = ',@UserId,'
										@SearchKeyword = ','',@SearchKeyword,'','
										@Status = ',@Status,'
										@SortExpression = ','',@SortExpression,'','
										@Start = ',@Start,'
										@Length = ',@Length,'
										@IpAddress = ','',@IpAddress,'','
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[subscriber].[GetAllUsers]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		CREATE TABLE #TempTable(Id INT); 
		
		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[subscriber].[Users] AS us
		WHERE 
			us.SubscriberId = @SubscriberId
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.EndDate IS NULL
			AND 
			(
					us.FirstName LIKE '%' + ISNULL(@SearchKeyword,us.FirstName) +'%'
				OR  us.LastName LIKE '%' + ISNULL(@SearchKeyword,us.LastName) +'%'
				OR  us.UserName LIKE '%' + ISNULL(@SearchKeyword,us.UserName) +'%'
				OR  us.MobileNumber LIKE '%' + ISNULL(@SearchKeyword,us.MobileNumber) +'%'
				OR  us.EmailAddress LIKE '%' + ISNULL(@SearchKeyword,us.EmailAddress) +'%'
			);

		SELECT @TotalRecords =COUNT(Id)  FROM #TempTable 

		SELECT
			us.Id,
			us.FirstName,
			us.LastName,
			us.UserName,
			us.EmailAddress,
			us.MobileNumber,
			us.[Type] AS UserType,
			us.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN subscriber.Users us ON tmp.Id = us.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.CreatedDate END ASC,
			CASE WHEN @SortExpression = 'name asc' THEN us.FirstName END ASC,
			CASE WHEN @SortExpression = 'name desc' THEN us.FirstName END DESC,
			CASE WHEN @SortExpression = 'userName asc' THEN us.UserName END ASC,
			CASE WHEN @SortExpression = 'userName desc' THEN us.UserName END DESC,
			CASE WHEN @SortExpression = 'emailAddress asc' THEN us.EmailAddress END ASC,
			CASE WHEN @SortExpression = 'emailAddress desc' THEN us.EmailAddress END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
