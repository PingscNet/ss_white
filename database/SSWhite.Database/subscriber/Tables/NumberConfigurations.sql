﻿CREATE TABLE [subscriber].[NumberConfigurations] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId]     INT           NOT NULL,
    [EntityId]         INT           NULL,
    [ModuleType]       TINYINT       NULL,
    [DocumentType]     TINYINT       NULL,
    [FinancialYear]    INT           NULL,
    [ActualLastNumber] INT           NOT NULL,
    [LastNumber]       INT           NOT NULL,
    [Prefix]           VARCHAR (MAX) NULL,
    [Suffix]           VARCHAR (MAX) NULL,
    [Status]           TINYINT       CONSTRAINT [DF__NumberCon__Statu__019E3B86] DEFAULT ((1)) NOT NULL,
    [EndDate]          SMALLDATETIME NULL,
    [IpAddress]        VARCHAR (45)  NULL,
    [CreatedBy]        INT           NULL,
    [CreatedDate]      SMALLDATETIME NOT NULL,
    [UpdatedBy]        INT           NULL,
    [UpdatedDate]      SMALLDATETIME NULL,
    CONSTRAINT [PK__NumberCo__3214EC07A74256B1] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__NumberCon__Subsc__00AA174D] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id])
);

