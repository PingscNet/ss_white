﻿CREATE TABLE [emp].[EpoPaymentType] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (255) NOT NULL,
    [status]       TINYINT       NOT NULL,
    [CreatedBy]    INT           NULL,
    [CreatedDate]  DATETIME      NOT NULL,
    [ModifiedBy]   INT           NULL,
    [ModifiedDate] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

