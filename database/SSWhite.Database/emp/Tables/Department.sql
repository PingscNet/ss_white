﻿CREATE TABLE [emp].[Department] (
    [Id]                       INT            IDENTITY (1, 1) NOT NULL,
    [mid]                      INT            NOT NULL,
    [DepartmentName]           NVARCHAR (MAX) NULL,
    [DepartmentSupervisor]     NVARCHAR (MAX) NULL,
    [DepartmentSupervisorName] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_department] PRIMARY KEY CLUSTERED ([Id] ASC, [mid] ASC)
);

