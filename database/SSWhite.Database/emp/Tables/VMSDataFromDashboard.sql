﻿CREATE TABLE [emp].[VMSDataFromDashboard] (
    [Id]            BIGINT   IDENTITY (1, 1) NOT NULL,
    [EmployeeId]    INT      NOT NULL,
    [PunchDateTime] DATETIME NOT NULL,
    [InOutStatus]   TINYINT  NULL,
    [PunchType]     TINYINT  NOT NULL,
    [IsDeleted]     BIT      CONSTRAINT [DF_VMSDataFromDashboard_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]     INT      NOT NULL,
    [CreatedDate]   DATETIME NOT NULL,
    [UpdateBy]      INT      NULL,
    [UpdateDate]    DATETIME NULL,
    CONSTRAINT [PK_emp]].[VMSDataFromDashboard] PRIMARY KEY CLUSTERED ([Id] ASC)
);

