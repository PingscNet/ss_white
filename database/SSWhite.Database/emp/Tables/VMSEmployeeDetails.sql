﻿CREATE TABLE [emp].[VMSEmployeeDetails] (
    [Id]                  INT            IDENTITY (1, 1) NOT NULL,
    [PersonID]            NVARCHAR (50)  NOT NULL,
    [Organization]        NVARCHAR (500) NULL,
    [PersonName]          NVARCHAR (500) NULL,
    [Gender]              NVARCHAR (50)  NULL,
    [EffectiveTime]       DATETIME       NULL,
    [ExpiryTime]          DATETIME       NULL,
    [CardNo]              NVARCHAR (500) NULL,
    [RoomNo]              NVARCHAR (500) NULL,
    [FloorNo]             NVARCHAR (500) NULL,
    [Picture]             NVARCHAR (500) NULL,
    [Add1]                NVARCHAR (50)  NULL,
    [Add2]                NVARCHAR (50)  NULL,
    [DOB]                 DATE           NULL,
    [DOJ]                 DATE           NULL,
    [DOI]                 DATE           NULL,
    [BloodGroup]          NVARCHAR (50)  NULL,
    [signature]           NVARCHAR (200) NULL,
    [EmailAddress]        NVARCHAR (500) NULL,
    [EmpPosition]         NVARCHAR (500) NULL,
    [ReportingUnder]      NVARCHAR (500) NULL,
    [BdayCards]           NVARCHAR (500) NULL,
    [ApprovalForDocument] BIT            NULL,
    [userID]              INT            NULL,
    [Department]          NVARCHAR (200) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([userID]) REFERENCES [emp].[UserMaster] ([Id]),
    CONSTRAINT [uniquekeyvmsonPersonId] UNIQUE NONCLUSTERED ([PersonID] ASC)
);

ALTER TABLE [emp].[VMSEmployeeDetails]
ADD WorkingStatus int;

ALTER TABLE [emp].[VMSEmployeeDetails]
ADD CONSTRAINT vms_WorkingStatus
DEFAULT 0 FOR WorkingStatus;

