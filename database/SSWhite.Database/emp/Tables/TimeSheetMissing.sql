﻿CREATE TABLE [emp].[TimeSheetMissing] (
    [Id]            INT      NOT NULL,
    [EmployeeId]    INT      NOT NULL,
    [PunchTimeDate] DATETIME NOT NULL,
    [IsDeleted]     BIT      NOT NULL,
    [CreatedBy]     INT      NOT NULL,
    [CreatedDate]   DATETIME NOT NULL,
    [UpdatedBy]     INT      NULL,
    [UpdatedDate]   DATETIME NULL
);

