﻿

select * from Roles
select * from Modules
select * from Modules_Pages

--delete from Modules_Pages where ModulePageID in (11,12)
--truncate table Modules_Pages 
--insert into Modules_Pages values(1	,1	,'Add Employee')                                                                                        
--insert into Modules_Pages values(2	,1	,'Employee Limit')                                                                                      
--insert into Modules_Pages values(3	,1	,'Approval Document Person')                                                                            
--insert into Modules_Pages values(4	,2	,'Dashboard')                                                                                           
--insert into Modules_Pages values(5	,3	,'In/Out for Lunch')                                                                                    
--insert into Modules_Pages values(6	,3	,'In/Out for Business')                                                                                 
--insert into Modules_Pages values(7	,3	,'In/Out for Personal Business')                                                                        
--insert into Modules_Pages values(8	,3	,'My Missing Punches')                                                                                  
--insert into Modules_Pages values(9	,3	,'My Time Punch Records')                                                                               
--insert into Modules_Pages values(10	,3	,'Who is In the Building')                                                                              
--insert into Modules_Pages values(11	,4	,'Manage EPO')                                                                              
--insert into Modules_Pages values(12	,4	,'MPC EPO Viewer')                                                                                      
--insert into Modules_Pages values(13	,4	,'MPC Vote For EPO')                                                                                    
--insert into Modules_Pages values(14	,4	,'My EPOs')                                                                                             
--insert into Modules_Pages values(15	,4	,'View My Subordinates EPO')                                                                            
--insert into Modules_Pages values(16	,4	,'My Approval For EPO')                                                                                 
--insert into Modules_Pages values(17	,5	,'Submit New FTO Request')                                                                              
--insert into Modules_Pages values(18	,5	,'Approve FTO')                                                                                         
--insert into Modules_Pages values(19	,5	,'FTO HR Approval')                                                                                     
--insert into Modules_Pages values(20	,5	,'Show All FTO Of My Dept')                                                                             
--insert into Modules_Pages values(21	,5	,'Review YTD FTO Status')                                                                               
--insert into Modules_Pages values(22	,5	,'Show All Future FTO Of My Dept')                                                                     
--insert into Modules_Pages values(23	,6	,'All Documents')                                                                                       
--insert into Modules_Pages values(24	,6	,'Create New ECN')                                                                                      
--insert into Modules_Pages values(25	,6	,'Create New FCD')                                                                                      
--insert into Modules_Pages values(26	,6	,'Create New LIT')                                                                                      
--insert into Modules_Pages values(27	,6	,'Create New SOP')                                                                                      
--insert into Modules_Pages values(28	,6	,'Create New WI')                                                                                       
--insert into Modules_Pages values(29	,6	,'Document awaiting my Approval')                                                                       
--insert into Modules_Pages values(30	,7	,'Roles & Permissions')                                                                                 
--update Modules_Pages set PageName = 'Show All Future FTO Of My Dept' where ModulePageID = 24
--insert into Modules_Pages values(33,5,'FTO Approval' )                                                                                   ) )
select * from Modules_PagesAccess
--SELECT RoleId,* FROM EMP.UserMaster 

select M.ModuleCode+'_'+ MP.PageName UniqureCode, * from Modules_pages MP INNER JOIN Modules M 
ON MP.ModuleID = M.ModuleID

INSERT INTO Modules_PagesAccess (RoleId,ModulePageID,UniqueCode,IsAccess)
SELECT 1,ModulePageID,  REPLACE((M.ModuleCode+'_'+ MP.PageName), ' ', '') ,1 from Modules_pages MP INNER JOIN Modules M 
ON MP.ModuleID = M.ModuleID
INSERT INTO Modules_PagesAccess (RoleId,ModulePageID,UniqueCode,IsAccess)
SELECT 2,ModulePageID,  REPLACE((M.ModuleCode+'_'+ MP.PageName), ' ', '') ,1 from Modules_pages MP INNER JOIN Modules M 
ON MP.ModuleID = M.ModuleID
INSERT INTO Modules_PagesAccess (RoleId,ModulePageID,UniqueCode,IsAccess)
SELECT 3,ModulePageID,  REPLACE((M.ModuleCode+'_'+ MP.PageName), ' ', '') ,1 from Modules_pages MP INNER JOIN Modules M 
ON MP.ModuleID = M.ModuleID
INSERT INTO Modules_PagesAccess (RoleId,ModulePageID,UniqueCode,IsAccess)
SELECT 4,ModulePageID,  REPLACE((M.ModuleCode+'_'+ MP.PageName), ' ', '') ,1 from Modules_pages MP INNER JOIN Modules M 
ON MP.ModuleID = M.ModuleID

--truncate table Modules_PagesAccess 
select * from Modules_PagesAccess where RoleId = 1
----for hr-
updATE Modules_PagesAccess set IsAccess = 0 where UniqueCode in(
'Admin_AddEmployee'                                                                                     
,'Admin_EmployeeLimit'                                                                                      
,'Admin_ApprovalDocumentPerson'
,'EPO_MPCEPOViewer'                                                                                      
,'EPO_MPCVoteForEPO' 
,'Roles_Roles&Permissions'                                                                                 
,'FTO_ApproveFTO')    and roleid = 2   

----for MPC-
updATE Modules_PagesAccess set IsAccess = 0 where UniqueCode in(
'Admin_AddEmployee'                                                                                     
,'Admin_EmployeeLimit'                                                                                      
,'Admin_ApprovalDocumentPerson'
,'FTO_FTOHRApproval                                                                                     '
,'Roles_Roles&Permissions' )    and roleid = 3   



----for employee-
updATE Modules_PagesAccess set IsAccess = 0 where UniqueCode in(
'Admin_AddEmployee'                                                                                     
,'Admin_EmployeeLimit'                                                                                      
,'Admin_ApprovalDocumentPerson'
,'FTO_FTOHRApproval                                                                                     '
,'Roles_Roles&Permissions'                                                                                 
,'EPO_MPCEPOViewer'                                                                                      
,'EPO_MPCVoteForEPO' )    and roleid = 4

