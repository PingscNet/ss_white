﻿CREATE TABLE [emp].[EPORefTable] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [MID]          INT            NOT NULL,
    [PID]          INT            NOT NULL,
    [RefID]        INT            NOT NULL,
    [RefType]      NVARCHAR (100) NULL,
    [RefSubType]   NVARCHAR (100) NULL,
    [RefName]      NVARCHAR (MAX) NULL,
    [CreatedBy]    INT            NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [ModifiedBy]   INT            NULL,
    [ModifiedDate] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

