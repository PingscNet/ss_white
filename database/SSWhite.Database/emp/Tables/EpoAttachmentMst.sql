﻿CREATE TABLE [emp].[EpoAttachmentMst] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [MID]              INT            NOT NULL,
    [EmployeeId]       NVARCHAR (MAX) NULL,
    [UserId]           INT            NULL,
    [AttachedFileName] NVARCHAR (MAX) NULL,
    [AttachmentDate]   DATETIME       NULL,
    [FileName]         NVARCHAR (MAX) NULL,
    [Title]            NVARCHAR (MAX) NULL,
    [Approval]         BIT            NULL,
    [AttachmentType]   NVARCHAR (500) NULL,
    [EPONo]            INT            NULL,
    [Status]           TINYINT        NOT NULL,
    [CreatedBy]        INT            NULL,
    [CreatedDate]      DATETIME       NULL,
    [ModifiedBy]       INT            NULL,
    [ModifiedDate]     DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([EPONo]) REFERENCES [emp].[EpoCreation] ([Id])
);

