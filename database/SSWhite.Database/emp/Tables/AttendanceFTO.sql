﻿

CREATE TABLE [emp].[AttendanceFTO] (
    [ID]                 INT            IDENTITY (1, 1) NOT NULL,
    [UserID]             INT            NULL,
    [EmployeeID]         NVARCHAR (50)  NOT NULL,
    [EmployeeName]       NVARCHAR (500) NULL,
    [StartDate]          DATE           NULL,
    [StartDateDayType]   TINYINT        NOT NULL,
    [EndDate]            DATE           NULL,
    [EndDateDayType]     TINYINT        NOT NULL,
    [LeaveTypeID]        INT            NOT NULL,
    [OtherLeaveTypeId]   INT            NULL,
    [EmployeeComment]    NVARCHAR (MAX) NULL,
    [SupervisorName]     NVARCHAR (MAX) NULL,
    [SupervisorUserId]   INT  NULL,
    [SupervisorNotes]    NVARCHAR (MAX) NULL,
    [HRNotes]            NVARCHAR (MAX) NULL,
    [SupervisorApproval] INT            NULL,
    [HRApproval]         INT            NULL,
    [MPCApproval]        INT            NULL,
    [Approval]           INT            NULL,
    [Status]             TINYINT        NOT NULL,
    [CreatedBy]          INT            NULL,
    [CreatedDate]        DATETIME       NULL,
    [ModifiedBy]         INT            NULL,
    [ModifiedDate]       DATETIME       NULL,
    FOREIGN KEY ([LeaveTypeID]) REFERENCES [master].[LeaveType] ([Id]),
    FOREIGN KEY ([OtherLeaveTypeId]) REFERENCES [master].[OtherLeaveType] ([Id])
);

ALTER TABLE [emp].[AttendanceFTO]
ADD NumberOfDays Float;

