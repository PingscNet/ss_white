﻿CREATE TABLE [emp].[EpoApprovalList] (
    [Id]           INT      IDENTITY (1, 1) NOT NULL,
    [UserId]       INT      NOT NULL,
    [EpoNo]        INT      NOT NULL,
    [ApprovalType] INT      NOT NULL,
    [CreatedBy]    INT      NULL,
    [CreatedDate]  DATETIME NULL,
    [ModifiedBy]   INT      NULL,
    [ModifiedDate] DATETIME NULL
);

