﻿/*------------------------------------------------------------------------------------------------------------
Name			: [emp].[GetDepartmentFtoByUserId]
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	HR userid =68
					[emp].[GetAllDepartmentOrUserFtoByUserId] 
					@UserId = 42,
					@StatusTypeActive = 1,
					@CurrentDate = '2022-05-31',
					@OnlyFutureDate = 0, 
					@ApproveFTO = 0, 
					@OnlyUsersData = 1, 
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL, 
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [emp].[GetAllDepartmentOrUserFtoByUserId] 
(
	@UserId INT,
	@StatusTypeActive INT,
	@CurrentDate DATETIME,
	@OnlyFutureDate BIT,
	@OnlyUsersData BIT,
	@ApproveFTO BIT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@TotalRecords INT OUT
)
AS
BEGIN
	SET NOCOUNT ON;
	CREATE TABLE #TempTable(Id INT,PersonName VARCHAR(500), PersonId VARCHAR(500) ,StartDate DATETIME, ENDDate DATETIME,LeaveType varchar(100),
		EmployeeComment VARCHAR(1000),SupervisorApproval INT, HRApproval INT); 
	
	DECLARE @Department VARCHAR(100);

	SELECT Top 1
		@Department = department
	FROM 
		emp.VMSEmployeeDetails 
	WHERE 
		UserId = @UserId

	IF(@ApproveFTO = 1)
	BEGIN
		INSERT INTO #TempTable
		SELECT  
			ef.Id,
			vms.PersonName,
			vms.PersonID,
			ef.StartDate,
			ef.EndDate,
			lt.[Name] AS LeaveType,
			ef.EmployeeComment,
			ef.SupervisorApproval,
			ef.HRApproval
		FROM [emp].[AttendanceFto] ef
		INNER JOIN 
			[emp].[VMSEmployeeDetails] vms ON vms.UserId = ef.CreatedBY
		INNER JOIN 
			[master].[LeaveType] lt ON lt.Id = ef.LeaveTypeID
		WHERE
			ef.[Status] = @StatusTypeActive
			AND ef.SupervisorUserId =  CASE WHEN @Department = 'HR' THEN ef.SupervisorUserId ELSE @UserId  END
			AND 
				(
						ef.Id LIKE '%' + ISNULL(@SearchKeyword, ef.Id) +'%'
						OR vms.PersonName LIKE '%' + ISNULL(@SearchKeyword, vms.PersonName) +'%'
						OR ef.StartDate  LIKE '%' + ISNULL(@SearchKeyword, ef.StartDate) +'%'
						OR ef.EndDate  LIKE '%' + ISNULL(@SearchKeyword, ef.EndDate) +'%'
						OR ef.EmployeeComment  LIKE '%' + ISNULL(@SearchKeyword, ef.EmployeeComment) +'%'
						OR lt.[Name]  LIKE '%' + ISNULL(@SearchKeyword, lt.[Name]) +'%'
				);
	END
	ELSE IF(@OnlyUsersData = 1)
	BEGIN
		INSERT INTO #TempTable
		SELECT  
			ef.Id,
			vms.PersonName,
			vms.PersonID,
			ef.StartDate,
			ef.EndDate,
			lt.[Name] AS LeaveType,
			ef.EmployeeComment,
			ef.SupervisorApproval,
			ef.HRApproval
		FROM [emp].[AttendanceFto] ef
		INNER JOIN 
			[emp].[VMSEmployeeDetails] vms ON vms.UserId = ef.CreatedBY
		INNER JOIN 
			[master].[LeaveType] lt ON lt.Id = ef.LeaveTypeID
		WHERE
			ef.[Status] = @StatusTypeActive
			AND ef.CreatedBy  = @UserId
			AND 
				(
						ef.Id LIKE '%' + ISNULL(@SearchKeyword, ef.Id) +'%'
						OR vms.PersonName LIKE '%' + ISNULL(@SearchKeyword, vms.PersonName) +'%'
						OR ef.StartDate  LIKE '%' + ISNULL(@SearchKeyword, ef.StartDate) +'%'
						OR ef.EndDate  LIKE '%' + ISNULL(@SearchKeyword, ef.EndDate) +'%'
						OR ef.EmployeeComment  LIKE '%' + ISNULL(@SearchKeyword, ef.EmployeeComment) +'%'
						OR lt.[Name]  LIKE '%' + ISNULL(@SearchKeyword, lt.[Name]) +'%'
				);
	END
	ELSE
	BEGIN
		INSERT INTO #TempTable
		SELECT  
			ef.Id,
			vms.PersonName,
			vms.PersonID,
			ef.StartDate,
			ef.EndDate,
			lt.[Name] AS LeaveType,
			ef.EmployeeComment,
			ef.SupervisorApproval,
			ef.HRApproval
		FROM [emp].[AttendanceFto] ef
		INNER JOIN 
			[emp].[VMSEmployeeDetails] vms ON vms.UserId = ef.CreatedBY
		INNER JOIN 
			[master].[LeaveType] lt ON lt.Id = ef.LeaveTypeID
		WHERE
			ef.[Status] = @StatusTypeActive
			AND ef.CreatedBy  != @UserId
			AND ef.StartDate >= CASE WHEN @OnlyFutureDate = 1 
										THEN   @CurrentDate
									ELSE ef.StartDate 
							    END 
			AND vms.Department = (SELECT  [Department] FROM emp.[VMSEmployeeDetails] WHERE UserId = @UserId)
			AND 
				(
						ef.Id LIKE '%' + ISNULL(@SearchKeyword, ef.Id) +'%'
						OR vms.PersonName LIKE '%' + ISNULL(@SearchKeyword, vms.PersonName) +'%'
						OR ef.StartDate  LIKE '%' + ISNULL(@SearchKeyword, ef.StartDate) +'%'
						OR ef.EndDate  LIKE '%' + ISNULL(@SearchKeyword, ef.EndDate) +'%'
						OR ef.EmployeeComment  LIKE '%' + ISNULL(@SearchKeyword, ef.EmployeeComment) +'%'
						OR lt.[Name]  LIKE '%' + ISNULL(@SearchKeyword, lt.[Name]) +'%'
				);	
	END
	
	SELECT @TotalRecords = COUNT(Id)  FROM #TempTable 

	SELECT
		Id,
		PersonName,
		PersonId,
		StartDate,
		EndDate,
		LeaveType,
		EmployeeComment,
		SupervisorApproval,
		HRApproval
	FROM 
		#TempTable AS tmp
	ORDER BY 
		CASE WHEN @SortExpression IS NULL THEN Id END DESC,
		CASE WHEN @SortExpression = 'Id asc' THEN Id END ASC,
		CASE WHEN @SortExpression = 'Id desc' THEN Id END DESC,
		CASE WHEN @SortExpression = 'PersonName asc' THEN PersonName END ASC,
		CASE WHEN @SortExpression = 'PersonName desc' THEN PersonName END DESC,
		CASE WHEN @SortExpression = 'StartDate asc' THEN StartDate  END ASC,
		CASE WHEN @SortExpression = 'StartDate desc' THEN StartDate  END DESC,
		CASE WHEN @SortExpression = 'EndDate asc' THEN EndDate END ASC,
		CASE WHEN @SortExpression = 'EndDate desc' THEN EndDate END DESC,
		CASE WHEN @SortExpression = 'LeaveType asc' THEN LeaveType END ASC,
		CASE WHEN @SortExpression = 'LeaveType desc' THEN LeaveType END DESC,
		CASE WHEN @SortExpression = 'EmployeeComment asc' THEN EmployeeComment END ASC,
		CASE WHEN @SortExpression = 'EmployeeComment desc' THEN EmployeeComment END DESC
	OFFSET 
		@Start ROWS
	FETCH NEXT 
		@Length ROWS ONLY;

	DROP TABLE #TempTable;
END