﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.GetAllEmployeeLimit
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: EXEC [emp].[GetAllEmployeeLimit]
					@PId = 1,
					@Start =  1,
					@Length = 5,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetAllEmployeeLimit]
(
	@PId INT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@TotalRecords INT OUT
)
AS
BEGIN

	SET NOCOUNT ON;
			
	--CREATE TABLE #TempTable(Id INT, ); 
		--INSERT INTO #TempTable
		SELECT  
			EL.Id,
			EmployeeId,
			El.EmpLimitNew ,
			VMSEmp.PersonName ,
			VMSEmp.ReportingUnder ,
			um.EmpDept 
		INTO #TempTable
		FROM emp.EmpLimit EL
	INNER JOIN 
		emp.VMSEmployeeDetails VMSEmp ON EL.EmployeeId = VMSEmp.personID
	INNER JOIN
		emp.UserMaster um ON EL.EmployeeId = um.personID
	WHERE
			EL.PId = @PId
			AND um.[Status] = 1
			AND 
			(
					VMSEmp.PersonName LIKE '%' + ISNULL(@SearchKeyword, VMSEmp.PersonName) +'%'
					OR VMSEmp.ReportingUnder LIKE '%' + ISNULL(@SearchKeyword,VMSEmp.ReportingUnder) +'%'
					OR um.EmpDept  LIKE '%' + ISNULL(@SearchKeyword,um.EmpDept ) +'%'
			);

		SELECT @TotalRecords = COUNT(Id)  FROM #TempTable 

		SELECT
			Id,
			EmployeeId,
			EmpLimitNew ,
			PersonName ,
			ReportingUnder ,
			EmpDept
		FROM 
			#TempTable AS tmp
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN Id END ASC,
			CASE WHEN @SortExpression = 'PersonName asc' THEN PersonName END ASC,
			CASE WHEN @SortExpression = 'PersonName desc' THEN PersonName END DESC,
			CASE WHEN @SortExpression = 'ReportingUnder asc' THEN ReportingUnder  END ASC,
			CASE WHEN @SortExpression = 'ReportingUnder desc' THEN ReportingUnder  END DESC,
			CASE WHEN @SortExpression = 'EmpDept asc' THEN EmpDept END ASC,
			CASE WHEN @SortExpression = 'EmpDept desc' THEN EmpDept END DESC
		--OFFSET 
		--	@Start ROWS
		--FETCH NEXT 
		--	@Length ROWS ONLY;
	DROP TABLE #TempTable;
	END