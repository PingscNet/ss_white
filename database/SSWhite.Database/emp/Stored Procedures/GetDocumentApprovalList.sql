﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[GetDocumentApprovalList] 
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 EXEC [emp].[GetDocumentApprovalList]
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetDocumentApprovalList] 
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		Id,
		PersonName,
		Approvalfordocument 
	FROM 
		emp.VMSEmployeeDetails 
	WHERE 
		Organization ='Office Employee' 
END 