﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.MpcVoteForEpo
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: EXEC [emp].[GetMpcVoteForEpo]
					@UserId = 60,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetMpcVoteForEpo]
(
	@UserId INT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@TotalRecords INT OUT
)
AS
BEGIN

	SET NOCOUNT ON;
	CREATE TABLE #TempTable(Id INT,Vendor varchar(max),CreatedBy nvarchar(max),GrandTotal decimal(18,2),PurposeOfPurchase nvarchar(max), CreatedDate datetime); 

			CREATE TABLE #EpoCreationId(Id INT);
	DECLARE @MPCMember varchar(100),@MPCNumber varchar(100), @EPOVoteIds varchar(max) ;
	SELECT @MPCMember = MPCMember, @MPCNumber = MPCNumber from [emp].[emplimit] WHere UserId = @UserId
	IF (@MPCMember = 'Y')
	BEGIN
		IF(@MPCNumber = 'MPC1') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc1Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC2') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc2Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC3') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc3Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC4') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc4Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC5') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc5Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC6') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc6Vote = 'NO'
		END

		INSERT into #TempTable
		SELECT  
			ec.Id,
			ec.Vendor,
			vms.PersonName AS CreatedBy,
			ec.GrandTotal ,
			ec.PurposeOfPurchase,
			ec.CreatedDate
		--INTO #TempTable
		FROM [emp].[EpoCreation] ec
		INNER JOIN 
		emp.UserMaster um ON um.id = ec.CreatedBy
		INNER JOIN 
		[emp].[VMSEmployeeDetails] vms ON vms.PersonID = um.PersonId
		WHERE
				ec.Id in (select Id from #EpoCreationId)
				AND ec.[Status] = 1
				AND 
				(
						ec.Id LIKE '%' + ISNULL(@SearchKeyword, ec.Id) +'%'
						OR ec.PurposeOfPurchase LIKE '%' + ISNULL(@SearchKeyword,ec.PurposeOfPurchase) +'%'
						OR ec.Vendor LIKE '%' + ISNULL(@SearchKeyword,ec.Vendor) +'%'
						OR vms.PersonName  LIKE '%' + ISNULL(@SearchKeyword,vms.PersonName ) +'%'
						OR ec.GrandTotal  LIKE '%' + ISNULL(@SearchKeyword,ec.GrandTotal ) +'%'
				);
	END
		SELECT @TotalRecords = COUNT(Id)  FROM #TempTable 

		SELECT
			Id,
			Vendor,
			CreatedBy,
			GrandTotal ,
			PurposeOfPurchase,
			CreatedDate
		FROM 
			#TempTable AS tmp
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN Id END desc,
			CASE WHEN @SortExpression = 'Id asc' THEN Id END ASC,
			CASE WHEN @SortExpression = 'Id desc' THEN Id END DESC,
			CASE WHEN @SortExpression = 'Vendor asc' THEN Id END ASC,
			CASE WHEN @SortExpression = 'Vendor desc' THEN Id END DESC,
			CASE WHEN @SortExpression = 'CreatedDate asc' THEN CreatedDate  END ASC,
			CASE WHEN @SortExpression = 'CreatedDate desc' THEN CreatedDate  END DESC,
			CASE WHEN @SortExpression = 'PurposeOfPurchase asc' THEN PurposeOfPurchase END ASC,
			CASE WHEN @SortExpression = 'PurposeOfPurchase desc' THEN PurposeOfPurchase END DESC,
			CASE WHEN @SortExpression = 'CreatedBy asc' THEN  CreatedBy END ASC,
			CASE WHEN @SortExpression = 'CreatedBy desc' THEN CreatedBy END DESC,
			CASE WHEN @SortExpression = 'GrandTotal asc' THEN  GrandTotal END ASC,
			CASE WHEN @SortExpression = 'GrandTotal desc' THEN GrandTotal END DESC
			--CASE WHEN @SortExpression = 'SelfApproval asc' THEN  SelfApproval END ASC,
			--CASE WHEN @SortExpression = 'SelfApproval desc' THEN SelfApproval END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;
	DROP TABLE #TempTable,#EpoCreationId;
	END

	--select * from [emp].[EpoMpcVote]
	
	--update [emp].[EpoMpcVote] set Mpc1Vote = 'Yes' where EpoCreationId = 155
	--155

	----update emp.epocreation set CreatedBy =60  where id =153 

	--select * from emp.epocreation 
	--select * from [emp].[VMSEmployeeDetails]
	--select * from emp.EmpLimit