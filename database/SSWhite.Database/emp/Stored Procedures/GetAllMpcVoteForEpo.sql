﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.[GetAllMpcVoteForEpo]
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: EXEC [emp].[GetAllMpcVoteForEpo]
					@UserId = 60,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [emp].[GetAllMpcVoteForEpo]
(
	@UserId INT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@TotalRecords INT OUT
)
AS
BEGIN

	SET NOCOUNT ON;
			CREATE TABLE #EpoCreationId(Id INT);
	--CREATE TABLE #TempTable(Id INT,CreatedDate datetime,PurposeOfPurchase nvarchar(max),CreatedBy nvarchar(max),GrandTotal decimal(18,2),SelfApproval int); 
	CREATE TABLE #TempTable(Id INT,Vendor varchar(max),CreatedBy nvarchar(max),GrandTotal decimal(18,2),PurposeOfPurchase nvarchar(max), CreatedDate datetime,canApprove bit); 
	DECLARE @MPCMember varchar(100),@MPCNumber varchar(100), @EPOVoteIds varchar(max) ;
	SELECT @MPCMember = MPCMember, @MPCNumber = MPCNumber from [emp].[emplimit] WHere UserId = @UserId
	IF (@MPCMember = 'Y')
	BEGIN
		
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote 
										WHERE (Mpc1Vote = 'NO' OR Mpc2Vote = 'NO' OR Mpc3Vote = 'NO' OR Mpc4Vote = 'NO' OR Mpc5Vote = 'NO' OR Mpc6Vote = 'NO')
		INSERT into #TempTable
		SELECT  
			ec.Id,
			ec.Vendor,
			vms.PersonName AS CreatedBy,
			ec.GrandTotal ,
			ec.PurposeOfPurchase,
			ec.CreatedDate,
			( CASE  WHEN @MPCNumber = 'MPC1' AND Mpc1Vote = 'NO' THEN 1
					WHEN @MPCNumber = 'MPC2' AND Mpc2Vote = 'NO' THEN 1
					WHEN @MPCNumber = 'MPC3' AND Mpc3Vote = 'NO' THEN 1
					WHEN @MPCNumber = 'MPC4' AND Mpc4Vote = 'NO' THEN 1
					WHEN @MPCNumber = 'MPC5' AND Mpc5Vote = 'NO' THEN 1
					WHEN @MPCNumber = 'MPC6' AND Mpc6Vote = 'NO' THEN 1
					ELSE 0 END) as canApprove
		--INTO #TempTable
		FROM [emp].[EpoCreation] ec
		INNER JOIN 
			emp.EpoMpcVote ev ON ev.EpoCreationId = ec.Id
		INNER JOIN 
		emp.UserMaster um ON um.id = ec.CreatedBy
		INNER JOIN 
		[emp].[VMSEmployeeDetails] vms ON vms.PersonID = um.PersonId
		WHERE
				ec.Id in (select Id from #EpoCreationId)
				AND ec.[Status] = 1
				AND 
				(
						ec.Id LIKE '%' + ISNULL(@SearchKeyword, ec.Id) +'%'
						OR ec.PurposeOfPurchase LIKE '%' + ISNULL(@SearchKeyword,ec.PurposeOfPurchase) +'%'
						OR ec.Vendor LIKE '%' + ISNULL(@SearchKeyword,ec.Vendor) +'%'
						OR vms.PersonName  LIKE '%' + ISNULL(@SearchKeyword,vms.PersonName ) +'%'
						OR ec.GrandTotal  LIKE '%' + ISNULL(@SearchKeyword,ec.GrandTotal ) +'%'
				);
	END
		SELECT @TotalRecords = COUNT(Id)  FROM #TempTable 

		SELECT
			Id,
			Vendor,
			CreatedBy,
			GrandTotal ,
			PurposeOfPurchase,
			CreatedDate,
			canApprove
		FROM 
			#TempTable AS tmp
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN Id END DESC,
			CASE WHEN @SortExpression = 'Id asc' THEN Id END ASC,
			CASE WHEN @SortExpression = 'Id desc' THEN Id END DESC,
			CASE WHEN @SortExpression = 'Vendor asc' THEN Vendor END ASC,
			CASE WHEN @SortExpression = 'Vendor desc' THEN Vendor END DESC,
			CASE WHEN @SortExpression = 'CreatedDate asc' THEN CreatedDate  END ASC,
			CASE WHEN @SortExpression = 'CreatedDate desc' THEN CreatedDate  END DESC,
			CASE WHEN @SortExpression = 'PurposeOfPurchase asc' THEN PurposeOfPurchase END ASC,
			CASE WHEN @SortExpression = 'PurposeOfPurchase desc' THEN PurposeOfPurchase END DESC,
			CASE WHEN @SortExpression = 'CreatedBy asc' THEN  CreatedBy END ASC,
			CASE WHEN @SortExpression = 'CreatedBy desc' THEN CreatedBy END DESC,
			CASE WHEN @SortExpression = 'GrandTotal asc' THEN  GrandTotal END ASC,
			CASE WHEN @SortExpression = 'GrandTotal desc' THEN GrandTotal END DESC
			--CASE WHEN @SortExpression = 'SelfApproval asc' THEN  SelfApproval END ASC,
			--CASE WHEN @SortExpression = 'SelfApproval desc' THEN SelfApproval END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;
	DROP TABLE #TempTable,#EpoCreationId;
	END

	--select * from [emp].[EpoMpcVote]

	----update emp.epocreation set CreatedBy =60  where id =153 

	--select * from emp.epocreation 
	--select * from [emp].[VMSEmployeeDetails]
	--select * from emp.EmpLimit