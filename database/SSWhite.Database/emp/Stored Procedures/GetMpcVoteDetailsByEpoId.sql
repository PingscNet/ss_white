﻿USE [db_a5dc4b_sswhite]
GO
/****** Object:  StoredProcedure [emp].[GetMpcVOteDetailsByEpoId]    Script Date: 6/3/2022 5:51:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC  [emp].[GetEpoDetailsByEpoId]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 TEst Execution :	 [emp].[GetMpcVoteDetailsByEpoId] 194
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE  [emp].[GetMpcVOteDetailsByEpoId]
(
	@EpoId int
)
AS
BEGIN
	SET NOCOUNT ON;
	CREATE TABLE #TempTable (PersonName VARCHAR(500), MpcNumber VARCHAR(500), MpcApproval INT,MpcVoteDate DATETIME);

	
	INSERT INTO
		#TempTable 
	SELECT 
		PersonName,
		MpcNumber,
		NULL,
		NULL
	FROM 
		emp.VMSEmployeeDetails vms INNER JOIN emp.EmpLimit el ON vms.userID = el.userID 
	WHERE MPCMember = 'Y'
	
	DECLARE @Mpc1Approval INT, @Mpc2Approval INT, @Mpc3Approval INT, @Mpc4Approval INT, @Mpc5Approval INT, @Mpc6Approval INT,
			@Mpc1VoteDate DATETIME, @Mpc2VoteDate DATETIME, @Mpc3VoteDate DATETIME, @Mpc4VoteDate DATETIME,
			@Mpc5VoteDate DATETIME, @Mpc6VoteDate DATETIME;
	
	SELECT 
		@Mpc1Approval = Mpc1Approval, 
		@Mpc2Approval = Mpc2Approval, 
		@Mpc3Approval = Mpc3Approval, 
		@Mpc4Approval = Mpc4Approval, 
		@Mpc5Approval = Mpc5Approval, 
		@Mpc6Approval = Mpc6Approval,
		@Mpc1VoteDate = Mpc1VoteDate, 
		@Mpc2VoteDate = Mpc2VoteDate, 
		@Mpc3VoteDate = Mpc3VoteDate, 
		@Mpc4VoteDate = Mpc4VoteDate, 
		@Mpc5VoteDate = Mpc5VoteDate, 
		@Mpc6VoteDate = Mpc6VoteDate
	FROM 
		emp.EpoMpcVote 
	WHERE
		EpoCreationId = @EpoId
	
	UPDATE #TempTable SET MpcApproval = @Mpc1Approval, MpcVoteDate = @Mpc1VoteDate  WHERE MpcNumber = 'MPC1';
	UPDATE #TempTable SET MpcApproval = @Mpc2Approval, MpcVoteDate = @Mpc2VoteDate WHERE MpcNumber = 'MPC2';
	UPDATE #TempTable SET MpcApproval = @Mpc3Approval, MpcVoteDate = @Mpc3VoteDate WHERE MpcNumber = 'MPC3';
	UPDATE #TempTable SET MpcApproval = @Mpc4Approval, MpcVoteDate = @Mpc4VoteDate WHERE MpcNumber = 'MPC4';
	UPDATE #TempTable SET MpcApproval = @Mpc5Approval, MpcVoteDate = @Mpc5VoteDate WHERE MpcNumber = 'MPC5';
	UPDATE #TempTable SET MpcApproval = @Mpc6Approval, MpcVoteDate = @Mpc6VoteDate WHERE MpcNumber = 'MPC6';
	
	SELECT 
		PersonName,
		MpcNumber,
		MpcApproval,
		MpcVoteDate
	FROM 
		#TempTable
																			 
	DROP TABLE #TempTable 
END 
