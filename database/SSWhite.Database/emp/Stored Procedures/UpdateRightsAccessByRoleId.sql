﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.GetAllEpos
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: DECLARE  @RoleRightsTypeList AS emp.RoleRightsType;
					INSERT INTO  @RoleRightsTypeList (UniqueCode, IsAccess)
					VALUES('Admin_AddEmployee',0),
					('Admin_EmployeeLimit',1),
					('Admin_ApprovalDocumentPerson',1),
					('Dashboard_Dashboard',1),
					('T&A_In/OutforLunch',1),
					('T&A_In/OutforBusiness',1),
					('T&A_In/OutforPersonalBusiness',1),
					('T&A_MyMissingPunches',1),
					('T&A_MyTimePunchRecords',1),
					('T&A_WhoisIntheBuilding',1),
					('EPO_CreateNewEPO',1),
					('EPO_EditEPO',1),
					('EPO_MPCEPOViewer',1),
					('EPO_MPCVoteForEPO',1),
					('EPO_MyEPOs',1),
					('EPO_ViewMySubordinatesEPO',1),
					('EPO_ReviewEPO',1),
					('EPO_MyApprovalForEPO',1),
					('FTO_SubmitNewFTORequest',1),
					('FTO_ApproveFTO',1),
					('FTO_FTOHRApproval',1),
					('FTO_ShowAllFTOOfMyDept',1),
					('FTO_ReviewYTDFTOStatus',1),
					('FTO_ShowAllFutureFTOOfMyDept.',1),
					('Documents_AllDocuments',1),
					('Documents_CreateNewECN',1),
					('Documents_CreateNewFCD',1),
					('Documents_CreateNewLIT',1),
					('Documents_CreateNewSOP',1),
					('Documents_CreateNewWI',1),
					('Documents_DocumentawaitingmyApproval',1),
					('Roles_Roles&Permissions',1),
					('FTO_FTOApproval',1);
					EXEC [emp].[UpdateRightsAccessByRoleId]
					@RoleId = 1,
					@RoleRightsTypeList = @RoleRightsTypeList
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [emp].[UpdateRightsAccessByRoleId]
(
	@RoleId INT,
	@RoleRightsTypeList emp.RoleRightsType READONLY

)
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE m
	SET 
		m.IsAccess = r.IsAccess
	FROM 
		dbo.Modules_PagesAccess m
	INNER JOIN 
		@RoleRightsTypeList r ON r.UniqueCode = m.UniqueCode
	WHERE 
		 m.RoleId = @RoleId
END

