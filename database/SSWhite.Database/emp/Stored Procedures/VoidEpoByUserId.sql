﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC  [emp].[VoidEpoByUserId]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 TEst Execution :	[emp].[VoidEpoByUserId] 140, 42,3
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE  [emp].[VoidEpoByUserId]
(
	@EpoId int,
	@UserId int,
	@StatusTypeDeleted int
)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE
		[emp].[EpoCreation]
	SET status = @StatusTypeDeleted
	WHERE 
		 CreatedBy = @UserId  and Id = @EpoId;
END 

