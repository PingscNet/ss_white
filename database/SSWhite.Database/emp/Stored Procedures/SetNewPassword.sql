﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[CheckIfEmployeeIdExists]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution : EXEC [emp].[CheckIfEmailIdExists] '101'
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [emp].[CheckIfEmailAddressExists]
(
	@EmailAddress nvarchar(100)
)
AS
BEGIN
	SET NOCOUNT ON;
	Declare @EmailExists as varchar(100);

	IF EXISTS(SELECT EmailAddress from emp.UserMaster Where EmailAddress = @EmailAddress)
    BEGIN
        SET  @EmailExists = 'EmailAddress Already Exists';
	END;

	select @EmailExists as [Message];
End;
