﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC  [emp].[GetEpoDetailsByEpoId]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 TEst Execution :	EXEC [emp].[GetEpoDetailsByEpoId] 176,42,1
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE  [emp].[GetEpoDetailsByEpoId]
(
	@EpoNo INT ,
	@UserId INT,
	@ApprovalTypeApproved INT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		 ec.Id
		,ec.EmployeeVendor
		,ec.EmployeeId
		,ec.Vendor
		,ec.VendorId
		,ec.Street1
		,ec.Street2
		,ec.Country
		,ec.State
		,ec.City
		,ec.Zip
		,ec.Phone
		,ec.Fax
		,ec.GstNo
		,ec.Email
		,ec.PurposeOfPurchase
		,ec.EpoType
		,ec.CurrencyType
		,ec.ReceiptMethod
		,ec.EpoPurchaseStatus
		,ec.PaymentType
		,ec.PurchaseNotes
		,ec.InternalNotes
		,ec.SubTotal
		,ec.GrandTotal
		,ec.DateOfPaymentCheckRequestOrCashAdvance
		,ec.DaysOfPaymentPayWhenBilled
		,ec.DollarPrice
		,ec.TotalIgst
		,ec.TotalCgst
		,ec.TotalSgst
		,ec.IsWitinGujarat
		,ec.SelfApproval
		,ec.ManagerApproval
		,ec.MPCApproval
		,ec.MPCHigherApproval	
		,el.EmpLimitNew
		,vms.PersonName
		,el.createdDate
	FROM 
		[emp].[EpoCreation] ec
	INNER JOIN emp.EmpLimit el ON el.userId = ec.createdby
	INNER JOIN emp.VMSEmployeeDetails vms ON vms.userId = ec.createdby
	WHERE 
		ec.Id = @EpoNo
		--AND CreatedBy = @UserId;

	SELECT
		Id
		,[lineNo]
		,ProductDescription
		,ProductGroup
		,Qty
		,UnitPrice
		,Uom
		,Department
		,AccountNo
		,IsWitinGujarat
		,Tax
		,Discount
		,UnitPriceQty
		,QtyUnitPriceTax
		,TotalCostDiscount
		,Gst
		,Cgst
		,Sgst
		,Igst
		,ExtendedCost
		,Purchase
		,DueDate
		,CgstValue
		,SgstValue
		,TaxValue
		,IgstValue
		,CurrencyType
	FROM 
		[emp].[EpoLineItems]
	WHERE 
		EpoNo = @EpoNo
		--AND pid = @UserId;

	SELECT
		Id
		,AttachedFileName
		,AttachmentDate
	FROM 
		emp.EpoAttachmentMst
	WHERE 
		EpoNo = @EpoNo  ;
		--AND UserId = @UserId;

	DECLARE @canEdit BIT =0 , @MPCMember VARCHAR(100), @IsHigherAuthorityMember VARCHAR(100), 
			@DepartmentName VARCHAR(100) ;

	SELECT 
		@MPCMember = MPCMember,
		@IsHigherAuthorityMember = IsHigherAuthorityMember 
	FROM 
		emp.emplimit 
	WHERE 
		userID = @UserId
	
	SELECT 
		@DepartmentName = DepartmentName 
	FROM 
		emp.vmsemployeedetails v INNER JOIN  emp.Department d ON v.PersonName = d.DepartmentSupervisorName
	WHERE userid = @UserId

	SELECT 
		@MPCMember = MPCMember,
		@IsHigherAuthorityMember = IsHigherAuthorityMember 
	FROM 
		emp.emplimit 
	WHERE 
		userID = @UserId

	IF EXISTS (SELECT Id FROM emp.EpoCreation WHERE Id = @EpoNo) 
	BEGIN
		--IF(@MPCMember = 'Y' OR @IsHigherAuthorityMember = 'Y')
		--BEGIN
		--	SET @canEdit = 1;
		--END
		IF EXISTS(SELECT Id FROM emp.EpoCreation WHERE CreatedBy = @UserId and Id =@EpoNo)
		BEGIN
			SET @canEdit = 1;
		END
		--ELSE IF EXISTS(SELECT Id FROM emp.EpoCreation WHERE CreatedBy = @UserId)
		--BEGIN
		--	SET @canEdit = 1;
		--END
		--ELSE IF (@DepartmentName IS NOT NULL)
		--BEGIN
		--IF EXISTS(SELECT ec.id FROM emp.EpoCreation ec INNER JOIN emp.VMSEmployeeDetails vms ON vms.userID = ec.CreatedBy WHERE 
		--	ec.Id = @EpoNo AND vms.Department = @DepartmentName)
		--	SET @canEdit = 1;
		--END
		
		DECLARE @SelfApproval INT, @ManagerApproval INT, @MPCApproval INT, @MPCHigherApproval INT, @ApprovedBY VARCHAR(100);
		SELECT
			@SelfApproval = SelfApproval,@ManagerApproval = ManagerApproval,
			@MPCApproval = MPCApproval, @MPCHigherApproval = MPCHigherApproval
		FROM 
			emp.EpoCreation 
		WHERE 
			Id = @EpoNo
			
			--for approval person name
		IF(@SelfApproval = @ApprovalTypeApproved AND @ManagerApproval IS NULL AND @MPCApproval IS NULL AND @MPCHigherApproval IS NULL)
		BEGIN
			SELECT @ApprovedBY = PersonName FROM emp.VMSEmployeeDetails WHERE UserId = @UserId 
		END
		ELSE IF(@SelfApproval = @ApprovalTypeApproved AND @ManagerApproval = @ApprovalTypeApproved AND @MPCApproval IS NULL AND @MPCHigherApproval IS NULL)
		BEGIN
			SELECT @ApprovedBY = PersonName FROM 
				emp.VMSEmployeeDetails vms INNER JOIN emp.EpoApprovalList el ON el.UserId = vms.userID 
				WHERE el.EpoNo = @EpoNo ORDER BY el.id DESC
		END
		ELSE IF(@SelfApproval = @ApprovalTypeApproved AND @ManagerApproval = @ApprovalTypeApproved AND @MPCApproval = @ApprovalTypeApproved AND @MPCHigherApproval IS NULL)
		BEGIN
			SELECT @ApprovedBY = 'MPC Approved'
		END
		ELSE IF(@SelfApproval = @ApprovalTypeApproved AND @ManagerApproval = @ApprovalTypeApproved AND @MPCApproval = @ApprovalTypeApproved AND @MPCHigherApproval = @ApprovalTypeApproved)
		BEGIN
			SELECT @ApprovedBY = 'Higher MPC Approved'
		END
	END
	ELSE
	BEGIN
		SET @canEdit = NULL;
	END

	SELECT @canEdit AS CanEdit
	SELECT @ApprovedBY AS ApprovedBY
END 