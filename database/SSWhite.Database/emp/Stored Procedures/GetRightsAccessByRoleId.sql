﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.GetAllEpos
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: EXEC [emp].[GetRightsAccessByRoleId]
					@RoleId = 2
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetRightsAccessByRoleId]
(
	@RoleId INT
)
AS
BEGIN

	SET NOCOUNT ON;
	SELECT 
		UniqueCode,
		IsAccess	
	FROM 
		dbo.Modules_PagesAccess
	WHERE 
		RoleId = @RoleId
END
