﻿/*------------------------------------------------------------------------------------------------------------
Name			: [document].[GetAllDocuments] 
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	--HR userid =68
					[emp].[UpdateWorkingStatus] 
					@UserId = 42,
					@Status = 1,
					@StatusTypeActive = 1,
					@StatusTypeInActive = 0
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [emp].[UpdateWorkingStatus] 
(
	@UserId INT,
	@Status INT,
	@StatusTypeActive INT,
	@StatusTypeInActive INT
)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE 
		[emp].[VMSEmployeeDetails]
	SET 
		WorkingStatus = @Status
	WHERE 
		UserId = @UserId

	SELECT 
		WorkingStatus
	FROM
		[emp].[VMSEmployeeDetails]
	WHERE 
		UserId = @UserId
 END

	