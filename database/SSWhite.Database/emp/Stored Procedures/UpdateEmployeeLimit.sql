﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[UpdateEmployeeLimit]
Comments	: 26-04-2022 | Nirali Chitroda | This procedure is used to update Employee Limit details.
USE [db_a5dc4b_sswhite]
 EXEC [emp].[UpdateEmployeeLimit] 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[UpdateEmployeeLimit] 
(
	@Id INT,
	@EmpLimitNew DECIMAL(18, 2)
)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE
		emp.EmpLimit
	SET
		EmpLimitNew = @EmpLimitNew
	WHERE 
		Id = @Id
END