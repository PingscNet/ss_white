﻿/*------------------------------------------------------------------------------------------------------------
Name			: [emp].[GetDepartmentFtoByUserId]
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	: [emp].[GetDepartmentFtoByUserId] 
					@UserId = 42,
					@StartDate = '2022-05-29',
					@EndDate = '2022-05-29',
					@StatusTypeActive = 1
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [emp].[GetDepartmentFtoByUserId]
(
	@UserId INT,
	@StartDate Date,
	@EndDate Date,
	@StatusTypeActive INT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT  
		ef.Id,
		vms.PersonName,
		ef.StartDate,
		ef.EndDate
	FROM [emp].[AttendanceFto] ef
	INNER JOIN 
		[emp].[VMSEmployeeDetails] vms ON vms.UserId = ef.CreatedBY
	WHERE
		ef.[Status] = @StatusTypeActive
		AND ef.CreatedBy  != @UserId
		AND vms.Department = (SELECT  [Department] FROM emp.[VMSEmployeeDetails] WHERE UserId = @UserId)
		AND (
				(ef.StartDate BETWEEN @StartDate AND @EndDate)
				OR (ef.EndDate BETWEEN @StartDate AND @EndDate)
			)
END