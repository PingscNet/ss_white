﻿USE [db_a5dc4b_sswhite]
GO
/****** Object:  StoredProcedure [emp].[UserNotification]    Script Date: 6/16/2022 3:06:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC  [emp].[GetEpoDetailsByEpoId]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 TEst Execution :	[emp].[UserNotification] 42, 1, '2022-05-31',1
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE  [emp].[UserNotification]
(
	@UserId INT,
	@StatusTypeActive INT,
	@CurrentDate DATETIME,
	@ApprovalTypeApproved INT
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
		Id AS EpoId
		,IsAttachmentPending
	FROM 
		[emp].[EpoCreation]
	WHERE 
		 CreatedBy = @UserId AND IsAttachmentPending=1 and Status =1;

	SELECT 
		a.Id,
		a.StartDate,
		a.EndDate,
		a.NumberOfDays,
		a.SupervisorApproval,
		a.HRApproval,
		vm.PersonName
	FROM 
		[emp].[AttendanceFto] a
		INNER JOIN emp.VMSEmployeeDetails  vm ON a.createdBy = vm.userId
	WHERE
		CreatedBy = @UserId 
		AND [Status] = @StatusTypeActive 
		AND StartDate > @CurrentDate
		AND (SupervisorApproval = @ApprovalTypeApproved OR HRApproval = @ApprovalTypeApproved)
END 
