﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[AddNewEmployee]
Comments	: 26-04-2022 | Kartik Bariya | This procedure is used to Add New Employee.

Test Execution : EXEC [emp].[AddNewEmployee]
					 @PersonID = 'roles1'
					,@Organization= 'Employee'
					,@FirstName= 'rakesh'
					,@LastName= 'Goel'
					,@Gender = 1
					,@DOB  = '2022-04-26 13:34:25.903'
					,@DOJ  = '2022-04-26 13:34:25.903'
					,@DOI  = '2022-04-26 13:34:25.903'
					,@Address1 = 'addreess1'
					,@Address2 = 'addess'
					,@BloodGroup = '0-'
					,@EmailAddress= 'HR@gmail.com'
					,@EmpPosition= 'HR'
					,@ReportingUnder= 'Kazumi Parikh'
					,@Department= 'HR'
					,@Signature = 'signaturePath'
					,@Picture= 'picturePath'
					,@Password  = '6G94qKPK8LYNjnTllCqm2G3BUM08AzOK7yW30tfjrMc='
					,@CurrentDate ='2022-04-26 13:34:25.903'
					,@PhoneNo = '9991113332'
					,@CreatedBy = 1
					,@StatusActive =1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [emp].[AddNewEmployee]
(
	@PersonID nvarchar(50),
	@Organization nvarchar(500),
	@FirstName nvarchar(500),
	@LastName nvarchar(500),
	@Gender nvarchar(50),
	@DOB date,
	@DOJ date,
	@DOI date,
	@Address1 nvarchar(50),
	@Address2 nvarchar(50),
	@BloodGroup nvarchar(50),
	@EmailAddress nvarchar(500),
	@EmpPosition nvarchar(500),
	@ReportingUnder nvarchar(500),
	@Department nvarchar(500),
	@Signature nvarchar(200),
	@Picture nvarchar(500),
	@Password  nvarchar(150),
	@CurrentDate datetime,
	@PhoneNo nvarchar(50),
	@CreatedBy int,
	@StatusActive smallint
)
AS
BEGIN
	SET NOCOUNT ON;

	
	DECLARE @IsSignature AS int,  @InsertedId int, @LoginIdTemp VARCHAR(250) ;

	IF(@Signature is NULL)
		set @IsSignature = 0;
	ELSE
		set @IsSignature = 1;
	
	BEGIN TRY
	Begin tran

	INSERT INTO [emp].[UserMaster]
           ([TenentId]
           ,[LocationId]
           ,[CRUPId]
           ,[FirstName]
           ,[LastName]
           ,[LoginId]
           ,[Password]
           ,[UserType]
           ,[Remarks]
           ,[ActiveFlag]
           ,[LastLoginDt]
           --,[UserDetailId]
           ,[AccLock]
           ,[FirstTime]
           ,[PasswordChng]
           --,[ThemeName]
           --,[ApprovalDt]
           --,[VerificationCd]
           ,[EmailAddress]
           ,[TillDt]
           ,[IsSignature]
           ,[SignatureImage]
           ,[Avtar]
           ,[CompId]
           --,[EmpId]
           --,[CheckInSwitch]
           ,[LoginActive]
           ,[ActiveUser]
           ,[UserDate]
           ,[Language1]
           ,[Language2]
           ,[Language3]
           ,[DateofBirth]
           ,[EmployeePosition]
           ,[Salary]
           ,[DateOfJoining]
           ,[PhoneNo]
           ,[PinCode]
           ,[Address1]
           ,[Address2]
           ,[Img]
           ,[PersonId]
           --,[LibraryMenu]
           --,[LibraryReader]
           --,[LibraryMenuDept
		    ,CreatedBy
			,CreatedDate 
			,Status 
			,EmpDept
			,RoleId
		   )
     VALUES
           (
		   10
           ,1
           ,0
           ,@FirstName
           ,@LastName
           ,CONCAT(trim(@FirstName),'.', trim(@LastName))
           ,@Password
           ,3
           ,'SSWHITE'
           ,'Y'
           ,@CurrentDate
           --,<UserDetailId, int,>
           ,'Y'
           ,'Y'
           ,'SSWHITE'
           --,<ThemeName, nvarchar(50),>
           --,<ApprovalDt, datetime,>
           --,<VerificationCd, nvarchar(40),>
           ,@EmailAddress
           ,'2025-12-04 00:00:00.000'
           ,@IsSignature  
           ,@Signature
           ,123
           ,12
           --,<EmpId, int,>
           --,<CheckInSwitch, bit,>
            ,1
           ,1
           ,'2025-12-31'
           ,''
           ,'0'
           ,'1'
           ,@DOB
           ,@EmpPosition
           ,''
           ,@DOJ
           ,@PhoneNo
           ,'363030'
           ,@Address1
           ,@Address2
           ,@Picture
           ,@PersonId
           --,<LibraryMenu, nvarchar(50),>
           --,<LibraryReader, nvarchar(max),>
           --,<LibraryMenuDept, nvarchar(500),>
		   ,@CreatedBy
		   ,@CurrentDate
		   ,@StatusActive
		   ,@Department
		   ,4
		   )
		
	SET @InsertedId = SCOPE_IDENTITY();
	SELECT @LoginIdTemp = LoginId FROM [emp].[UserMaster] WHERE Id = @InsertedId;
	
	IF Exists(Select 1 FROM [emp].[UserMaster] WHERE LoginId = @LoginIdTemp and id not in (@InsertedId))
	BEGIN
	--print @loginIdTemp;
		UPDATE [emp].[UserMaster]  SET LoginId = CONCAT(trim(LoginId),@InsertedId) 
		WHERE Id = @InsertedId;
	END

	

	INSERT INTO [emp].[VMSEmployeeDetails]
           ([PersonID]
           ,[Organization]
           ,[PersonName]
           ,[Gender]
           ,[EffectiveTime]
           ,[ExpiryTime]
           ,[CardNo]
           ,[RoomNo]
           ,[FloorNo]
           ,[Picture]
           ,[Add1]
           ,[Add2]
           ,[DOB]
           ,[DOJ]
           ,[DOI]
           ,[BloodGroup]
           ,[signature]
           ,[EmailAddress]
           ,[EmpPosition]
           ,[ReportingUnder]
		   ,ApprovalForDocument
		   ,UserId
		   ,Department
           )
     VALUES
           (
		   @PersonID,
		   @Organization,
		   CONCAT(trim(@FirstName),' ',trim(@LastName)),
		   @Gender,
		   '2000-01-01 00:00:00.000',
		   '2037-12-31 23:59:59.000',
		   '',
		   '',
		   '',
		   @Picture,
		   @Address1,
		   @Address2,
		   @DOB,
		   @DOJ,
		   @DOI,
		   @BloodGroup,
		   @signature,
		   @EmailAddress,
		   @EmpPosition,
		   @ReportingUnder,
		   0,
		   @InsertedId,
		   @Department
		   )

		   

	 INSERT INTO [emp].[EmpLimit]
           ([PId]
           ,[EmployeeId]
           ,[EmpLimitNew]
           ,[PreviousEmpLimit_no]
           --,[PreviousEmpLimit1]
           --,[PreviousEmpLimit2]
           --,[PreviousEmpLimit3]
           --,[PreviousEmpLimit4]
           --,[PreviousEmpLimit5]
           --,[RevisedDate1]
           --,[RevisedDate2]
           --,[RevisedDate3]
           --,[RevisedDate4]
           --,[RevisedDate5]
           ,[MPCMember]
           --,[MPCNumber]
           ,[CreatedBy]
           ,[CreatedDate]
           --,[UpdatedBy]
           --,[UpdatedDate]
           --,[IsDeleted]
		   ,UserId
		   )
     VALUES
           (1
           ,@PersonID
           ,0
           ,1
           --,<PreviousEmpLimit1, decimal(18,2),>
           --,<PreviousEmpLimit2, decimal(18,2),>
           --,<PreviousEmpLimit3, decimal(18,2),>
           --,<PreviousEmpLimit4, decimal(18,2),>
           --,<PreviousEmpLimit5, decimal(18,2),>
           --,<RevisedDate1, datetime,>
           --,<RevisedDate2, datetime,>
           --,<RevisedDate3, datetime,>
           --,<RevisedDate4, datetime,>
           --,<RevisedDate5, datetime,>
           ,'N'
           --,<MPCNumber, nvarchar(50),>
           ,@CreatedBy
           ,@CurrentDate
		    ,@InsertedId)
		   commit
	SELECT
		Loginid,
		EmailAddress
	FROM 
		[emp].[UserMaster]
	WHERE 
		Id = @InsertedId
		   END TRY
	BEGIN CATCH
		Rollback
		DECLARE
		@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
End;
