﻿CREATE PROCEDURE [emp].[InsertPunchDetails]
(
	@EmployeeId BIGINT,
	@PunchDateTime DATETIME,
	@InOutStatus TINYINT,
	@PunchType TINYINT,
	@CreatedBy INT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO 
		emp.VMSDataFromDashboard
			(EmployeeId,PunchDateTime,InOutStatus,PunchType,CreatedBy,CreatedDate)
	VALUES
		(@EmployeeId,@PunchDateTime,@InOutStatus,@PunchType,@CreatedBy,@PunchDateTime)

END 