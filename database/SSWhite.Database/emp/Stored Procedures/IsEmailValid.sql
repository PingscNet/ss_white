﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.GetAllEpos
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: EXEC [emp].[IsEmailValid]
					@EmailAddress = "sigma@mail.com",
					@StatusTypeActive = 1
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [emp].[IsEmailValid]
(
	@EmailAddress VARCHAR(100),
	@StatusTypeActive SMALLINT
)
AS
BEGIN

	SET NOCOUNT ON;
			
	Declare @EmailExists as varchar(100) = 'Invalid';

	IF EXISTS(SELECT EmailAddress from emp.UserMaster Where EmailAddress = @EmailAddress AND Status = @StatusTypeActive)
    BEGIN
        SET  @EmailExists = 'Valid';
	END;

	select @EmailExists as [Message];
END
