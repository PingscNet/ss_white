﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.MpcVoteForEpo
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: EXEC [emp].[GetAllEpos]
					@UserId = null,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[MpcVoteForEpo]
(
	@UserId INT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@TotalRecords INT OUT
)
AS
BEGIN

	SET NOCOUNT ON;
			
	--CREATE TABLE #TempTable(Id INT,CreatedDate datetime,PurposeOfPurchase nvarchar(max),CreatedBy nvarchar(max),GrandTotal decimal(18,2),SelfApproval int); 
	
	SELECT  
		ec.Id,
		ec.Vendor,
		vms.PersonName AS CreatedBy,
		ec.GrandTotal ,
		ec.PurposeOfPurchase,
		ec.CreatedDate
	INTO #TempTable
	FROM [emp].[EpoCreation] ec
	INNER JOIN 
		[emp].[VMSEmployeeDetails] vms ON vms.UserId = ec.createdby
	INNER JOIN 
		[emp].[emplimit] el ON vms.UserId = el.UserId 
	WHERE
			el.MPCMember = 'Y'
			AND ec.[Status] = 1
			AND 
			(
					ec.Id LIKE '%' + ISNULL(@SearchKeyword, ec.Id) +'%'
					OR ec.PurposeOfPurchase LIKE '%' + ISNULL(@SearchKeyword,ec.PurposeOfPurchase) +'%'
					OR vms.PersonName  LIKE '%' + ISNULL(@SearchKeyword,vms.PersonName ) +'%'
					OR ec.GrandTotal  LIKE '%' + ISNULL(@SearchKeyword,ec.GrandTotal ) +'%'
			);

		SELECT @TotalRecords = COUNT(Id)  FROM #TempTable 

		SELECT
			Id,
			Vendor,
			CreatedBy,
			GrandTotal ,
			PurposeOfPurchase,
			CreatedDate
		FROM 
			#TempTable AS tmp
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN Id END ASC,
			CASE WHEN @SortExpression = 'Id asc' THEN Id END ASC,
			CASE WHEN @SortExpression = 'Id desc' THEN Id END DESC,
			CASE WHEN @SortExpression = 'Vendor asc' THEN Id END ASC,
			CASE WHEN @SortExpression = 'Vendor desc' THEN Id END DESC,
			CASE WHEN @SortExpression = 'CreatedDate asc' THEN CreatedDate  END ASC,
			CASE WHEN @SortExpression = 'CreatedDate desc' THEN CreatedDate  END DESC,
			CASE WHEN @SortExpression = 'PurposeOfPurchase asc' THEN PurposeOfPurchase END ASC,
			CASE WHEN @SortExpression = 'PurposeOfPurchase desc' THEN PurposeOfPurchase END DESC,
			CASE WHEN @SortExpression = 'CreatedBy asc' THEN  CreatedBy END ASC,
			CASE WHEN @SortExpression = 'CreatedBy desc' THEN CreatedBy END DESC,
			CASE WHEN @SortExpression = 'GrandTotal asc' THEN  GrandTotal END ASC,
			CASE WHEN @SortExpression = 'GrandTotal desc' THEN GrandTotal END DESC
			--CASE WHEN @SortExpression = 'SelfApproval asc' THEN  SelfApproval END ASC,
			--CASE WHEN @SortExpression = 'SelfApproval desc' THEN SelfApproval END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;
	DROP TABLE #TempTable;
	END

	--update emp.epocreation set CreatedBy =60  where id =153 

	--select * from emp.epocreation 