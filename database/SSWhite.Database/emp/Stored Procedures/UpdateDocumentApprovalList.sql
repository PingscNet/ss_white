﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[UpdateDocumentApprovalList] 
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
Test Execution	:  DECLARE @UpdateDocumentApprovalList  as  [emp].[DocumentApprovalType] ;
					INSERT INTO @UpdateDocumentApprovalList (id, Approvalfordocument)
					VALUES (18,1),(19,1);
					EXEC [emp].[UpdateDocumentApprovalList] 
						 @UpdateDocumentApprovalList  = @UpdateDocumentApprovalList
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[UpdateDocumentApprovalList] 
(
	@UpdateDocumentApprovalList [emp].[DocumentApprovalType] READONLY
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		udc.Id,
		udc.Approvalfordocument
	INTO
		#TempTable
	FROM
		@UpdateDocumentApprovalList AS udc;
		
	UPDATE ved    
		SET ved.Approvalfordocument = tt.Approvalfordocument   
	FROM emp.VMSEmployeeDetails  ved
	  INNER JOIN #TempTable tt
	ON ved.Id = tt.Id;

	DROP TABLE #TempTable
END 



