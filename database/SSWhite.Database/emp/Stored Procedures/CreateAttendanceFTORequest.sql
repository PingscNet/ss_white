﻿  
/*------------------------------------------------------------------------------------------------------------    
Procedure : [emp].[CreateAttendanceFTO]    
Comments : 23-05-2022 | Amit| This procedure is used to Add New Attendance FTO Request.    
    
Test Execution :	declare @CUrrentdate1 datetime =getdate()
					--select @CUrrentdate1
					EXEC   [emp].[CreateAttendanceFTORequest]   
							@UserID =42,
							@EmployeeID =42,
							@EmployeeName ='Shivam Dubey' ,
							@StartDate ='2022-05-29' ,
							@StartDateDayType = 1 ,
							@EndDate ='2022-05-30 ' ,
							@EndDateDayType  =1 ,
							@LeaveTypeID =1 ,
							@OtherLeaveTypeId =null,
							@EmployeeComment =null,
							@ApprovalTypePending =2 ,
							@StatusTypeActive =2,
							@CurrentDate =@CUrrentdate1
--------------------------------------------------------------------------------------------------------------*/    
CREATE PROCEDURE [emp].[CreateAttendanceFTORequest]    
(    
@UserID int,
@EmployeeID nvarchar(50),
@EmployeeName nvarchar(500) ,
@StartDate date ,
@StartDateDayType  tinyint ,
@EndDate date  ,
@EndDateDayType  tinyint ,
@LeaveTypeID int ,
@OtherLeaveTypeId int,
@EmployeeComment nvarchar (max) ,
@ApprovalTypePending nvarchar (max) ,
@StatusTypeActive int,
@CurrentDate datetime
)    
AS    
BEGIN    
 SET NOCOUNT ON;    
    
     
 BEGIN TRY    
  BEGIN    
  Begin tran    
   Declare @ReportingUnder varchar(max),@SupervisorId int,@InsertedId int,@HRUserId int= 68, @EmailIdsForFtoNotification varchar(max),
   @EmailIdForFtoApproval varchar(max);
   Select @ReportingUnder = ReportingUnder from emp.VMSEmployeeDetails Where UserId = @UserId
   Select @SupervisorId = userID from emp.VMSEmployeeDetails Where PersonName = @ReportingUnder
  INSERT INTO [emp].[CreateAttendanceFTO]    
           ( 
			UserID
			,EmployeeID
			,EmployeeName
			,StartDate
			,StartDateDayType
			,EndDate
			,EndDateDayType
			,LeaveTypeID
			,OtherLeaveTypeId
			,EmployeeComment
			,SupervisorName
			,SupervisorApproval
			,HRApproval
			,MPCApproval
			,Approval
			,[Status]
			,CreatedBy
			,CreatedDate
           )    
       VALUES    
           ( 
			@UserID
			,@EmployeeID
			,@EmployeeName
			,@StartDate
			,@StartDateDayType
			,@EndDate
			,@EndDateDayType
			,@LeaveTypeID
			,@OtherLeaveTypeId
			,@EmployeeComment
			,@ReportingUnder
			,@ApprovalTypePending
			,@ApprovalTypePending
			,@ApprovalTypePending
			,@ApprovalTypePending
			,@StatusTypeActive
			,@UserID
			,@CurrentDate
		)    

		SET @InsertedId = SCOPE_IDENTITY();
		SELECT @EmailIdsForFtoNotification = STRING_AGG(EmailAddress,',') from emp.UserMaster where id in (@UserID,@SupervisorId,@HRUserId) ;
		SELECT @EmailIdForFtoApproval = EmailAddress from emp.UserMaster where id in (@UserID) ;
		Select @InsertedId as FtoId, @EmailIdsForFtoNotification AS EmailIdsForFtoNotification, @EmailIdForFtoApproval AS EmailIdForFtoApproval ;

   commit
  END    
  END TRY    
 BEGIN CATCH    
  Rollback    
  DECLARE    
  @stringmsg VARCHAR(MAX),    
   @ErrorNumber VARCHAR(MAX),    
   @ErrorSeverity VARCHAR(MAX),    
   @ErrorState VARCHAR(MAX),    
   @ErrorLine VARCHAR(MAX),    
   @ErrorMessage VARCHAR(MAX)    
   set  @ErrorNumber =  ERROR_NUMBER();    
   set  @ErrorSeverity= ERROR_SEVERITY();    
   set  @ErrorState =  ERROR_STATE();    
   set  @ErrorMessage =   ERROR_MESSAGE();    
   set  @ErrorLine =  ERROR_LINE();    
    set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;    
    
    THROW 51000,@stringmsg , 1;     
 END CATCH;    
End; 