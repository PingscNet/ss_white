﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[GetWhoIsInTheBuilding]
Comments	: 13-05-2022 | Nirali | This procedure is used to get Who is in the Building.
USE [db_a5dc4b_sswhite]
 EXEC [emp].[GetWhoIsInTheBuilding]
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetWhoIsInTheBuilding]
AS 
BEGIN
	SET NOCOUNT ON;
	WITH result AS (
			SELECT 
				MAX(Id) AS 'Id'
			FROM  
				emp.VMSDataFromDashboard 
			WHERE
				CAST(PunchDateTime  AS DATE ) = CAST( GETDATE() AS DATE )
			GROUP BY 
				EmployeeId
			)
			SELECT 
				vmsd.Id,vmsd.EmployeeId,vmsd.PunchDateTime,vmsd.InOutStatus 
			FROM 
				emp.VMSDataFromDashboard VMSD
				INNER join result rs
			ON 
				VMSD.Id=rs.Id
END