﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.GetAllEpos
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: EXEC [emp].[SendForgetPasswordEmail]
					@UserLogin = "shivam.dubey",
					@StatusTypeActive = 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[SendForgetPasswordEmail]
(
	@UserLogin VARCHAR(100),
	@StatusTypeActive SMALLINT
)
AS
BEGIN

	SET NOCOUNT ON;
			
	SELECT TOP 1
		EmailAddress 
	FROM 
		emp.UserMaster 
	WHERE 
		(EmailAddress = @UserLogin
		OR LoginId = @UserLogin)
		AND Status = @StatusTypeActive
	Order by 1 desc
END
