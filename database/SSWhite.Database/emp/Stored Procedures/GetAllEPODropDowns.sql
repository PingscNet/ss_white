﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.GetAllEmployeeLimit
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	: EXEC [emp].[GetAllEPODropDowns]
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetAllEPODropDowns]
AS
BEGIN
	SET NOCOUNT ON;
	--Employee ID,  EmployeeName 
	select  PersonID, PersonName from emp.VMSEmployeeDetails where organization like '%Office Employee%'

	select Id ,CountryName from [emp].[Country] where TENENTID =1

	select Id,Name from emp.EPOType

	--EPODept
	select Id,RefName from [emp].[EPORefTable] WHERE RefType = 'Dept_Name' AND RefSubType = 'Dept_Name'

	--EPOProduct
	select Id,RefName from [emp].[EPORefTable] WHERE RefType = 'Product_Group' AND RefSubType = 'Product_Group'

	--EPOAcocuntNo
	select Id,RefName from [emp].[EPORefTable] WHERE RefType = 'Account_No' AND RefSubType = 'Account_No'

	--EPO UOM
	SELECT Id,UOMName from [emp].[UOM] WHERE mid != 0	

	--EPO TAX
	SELECT Id,Rate FROM [emp].[Tax]
END