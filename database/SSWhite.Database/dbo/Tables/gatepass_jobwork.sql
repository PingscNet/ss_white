﻿CREATE TABLE [dbo].[gatepass_jobwork] (
    [GPNo]            NVARCHAR (50)   NOT NULL,
    [id]              INT             NOT NULL,
    [gid]             INT             NOT NULL,
    [pid]             INT             NOT NULL,
    [gatedate]        DATETIME        NULL,
    [item_qty]        DECIMAL (18, 3) NULL,
    [remaining_qty]   DECIMAL (18, 3) NULL,
    [received_qty]    DECIMAL (18, 3) NULL,
    [transaction_qty] DECIMAL (18, 3) NULL,
    [received_date]   DATETIME        NULL,
    [received_person] NVARCHAR (MAX)  NULL,
    [item_name]       NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_gatepass_jobwork] PRIMARY KEY CLUSTERED ([GPNo] ASC, [id] ASC, [gid] ASC, [pid] ASC)
);

