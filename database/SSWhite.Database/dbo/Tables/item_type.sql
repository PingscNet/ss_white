﻿CREATE TABLE [dbo].[item_type] (
    [id]        INT            NOT NULL,
    [mid]       INT            NOT NULL,
    [item_name] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_item_type] PRIMARY KEY CLUSTERED ([id] ASC, [mid] ASC)
);

