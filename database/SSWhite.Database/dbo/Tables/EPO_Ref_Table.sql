﻿CREATE TABLE [dbo].[EPO_Ref_Table] (
    [ID]           INT            NOT NULL,
    [MID]          INT            NOT NULL,
    [PID]          INT            NOT NULL,
    [Ref_ID]       INT            NOT NULL,
    [Ref_Type]     NVARCHAR (100) NULL,
    [Ref_sub_type] NVARCHAR (100) NULL,
    [refname]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_EPO_Ref_Table] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC, [PID] ASC, [Ref_ID] ASC)
);

