﻿CREATE TABLE [dbo].[video_table] (
    [MID]    INT            NOT NULL,
    [VID]    INT            NOT NULL,
    [ID]     INT            NOT NULL,
    [Title]  NVARCHAR (500) NULL,
    [Type]   NVARCHAR (50)  NULL,
    [url]    NVARCHAR (500) NULL,
    [active] BIT            NULL,
    CONSTRAINT [PK_video_table] PRIMARY KEY CLUSTERED ([MID] ASC, [VID] ASC, [ID] ASC)
);

