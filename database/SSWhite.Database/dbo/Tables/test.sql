﻿CREATE TABLE [dbo].[test] (
    [data] NVARCHAR (50) NULL,
    [id]   INT           NOT NULL,
    CONSTRAINT [PK_test] PRIMARY KEY CLUSTERED ([id] ASC)
);

