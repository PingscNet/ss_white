﻿CREATE TABLE [dbo].[emailauto] (
    [ID]    INT           NOT NULL,
    [PID]   INT           NOT NULL,
    [Email] NVARCHAR (50) NULL,
    CONSTRAINT [PK_emailauto] PRIMARY KEY CLUSTERED ([ID] ASC, [PID] ASC)
);

