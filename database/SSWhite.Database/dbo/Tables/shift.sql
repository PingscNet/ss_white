﻿CREATE TABLE [dbo].[shift] (
    [id]    INT           NOT NULL,
    [pid]   INT           NOT NULL,
    [shift] NVARCHAR (50) NULL,
    CONSTRAINT [PK_shift] PRIMARY KEY CLUSTERED ([id] ASC, [pid] ASC)
);

