﻿CREATE TABLE [dbo].[CRUP_MST] (
    [TENANT_ID]     INT            NOT NULL,
    [CRUP_ID]       BIGINT         NOT NULL,
    [MySerial]      INT            NULL,
    [MENU_ID]       INT            NULL,
    [PHYSICALLOCID] NVARCHAR (100) NULL,
    [ActivityNote]  NVARCHAR (MAX) NULL,
    [CREATED_BY]    NVARCHAR (20)  NOT NULL,
    [CREATED_DT]    DATETIME       NOT NULL,
    [UPDATED_BY]    NVARCHAR (20)  NULL,
    [UPDATED_DT]    DATETIME       NULL,
    [Activity_Type] NVARCHAR (MAX) NULL,
    [USER_ID]       INT            NULL,
    [Table_Name]    NVARCHAR (MAX) NULL,
    [url]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ERP_CREATEUPDATE_MST] PRIMARY KEY CLUSTERED ([TENANT_ID] ASC, [CRUP_ID] ASC)
);

