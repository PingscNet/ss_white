﻿CREATE TABLE [dbo].[maintenance] (
    [MID]             INT            NOT NULL,
    [ID]              INT            NOT NULL,
    [Maintenancename] NVARCHAR (500) NULL,
    [active]          NVARCHAR (10)  NULL,
    CONSTRAINT [PK_maintenance] PRIMARY KEY CLUSTERED ([MID] ASC, [ID] ASC)
);

