﻿CREATE TABLE [dbo].[mytimepunchrecords] (
    [ID]         INT            NOT NULL,
    [PID]        INT            NOT NULL,
    [PersonID]   INT            NULL,
    [personname] NVARCHAR (500) NULL,
    [date]       DATETIME       NULL,
    [status]     NVARCHAR (500) NULL,
    [time]       TIME (7)       NULL,
    [datetime]   DATETIME       NULL,
    [approval]   NVARCHAR (500) NULL,
    [active]     NVARCHAR (10)  NULL,
    [new_added]  NVARCHAR (50)  NULL,
    CONSTRAINT [PK_mytimepunchrecords] PRIMARY KEY CLUSTERED ([ID] ASC, [PID] ASC)
);

