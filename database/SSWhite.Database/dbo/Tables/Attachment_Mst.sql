﻿CREATE TABLE [dbo].[Attachment_Mst] (
    [ID]                 INT            NOT NULL,
    [MID]                INT            NOT NULL,
    [employeeID]         INT            NULL,
    [userid]             INT            NULL,
    [attached_file_name] NVARCHAR (MAX) NULL,
    [attachment_date]    DATETIME       NULL,
    [file_name]          NVARCHAR (MAX) NULL,
    [title]              NVARCHAR (MAX) NULL,
    [approval]           NVARCHAR (10)  NULL,
    [attachment_Type]    NVARCHAR (500) NULL,
    [EPO_no]             INT            NULL,
    CONSTRAINT [PK_Attachment_Mst] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC)
);

