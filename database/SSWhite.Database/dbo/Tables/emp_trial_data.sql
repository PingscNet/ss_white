﻿CREATE TABLE [dbo].[emp_trial_data] (
    [ID]          INT            NOT NULL,
    [Course_name] NVARCHAR (MAX) NULL,
    [courseid]    INT            NULL,
    [location]    NVARCHAR (MAX) NULL,
    [description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_emp_trial_data] PRIMARY KEY CLUSTERED ([ID] ASC)
);

