﻿CREATE TABLE [dbo].[EPO_ITEM_Receive] (
    [ID]              INT            NOT NULL,
    [EPO_NO]          INT            NOT NULL,
    [line_no]         INT            NOT NULL,
    [PID]             INT            NOT NULL,
    [Order_qty]       INT            NULL,
    [received_qty]    INT            NULL,
    [remaining_qty]   INT            NULL,
    [transaction_qty] INT            NULL,
    [invoice_no]      NVARCHAR (MAX) NULL,
    [challan_no]      NVARCHAR (MAX) NULL,
    [received_date]   DATETIME       NULL,
    [received_by]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_EPO_ITEM_Receive] PRIMARY KEY CLUSTERED ([ID] ASC, [EPO_NO] ASC, [line_no] ASC, [PID] ASC)
);

