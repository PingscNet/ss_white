﻿CREATE TABLE [dbo].[SSW_Appliances_Main_added] (
    [ID]                 INT            NOT NULL,
    [PID]                INT            NOT NULL,
    [MID]                INT            NOT NULL,
    [Type_of_appliance]  NVARCHAR (100) NOT NULL,
    [Appliance_type]     NVARCHAR (MAX) NULL,
    [Appliance_Name]     NVARCHAR (MAX) NULL,
    [Appliance_Number]   NVARCHAR (100) NULL,
    [Active]             NVARCHAR (2)   NULL,
    [Hall]               NVARCHAR (50)  NULL,
    [IP]                 NVARCHAR (100) NULL,
    [monitor_detail]     NVARCHAR (MAX) NULL,
    [mount_stand]        NVARCHAR (100) NULL,
    [Window_version]     NVARCHAR (200) NULL,
    [PC_configuration]   NVARCHAR (MAX) NULL,
    [Keyboard_Mouse]     NVARCHAR (100) NULL,
    [Desktop_Phone]      NVARCHAR (50)  NULL,
    [MAC_Address]        NVARCHAR (500) NULL,
    [Appliance_sub_type] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_SSW_Appliances_Main] PRIMARY KEY CLUSTERED ([ID] ASC, [PID] ASC, [MID] ASC, [Type_of_appliance] ASC)
);

