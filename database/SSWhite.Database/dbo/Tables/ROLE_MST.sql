﻿CREATE TABLE [dbo].[ROLE_MST] (
    [TenentID]       INT            NOT NULL,
    [ROLE_ID]        INT            NOT NULL,
    [ROLE_NAME]      NVARCHAR (50)  NOT NULL,
    [ROLE_NAME1]     NVARCHAR (50)  NULL,
    [ROLE_NAME2]     NVARCHAR (50)  NULL,
    [ROLE_DESC]      NVARCHAR (250) NOT NULL,
    [ACTIVE_FLAG]    VARCHAR (1)    NOT NULL,
    [ACTIVE_FROM_DT] DATETIME       NOT NULL,
    [ACTIVE_TO_DT]   DATETIME       NOT NULL,
    [ERP_TENANT_ID]  INT            NULL,
    [CRUP_ID]        BIGINT         NOT NULL,
    [ACTIVEROLE]     BIT            NULL,
    [ROLLDATE]       DATE           NULL,
    CONSTRAINT [PK_ACM_ROLE_MST_1] PRIMARY KEY CLUSTERED ([TenentID] ASC, [ROLE_ID] ASC)
);

