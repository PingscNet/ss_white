﻿CREATE TABLE [dbo].[TBLLabelDTL] (
    [ID]          INT            NOT NULL,
    [LOCATION_ID] INT            NULL,
    [LabelMstID]  NVARCHAR (50)  NULL,
    [FieldName]   NVARCHAR (50)  NULL,
    [LabelID]     NVARCHAR (50)  NULL,
    [LabelName]   NVARCHAR (100) NULL,
    [LangID]      INT            NULL,
    [COUNTRYID]   INT            NULL,
    [LANGDISP]    NVARCHAR (5)   NULL,
    [Active]      BIT            NULL,
    [CRUP_ID]     BIGINT         NULL,
    CONSTRAINT [PK_TBLLabelDTLn] PRIMARY KEY CLUSTERED ([ID] ASC)
);

