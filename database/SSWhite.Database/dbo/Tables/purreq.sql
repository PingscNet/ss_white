﻿CREATE TABLE [dbo].[purreq] (
    [req_no]                         NVARCHAR (50)   NOT NULL,
    [id]                             INT             NOT NULL,
    [pid]                            INT             NOT NULL,
    [nameofinitiator]                NVARCHAR (MAX)  NULL,
    [department]                     NVARCHAR (MAX)  NULL,
    [itemname]                       NVARCHAR (MAX)  NULL,
    [purpose]                        NVARCHAR (MAX)  NULL,
    [itemtype]                       NVARCHAR (500)  NULL,
    [uom]                            NVARCHAR (50)   NULL,
    [itemqty]                        INT             NULL,
    [totalkg]                        INT             NULL,
    [peruom_price]                   DECIMAL (18, 3) NULL,
    [extracharge]                    DECIMAL (18, 3) NULL,
    [suggested_supplier]             NVARCHAR (MAX)  NULL,
    [remark_on_item]                 NVARCHAR (MAX)  NULL,
    [askforquotation_date]           DATETIME        NULL,
    [supplier_name]                  NVARCHAR (500)  NULL,
    [epo]                            NVARCHAR (500)  NULL,
    [epo_date]                       DATETIME        NULL,
    [epo_status]                     NVARCHAR (500)  NULL,
    [received_date]                  DATETIME        NULL,
    [bill_received_date]             DATETIME        NULL,
    [remark_on_received_item]        NVARCHAR (MAX)  NULL,
    [purchaser_remark]               NVARCHAR (MAX)  NULL,
    [check_point]                    NVARCHAR (50)   NULL,
    [item_issue_date_against_req_no] DATETIME        NULL,
    [item_issue_to_person]           NVARCHAR (MAX)  NULL,
    [status]                         NVARCHAR (500)  NULL,
    [cid]                            INT             NULL,
    [req_date]                       DATETIME        NULL,
    [received_qty]                   DECIMAL (18, 3) NULL,
    [approval]                       NVARCHAR (10)   NULL,
    [remarkonitem]                   NVARCHAR (MAX)  NULL,
    [due_date]                       DATETIME        NULL,
    [askforquotation]                BIT             NULL,
    [billno]                         NVARCHAR (50)   NULL,
    [pending_qty]                    INT             NULL,
    [challan_no]                     NVARCHAR (100)  NULL,
    [invoice_no]                     NVARCHAR (100)  NULL,
    [qtymanage]                      INT             NULL,
    [invoice_no2]                    NVARCHAR (100)  NULL,
    [invoice_no3]                    NVARCHAR (100)  NULL,
    [invoice_no4]                    NVARCHAR (100)  NULL,
    [invoice_no5]                    NVARCHAR (100)  NULL,
    [challan_no2]                    NVARCHAR (100)  NULL,
    [challan_no3]                    NVARCHAR (100)  NULL,
    [challan_no4]                    NVARCHAR (100)  NULL,
    [challan_no5]                    NVARCHAR (100)  NULL,
    [epo2]                           NVARCHAR (100)  NULL,
    [epo3]                           NVARCHAR (100)  NULL,
    [epo4]                           NVARCHAR (100)  NULL,
    [epo5]                           NVARCHAR (100)  NULL,
    [payslipnumber]                  NVARCHAR (500)  NULL,
    [quotation_type]                 NVARCHAR (50)   NULL,
    [epo_lineno]                     INT             NULL,
    [itemqty_new]                    DECIMAL (18, 3) NULL,
    [issue_status]                   NVARCHAR (100)  NULL,
    [attachment]                     NVARCHAR (MAX)  NULL,
    [filter_allow]                   NVARCHAR (2)    NULL,
    CONSTRAINT [PK_purreq] PRIMARY KEY CLUSTERED ([req_no] ASC, [id] ASC, [pid] ASC)
);

