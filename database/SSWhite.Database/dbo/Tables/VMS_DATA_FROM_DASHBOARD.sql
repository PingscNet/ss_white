﻿CREATE TABLE [dbo].[VMS_DATA_FROM_DASHBOARD] (
    [ID]         INT            NOT NULL,
    [MID]        INT            NOT NULL,
    [personId]   NVARCHAR (50)  NULL,
    [datetime]   DATETIME       NULL,
    [date]       DATE           NULL,
    [time]       TIME (7)       NULL,
    [status]     NVARCHAR (100) NULL,
    [Device]     NVARCHAR (255) NULL,
    [DeviceNo]   NVARCHAR (255) NULL,
    [PersonName] NVARCHAR (500) NULL,
    [UserId]     INT            NULL,
    CONSTRAINT [PK_VMS_DATA_FROM_DASHBOARD] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC)
);

