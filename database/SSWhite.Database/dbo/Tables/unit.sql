﻿CREATE TABLE [dbo].[unit] (
    [MID]      INT            NOT NULL,
    [ID]       INT            NOT NULL,
    [unitname] NVARCHAR (MAX) NULL,
    [active]   NVARCHAR (10)  NULL,
    [type]     NVARCHAR (50)  NULL,
    CONSTRAINT [PK_unit] PRIMARY KEY CLUSTERED ([MID] ASC, [ID] ASC)
);

