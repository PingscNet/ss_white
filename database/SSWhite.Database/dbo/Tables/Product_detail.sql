﻿CREATE TABLE [dbo].[Product_detail] (
    [MID]                 INT            NOT NULL,
    [PID]                 INT            NOT NULL,
    [ID]                  INT            NOT NULL,
    [product_name]        NVARCHAR (500) NULL,
    [product_info_main]   NVARCHAR (MAX) NULL,
    [product_info_inner]  NVARCHAR (MAX) NULL,
    [Description_Product] NVARCHAR (MAX) NULL,
    [Product_header]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Product_detail] PRIMARY KEY CLUSTERED ([MID] ASC, [PID] ASC, [ID] ASC)
);

