﻿CREATE TABLE [dbo].[Weathers] (
    [ID]          INT           IDENTITY (1, 1) NOT NULL,
    [DateTime]    DATETIME2 (7) NOT NULL,
    [Temperature] FLOAT (53)    NOT NULL,
    [Humidity]    FLOAT (53)    NOT NULL,
    [Pressure]    FLOAT (53)    NOT NULL,
    [Dust]        FLOAT (53)    NOT NULL,
    [UV]          FLOAT (53)    NOT NULL,
    CONSTRAINT [PK_Weathers] PRIMARY KEY CLUSTERED ([ID] ASC)
);

