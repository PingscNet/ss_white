﻿CREATE TABLE [dbo].[tblAudit] (
    [TENANT_ID]       BIGINT         NOT NULL,
    [CRUP_ID]         BIGINT         NOT NULL,
    [MySerial]        INT            NOT NULL,
    [AuditNo]         INT            IDENTITY (1, 1) NOT NULL,
    [AuditType]       VARCHAR (20)   NULL,
    [TableName]       VARCHAR (128)  NOT NULL,
    [FieldName]       VARCHAR (128)  NULL,
    [OldValue]        NVARCHAR (MAX) NULL,
    [NewValue]        NVARCHAR (MAX) NULL,
    [UpdateDate]      DATETIME       NULL,
    [UpdateUserName]  VARCHAR (128)  NULL,
    [CreatedDate]     DATETIME       NULL,
    [CreatedUserName] VARCHAR (128)  NULL,
    CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED ([TENANT_ID] ASC, [CRUP_ID] ASC, [MySerial] ASC, [AuditNo] ASC)
);

