﻿CREATE TABLE [dbo].[supp_email] (
    [id]         INT            NOT NULL,
    [supp_name]  NVARCHAR (MAX) NULL,
    [Supp_email] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_supp_email] PRIMARY KEY CLUSTERED ([id] ASC)
);

