﻿CREATE TABLE [dbo].[multinames] (
    [ID]    INT            NOT NULL,
    [pid]   INT            NOT NULL,
    [reqno] NVARCHAR (100) NOT NULL,
    [name]  NVARCHAR (100) NULL,
    CONSTRAINT [PK_multinames] PRIMARY KEY CLUSTERED ([ID] ASC, [pid] ASC, [reqno] ASC)
);

