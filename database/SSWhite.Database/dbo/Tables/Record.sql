﻿CREATE TABLE [dbo].[Record] (
    [Person_ID] INT            NULL,
    [Name]      NVARCHAR (250) NULL,
    [Age]       NVARCHAR (250) NULL,
    [City]      NVARCHAR (250) NULL,
    [City-Name] NVARCHAR (250) NULL,
    [Pincode]   INT            NULL,
    [Country]   NVARCHAR (250) NULL,
    [Values]    INT            NULL
);

