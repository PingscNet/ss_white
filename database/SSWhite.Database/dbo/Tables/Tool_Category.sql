﻿CREATE TABLE [dbo].[Tool_Category] (
    [ID]                    INT            NOT NULL,
    [Tool_Category]         NVARCHAR (500) NULL,
    [Tool_Type]             NVARCHAR (500) NULL,
    [Tool_type_search_word] NVARCHAR (MAX) NULL,
    [Tool_Type_description] NVARCHAR (MAX) NULL,
    [MID]                   INT            NULL,
    [PID]                   INT            NULL,
    [Tool_Type_Name]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Tool_Category] PRIMARY KEY CLUSTERED ([ID] ASC)
);

