﻿CREATE TABLE [dbo].[Employees] (
    [PersonId]   INT            NOT NULL,
    [Name]       NVARCHAR (255) NULL,
    [Age]        INT            NULL,
    [Country]    NVARCHAR (255) NULL,
    [Cityname]   NVARCHAR (255) NULL,
    [Postalcode] INT            NULL,
    [Password]   NVARCHAR (255) NULL,
    [Number]     INT            NULL,
    CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED ([PersonId] ASC)
);

