﻿CREATE TABLE [dbo].[InitiatorName] (
    [id]             INT            NOT NULL,
    [rid]            INT            NOT NULL,
    [initiator_name] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_InitiatorName] PRIMARY KEY CLUSTERED ([id] ASC, [rid] ASC)
);

