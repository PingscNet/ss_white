﻿CREATE TABLE [dbo].[jobworkhour] (
    [GPNO]         NVARCHAR (50)  NOT NULL,
    [suppliername] NVARCHAR (MAX) NULL,
    [countinghour] NVARCHAR (50)  NULL,
    CONSTRAINT [PK_jobworkhour] PRIMARY KEY CLUSTERED ([GPNO] ASC)
);

