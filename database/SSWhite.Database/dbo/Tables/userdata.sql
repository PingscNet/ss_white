﻿CREATE TABLE [dbo].[userdata] (
    [MID]    INT            NOT NULL,
    [ID]     INT            NOT NULL,
    [name]   NVARCHAR (MAX) NULL,
    [utype]  NVARCHAR (50)  NULL,
    [active] NVARCHAR (10)  NULL,
    CONSTRAINT [PK_userdata] PRIMARY KEY CLUSTERED ([MID] ASC, [ID] ASC)
);

