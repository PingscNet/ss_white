﻿CREATE TABLE [dbo].[required] (
    [PersonID]  INT           NULL,
    [LastName]  VARCHAR (255) NULL,
    [FirstName] VARCHAR (255) NULL,
    [Address]   VARCHAR (255) NULL,
    [City]      VARCHAR (255) NULL
);

