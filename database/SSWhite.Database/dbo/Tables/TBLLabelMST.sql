﻿CREATE TABLE [dbo].[TBLLabelMST] (
    [ID]          INT            NOT NULL,
    [LOCATION_ID] INT            NULL,
    [TableName]   NVARCHAR (50)  NULL,
    [PageName]    NVARCHAR (50)  NULL,
    [PageTitle]   NVARCHAR (50)  NULL,
    [Remark]      NVARCHAR (500) NULL,
    [MySysName]   NVARCHAR (3)   NULL,
    [TotalLang]   INT            NULL,
    [TenantID]    INT            NULL,
    [UserID]      INT            NULL,
    [CRUP_ID]     BIGINT         NULL,
    [DateTime]    DATETIME       NULL,
    [Active]      BIT            NULL,
    [IsExclusive] BIT            NULL,
    [Updated]     DATETIME       NULL,
    CONSTRAINT [PK_TBLLabelMSTn] PRIMARY KEY CLUSTERED ([ID] ASC)
);

