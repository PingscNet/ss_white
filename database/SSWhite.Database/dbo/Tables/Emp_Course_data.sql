﻿CREATE TABLE [dbo].[Emp_Course_data] (
    [ID]          INT             NOT NULL,
    [MID]         INT             NOT NULL,
    [courseid]    INT             NOT NULL,
    [course_name] NVARCHAR (MAX)  NULL,
    [from_date]   DATETIME        NULL,
    [to_date]     DATETIME        NULL,
    [Hours]       DECIMAL (18, 3) NULL,
    [location]    NVARCHAR (MAX)  NULL,
    [Description] NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_Emp_Course_data] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC, [courseid] ASC)
);

