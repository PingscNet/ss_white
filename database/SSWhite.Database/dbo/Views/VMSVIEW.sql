﻿CREATE VIEW [dbo].[VMSVIEW]
AS
SELECT        dbo.VMSAttendance.ID, dbo.VMSAttendance.datetime, dbo.VMSAttendance.date, dbo.VMSAttendance.time, dbo.VMSAttendance.Status, dbo.VMSAttendance.Device, dbo.VMSAttendance.DeviceNo, 
                         dbo.VMSAttendance.PersonName, dbo.VMSAttendance.CardNumber, dbo.VMSEmployeeDetails.Organization
FROM            dbo.VMSAttendance INNER JOIN
                         dbo.VMSEmployeeDetails ON dbo.VMSAttendance.ID = dbo.VMSEmployeeDetails.PersonID
