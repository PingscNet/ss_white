﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [dbo].[InsertInOutDetailsById]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution : EXEC [dbo].[GetInOutStatusByPunchType]
					@UserId = 12,
					@AttendancePunchTypeId = 2
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetInOutStatusByPunchType]
(
	@UserId int,
	@AttendancePunchTypeId int
)
AS
BEGIN
	SET NOCOUNT ON;
	--Select Top 1
	--	AttendanceInOutStatusId
	--from 
	--	UserAttendance 
	--where 
	--	UserId = @UserId 
	--	and AttendancePunchTypeId = @AttendancePunchTypeId
	--	order by PunchTime desc;

	Select Top 1
		InOutStatus as AttendanceInOutStatusId
	from 
		emp.VMSDataFromDashboard
	where 
		EmployeeId = @UserId 
		and PunchType = @AttendancePunchTypeId
		order by PunchDateTime desc;
End;
		
		
		select * from emp.VMSDataFromDashboard where PunchType=2 order by 1 desc