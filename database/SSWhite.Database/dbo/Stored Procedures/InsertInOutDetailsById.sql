﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [dbo].[InsertInOutDetailsById]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution : EXEC [dbo].[InsertInOutDetailsById]
					@UserId = 1,
					@AttendancePunchTypeId = 1,
					@AttendanceInOutStatusId = 1
					@CurrentDate = 
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[InsertInOutDetailsById]
(
	@UserId int,
	@AttendancePunchTypeId int,
	@AttendanceInOutStatusId int,
	@CurrentDate DateTime
)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO 
		dbo.UserAttendance
	(
		UserId,
		PunchTime,
		AttendancePunchTypeId,
		AttendanceInOutStatusId
	)
	Values
	(
		@UserId,
		@CurrentDate,
		@AttendancePunchTypeId,
		@AttendanceInOutStatusId
	)
End;
