﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetItemSubCategoryByCategoryId
Comments		: 19-05-2021 | Amit Khanna | This procedure is used to get All items sub categories by category Id for common call.

Test Execution	: EXEC master.GetItemSubCategoryByCategoryId
					@SubscriberId = 1,
					@CategoryId  = 1,
					@Status = NULL,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetItemSubCategoryByCategoryId]
(
	@SubscriberId INT,
	@CategoryId INT,
	@Status TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetItemSubCategoryByCategoryId]','
										@SubscriberId =',@SubscriberId,',
										@CategoryId =',@CategoryId,',
										@Status = ',@Status,'
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetItemSubCategoryByCategoryId]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
		SELECT
			st.Id,
			st.[Name],
			st.[Status]
		FROM
			[master].ItemSubCategories AS st
		WHERE
			st.SubscriberId = @SubscriberId
			AND st.ItemCategoryId = ISNULL(@CategoryId,st.ItemCategoryId)
			AND st.[Status] = ISNULL(@Status,st.[Status])
		ORDER BY
			st.[Name] ASC;
		
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
