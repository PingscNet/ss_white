﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateCompany
Comments		: 28-03-2021 | Amit Khanna | This procedure is used to insert company or update company by Id.

Test Execution	: DECLARE @ReturnValue INT = 1;
					EXEC @ReturnValue = [master].[InsertOrUpdateCompany]
					@SubscriberId =1,
					@Id = NULL,
					@Name ='AK Industries',
					@Gstin =NULL,
					@Pan =NULL,
					@Address ='Address',
					@CountryId = 1,
					@StateId = 11,
					@CityId = 331,
					@Pincode = 382345,
					@EmailAddress = 'amit@gmail.com',
					@ContactPerson = 'amit khanna',
					@ContactNumber = '9727466371',
					@Website =NULL,
					@OtherLicense =NULL,
					@TagLine =NULL,
					@TermsAndConditions =NULL,
					@LogoPath =NULL,
					@IpAddress =NULL,
					@CreatedBy = 1,
					@CreatedDate = '2021-03-03',
					@StatusTypeActive =1
					SELECT @ReturnValue;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateCompany]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(100),
	@Gstin VARCHAR(15),
	@Pan VARCHAR(10),
	@Address VARCHAR(500),
	@CountryId INT,
	@StateId INT,
	@CityId INT,
	@Pincode VARCHAR(6),
	@EmailAddress VARCHAR(100),
	@ContactPerson VARCHAR(100),
	@ContactNumber VARCHAR(20),
	@Website VARCHAR(80),
	@OtherLicense VARCHAR(200),
	@TagLine VARCHAR(100),
	@TermsAndConditions VARCHAR(MAX),
	@LogoPath VARCHAR(MAX),
	@BankDetails [master].BankDetailTableType READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateCompany]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =''',@Name,''',
										@Gstin =''',@Gstin,''',
										@Pan =''',@Pan,''',
										@Address =''',@Address,''',
										@CountryId = ',@CountryId,',
										@StateId = ',@StateId,',
										@CityId = ',@CityId,',
										@Pincode = ','',@Pincode,'','
										@EmailAddress = ','',@EmailAddress,'','
										@ContactPerson = ','',@ContactPerson,'','
										@ContactNumber = ','',@ContactNumber,'','
										@Website = ','',@Website,'','
										@OtherLicense = ','',@OtherLicense,'','
										@TagLine = ','',@TagLine,'','
										@TermsAndConditions = ','',@TermsAndConditions,'','
										@LogoPath = ','',@LogoPath,'','
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateCompany]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@CompanyId INT;
	CREATE TABLE #TempFinancialYears
	(
		Item INT
	);
	INSERT INTO #TempFinancialYears
	(
		Item
	)
	VALUES
	(201718),
	(201819),
	(201920),
	(202021),
	(202122);

	CREATE TABLE #TempModuleDocumentTypes
	(
		DocumentType TINYINT,
		ModuleType TINYINT
	)
	INSERT INTO #TempModuleDocumentTypes
	(
		DocumentType,
		ModuleType
	)
	VALUES
	(
		1,
		3
	),
	(
		2,
		4
	),
	(
		3,
		5
	),
	(
		4,
		5
	),
	(
		5,
		7
	),
	(
		6,
		7
	),
	(
		7,
		8
	);


	SELECT
		bdt.[Name],
		bdt.AccountNumber,
		bdt.Branch,
		bdt.IfscCode,
		bdt.IsDefault
	INTO
		#TempBankDetails
	FROM
		@BankDetails AS bdt

		
    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Companies WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND IIF(@Gstin IS NULL,'',Gstin) = ISNULL(@Gstin,'') AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO [master].Companies
				(
					SubscriberId,
					[Name],
					Gstin,
					Pan,
					[Address],
					CountryId,
					StateId,
					CityId,
					Pincode,
					EmailAddress,
					ContactPerson,
					ContactNumber,
					Website,
					OtherLicense,
					TagLine,
					TermsAndConditions,
					LogoPath,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@Name,
					@Gstin,
					@Pan,
					@Address,
					@CountryId,
					@StateId,
					@CityId,
					@Pincode,
					@EmailAddress,
					@ContactPerson,
					@ContactNumber,
					@Website,
					@OtherLicense,
					@TagLine,
					@TermsAndConditions,
					@LogoPath,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
				
				SET @CompanyId = SCOPE_IDENTITY();

				INSERT INTO [master].CompanyBankDetails
				(
					CompanyId,
					[Name],
					AccountNumber,
					Branch,
					IfscCode,
					IsDefault
				)
				SELECT
					@CompanyId,
					bdt.[Name],
					bdt.AccountNumber,
					bdt.Branch,
					bdt.IfscCode,
					bdt.IsDefault
				FROM
					#TempBankDetails AS bdt;

				INSERT INTO subscriber.NumberConfigurations
				(
					SubscriberId,
					EntityId,
					ModuleType,
					ActualLastNumber,
					LastNumber,
					Prefix,
					Suffix,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@CompanyId,
					6,
					0,
					0,
					NULL,
					NULL,
					@CreatedDate
				)
				INSERT INTO subscriber.NumberConfigurations
				(
					SubscriberId,
					EntityId,
					ModuleType,
					DocumentType,
					FinancialYear,
					ActualLastNumber,
					LastNumber,
					Prefix,
					Suffix,
					CreatedDate
				)
				SELECT
					@SubscriberId,
					@CompanyId,
					mdty.ModuleType,
					mdty.DocumentType,
					fy.Item,
					0,
					0,
					NULL,
					NULL,
					GETDATE()
				FROM
					#TempFinancialYears AS fy
					CROSS APPLY #TempModuleDocumentTypes AS mdty
			
				COMMIT TRAN
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Companies WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND IIF(@Gstin IS NULL,'',Gstin) = ISNULL(@Gstin,'') AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				UPDATE
					[master].Companies
				SET
					[Name] = @Name,
					Gstin = @Gstin,
					Pan = @Pan,
					[Address] = @Address,
					CountryId = @CountryId,
					StateId = @StateId,
					CityId = @CityId,
					Pincode = @Pincode,
					EmailAddress = @EmailAddress,
					ContactPerson = @ContactPerson,
					ContactNumber = @ContactNumber,
					Website = @Website,
					OtherLicense = @OtherLicense,
					TagLine = @TagLine,
					TermsAndConditions = @TermsAndConditions,
					LogoPath = ISNULL(@LogoPath,LogoPath),
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;

				DELETE FROM 
					[master].CompanyBankDetails
				WHERE
					CompanyId = @Id;

				INSERT INTO [master].CompanyBankDetails
				(
					CompanyId,
					[Name],
					AccountNumber,
					Branch,
					IfscCode,
					IsDefault
				)
				SELECT
					@Id,
					bdt.[Name],
					bdt.AccountNumber,
					bdt.Branch,
					bdt.IfscCode,
					bdt.IsDefault
				FROM
					#TempBankDetails AS bdt;
				COMMIT TRAN
			END
		END

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
		RETURN @ReturnValue;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
		RETURN @ReturnValue;
	END CATCH;

	DROP TABLE #TempBankDetails;
