﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllBrands
Comments		: 01-06-2021 | Amit Khanna | This procedure is used to get All Brands.

Test Execution	: EXEC master.GetAllBrands
					@SubscriberId = 1,
					@Status = NULL,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@IpAddress = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetAllBrands]
(
	@SubscriberId INT,
	@Status TINYINT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetAllBrands]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,',
										@Start = ',@Start,',
										@Length = ',@Length,',
										@SearchKeyword = ','',@SearchKeyword,'',',
										@SortExpression = ','',@SortExpression,'',',
										@IpAddress = ','',@IpAddress,'',',
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[master].[GetAllBrands]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT); 
    BEGIN TRY
		
		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[master].[Brands] AS us
		WHERE 
			us.SubscriberId = @SubscriberId
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.EndDate IS NULL
			AND 
			(
					us.[Name] LIKE '%' + ISNULL(@SearchKeyword,us.[Name]) +'%'
			);

		SELECT @TotalRecords =COUNT(Id)  FROM #TempTable 

		SELECT
			us.Id,
			us.[Name],
			us.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN [master].Brands us ON tmp.Id = us.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'Name asc' THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'Name desc' THEN us.[Name] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
