﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateItemSubGroup
Comments		: 04-03-2021 | Amit Khanna | This procedure is used to insert itemsubgroup or update item sub group by Id.

Test Execution	: EXEC master.InsertOrUpdateItemSubGroup
						@SubscriberId = 1,
						@Id = NULL,
						@ItemGroupId = 1,
						@Name = 'demo',
						@IpAddress = '',
						@CreatedBy = 1,
						@CreatedDate = '2021-02-28',
						@StatusTypeActive = 1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateItemSubGroup]
(
	@SubscriberId INT,
	@Id INT,
	@ItemGroupId INT,
	@Name VARCHAR(50),
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateItemSubGroup]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@ItemGroupId =',@ItemGroupId,',
										@Name =''',@Name,''',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateItemSubGroup]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1;
    BEGIN TRY
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ItemSubGroups WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND ItemGroupId = @ItemGroupId AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO [master].ItemSubGroups
				(
					SubscriberId,
					ItemGroupId,
					[Name],
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@ItemGroupId,
					@Name,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ItemSubGroups WHERE SubscriberId = @SubscriberId  AND [Name] = @Name AND ItemGroupId = @ItemGroupId AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				UPDATE
					[master].ItemSubGroups
				SET
					[Name] = @Name,
					ItemGroupId = @ItemGroupId,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id
					AND SubscriberId = @SubscriberId;
			END
		END

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
		RETURN @ReturnValue;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
		RETURN @ReturnValue;
	END CATCH;
