﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetClientOpeningBalanceDetails
Comments		: 13-05-2021 | Amit Khanna | This procedure is used to get client opening balance by Id.

Test Execution	: EXEC master.GetClientOpeningBalanceDetails
					@SubscriberId =  1,
					@ClientId =  1,
					@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetClientOpeningBalanceDetails]
(
	@SubscriberId INT,
	@ClientId INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetClientOpeningBalanceDetails]','
										@SubscriberId = ',@SubscriberId,',
										@ClientId = ',@ClientId,',
										@IpAddress = ''',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetClientOpeningBalanceDetails]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			ct.FinancialYear,
			ct.Balance,
			ct.[Type] AS TransactionType
		FROM
			[master].ClientOpeningBalance AS ct
		WHERE
			ct.ClientId = @ClientId;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
