﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[GetEmployeeJobDescription]
Comments	: 25-04-2022 | Kartik Bariya | This procedure is used to get GetEmployeeJobDescription details.

Test Execution : EXEC [master].[GetEmployeeJobDescription] 
					
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetEmployeeJobDescription]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		Id,
		departmentname AS DepartmentName
	FROM 
		emp.Department 
	WHERE 
		mid=1
End;
