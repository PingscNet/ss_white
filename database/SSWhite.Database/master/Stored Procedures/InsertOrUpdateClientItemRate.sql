﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateClientItemRate
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to insert client or update client by Id.

Test Execution	: DECLARE @ReturnValue INT;
					EXEC @ReturnValue =  [master].[InsertOrUpdateClientItemRate]
						 @SubscriberId =1,
						 @Id =NULL,
						 @ClientId = 1,
						 @IpAddress =NULL,
						 @CreatedBy = 1,
						 @CreatedDate = '2021-03-31',
						 @StatusTypeActive =1
					SELECT @ReturnValue
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateClientItemRate]
(
	@SubscriberId INT,
	@Id INT,
	@ClientId INT,
	@ClientItemRates [master].ClientItemRateType READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateClientItemRate]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@ClientId =',@ClientId,',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateClientItemRate]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@ItemRateId INT;
	SELECT
		bdt.ItemId,
		bdt.Rate
	INTO
		#TempRateDetails
	FROM
		@ClientItemRates AS bdt;

    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ClientItemRates WHERE SubscriberId = @SubscriberId AND ClientId = @ClientId AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO [master].ClientItemRates
				(
					SubscriberId,
					ClientId,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@ClientId,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
				
				SET @ItemRateId = SCOPE_IDENTITY();

				INSERT INTO [master].ClientItemRateDetails
				(
					ClientItemRateId,
					[ItemId],
					Rate
				)
				SELECT
					@ItemRateId,
					bdt.ItemId,
					bdt.Rate
				FROM
					#TempRateDetails AS bdt;

				COMMIT TRAN
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ClientItemRates WHERE SubscriberId = @SubscriberId AND ClientId = @ClientId AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				UPDATE
					[master].ClientItemRates
				SET
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;

				DELETE FROM 
					[master].ClientItemRateDetails
				WHERE
					ClientItemRateId = @Id;

				INSERT INTO [master].ClientItemRateDetails
				(
					ClientItemRateId,
					[ItemId],
					Rate
				)
				SELECT
					@Id,
					bdt.ItemId,
					bdt.Rate
				FROM
					#TempRateDetails AS bdt;

				COMMIT TRAN
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempRateDetails;
	RETURN @ReturnValue;
