﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateReferencePerson
Comments		: 17-06-2021 | Tanvi Pathak | This procedure is used to insert reference person or update reference person by Id.

Test Execution	: DECLARE @ReturnValue INT = 1;
					EXEC @ReturnValue = [master].[InsertOrUpdateReferencePerson]
					@SubscriberId =  1,
					@Id = NULL,
					@Name ='Paresh',
					@MobileNumber = '7689764213',
					@Address = 'Ahmedabad',
					@EmailAddress = 'paresh@gmail.com',
					@CityId = NULL,
					@StateId = NULL,
					@Code = '1',
					@ActualCodeNumber = 1,
					@Type = 2,
					@IpAddress =NULL,
					@CreatedBy = 1,
					@CreatedDate = '2021-06-17',
					@StatusTypeActive =1
					SELECT @ReturnValue;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateReferencePerson]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(100),
	@MobileNumber VARCHAR(10),
	@Address VARCHAR(50),
	@EmailAddress VARCHAR(80),
	@CityId INT,
	@StateId INT,
	@Code VARCHAR(100),
	@ActualCodeNumber INT,
	@Type TINYINT,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateReferencePerson]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =''',@Name,''',
										@MobileNumber = ''',@MobileNumber,''',
										@Address = ''',@Address,''',
										@EmailAddress = ''',@EmailAddress,''',
										@CityId = ',@CityId,',
										@StateId = ',@StateId,',
										@Code = ''',@Code,''',
										@ActualCodeNumber = ',@ActualCodeNumber,',
										@Type = ',@Type,',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateReferencePerson]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1
		
    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ReferencePersons WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO [master].ReferencePersons
				(
					SubscriberId,
					[Name],
					[MobileNumber],
					[Address],
					[EmailAddress],
					[CityId],
					[StateId],
					[Code],
					[ActualCodeNumber],
					[Type],
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@Name,
					@MobileNumber,
					@Address,
					@EmailAddress,
					@CityId,
					@StateId,
					@Code,
					@ActualCodeNumber,
					@Type,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ReferencePersons WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				UPDATE
					[master].ReferencePersons
				SET
					[Name] = @Name,
					[MobileNumber] = @MobileNumber,
					[Address] = @Address,
					[EmailAddress] = @EmailAddress,
					[CityId] = @CityId,
					[StateId] = @StateId,
					[Code] = @Code,
					[ActualCodeNumber] = @ActualCodeNumber,
					[Type] = @Type,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
