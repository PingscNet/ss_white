﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateZone
Comments		: 05-06-2021 | Tanvi Pathak | This procedure is used to insert zone or update zone by Id.

Test Execution	: DECLARE @ReturnValue INT = 1;
					EXEC @ReturnValue = [master].[InsertOrUpdateZone]
					@SubscriberId =1,
					@Id = NULL,
					@Name ='EastZone',
					@IpAddress =NULL,
					@CreatedBy = 1,
					@CreatedDate = '2021-03-03',
					@StatusTypeActive =1
					SELECT @ReturnValue;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateZone]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(100),
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateZone]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =''',@Name,''',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateZone]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1
		
    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Zones WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO [master].Zones
				(
					SubscriberId,
					[Name],
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@Name,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Zones WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				UPDATE
					[master].Zones
				SET
					[Name] = @Name,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
