﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[GetOrganization]
Comments	: 25-04-2022 | Kartik Bariya | This procedure is used to get Organization details.

Test Execution : EXEC [master].[GetOrganization] 1
					
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetOrganization]
(	
	@Status int
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		Id,
		Name
	FROM
		[master].[Organization]
	WHERE 
		Status = @Status
End;
