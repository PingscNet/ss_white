﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetClientItemDiscountById
Comments		: 23-06-2021 | Tanvi Pathak | This procedure is used to get client Item Discount by Id.

Test Execution	: EXEC master.GetClientItemDiscountById
					@SubscriberId =  1,
					@Id =  3,
					@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetClientItemDiscountById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetClientItemDiscountById]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetClientItemDiscountById]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			cmp.Id,
			cmp.ClientId,
			ct.CompanyName AS ClientName
		FROM
			[master].ClientItemDiscounts AS cmp
			INNER JOIN [master].Clients AS ct ON cmp.ClientId = ct.Id
		WHERE
			cmp.Id = @Id;

		SELECT
			ci.ItemTypeId,
			ci.Discount,
			ci.AdditionalDiscount,
			it.[Name] AS ItemName
		FROM
			[master].ClientItemDiscountDetails AS ci
			INNER JOIN [master].ItemTypes AS it ON ci.ItemTypeId = it.Id
		WHERE
			ci.ClientItemDiscountId = @Id;
			

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
