﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateCountry
Comments		: 26-03-2021 | Vikas Patel | This procedure is used to insert country or update country by Id.

Test Execution	: EXEC master.InsertOrUpdateCountry
						@SubscriberId = 1,
						@Id = NULL,
						@Name = 'demo',
						@IpAddress = '',
						@CreatedBy = 1,
						@CreatedDate = '2021-02-28',
						@StatusTypeActive = 1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateCountry]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(50),
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateCountry]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =''',@Name,''',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateCountry]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1;
    BEGIN TRY
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [app].Countries WHERE  [Name] = @Name AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO [app].Countries
				(
					[Name],
					[Status],
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@Name,
					@StatusTypeActive,
					@CreatedBy,
					@CreatedDate
				);
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [app].Countries WHERE  [Name] = @Name AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				UPDATE
					[app].Countries
				SET
					[Name] = @Name,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;
			END
		END

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
		RETURN @ReturnValue;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
		RETURN @ReturnValue;
	END CATCH;
