﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateItemType
Comments		: 21-06-2021 | Tanvi Pathak | This procedure is used to insert item type or update item type by Id.

Test Execution	: DECLARE @ReturnValue INT = 1;
					EXEC @ReturnValue = [master].[InsertOrUpdateItemType]
					@SubscriberId =1,
					@Id = NULL,
					@Name ='Computer',
					@IpAddress =NULL,
					@CreatedBy = 1,
					@CreatedDate = '2021-06-21',
					@StatusTypeActive =1
					SELECT @ReturnValue;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateItemType]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(100),
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateItemType]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =''',@Name,''',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateItemType]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1
		
    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ItemTypes WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO [master].ItemTypes
				(
					SubscriberId,
					[Name],
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@Name,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ItemTypes WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				UPDATE
					[master].ItemTypes
				SET
					[Name] = @Name,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
