﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateItemSubCategory
Comments		: 23-06-2021 | Tanvi Pathak | This procedure is used to insert itemsub category or update item sub category by Id.

Test Execution	: EXEC master.InsertOrUpdateItemSubCategory
						@SubscriberId = 1,
						@Id = NULL,
						@ItemCategoryId = 1,
						@Name = 'demo',
						@Rate = NULL,
						@IpAddress = '',
						@CreatedBy = 1,
						@CreatedDate = '2021-06-23',
						@StatusTypeActive = 1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateItemSubCategory]
(
	@SubscriberId INT,
	@Id INT,
	@ItemCategoryId INT,
	@Name VARCHAR(50),
	@Rate VARCHAR(MAX),
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateItemSubCategory]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@ItemCategoryId =',@ItemCategoryId,',
										@Name =''',@Name,''',
										@Rate = ''',@Rate,''',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateItemSubCategory]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1;
    BEGIN TRY
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ItemSubCategories WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND ItemCategoryId = @ItemCategoryId AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO [master].ItemSubCategories
				(
					SubscriberId,
					ItemCategoryId,
					[Name],
					[Rate],
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@ItemCategoryId,
					@Name,
					@Rate,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].ItemSubCategories WHERE SubscriberId = @SubscriberId  AND [Name] = @Name AND ItemCategoryId = @ItemCategoryId AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				UPDATE
					[master].ItemSubCategories
				SET
					[Name] = @Name,
					ItemCategoryId = @ItemCategoryId,
					Rate = @Rate,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id
					AND SubscriberId = @SubscriberId;
			END
		END

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
		RETURN @ReturnValue;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
		RETURN @ReturnValue;
	END CATCH;
