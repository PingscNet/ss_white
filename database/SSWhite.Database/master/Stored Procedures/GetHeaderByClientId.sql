﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetHeaderByClientId
Comments		: 06-04-2021 | Amit Khanna | This procedure is used to get headers by client and module type.

Test Execution	: EXEC master.GetHeaderByClientId
					@SubscriberId = 1,
					@ClientId = 5,
					@ModuleType = 4,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetHeaderByClientId]
(
	@SubscriberId INT,
	@ClientId INT,
	@ModuleType TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetHeaderByClientId]','
										@SubscriberId =',@SubscriberId,',
										@ClientId = ',@ClientId,',
										@ModuleTye = ',@ModuleType,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetHeaderByClientId]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
		SELECT
			ct.HeaderId,
			hdr.[Name],
			hdt.CalculationType,
			ct.[Value] AS [Percentage]
		FROM 
			[master].ClientHeaderDetails AS ct
			INNER JOIN [master].Headers AS hdr ON ct.HeaderId = hdr.Id 
			INNER JOIN [master].HeaderDetails AS hdt ON ct.HeaderId = hdt.HeaderId AND ct.ModuleId = hdt.ModuleId
			INNER JOIN subscriber.Modules AS mdl ON ct.ModuleId = mdl.Id
		WHERE
			ct.ClientId = @ClientId
			AND mdl.ModuleType = @ModuleType
		ORDER BY
			hdr.[Name] ASC;
		
		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
