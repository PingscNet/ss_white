﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateSize
Comments		: 04-06-2021 | Tanvi Pathak | This procedure is used to insert size or update size by Id.

Test Execution	: DECLARE @ReturnValue INT = 1;
					EXEC @ReturnValue = [master].[InsertOrUpdateSize]
					@SubscriberId =1,
					@Id = NULL,
					@SizeName ='16X16',
					@PackPerBox = '10.00',
					@WeightPerBox = '200.000',
					@IsInch = '16.00',
					@LInch = '16.00',
					@BInch = '16.00',
					@IsCM = '16.00',
					@LCM ='16.00',
					@BCM = '16.00',
					@IsMM = '16.00',
					@LMM = '16.00',
					@BMM = '16.00',
					@SqureFeet = '20.000',
					@SqureMeter = '20.000',
					@IpAddress =NULL,
					@CreatedBy = 1,
					@CreatedDate = '2021-06-04',
					@StatusTypeActive =1
					SELECT @ReturnValue;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateSize]
(
	@SubscriberId INT,
	@Id INT,
	@SizeName VARCHAR(100),
    @PackPerBox DECIMAL(18,2),
    @WeightPerBox DECIMAL(18,3),
    @IsInch DECIMAL(18,2),
    @LInch DECIMAL(18,2),
    @BInch DECIMAL(18,2),
    @IsCM DECIMAL(18,2),
    @LCM DECIMAL(18,2),
    @BCM DECIMAL(18,2),
    @IsMM DECIMAL(18,2),
    @LMM DECIMAL(18,2),
    @BMM DECIMAL(18,2),
    @SqureFeet DECIMAL(18,3),
    @SqureMeter DECIMAL(18,3),
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateSize]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@SizeName =''',@SIzeName,''',
										@PackPerBox = ',@PackPerBox,',
										@WeightPerBox = ',@WeightPerBox,',
										@IsInch = ',@IsInch,',
										@LInch = ',@LInch,',
										@BInch = ',@BInch,',
										@IsCM = ',@IsCM,',
										@LCM =',@LCM,',
										@BCM = ',@BCM,',
										@IsMM = ',@IsMM,',
										@LMM = ',@LMM,',
										@BMM = ',@BMM,',
										@SqureFeet = ',@SqureFeet,',
										@SqureMeter = ',@SqureMeter,',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateSize]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1
		
    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Sizes WHERE SubscriberId = @SubscriberId AND [SizeName] = @SizeName AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO [master].Sizes
				(
					SubscriberId,
					[SizeName],    
					[PackPerBox],  
					[WeightPerBox], 
					[IsInch],       
					[LInch],        
					[BInch],       
					[IsCM],      
					[LCM],    
					[BCM],       
					[IsMM],       
					[LMM],      
					[BMM],    
					[SqureFeet],  
					[SqureMeter],  
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@SizeName,
					@PackPerBox,
					@WeightPerBox,
					@IsInch,
					@LInch,
					@BInch,
					@IsCM,
					@LCM,
					@BCM,
					@IsMM,
					@LMM,
					@BMM,
					@SqureFeet,
					@SqureMeter,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Sizes WHERE SubscriberId = @SubscriberId AND [SizeName] = @SizeName AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				UPDATE
					[master].Sizes
				SET
					[SizeName] = @SIzeName,
					[PackPerBox] = @PackPerBox,
					[WeightPerBox] = @WeightPerBox,
					[IsInch] = @IsInch,
					[LInch] = @LInch,
					[BInch] = @BInch,
					[IsCM] = @IsCM,
					[LCM] = @LCM,
					[BCM] = @BCM,
					[IsMM] = @IsMM,
					[LMM] = @LMM,
					[BMM] = @BMM,
					[SqureFeet] = @SqureFeet,
					[SqureMeter] = @SqureMeter,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
