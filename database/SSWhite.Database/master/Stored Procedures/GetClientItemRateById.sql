﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetClientItemRateById
Comments		: 23-06-2021 | Amit Khanna | This procedure is used to get client Item Rate by Id.

Test Execution	: EXEC master.GetClientItemRateById
					@SubscriberId =  1,
					@Id =  1,
					@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetClientItemRateById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetClientItemRateById]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetClientItemRateById]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			cmp.Id,
			cmp.ClientId,
			ct.CompanyName AS ClientName
		FROM
			[master].ClientItemRates AS cmp
			INNER JOIN [master].Clients AS ct ON cmp.ClientId = ct.Id
		WHERE
			cmp.Id = @Id;

		SELECT
			ci.ItemId,
			ci.Rate,
			it.[Name] AS ItemName
		FROM
			[master].ClientItemRateDetails AS ci
			INNER JOIN [master].Items AS it ON ci.ItemId = it.Id
		WHERE
			ci.ClientItemRateId = @Id;
			

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
