﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllLedgerGroups
Comments		: 30-03-2021 | Vikas Pastel | This procedure is used to get All Ledger Groups.

Test Execution	: EXEC master.GetAllLedgerGroups
					@SubscriberId = 1,
					@Status = NULL,
					@ItemGroupId = NULL,
					@ItemCategoryId = NULL,
					@UnitId = NULL,
					@CrateId = NULL,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@IpAddress = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetAllLedgerGroups]
(
	@SubscriberId INT,
	@Status TINYINT,
	@LedgerGroupType TINYINT,
	@LedgerAccountType TINYINT,	
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@Start INT,
	@Length INT,
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetAllLedgerGroups]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,',
										@LedgerGroupType = ',@LedgerGroupType,',
										@LedgerAccountType =',@LedgerAccountType,',										
										@Start = ',@Start,',
										@Length = ',@Length,',
										@SearchKeyword = ','',@SearchKeyword,'',',
										@SortExpression = ','',@SortExpression,'',',
										@IpAddress = ','',@IpAddress,'',',
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[master].[GetAllLedgerGroups]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT); 
    BEGIN TRY
		
		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[master].[LedgerGroups] AS us
		WHERE 
			us.SubscriberId = @SubscriberId
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.[Type] = ISNULL(@LedgerGroupType,us.Type)
			AND us.AccountType = ISNULL(@LedgerAccountType,us.AccountType)			
			AND us.EndDate IS NULL
			AND 
			(
					us.[Name] LIKE '%' + ISNULL(@SearchKeyword,us.[Name]) +'%'
					OR us.[Code] LIKE '%' + ISNULL(@SearchKeyword,us.[Code]) +'%'
			);

		SELECT @TotalRecords =COUNT(Id)  FROM #TempTable 

		SELECT
			us.Id,
			us.[Name],
			us.Code,			
			us.[Type] AS LedgerGroupType,
			us.[AccountType] AS LedgerAccountType,			
			us.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN [master].LedgerGroups us ON tmp.Id = us.Id
			
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'name asc' THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'name desc' THEN us.[Name] END DESC,
			CASE WHEN @SortExpression = 'code asc' THEN us.[Code] END ASC,
			CASE WHEN @SortExpression = 'code desc' THEN us.[Code] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
