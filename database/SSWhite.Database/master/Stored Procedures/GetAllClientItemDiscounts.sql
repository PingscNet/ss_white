﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllClientItemDiscounts
Comments		: 23-06-2021 | Tanvi Pathak | This procedure is used to get All Clients.

Test Execution	: EXEC master.GetAllClientItemDiscounts
					@SubscriberId = 1,
					@Status = NULL,
					@ClientId = NULL,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@IpAddress = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetAllClientItemDiscounts]
(
	@SubscriberId INT,
	@Status TINYINT,
	@ClientId INT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetAllClientItemDiscounts]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,',
										@ClientId = ',@ClientId,',
										@Start = ',@Start,',
										@Length = ',@Length,',
										@SearchKeyword = ','',@SearchKeyword,'',',
										@SortExpression = ','',@SortExpression,'',',
										@IpAddress = ','',@IpAddress,'',',
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[master].[GetAllClientItemDiscounts]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT); 
    BEGIN TRY
		
		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[master].[ClientItemDiscounts] AS us
			INNER JOIN [master].Clients AS ct ON us.ClientId = ct.Id
		WHERE 
			us.SubscriberId = @SubscriberId
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.ClientId = ISNULL(@ClientId,us.ClientId)
			AND us.EndDate IS NULL
			AND 
			(
					ct.[CompanyName] LIKE '%' + ISNULL(@SearchKeyword,ct.[CompanyName]) +'%'
			);

		SELECT @TotalRecords =COUNT(Id)  FROM #TempTable 

		SELECT
			cr.Id,
			cr.ClientId,
			us.CompanyName AS ClientName,
			cr.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN [master].ClientItemDiscounts AS cr ON tmp.Id = cr.Id
			INNER JOIN [master].Clients us ON cr.ClientId = us.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'companyName asc' THEN us.[CompanyName] END ASC,
			CASE WHEN @SortExpression = 'companyName desc' THEN us.[CompanyName] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
