﻿CREATE TABLE [master].[ClientOpeningBalance] (
    [Id]            INT             IDENTITY (1, 1) NOT NULL,
    [SubscriberId]  INT             NOT NULL,
    [ClientId]      INT             NOT NULL,
    [Balance]       DECIMAL (18, 2) NULL,
    [Type]          TINYINT         NULL,
    [FinancialYear] INT             NOT NULL,
    [IpAddress]     VARCHAR (45)    NULL,
    [CreatedBy]     INT             NOT NULL,
    [CreatedDate]   SMALLDATETIME   NOT NULL,
    [UpdatedBy]     INT             NULL,
    [UpdatedDate]   SMALLDATETIME   NULL,
    CONSTRAINT [PK__ClientOp__3214EC0715FFC55D] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ClientOpe__Clien__613C58EC] FOREIGN KEY ([ClientId]) REFERENCES [master].[Clients] ([Id]),
    CONSTRAINT [FK__ClientOpe__Creat__62307D25] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__ClientOpe__Subsc__604834B3] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__ClientOpe__Updat__6324A15E] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

