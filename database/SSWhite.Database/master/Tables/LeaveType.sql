﻿CREATE TABLE [master].[LeaveType] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]   VARCHAR (255) NOT NULL,
    [Status] TINYINT       NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

