﻿CREATE TABLE [master].[ClientItemRateDetails] (
    [Id]               INT             IDENTITY (1, 1) NOT NULL,
    [ClientItemRateId] INT             NOT NULL,
    [ItemId]           INT             NOT NULL,
    [Rate]             DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__ClientIt__3214EC0731A2B5A8] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ClientIte__Clien__57FD0775] FOREIGN KEY ([ClientItemRateId]) REFERENCES [master].[ClientItemRates] ([Id]),
    CONSTRAINT [FK__ClientIte__ItemI__58F12BAE] FOREIGN KEY ([ItemId]) REFERENCES [master].[Items] ([Id])
);

