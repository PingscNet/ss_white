﻿CREATE TABLE [master].[Taxes] (
    [Id]           INT             IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT             NOT NULL,
    [Name]         VARCHAR (100)   NOT NULL,
    [Igst]         DECIMAL (18, 2) NOT NULL,
    [Cgst]         DECIMAL (18, 2) NOT NULL,
    [Sgst]         DECIMAL (18, 2) NOT NULL,
    [Cess]         DECIMAL (18, 2) NULL,
    [Status]       TINYINT         NOT NULL,
    [EndDate]      SMALLDATETIME   NULL,
    [IpAddress]    VARCHAR (45)    NULL,
    [CreatedBy]    INT             NOT NULL,
    [CreatedDate]  SMALLDATETIME   NOT NULL,
    [UpdatedBy]    INT             NULL,
    [UpdatedDate]  SMALLDATETIME   NULL,
    CONSTRAINT [PK__Taxes__3214EC0730DF7DFD] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Taxes__CreatedBy__795DFB40] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__Taxes__Subscribe__7869D707] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__Taxes__UpdatedBy__7A521F79] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

