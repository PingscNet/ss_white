﻿CREATE TABLE [master].[LedgerGroups] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [Name]         VARCHAR (50)  NOT NULL,
    [Code]         VARCHAR (50)  NOT NULL,
    [Type]         TINYINT       NOT NULL,
    [AccountType]  TINYINT       NOT NULL,
    [Order]        TINYINT       NULL,
    [Sections]     VARCHAR (120) NULL,
    [Status]       TINYINT       NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT           NOT NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT           NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__LedgerGr__3214EC078B115E90] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__LedgerGro__Creat__4CF5691D] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__LedgerGro__Subsc__4C0144E4] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__LedgerGro__Updat__4DE98D56] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

