﻿CREATE TABLE [master].[ClientBankDetails] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [ClientId]      INT           NOT NULL,
    [Name]          VARCHAR (100) NOT NULL,
    [AccountNumber] VARCHAR (18)  NOT NULL,
    [Branch]        VARCHAR (50)  NOT NULL,
    [IfscCode]      VARCHAR (11)  NOT NULL,
    [IsDefault]     BIT           NOT NULL,
    CONSTRAINT [PK__ClientBa__3214EC07DF761CD0] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ClientBan__Clien__451F3D2B] FOREIGN KEY ([ClientId]) REFERENCES [master].[Clients] ([Id])
);

