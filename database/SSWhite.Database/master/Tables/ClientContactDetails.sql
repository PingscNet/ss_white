﻿CREATE TABLE [master].[ClientContactDetails] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [ClientId]     INT           NOT NULL,
    [Name]         VARCHAR (100) NOT NULL,
    [MobileNumber] VARCHAR (20)  NULL,
    [Phone]        VARCHAR (20)  NULL,
    [EmailAddress] VARCHAR (80)  NULL,
    CONSTRAINT [PK__ClientCo__3214EC075A40E5A7] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ClientCon__Clien__4CC05EF3] FOREIGN KEY ([ClientId]) REFERENCES [master].[Clients] ([Id])
);

