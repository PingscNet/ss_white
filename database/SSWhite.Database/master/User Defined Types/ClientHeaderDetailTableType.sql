﻿CREATE TYPE [master].[ClientHeaderDetailTableType] AS TABLE (
    [HeaderId] INT             NOT NULL,
    [ModuleId] INT             NOT NULL,
    [Value]    DECIMAL (18, 2) NULL);

