﻿
CREATE TYPE document.ApprovalNotification AS TABLE (
    [Id]                 INT             NULL,
 Name VARCHAR(255),
 UserId INT,
 PersonId VARCHAR(255)
 );

