﻿/*------------------------------------------------------------------------------------------------------------
Name			: DeleteReceipt
Comments		: 27-04-2021 | Amit Khanna | This procedure is used to delete receipt by Id.

Test Execution	: EXEC [document].[DeleteReceipt]
						@SubscriberId =  1,
						@Id =  5,
						@IpAddress =  NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[DeleteReceipt]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[DeleteReceipt]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].[DeleteReceipt]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY
		BEGIN TRAN
		
		DELETE FROM
			document.ReceiptHeaders
		WHERE
			ReceiptId = @Id;

		DELETE FROM 
			document.Receipts
		WHERE
			Id = @Id;

		COMMIT TRAN;
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
