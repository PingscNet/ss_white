﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetReceiptById
Comments		: 27-04-2021 | Amit Khanna | This procedure is used to get receipt by Id.

Test Execution	: EXEC [document].GetReceiptById
					@SubscriberId =  1,
					@Id =  3,
					@IpAddress =  NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetReceiptById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].GetReceiptById','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].GetReceiptById',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			pd.Id,
			pd.ReceiptNumber,
			pd.CompanyId,
			pd.ClientId,
			pd.DayBookId,
			ct.[CompanyName] AS ClientName,
			cd.[CompanyName] AS DayBook,
			pd.[Date],
			pd.[Type] AS ReceiptType,
			pd.[Month],
			pd.FinancialYear,
			pd.ReceivedAmount,
			pd.TotalAmount,
			bd.[CompanyName] AS BankName,
			pd.BankId,
			pd.ChequeNumber,
			pd.Branch,
			pd.IsReturnedCheque,
			pd.ChequeReturnedDate,
			pd.Remarks
		FROM
			[document].Receipts AS pd
			INNER JOIN [master].Clients AS ct ON pd.ClientId = ct.Id
			INNER JOIN [master].Clients cd ON pd.DayBookId = cd.Id
			LEFT JOIN [master].Clients bd ON pd.BankId = bd.Id
		WHERE
			pd.Id = @Id;

		SELECT
			phdr.HeaderId,
			hdr.[Name] AS [Name],
			phdr.CalculationType,
			phdr.[Percentage],
			phdr.[Value]
		FROM
			[document].ReceiptHeaders phdr
			INNER JOIN [master].Headers AS hdr ON phdr.HeaderId = hdr.Id
		WHERE
			ReceiptId = @Id;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
