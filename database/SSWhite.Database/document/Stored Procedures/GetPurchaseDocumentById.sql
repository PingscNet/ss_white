﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetPurchaseDocumentById
Comments		: 01-07-2021 | Tanvi Pathak | This procedure is used to get purchase document by Id.

Test Execution	: EXEC [document].GetPurchaseDocumentById
					@SubscriberId =  1,
					@Id =  2,
					@IpAddress =  NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetPurchaseDocumentById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].GetPurchaseDocumentById','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].GetPurchaseDocumentById',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			pd.Id,
			pd.ClientId,
			ct.[CompanyName] AS ClientName,
			pd.VoucherNumber,
			pd.DocumentNumber,
			pd.[Date],
			pd.Remarks,
			pd.GrossTotal,
			pd.TaxAmount,
			pd.OtherAmount,
			pd.Discount,
			pd.GrandAmount,
			pd.BillAmount,
			pd.CashAmount,
			pd.ActualVoucherNumber
		FROM
			[document].PurchaseDocuments AS pd
			INNER JOIN [master].Clients AS ct ON pd.ClientId = ct.Id
		WHERE
			pd.Id = @Id;

		SELECT
			pdi.ItemId,
			pdi.ItemCategoryId,
			pdi.ItemSubCategoryId,
			pdi.SizeId,
			pdi.BrandId,
			pdi.GradeId,
			pdi.TaxId,
			pdi.Qty,
			pdi.Rate,
			pdi.Total
		FROM
			[document].PurchaseDocumentItems AS pdi

		WHERE
			pdi.PurchaseDocumentId = @Id

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
