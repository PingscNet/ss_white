﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC  [document].[GetAssociateDocuments]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 TEst Execution :	[document].[GetAssociateDocuments] 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE  [document].[GetAssociateDocuments]
(
	@StatusTypeActive int
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		d.Id,
		d.DocumentId,
		d.Documenttitle,
		d.Rev,
		vms.PersonName,
		d.CreatedDate,
		d.ApprovalStatus,
		dt.Name AS DocumentType
	FROM 
		document.Documents d
		INNER JOIN 
			[emp].[VMSEmployeeDetails] vms ON vms.UserId = d.CreatedBY
		INNER JOIN 
			document.DocumentType dt ON dt.Id = d.DocumentType
	WHERE 
		IsLatest = @StatusTypeActive
		AND d.Status = @StatusTypeActive
	ORDER BY Id DESC
END 

