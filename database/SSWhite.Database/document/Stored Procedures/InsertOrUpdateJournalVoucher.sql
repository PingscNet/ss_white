﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateJournalVoucher
Comments		: 09-04-2021 | Amit Khanna | This procedure is used to insert or update Sales document.

Test Execution	: EXEC document.InsertOrUpdateJournalVoucher
						@SubscriberId = 1,
						@FinancialYear = 2,
						@Id = 1,
						@CompanyId = 1,
						@CreditedClientId = 1,
						@DebitedClientId = 2,
						@VoucherNumber = 1,
						@Date = '2022-01-01',
						@CreditedReferenceNumber = NULL,
						@CreditedReferenceDate = NULL,
						@DebitedReferenceNumber = NULL,
						@DebitedReferenceDate = NULL,
						@Month = 4,
						@TotalAmount = 410.05,
						@Remarks = NULL,
						@Prefix = NULL,
						@Suffix = NULL,
						@IpAddress = NULL,
						@CreatedBy = 1,
						@CreatedDate  = '2021-01-01',
						@DocumentTypeJournalVoucher = 3;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[InsertOrUpdateJournalVoucher]
(
	@SubscriberId INT,
	@FinancialYear INT,
	@Id INT,
	@CompanyId INT,
	@CreditedClientId INT,
	@DebitedClientId INT,
	@VoucherNumber VARCHAR(MAX),
	@Date DATE,
	@TotalAmount DECIMAL(18,2),
	@CreditedReferenceNumber VARCHAR(100),
	@CreditedReferenceDate DATE,
	@DebitedReferenceNumber VARCHAR(100),
	@DebitedReferenceDate DATE,
	@Month TINYINT,
	@Remarks VARCHAR(100),
	@Prefix VARCHAR(MAX),
	@Suffix VARCHAR(MAX),
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT,
	@DocumentTypeJournalVoucher TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC document.InsertOrUpdateJournalVoucher','
										@SubscriberId =',@SubscriberId,',
										@FinancialYear =',@FinancialYear,',
										@Id =',@Id,',
										@CompanyId =',@CompanyId,',
										@CreditedClientId =',@CreditedClientId,',
										@DebitedClientId =',@DebitedClientId,',
										@VoucherNumber =''',@VoucherNumber,''',
										@Date =',@Date,',
										@TotalAmount =',@TotalAmount,',
										@CreditedReferenceNumber =',@CreditedReferenceNumber,',
										@CreditedReferenceDate =',@CreditedReferenceDate,',
										@DebitedReferenceNumber =',@DebitedReferenceNumber,',
										@DebitedReferenceDate =',@DebitedReferenceDate,',
										@TotalAmount =',@TotalAmount,',
										@Month =',@Month,',
										@Remarks =''',@Remarks,''',
										@Prefix = ','',@Prefix,'','
										@Suffix = ','',@Suffix,'','
										@IpAddress = ,','',@IpAddress,'','
										@CreatedBy = ,',@CreatedBy,',
										@CreatedDate = ,',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive,',
										@DocumnetTypeJournalVoucher = ,',@DocumentTypeJournalVoucher
									  ),

			@ProcedureName = 'document.InsertOrUpdateJournalVoucher',
			@ExecutionTime = GETDATE()	
			
	DECLARE @LastNumber INT;
	DECLARE @ReturnValue  INT = 1;
    BEGIN TRY
		EXEC @LastNumber = [subscriber].GetLastNumber
									@SubscriberId = @SubscriberId,
									@EntityId = @CompanyId,
									@FinancialYear = @FinancialYear,
									@DocumentType = @DocumentTypeJournalVoucher,
									@ModuleType = NULL,
									@Status = @StatusTypeActive,
									@IpAddress = @IpAddress;
		
		SET @VoucherNumber = CONCAT(@Prefix,@LastNumber,@Suffix);
		
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [document].JournalVouchers WHERE SubscriberId = @SubscriberId AND [VoucherNumber] = @VoucherNumber AND CompanyId = @CompanyId AND FinancialYear = @FinancialYear AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO [document].JournalVouchers
				(
					SubscriberId,
					CompanyId,
					VoucherNumber,
					[Date],
					CreditedClientId,
					DebitedClientId,
					CreditedReferenceNumber,
					CreditedReferenceDate,
					DebitedReferenceNumber,
					DebitedReferenceDate,
					[Month],
					FinancialYear,
					TotalAmount,
					Remarks,
					ActualVoucherNumber,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@CompanyId,
					@VoucherNumber,
					@Date,
					@CreditedClientId,
					@DebitedClientId,
					@CreditedReferenceNumber,
					@CreditedReferenceDate,
					@DebitedReferenceNumber,
					@DebitedReferenceDate,
					@Month,
					@FinancialYear,
					@TotalAmount,
					@Remarks,
					@LastNumber,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);

				UPDATE
					subscriber.NumberConfigurations
				SET
					LastNumber = @LastNumber,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					SubscriberId = @SubscriberId
					AND EntityId = @CompanyId
					AND DocumentType = @DocumentTypeJournalVoucher
					AND FinancialYear = @FinancialYear;

				COMMIT TRAN;
			END
		END
		ELSE
		BEGIN
			BEGIN TRAN
			UPDATE
				[document].JournalVouchers
			SET
				CreditedClientId = @CreditedClientId,
				DebitedClientId = @DebitedClientId,
				CreditedReferenceNumber = @CreditedReferenceNumber,
				CreditedReferenceDate = @CreditedReferenceDate,
				DebitedReferenceNumber = @DebitedReferenceNumber,
				DebitedReferenceDate = @DebitedReferenceDate,
				[Date] = @Date,
				[Month] = @Month,
				TotalAmount = @TotalAmount,
				Remarks = @Remarks,
				IpAddress = @IpAddress,
				UpdatedBy = @CreatedBy,
				UpdatedDate = @CreatedDate
			WHERE
				Id = @Id;

			COMMIT TRAN;
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
