﻿USE [db_a5dc4b_sswhite]
GO
/****** Object:  StoredProcedure [emp].[GetMpcVOteDetailsByEpoId]    Script Date: 6/3/2022 5:51:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC [document].[GetDocumentsDropdownAndUserDetails]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]
 TEst Execution :	 [document].[GetEmployeeByDepartment] 'CNC'
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [document].[GetEmployeeByDepartment]
(
	@DepartmentName VARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		UserId AS Id,
		PersonName
		--Department
	FROM 
		emp.VMSEmployeeDetails
	WHERE 
		Department = @DepartmentName
END 
