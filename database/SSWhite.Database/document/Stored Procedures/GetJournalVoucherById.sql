﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetJournalVoucherById
Comments		: 11-05-2021 | Amit Khanna | This procedure is used to get journal voucher by Id.

Test Execution	: EXEC [document].GetJournalVoucherById
					@SubscriberId =  1,
					@Id =  10,
					@IpAddress =  NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetJournalVoucherById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].GetJournalVoucherById','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].GetJournalVoucherById',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			pd.Id,
			pd.CompanyId,
			pd.VoucherNumber,
			pd.[Date],
			pd.CreditedClientId,
			ct.[CompanyName] AS CreditedClientName,
			pd.DebitedClientId,
			dct.[CompanyName] AS DebitedClientName,
			pd.CreditedReferenceNumber,
			pd.CreditedReferenceDate,
			pd.DebitedReferenceNumber,
			pd.DebitedReferenceDate,
			pd.[Month],
			pd.FinancialYear,
			pd.TotalAmount,
			pd.Remarks
		FROM
			[document].JournalVouchers AS pd
			INNER JOIN [master].Clients AS ct ON pd.CreditedClientId = ct.Id
			INNER JOIN [master].Clients AS dct ON pd.DebitedClientId = dct.Id
		WHERE
			pd.SubscriberId = @SubscriberId
			AND pd.Id = @Id;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
