﻿CREATE TABLE [document].[PaymentHeaders] (
    [Id]              INT             IDENTITY (1, 1) NOT NULL,
    [PaymentId]       INT             NOT NULL,
    [HeaderId]        INT             NOT NULL,
    [CalculationType] TINYINT         NOT NULL,
    [Percentage]      DECIMAL (18, 2) NOT NULL,
    [Value]           DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__PaymentH__3214EC07004B192F] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__PaymentHe__Heade__70B3A6A6] FOREIGN KEY ([HeaderId]) REFERENCES [master].[Headers] ([Id]),
    CONSTRAINT [FK__PaymentHe__Payme__6FBF826D] FOREIGN KEY ([PaymentId]) REFERENCES [document].[Payments] ([Id])
);

