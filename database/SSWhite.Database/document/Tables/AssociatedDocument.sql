﻿
CREATE TABLE document.AssociatedDocument
(
 Id INT IDENTITY(1,1) PRIMARY KEY,
 DocumentId INT FOREIGN KEY REFERENCES document.Documents(Id),
 AssociatedDocumentId varchar(255),
 Rev VARCHAR(255),
 Title VARCHAR(1000),
)

