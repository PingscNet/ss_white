﻿CREATE TABLE document.Attachment
(
 Id INT IDENTITY(1,1) PRIMARY KEY,
 DocumentId INT FOREIGN KEY REFERENCES document.Documents(Id),
 LocalFileName varchar(1000),
 Title VARCHAR(255),
 AttachmentPath VARCHAR(1000),
)