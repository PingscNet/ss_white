﻿create table document.EcnChange
(
 Id int IDENTITY(1,1) PRIMARY KEY,
 Name varchar(255) NOT NULL,
 Status tinyint NOT NULL,
 CreatedBy  int ,
 CreatedDate  DateTime ,
 ModifiedBy  int,
 ModifiedDate  DateTime
)


insert into document.EcnChange values('Acceptance change',1,41,getdate(),NULL,NULL);
insert into document.EcnChange values('BOM/BOR',1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('C of C',1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('CUSTOMER NOTIFICATION',1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('CNC PROGRAM',1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Dispose/Purge',1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Drawing'				,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('EPICOR BOM'				,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('EPICOR MOM'				,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('EPICOR PART'			,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('FAIR'					,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Form Control'			,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Instructions'			,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Inspection Test Plan'	,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Label/Lasermark Program',1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('MAFIA'					,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('MPC Plan'				,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('PMA Approval'			,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('PPAP'					,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('QD Table'				,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Rework Inventory'		,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Supplier Notification'	,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Update Open Orders'		,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Use To'					,1,41,getdate(),NULL,NULL)
insert into document.EcnChange values('Other'					,1,41,getdate(),NULL,NULL)