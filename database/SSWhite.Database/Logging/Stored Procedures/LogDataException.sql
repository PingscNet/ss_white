﻿/*------------------------------------------------------------------------------------------------
ProcedureName	:	LogDataException
Comments        :	18-04-2021 | Amit Khanna | This procedure is used to log application exceptions with data.

Test Execution	:	EXEC [logging].[LogDataException] 
						@SubscriberId ='',
						@Source ='',
						@StackTrace ='',
						@InnerException='',
						@Message ='',
						@Data ='',
						@FileName = '',
						@IpAdress = ''
---------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [Logging].[LogDataException]
(
	@SubscriberId INT,
	@Source VARCHAR(MAX),
	@StackTrace VARCHAR(MAX),
	@InnerException VARCHAR(MAX),
	@Message VARCHAR(MAX),
	@Data VARCHAR(MAX),
	@FileName VARCHAR(MAX),
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(200),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [logging].[LogDataException]','
										@SubscriberId =''',@SubscriberId,''',
										@IpAddress =''',@IpAddress,''',
										@Source =''',@Source,''',
										@StackTrace =''',@StackTrace,''',
										@InnerException =''',@InnerException,''',
										@Message =''',@Message,''',
										@Data =''',@Data,''',
										@FileName =''',@FileName,''
									  ),
			@ProcedureName = '[logging].[LogDataException]',
			@ExecutionTime = GETDATE()	
    BEGIN TRY
		INSERT INTO logging.Exceptions
		(
			SubscriberId,
			[Source],
			StackTrace,
			InnerException,
			[Message],
			[Data],
			[FileName],
			IpAddress,
			Stamp
		)	
		VALUES
		(
			@SubscriberId,
			@Source,
			@StackTrace,
			@InnerException,
			@Message,
			@Data,
			@FileName,
			@IpAddress,
			GETDATE()		
		)

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH
	;
