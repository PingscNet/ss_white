﻿/*------------------------------------------------------------------------------------------------
ProcedureName	:	LogException
Comments        :	26-02-2021 | Amit Khanna | This procedure is used to log application exceptions.

Test Execution	:	EXEC [logging].[LogExceptions] 
						@FileName ='',
						@Source ='',
						@StackTrace ='',
						@InnerException='',
						@Message ='',
						@RequestedData =''
---------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [Logging].[LogException]
(
	@SubscriberId INT,
	@IpAddress VARCHAR(45),
	@Source VARCHAR(MAX),
	@StackTrace VARCHAR(MAX),
	@InnerException VARCHAR(MAX),
	@Message VARCHAR(MAX),
	@Data VARCHAR(MAX),
	@FileName VARCHAR(MAX)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(200),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [logging].[LogException]','
										@SubscriberId =',@FileName,',
										@IpAddress =''',@FileName,''',
										@Source =''',@Source,''',
										@StackTrace =''',@StackTrace,''',
										@InnerException =''',@InnerException,''',
										@Message =''',@Message,''',
										@Data =''',@Data,''',
										@FileName =''',@FileName,''
									  ),
			@ProcedureName = '[logging].[LogException]',
			@ExecutionTime = GETDATE()	
    BEGIN TRY
		INSERT INTO logging.Exceptions
		(
			SubscriberId,
			Source,
			StackTrace,
			InnerException,
			[Message],
			[Data],
			[FileName],
			IpAddress,
			Stamp
		)	
		VALUES
		(
			@SubscriberId,
			@Source,
			@StackTrace,
			@InnerException,
			@Message,
			@Data,
			@FileName,
			@IpAddress,
			GETDATE()		
		)

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH
	;
