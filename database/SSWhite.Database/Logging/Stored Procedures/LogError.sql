﻿/*-------------------------------------------------------------------------------------
ProcedureName : LogError
Comments      : Amit Khanna | 26-02-2021 | This procedure Logs Error Execution
--------------------------------------------------------------------------------------*/
CREATE PROCEDURE [Logging].[LogError]
(
	@SubscriberId INT,
	@IpAddress VARCHAR(45),
	@ErrorNumber VARCHAR(MAX),
	@ErrorSeverity VARCHAR(MAX),
	@ErrorLine  VARCHAR(MAX),
	@ProcedureName VARCHAR(200),
	@ExecutionCommand VARCHAR(MAX),
	@Message VARCHAR(MAX),
	@ErrorState INT
)

AS
	SET NOCOUNT ON;
     BEGIN TRY
         INSERT	INTO [logging].[ErrorLogs]
			(	
				SubscriberId,
				ErrorNumber,
				ErrorSeverity,
				ErrorLine,
				ProcedureName,
				ExecutionCommand,
				[Message],
				[ErrorState],
				IpAddress,
				Stamp
			)
			VALUES
			(
				@SubscriberId,
				@ErrorNumber,
				@ErrorSeverity,
				@ErrorLine,
				@ProcedureName,
				@ExecutionCommand,
				@Message,
				@ErrorState,
				@IpAddress,
				GETDATE()
			)
	END TRY
	BEGIN CATCH
		THROW 00001, 'Not able to insert error log.',1;
	END CATCH
	;	
