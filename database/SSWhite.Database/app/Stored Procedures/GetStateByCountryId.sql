﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetStateByCountryId
Comments		: 26-03-2021 | Amit Khanna | This procedure is used to get states by CountryId.

Test Execution	: EXEC app.GetStateByCountryId
					@SubscriberId = 1,
					@StateId = 11,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [app].[GetStateByCountryId]
(
	@SubscriberId INT,
	@CountryId INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [app].[GetStateByCountryId]','
										@SubscriberId =',@SubscriberId,',
										@CountryId = ',@CountryId,'
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[app].[GetStateByCountryId]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
		SELECT
			st.Id,
			st.[Name],
			st.[Status]
		FROM
			app.States AS st
		WHERE
			st.[CountryId] = ISNULL(@CountryId,st.[CountryId])
		ORDER BY
			st.[Name] ASC;
		
		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
