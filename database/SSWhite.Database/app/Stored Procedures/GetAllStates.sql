﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllStates
Comments		: 26-03-2021 | Amit Khanna | This procedure is used to get All states for commmon call.

Test Execution	: EXEC app.GetAllStates
					@SubscriberId = 1,
					@CountryId = NULL,
					@Status = NULL,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [app].[GetAllStates]
(
	@SubscriberId INT,
	@CountryId INT,
	@Status TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [app].[GetAllStates]','
										@SubscriberId =',@SubscriberId,',
										@CountryId = ',@CountryId,',
										@Status = ',@Status,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[app].[GetAllStates]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
		SELECT
			st.Id,
			st.[Name],
			st.[Status]
		FROM
			app.States AS st
		WHERE
			st.[CountryId] = ISNULL(@CountryId,st.CountryId)
			AND st.[Status] = ISNULL(@Status,st.[Status])
		ORDER BY 
			st.[Name] ASC;
		
		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
