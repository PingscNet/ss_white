﻿CREATE TABLE [app].[Countries] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (50)  NOT NULL,
    [Status]      TINYINT       CONSTRAINT [DF__Countries__Statu__19AACF41] DEFAULT ((1)) NOT NULL,
    [EndDate]     SMALLDATETIME NULL,
    [CreatedBy]   INT           NULL,
    [CreatedDate] SMALLDATETIME CONSTRAINT [DF__Countries__Creat__1A9EF37A] DEFAULT (getdate()) NOT NULL,
    [UpdatedBy]   INT           NULL,
    [UpdatedDate] SMALLDATETIME NULL,
    CONSTRAINT [PK__Countrie__3214EC077D8C0136] PRIMARY KEY CLUSTERED ([Id] ASC)
);

