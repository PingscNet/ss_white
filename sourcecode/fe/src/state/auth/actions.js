import axios from "../../../node_modules/axios";

export default {
    async logIn(context, payload) {
        try {
            const response = await axios.post("authenticate/token", {
                userName: payload.userName,
                password: payload.password
            });

            if (response.status == "200") {
                const responseData = response.data;
                context.commit('setAuthToken', {
                    authToken: responseData.authToken,
                    expiringInMinutes: responseData.expiringInMinutes
                });
                axios.defaults.headers.common['auth-token'] = responseData.authToken;
                axios.defaults.headers.common['source-type'] = 'web';
            } else {
                throw response;
            }
        }
        catch (error) {
            throw error;
        }
    },
    async logOut(context) {

        try {

            const response = await axios.post("Authenticate/logOut");

            if (response.status == "200") {
                context.commit('deleteAuthToken');
                delete axios.defaults.headers.common['auth-token'];
            }
        } catch (error) {
            throw error;
        }
    }
};