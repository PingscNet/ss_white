var path = require('path')
module.exports = {
  build: {
    // ...
  },
  buildStaging: {
    env: require('./staging.env'),
    // ...
  },
  buildProduction: {
    env: require('./prod.env'),
    // ...
  },
  dev: {
    env: require('./dev.env'),
    port: 8080,
    // ...
  }
}