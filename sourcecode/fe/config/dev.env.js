module.exports = {
    NODE_ENV: '"development"',
    VUE_APP_BASE_URL: '"https://localhost:44326/v1.0/"',
    STRIPE_TOKEN: '"YOUR_TOKEN"'
  }