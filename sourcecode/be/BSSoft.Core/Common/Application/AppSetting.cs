﻿namespace SSWhite.Core.Common.Application
{
    public class AppSetting
    {
        public int Pagination { get; set; }

        public string DefaultPath { get; set; }

        public string DefaultCompanyLogoPath { get; set; }

        public string DefaultExceptionFilePath { get; set; }

        public bool IsLogRequest { get; set; }

        public string AvatarRootPathToStoreImage { get; set; }

        public string AttachmentRootPathToStoreImage { get; set; }

        public string AttachmentRootPathToGetImage { get; set; }

        public string DocumentAttachmentRootPathImage { get; set; }

        public string DocumentAttachmentPathToGetImage { get; set; }

        public string FtoApprovalLink { get; set; }

        public string EpoApprovalLink { get; set; }

        public string DocumentApprovalLink { get; set; }

        public string CreateUserPassword { get; set; }

        public string ForgetPasswordLink { get; set; }

        public Pdf Pdf { get; set; }

        public NotificationCredentials NotificationCredentials { get; set; }

        public bool IsRedisCacheSession { get; set; }
    }
}
