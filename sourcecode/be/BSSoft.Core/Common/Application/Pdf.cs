﻿namespace SSWhite.Core.Common.Application
{
    public class Pdf
    {
        public int Top { get; set; }

        public int Left { get; set; }

        public int Right { get; set; }

        public int Bottom { get; set; }
    }
}
