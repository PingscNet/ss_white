﻿namespace SSWhite.Core.Common
{
    using SSWhite.Core.Enums;

    public class UserRight
    {
        public string Name { get; set; }

        public RightsType Acronym { get; set; }

        public RightsGroupType RightsGroupId { get; set; }
    }
}
