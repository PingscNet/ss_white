﻿namespace SSWhite.Core.Common
{
    public static class DateFormat
    {
        public const string DATE_FORMAT = "dd-MM-yyyy";
        public const string DATE_FORMAT_YEAR = "yyyy-MM-dd";
        public const string DATE_TIME_FORMAT = "dd-MM-yyyy hh:mm:ss tt";
        public const string TIME_FORMAT = "hh:mm:ss tt";
        public const string EXCEL_TIME_FORMAT = "hh:mm:ss AM/PM";
    }
}
