﻿namespace SSWhite.Core.Common.Session
{
    public static class SessionGetterSetter
    {
        private static SessionDetail Session { get; set; }

        public static void Set(SessionDetail session)
        {
            Session = session;
        }

        public static SessionDetail Get()
        {
            return Session;
        }
    }
}
