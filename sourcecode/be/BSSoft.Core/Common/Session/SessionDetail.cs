﻿namespace SSWhite.Core.Common.Session
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Enums;

    public class SessionDetail
    {
        public int Id { get; set; }

        public int SubscriberId { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public UserType UserType { get; set; }

        public bool IsPasswordReset { get; set; }

        public int? CurrentEntityId { get; set; }

        public int? CurrentFinancialYear { get; set; }

        public List<UserEntity> Entities { get; set; }

        public List<UserRight> Rights { get; set; }

        public AppSetting AppSetting { get; set; }

        public string IpAddress { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
