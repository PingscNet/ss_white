﻿namespace SSWhite.Core.Utilities
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    public static class Generator
    {
        public static string EncryptPassword(string password)
        {
            using (var hash = SHA256.Create())
            {
                return Convert.ToBase64String(hash.ComputeHash(Encoding.UTF8.GetBytes(password)));
            }
        }

        public static string GenerateSHA256Hash(this string data)
        {
            using (var hash = SHA256.Create())
            {
                return Convert.ToBase64String(hash.ComputeHash(Encoding.UTF8.GetBytes(data)));
            }
        }
    }
}
