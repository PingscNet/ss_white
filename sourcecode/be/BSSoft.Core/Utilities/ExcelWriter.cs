﻿namespace SSWhite.Core.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;
    using OfficeOpenXml.Table;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Common;
    using SSWhite.Core.Extensions;

    public static class ExcelWriter
    {
        public static byte[] Export<T>(List<T> list, string sheetName, string additionalData = null)
        {
            var (dataTable, propertyInfo) = list.ToExportDataTable<T>();
            var columNames = new List<string>();
            var ROW_INDEX = 1;
            var COLUMN_INDEX = 0;

            foreach (PropertyInfo item in propertyInfo)
            {
                columNames.Add(((ExportMappingAttribute)item.GetCustomAttribute(typeof(ExportMappingAttribute))).ExcelName);
            }

            var excelPackage = new ExcelPackage();
            var sheet = excelPackage.Workbook.Worksheets.Add(sheetName);
            foreach (var columName in columNames)
            {
                sheet.Cells[ROW_INDEX, ++COLUMN_INDEX].Value = columName.ToUpper();
            }

            ROW_INDEX++;
            COLUMN_INDEX = 1;
            for (int row = 0; row < dataTable.Rows.Count; row++)
            {
                for (int column = 0; column < dataTable.Columns.Count; column++)
                {
                    sheet.Cells[ROW_INDEX, COLUMN_INDEX].Value = dataTable.Rows[row][column];
                    COLUMN_INDEX++;
                }

                COLUMN_INDEX = 1;
                ROW_INDEX++;
            }

            sheet.Cells[++ROW_INDEX, COLUMN_INDEX].Value = additionalData;

            return excelPackage.GetAsByteArray();
        }

        public static byte[] CreateExcel<T>(IEnumerable<T> list, string author, string title)
        {
            using (ExcelPackage package = new ExcelPackage())
            {
                // create the excel file and set some properties
                package.Workbook.Properties.Author = author;
                package.Workbook.Properties.Title = title;
                package.Workbook.Properties.Created = DateTime.Now;

                // create a new sheet
                package.Workbook.Worksheets.Add("Sheet 1");
                ExcelWorksheet ws = package.Workbook.Worksheets[1];
                ws.Cells.Style.Font.Size = 11;
                ws.Cells.Style.Font.Name = "Calibri";

                // put the data in the sheet, starting from column A, row 1
                ws.Cells["A1"].LoadFromCollection(list, true);

                // set some styling on the header row
                var header = ws.Cells[1, 1, 1, ws.Dimension.End.Column];
                header.Style.Font.Bold = true;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#BFBFBF"));

                // loop the header row to capitalize the values
                for (int col = 1; col <= ws.Dimension.End.Column; col++)
                {
                    var cell = ws.Cells[1, col];
                    cell.Value = cell.Value.ToString().ToUpper();
                }

                // loop the properties in list<t> to apply some data formatting based on data type and check for nested lists
                var listObject = list.First();
                var columns_to_delete = new List<int>();
                for (int i = 0; i < listObject.GetType().GetProperties().Count(); i++)
                {
                    var prop = listObject.GetType().GetProperties()[i];
                    var range = ws.Cells[2, i + 1, ws.Dimension.End.Row, i + 1];

                    // check if the property is a List, if yes add it to columns_to_delete
                    if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                    {
                        columns_to_delete.Add(i + 1);
                    }

                    // set the date format
                    if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                    {
                        range.Style.Numberformat.Format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
                    }

                    // set the decimal format
                    if (prop.PropertyType == typeof(decimal) || prop.PropertyType == typeof(decimal?))
                    {
                        range.Style.Numberformat.Format = "0.00";
                    }
                }

                // remove all lists from the sheet, starting with the last column
                foreach (var item in columns_to_delete.OrderByDescending(x => x))
                {
                    ws.DeleteColumn(item);
                }

                // auto fit the column width
                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                // sometimes the column width is slightly too small (maybe because of font type).
                // So add some extra width just to be sure
                for (int col = 1; col <= ws.Dimension.End.Column; col++)
                {
                    ws.Column(col).Width += 3;
                }

                ws.InsertRow(1, 6);
                ws.Cells["A1"].Value = "Company Logo";
                ws.Cells["B1"].Value = "Company Name";
                ws.Cells["B2"].Value = "Report Name";
                ws.Cells["B3"].Value = "Party Name";
                ws.Cells["B4"].Value = "Site";
                ws.Cells["B5"].Value = "Date Range";

                var last = ws.Cells.Last();
                var rowCount = last.First().End.Row;

                var appendRow = $"A{rowCount + 2}";

                // put the data in the sheet, starting from column A, appended row
                ws.Cells[appendRow].LoadFromCollection(list, true);

                // set some styling on the header row
                header = ws.Cells[rowCount + 2, 1, rowCount + 2, ws.Dimension.End.Column];
                header.Style.Font.Bold = true;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#BFBFBF"));

                // loop the header row to capitalize the values
                for (int col = 1; col <= ws.Dimension.End.Column; col++)
                {
                    var cell = ws.Cells[rowCount + 2, col];
                    cell.Value = cell.Value?.ToString().ToUpper();
                }

                // loop the properties in list<t> to apply some data formatting based on data type and check for nested lists
                listObject = list.First();
                columns_to_delete = new List<int>();
                for (int i = 0; i < listObject.GetType().GetProperties().Count(); i++)
                {
                    var prop = listObject.GetType().GetProperties()[i];
                    var range = ws.Cells[rowCount + 3, i + 1, ws.Dimension.End.Row, i + 1];

                    // check if the property is a List, if yes add it to columns_to_delete
                    if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                    {
                        columns_to_delete.Add(i + 1);
                    }

                    // set the date format
                    if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                    {
                        range.Style.Numberformat.Format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
                    }

                    // set the decimal format
                    if (prop.PropertyType == typeof(decimal) || prop.PropertyType == typeof(decimal?))
                    {
                        range.Style.Numberformat.Format = "0.00";
                    }
                }

                // remove all lists from the sheet, starting with the last column
                foreach (var item in columns_to_delete.OrderByDescending(x => x))
                {
                    ws.DeleteColumn(item);
                }

                // auto fit the column width
                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                // sometimes the column width is slightly too small (maybe because of font type).
                // So add some extra width just to be sure
                for (int col = 1; col <= ws.Dimension.End.Column; col++)
                {
                    ws.Column(col).Width += 3;
                }

                // send the excel back as byte array
                return package.GetAsByteArray();
            }
        }

        public static ExcelWorksheet CreateExcelWithMultipleTable<T>(this IEnumerable<T> list, ExcelPackage package, string sheetName = null, bool isFirstList = false, string tableName = null)
        {
            // create a new sheet
            ExcelWorksheet ws = null;
            if (isFirstList)
            {
                package.Workbook.Worksheets.Add(sheetName);
                ExcelWorksheet excelWorksheet = package.Workbook.Worksheets[0];
                ws = excelWorksheet;
                ws.Cells.Style.Font.Name = "Calibri";
            }
            else
            {
                ws = package.Workbook.Worksheets[0];
                ws.Cells.Style.Font.Name = "Calibri";
            }

            var rowCount = 1;
            var appendRow = string.Empty;
            ExcelRange header;
            var startRange = 0;
            var tableStyle = TableStyles.Medium26;
            if (isFirstList)
            {
                // put the data in the sheet, starting from column A, row 1
                var table = ws.Cells["A1"].LoadFromCollection(list, true, tableStyle);
                table.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                table.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                table.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                table.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                table.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                table.Style.Fill.PatternType = ExcelFillStyle.Solid;
                table.Style.Fill.BackgroundColor.SetColor(Color.White);
                startRange = 2;

                // set some styling on the header row
                header = ws.Cells[1, 1, 1, list.First().GetType().GetProperties().Count()];
                header.Style.Font.Bold = true;
                header.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                header.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#BFBFBF"));

                // loop the header row to capitalize the values
                for (int col = 1; col <= list.First().GetType().GetProperties().Count(); col++)
                {
                    var cell = ws.Cells[1, col];
                    cell.Value = cell.Value.ToString().ToUpper();
                }
            }
            else
            {
                if (!tableName.IsNullOrWhiteSpace())
                {
                    rowCount = ws.Cells.Last().End.Row;
                    ws.InsertRow(rowCount + 2, 1);
                    ws.Cells[rowCount + 2, 1].Value = tableName;
                    ws.Cells[rowCount + 2, 1].Style.Font.Size = 14;
                    ws.Cells[rowCount + 2, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[rowCount + 2, 1].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#BFBFBF"));
                    ws.Cells[rowCount + 2, 1, rowCount + 2, list.First().GetType().GetProperties().Count()].Merge = true;
                }

                rowCount = ws.Cells.Last().End.Row;
                appendRow = $"A{rowCount + 2}";

                // put the data in the sheet, starting from column A, appended row
                var table = ws.Cells[appendRow].LoadFromCollection(list, true, tableStyle);
                table.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                table.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                table.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                table.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                table.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                table.Style.Fill.PatternType = ExcelFillStyle.Solid;
                table.Style.Fill.BackgroundColor.SetColor(Color.White);

                // set some styling on the header row
                header = ws.Cells[rowCount + 2, 1, rowCount + 2, list.First().GetType().GetProperties().Count()];
                header.Style.Font.Bold = true;
                header.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                header.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#BFBFBF"));

                startRange = rowCount + 3;

                // loop the header row to capitalize the values
                for (int col = 1; col <= list.First().GetType().GetProperties().Count(); col++)
                {
                    var cell = ws.Cells[rowCount + 2, col];
                    cell.Value = cell.Value?.ToString().ToUpper();
                }
            }

            // loop the properties in list<t> to apply some data formatting based on data type and check for nested lists
            var listObject = list.First();
            var columns_to_delete = new List<int>();
            for (int i = 0; i < listObject.GetType().GetProperties().Count(); i++)
            {
                var prop = listObject.GetType().GetProperties()[i];
                var range = ws.Cells[startRange, i + 1, ws.Dimension.End.Row, i + 1];

                // check if the property is a List, if yes add it to columns_to_delete
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                {
                    columns_to_delete.Add(i + 1);
                }

                // set the date format
                if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                {
                    range.Style.Numberformat.Format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
                }

                // set the date format
                if (prop.PropertyType == typeof(TimeSpan) || prop.PropertyType == typeof(TimeSpan?))
                {
                    range.Style.Numberformat.Format = DateFormat.EXCEL_TIME_FORMAT;
                }

                // set the decimal format
                if (prop.PropertyType == typeof(decimal) || prop.PropertyType == typeof(decimal?))
                {
                    range.Style.Numberformat.Format = "0.00";
                }
            }

            // remove all lists from the sheet, starting with the last column
            foreach (var item in columns_to_delete.OrderByDescending(x => x))
            {
                ws.DeleteColumn(item);
            }

            // auto fit the column width
            ws.Cells[ws.Dimension.Address].AutoFitColumns();

            // sometimes the column width is slightly too small (maybe because of font type).
            // So add some extra width just to be sure
            for (int col = 1; col <= list.First().GetType().GetProperties().Count(); col++)
            {
                ws.Column(col).Width += 3;
            }

            // send the excel package
            return ws;
        }

        public static byte[] CreateExcelWithSingleTable<T>(IEnumerable<T> list, string author, string title)
        {
            using (ExcelPackage package = new ExcelPackage())
            {
                // create the excel file and set some properties
                package.Workbook.Properties.Author = author;
                package.Workbook.Properties.Title = title;
                package.Workbook.Properties.Created = DateTime.Now;

                // create a new sheet
                package.Workbook.Worksheets.Add("Sheet 1");
                ExcelWorksheet ws = package.Workbook.Worksheets[1];
                ws.Cells.Style.Font.Size = 11;
                ws.Cells.Style.Font.Name = "Calibri";

                // put the data in the sheet, starting from column A, row 1
                ws.Cells["A1"].LoadFromCollection(list, true);

                // set some styling on the header row
                var header = ws.Cells[1, 1, 1, ws.Dimension.End.Column];
                header.Style.Font.Bold = true;
                header.Style.Fill.PatternType = ExcelFillStyle.Solid;
                header.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#BFBFBF"));

                // loop the header row to capitalize the values
                for (int col = 1; col <= ws.Dimension.End.Column; col++)
                {
                    var cell = ws.Cells[1, col];
                    cell.Value = cell.Value.ToString().ToUpper();
                }

                // loop the properties in list<t> to apply some data formatting based on data type and check for nested lists
                var listObject = list.First();
                var columns_to_delete = new List<int>();
                for (int i = 0; i < listObject.GetType().GetProperties().Count(); i++)
                {
                    var prop = listObject.GetType().GetProperties()[i];
                    var range = ws.Cells[2, i + 1, ws.Dimension.End.Row, i + 1];

                    // check if the property is a List, if yes add it to columns_to_delete
                    if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                    {
                        columns_to_delete.Add(i + 1);
                    }

                    // set the date format
                    if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                    {
                        range.Style.Numberformat.Format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
                    }

                    // set the decimal format
                    if (prop.PropertyType == typeof(decimal) || prop.PropertyType == typeof(decimal?))
                    {
                        range.Style.Numberformat.Format = "0.00";
                    }
                }

                // remove all lists from the sheet, starting with the last column
                foreach (var item in columns_to_delete.OrderByDescending(x => x))
                {
                    ws.DeleteColumn(item);
                }

                // auto fit the column width
                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                // sometimes the column width is slightly too small (maybe because of font type).
                // So add some extra width just to be sure
                for (int col = 1; col <= ws.Dimension.End.Column; col++)
                {
                    ws.Column(col).Width += 3;
                }

                // send the excel back as byte array
                return package.GetAsByteArray();
            }
        }
    }
}