﻿namespace SSWhite.Core.Request
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    /// <summary>
    /// This Class is used to Create GenericSearchRequest for request having id parameter only.
    /// </summary>
    public class RequestEntity
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }
    }
}
