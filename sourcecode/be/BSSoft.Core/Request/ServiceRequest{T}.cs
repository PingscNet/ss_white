﻿namespace SSWhite.Core.Request
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;

    /// <summary>
    /// This Class is used to Create GenericRequest.
    /// It will bind any request parameter of any Viewmodel.
    /// </summary>
    public class ServiceRequest<T> : ServiceRequest
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public T Data { get; set; }

        public List<T> DataList { get; set; }

        public object Date { get; set; }
    }
}
