﻿namespace SSWhite.Core.Request
{
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Common.Session;

    public class ServiceRequest
    {
        [Internal]
        public SessionDetail Session { get; set; }

        [Internal]
        public string IpAddress => Session != null ? Session.IpAddress : string.Empty;

        [Internal]
        public string RedirectToUrl { get; set; }
    }
}
