﻿namespace SSWhite.Core.Converters
{
    using System;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using SSWhite.Core.Utilities;

    public class UpperCaseConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return !objectType.IsClass;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
