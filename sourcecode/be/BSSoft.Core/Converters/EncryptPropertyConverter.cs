﻿namespace SSWhite.Core.Converters
{
    using System;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using SSWhite.Core.Utilities;

    public class EncryptPropertyConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
            {
                return null;
            }

            JToken jToken = EncryptDecrypt.Decrypt(reader.Value.ToString(), null);
            return jToken.ToObject(objectType, serializer);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JToken jToken = EncryptDecrypt.Encrypt(value.ToString(), null);
            jToken.WriteTo(writer);
        }
    }
}
