﻿namespace SSWhite.Core.Converters
{
    using System;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using SSWhite.Core.Utilities;

    public class EnumStringConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
            {
                return null;
            }

            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JToken jToken = CommonMethods.GetDisplayValue(value);
            jToken.WriteTo(writer);
        }
    }
}
