﻿namespace SSWhite.Core.Attributes
{
    using System;

    public class ExportMappingAttribute : Attribute
    {
        public string ExcelName { get; set; }
    }
}
