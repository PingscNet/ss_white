﻿namespace SSWhite.Core.Response.Error
{
    using SSWhite.Core.Enums;

    public class ErrorJsonResponse
    {
        public string Code { get; set; }

        public string Message { get; set; }

        public ErrorStatus ErrorStatus { get; set; }
    }
}
