﻿namespace SSWhite.Core.Response
{
    using System.Collections.Generic;

    /// <summary>
    /// This class is used to Create GenericSearchResponse.
    /// It will bind any search request parameter of any model.
    /// </summary>
    public class ServiceSearchResponse<T> : ServiceResponse
    {
        public List<T> Result { get; set; }

        public int TotalRecords { get; set; }
    }
}
