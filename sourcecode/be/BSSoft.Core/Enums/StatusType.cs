﻿namespace SSWhite.Core.Enums
{
    public enum StatusType : short
    {
        Active = 1,
        InActive = 0,
        Deleted = 3
    }
}
