﻿namespace SSWhite.Core.Enums
{
    using System.ComponentModel.DataAnnotations;

    public enum RightsGroupType : int
    {
        Dashboard = 1,
        User = 2,
        Role = 3,
        Company = 4,
        Client = 5,

        [Display(Name = "Item Group")]
        ItemGroup = 6,

        [Display(Name = "Item Sub Group")]
        ItemSubGroup = 7,

        [Display(Name = "Item Category")]
        ItemCategory = 8,

        [Display(Name = "Item Sub Category")]
        ItemSubCategory = 9,
        Unit = 10,
        Item = 11,
        Crate = 12,
        Region = 13,
        Country = 14,
        State = 15,
        City = 16,
        Header = 17,

        [Display(Name = "Ledger Group")]
        LedgerGroup = 18,
        Purchase = 19,
        Sales = 20,
        Receipt = 21,
        Payment = 22,

        [Display(Name = "Journal Voucher")]
        JournalVoucher = 23,

        [Display(Name = "Ledger Report")]
        LedgerReport = 24,

        [Display(Name = "Packing Type")]
        PackingType = 25,

        [Display(Name = "Sub Unit")]
        SubUnit = 26
    }
}
