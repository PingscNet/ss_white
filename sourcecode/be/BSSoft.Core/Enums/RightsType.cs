﻿namespace SSWhite.Core.Enums
{
    public enum RightsType : int
    {
        // Dashboard
        SRH_VW_DBD = 1,

        // User
        SRH_VW_USR = 2,
        ADD_USR = 3,
        EDT_USR = 4,
        DLT_USR = 5,
        CHG_USR_STS = 6,

        // Role
        SRH_VW_ROL = 7,
        ADD_ROL = 8,
        EDT_ROL = 9,
        DLT_ROL = 10,
        CHG_ROL_STS = 11,

        // Company
        SRH_VW_CMP = 12,
        ADD_CMP = 13,
        EDT_CMP = 14,
        DLT_CMP = 15,
        CHG_CMP_STS = 16,

        // Client
        SRH_VW_CLT = 17,
        ADD_CLT = 18,
        EDT_CLT = 19,
        DLT_CLT = 20,
        CHG_CLT_STS = 21,

        // Item Group
        SRH_VW_ITM_GRP = 22,
        ADD_ITM_GRP = 23,
        EDT_ITM_GRP = 24,
        DLT_ITM_GRP = 25,
        CHG_ITM_GRP_STS = 26,

        // Item Sub Group
        SRH_VW_ITM_SUB_GRP = 27,
        ADD_ITM_SUB_GRP = 28,
        EDT_ITM_SUB_GRP = 29,
        DLT_ITM_SUB_GRP = 30,
        CHG_ITM_SUB_GRP_STS = 31,

        // Item Category
        SRH_VW_ITM_CTG = 32,
        ADD_ITM_CTG = 33,
        EDT_ITM_CTG = 34,
        DLT_ITM_CTG = 35,
        CHG_ITM_CTG_STS = 36,

        // Item Sub Category
        SRH_VW_ITM_SUB_CTG = 37,
        ADD_ITM_SUB_CTG = 38,
        EDT_ITM_SUB_CTG = 39,
        DLT_ITM_SUB_CTG = 40,
        CHG_ITM_SUB_CTG_STS = 41,

        // Unit
        SRH_VW_UQC = 42,
        ADD_UQC = 43,
        EDT_UQC = 44,
        DLT_UQC = 45,
        CHG_UQC_STS = 46,

        // Item
        SRH_VW_ITM = 47,
        ADD_ITM = 48,
        EDT_ITM = 49,
        DLT_ITM = 50,
        CHG_ITM_STS = 51,

        // Crate
        SRH_VW_CRT = 52,
        ADD_CRT = 53,
        EDT_CRT = 54,
        DLT_CRT = 55,
        CHG_CRT_STS = 56,

        // Region
        SRH_VW_RGN = 57,
        ADD_RGN = 58,
        EDT_RGN = 59,
        DLT_RGN = 60,
        CHG_RGN_STS = 61,

        // Country
        SRH_VW_CTR = 62,
        ADD_CTR = 63,
        EDT_CTR = 64,
        DLT_CTR = 65,
        CHG_CTR_STS = 66,

        // State
        SRH_VW_ST = 67,
        ADD_ST = 68,
        EDT_ST = 69,
        DLT_ST = 70,
        CHG_ST_STS = 71,

        // City
        SRH_VW_CTY = 72,
        ADD_CTY = 73,
        EDT_CTY = 74,
        DLT_CTY = 75,
        CHG_CTY_STS = 76,

        // Header
        SRH_VW_HDR = 77,
        ADD_HDR = 78,
        EDT_HDR = 79,
        DLT_HDR = 80,
        CHG_HDR_STS = 81,

        // Ledger Group
        SRH_VW_LDR_GRP = 82,
        ADD_LDR_GRP = 83,
        EDT_LDR_GRP = 84,
        DLT_LDR_GRP = 85,
        CHG_LDR_GRP_STS = 86,

        // Purchase
        SRH_VW_PUR = 87,
        ADD_PUR = 88,
        EDT_PUR = 89,
        DLT_PUR = 90,
        CHG_PUR_STS = 91,
        VW_PUR_DTL = 92,

        // Sale
        SRH_VW_SAL = 93,
        ADD_SAL = 94,
        EDT_SAL = 95,
        DLT_SAL = 96,
        CHG_SAL_STS = 97,
        VW_SAL_DTL = 98,

        // Receipt
        SRH_VW_RCT = 99,
        ADD_RCT = 100,
        EDT_RCT = 101,
        DLT_RCT = 102,
        CHG_RCT_STS = 103,
        VW_RCT_DTL = 104,

        // Payment
        SRH_VW_PMT = 105,
        ADD_PMT = 106,
        EDT_PMT = 107,
        DLT_PMT = 108,
        CHG_PMT_STS = 109,
        VW_PMT_DTL = 110,

        // Sales
        SRH_VW_SAL_RAT_PND = 111,

        // Purchase
        SRH_VW_PUR_RAT_PND = 112,

        // JournalVoucher
        SRH_VW_JV = 113,
        ADD_JV = 114,
        EDT_JV = 115,
        DLT_JV = 116,
        CHG_JV_STS = 117,
        VW_JV_DTL = 118,

        // LedgerReport
        SRH_VW_LDG_RPT = 119,
        DLD_LDG_RPT_PDF = 120,

        // Client
        ADD_EDT_CLT_OPN_BAL = 121,

        // PackingType
        SRH_VW_PKG_TYP = 122,
        ADD_PKG_TYP = 123,
        EDT_PKG_TYP = 124,
        DLT_PKG_TYP = 125,
        CHG_PKG_TYP_STS = 126,

        // SubUnit
        SRH_VW_SUB_UNT = 127,
        ADD_SUB_UNT = 128,
        EDT_SUB_UNT = 129,
        DLT_SUB_UNT = 130,
        CHG_SUB_UNT_STS = 131
    }
}
