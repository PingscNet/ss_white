﻿namespace SSWhite.Core.Enums
{
    public enum UserType : short
    {
        SuperAdmin = 1,
        Admin = 2,
        User = 3
    }
}
