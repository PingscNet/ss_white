﻿namespace SSWhite.Core.Enums
{
    public enum SourceType : short
    {
        Web = 1,
        Mobile = 2,
        Swagger = 3
    }
}
