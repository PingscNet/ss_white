﻿namespace SSWhite.Core.Enums
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public enum ApprovalType : short
    {
        Approved = 1,
        Pending = 2,
        SelfApproved = 3,
        ManagerApproval = 4,
        HigherManagerApproval = 5,
        MPCApproval = 6,
        HigherAuthorityApprovalAndMPCApproval = 7,
        Denied = 8,
        UnApproved = 9,
        ApprovedAndReleased = 10,
        Void = 11,
        Saved = 12,
        PendingForApproval = 13
    }
}