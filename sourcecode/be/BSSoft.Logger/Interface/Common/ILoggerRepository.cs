﻿namespace SSWhite.Logger.Interface.Common
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Logger.Request.Exception;
    using SSWhite.Logger.Request.Request;

    public interface ILoggerRepository
    {
        Task LogToDatabase(ServiceRequest<LogException> request);

        Task LogToDatabase(LogException request);

        Task LogToDatabase(ServiceRequest<LogRequest> request);
    }
}
