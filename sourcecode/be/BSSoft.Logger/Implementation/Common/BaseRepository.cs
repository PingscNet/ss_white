﻿namespace SSWhite.Logger.Implementation.Common
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.Extensions.Configuration;
    using SSWhite.Core.Utilities;
    using SSWhite.Logger.Interface.Common;

    public class BaseRepository : IBaseRepository
    {
        private readonly IConfiguration _config;

        public BaseRepository(IConfiguration configuration)
        {
            _config = configuration;
        }

        public async Task<int> ExecuteAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var connection = new SqlConnection(EncryptDecrypt.Decrypt(_config.GetConnectionString("ConnectionString"))))
            {
                return await connection.ExecuteAsync(sql, param, transaction, commandTimeout, commandType);
            }
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var connection = new SqlConnection(EncryptDecrypt.Decrypt(_config.GetConnectionString("ConnectionString"))))
            {
                return await connection.QueryAsync<T>(sql, param, transaction, commandTimeout, commandType);
            }
        }

        public async Task<T> QueryFirstAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var connection = new SqlConnection(EncryptDecrypt.Decrypt(_config.GetConnectionString("ConnectionString"))))
            {
                return await connection.QueryFirstAsync<T>(sql, param, transaction, commandTimeout, commandType);
            }
        }

        public async Task<T> QueryFirstOrDefaultAsync<T>(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var connection = new SqlConnection(EncryptDecrypt.Decrypt(_config.GetConnectionString("ConnectionString"))))
            {
                return await connection.QueryFirstOrDefaultAsync<T>(sql, param, transaction, commandTimeout, commandType);
            }
        }

        public async Task<SqlMapper.GridReader> QueryMultipleAsync(string sql, object param = null, IDbTransaction transaction = null, int? commandTimeout = null, CommandType? commandType = null)
        {
            var connection = new SqlConnection(EncryptDecrypt.Decrypt(_config.GetConnectionString("ConnectionString")));
            try
            {
                return await connection.QueryMultipleAsync(sql, param, transaction, commandTimeout, commandType);
            }
            catch (System.Exception)
            {
                throw;
            }
        }
    }
}
