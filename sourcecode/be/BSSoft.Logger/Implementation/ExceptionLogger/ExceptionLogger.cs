﻿namespace SSWhite.Logger.Implementation.ExceptionLogger
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Logger.Common;
    using SSWhite.Logger.Interface.Common;
    using SSWhite.Logger.Interface.ExceptionLogger;
    using SSWhite.Logger.Request.Exception;

    public class ExceptionLogger : IExceptionLogger
    {
        private readonly ILoggerRepository _databaseRepository;
        private readonly AppSetting _appSetting;

        public ExceptionLogger(ILoggerRepository databaseRepository, IOptions<AppSetting> appSetting)
        {
            _databaseRepository = databaseRepository;
            _appSetting = appSetting.Value;
        }

        public async Task LogToDatabase<T>(ServiceRequest<T> request, Exception exception)
        {
            try
            {
                var logExceptionRequest = new ServiceRequest<LogException>()
                {
                    Data = PrepareLogRequest.PrepareExceptionRequest(exception, request),
                    Session = request.Session
                };
                await _databaseRepository.LogToDatabase(logExceptionRequest);
            }
            catch (Exception ex)
            {
                var filePath = $"{_appSetting.DefaultExceptionFilePath}Exception_{DateTime.Now.Date.ToString(DateFormat.DATE_FORMAT)}.txt";
                using (var stream = File.AppendText(filePath))
                {
                    stream.WriteLine($"\n -----{DateTime.Now}------ \n ---------Message ------- \n {exception.Message} \n -------------Stack Trace ---------- \n {exception.StackTrace} \n -----Logger Exception ---- \n{ex.Message}");
                    stream.Flush();
                    stream.Dispose();
                    stream.Close();
                }
            }
        }

        public Task LogToDatabase(Exception exception)
        {
            try
            {
                var logExceptionRequest = new LogException()
                {
                };
                _databaseRepository.LogToDatabase(logExceptionRequest);
            }
            catch (Exception error)
            {
                throw error;
            }

            return Task.CompletedTask;
        }

        public Task LogToFile(Exception appException, Exception ex)
        {
            var filePath = $"{_appSetting.DefaultExceptionFilePath}Exception_{DateTime.Now.Date.ToString(DateFormat.DATE_FORMAT)}.txt";
            using (var stream = File.AppendText(filePath))
            {
                stream.WriteLine($"\n -----{DateTime.Now}------ \n ---------Message ------- \n {appException.Message} \n -------------Stack Trace ---------- \n {appException.StackTrace} \n -----Logger Exception ---- \n{ex.Message}");
                stream.Flush();
                stream.Dispose();
                stream.Close();
            }

            return Task.CompletedTask;
        }
    }
}
