﻿namespace SSWhite.Logger.Common
{
    public static class StoredProcedures
    {
        public const string LOGGING_LOG_EXCEPTION = "logging.LogException";
        public const string LOGGING_LOG_DATA_EXCEPTION = "logging.LogDataException";
        public const string LOGGING_LOG_REQUEST = "logging.LogRequest";
    }
}
