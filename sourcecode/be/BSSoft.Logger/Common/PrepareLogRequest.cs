﻿namespace SSWhite.Logger.Common
{
    using System;
    using SSWhite.Logger.Request.Exception;

    public static class PrepareLogRequest
    {
        public static LogException PrepareExceptionRequest<T>(Exception exception, Core.Request.ServiceRequest<T> request)
        {
            return new LogException()
            {
                Source = exception.Source,
                StrackTrace = exception.StackTrace,
                InnerException = exception.InnerException?.Message,
                Message = exception.Message,
                Data = request.Data,
                FileName = new System.Diagnostics.StackTrace(exception, true).GetFrame(0).GetFileName()
            };
        }
    }
}
