﻿namespace SSWhite.Logger.Request.Exception
{
    public class LogException
    {
        public string Source { get; set; }

        public string StrackTrace { get; set; }

        public string InnerException { get; set; }

        public string Message { get; set; }

        public object Data { get; set; }

        public string FileName { get; set; }
    }
}
