﻿namespace SSWhite.Logger.Request.Request
{
    using System;

    public class LogRequest
    {
        public Guid RequestId { get; set; }

        public object Data { get; set; }

        public string FileName { get; set; }

        public string Method { get; set; }
    }
}
