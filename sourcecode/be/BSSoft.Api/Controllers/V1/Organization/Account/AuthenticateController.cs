﻿namespace SSWhite.Api.Controllers.V1.Organization.Account
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Domain.Request.Account;
    using SSWhite.ResourceFile;
    using SSWhite.Service.Interface.Account;

    [Route("/v1.0/authenticate/")]
    public class AuthenticateController : BaseController
    {
        private readonly IAccountService _accountService;

        public AuthenticateController(IAccountService accountService, ISession session, IOptions<AppSetting> appsetting, IOptions<IPAddress> ipAddress)
            : base(session, appsetting, ipAddress)
        {
            _accountService = accountService;
        }

        [HttpPost("token")]
        public async Task<JsonResult> Authenticate([FromBody] LogInRequest request)
        {
            var serviceRequest = await CreateServiceRequest(request);
            var response = await _accountService.ValidateUserForLogIn(serviceRequest);

            if (response.Data == null)
            {
                Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status401Unauthorized;

                return new JsonResult(new { error = Validations.UserNameOrPasswordIsIncorrect });
            }
            else if (response.Data.SubscriberId == -1)
            {
                return new JsonResult(new { error = Validations.SubscriptionExpired });
            }
            ////else if (response.Data.IsPasswordReset)
            ////{
            ////    return Json(new { status = false, resetPassword = response.Data.IsPasswordReset, id = response.Data.UserId.Encrypt(), redirectToURL = Url.Action(Actions.RESET_PASSWORD, Controllers.ACCOUNT) });
            ////}
            else
            {
                var authToken = string.Empty;
                int? expiringInMinutes = 0;
                if (_appSetting.IsRedisCacheSession)
                {
                    (authToken, expiringInMinutes) = await CheckAndSetSessionDetails(response.Data);
                }
                else
                {
                    (authToken, expiringInMinutes) = CheckAndSetInMemomrySessionDetails(response.Data);
                }

                return new JsonResult(new { authToken, expiringInMinutes });
            }
        }

        [HttpPost("SendForgetPasswordEmail")]
        public async Task SendForgetPasswordEmail([FromQuery] SendForgetPasswordEmailRequest request)
        {
            await _accountService.SendForgetPasswordEmail(request);
        }

        [HttpPost("IsEmailValid")]
        public async Task<string> IsEmailValid([FromQuery] IsEmailValidRequest request)
        {
            return await _accountService.IsEmailValid(request);
        }

        [HttpPost("SetNewPassword")]
        public async Task SetNewPassword([FromQuery] SetNewPasswordRequest request)
        {
            await _accountService.SetNewPassword(request);
        }

        [HttpPost("logOut")]
        public async Task LogOut()
        {
            await DeleteSession();
        }
    }
}
