﻿namespace SSWhite.Api.Controllers.V1.Organization.User
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Organization.User;
    using SSWhite.Domain.Response.Organization.User;
    using SSWhite.ResourceFile;
    using SSWhite.Service.Interface.Organization.User;

    [Route("/v1.0/user/")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("search")]
        public async Task<ServiceSearchResponse<GetAllUsersResponse>> GetAllUsers([FromBody] ServiceSearchRequest<GetAllUsers> request)
        {
            await SetSessionDetails(request);
            return await _userService.GetAllUsers(request);
        }

        [HttpPost("add")]
        public async Task<string> AddUser([FromBody] AddUser request)
        {
            var response = await _userService.AddUser(await CreateServiceRequest(request));
            return response.Data == -1 ? Validations.UserAlreadyExists : string.Format(Messages.Successfully, Labels.User, Labels.Added);
        }

        [HttpPost("edit")]
        public async Task<string> EditUser([FromBody] EditUser request)
        {
            var response = await _userService.EditUser(await CreateServiceRequest(request));
            return response.Data == -1 ? Validations.UserAlreadyExists : string.Format(Messages.Successfully, Labels.User, Labels.Updated);
        }

        [HttpGet("detail")]
        public async Task<GetUserByIdResponse> GetUserById([FQuery] RequestEntity request)
        {
            return (await _userService.GetUserById(await CreateServiceRequest(request.Id))).Data;
        }

        [HttpGet("changeStatus")]
        public async Task ChangeUserStatus(RequestEntity request)
        {
            await _userService.ChangeUserStatus(await CreateServiceRequest(request.Id));
        }

        [HttpPost("delete")]
        public async Task DeleteUser([FromBody] RequestEntity request)
        {
            await _userService.DeleteUser(await CreateServiceRequest(request.Id));
        }
    }
}
