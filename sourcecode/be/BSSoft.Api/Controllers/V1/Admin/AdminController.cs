﻿namespace SSWhite.Api.Controllers.V1.Admin
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Service.Interface.Admin;

    [Route("/v1.0/admin/common")]
    [ApiController]
    public class AdminController : BaseController
    {
        public static IWebHostEnvironment _webHostEnvironment;
        private readonly IAdminService _adminService;
        private readonly IConfiguration _configuration;

        public AdminController(IAdminService adminService, IConfiguration configuration, IWebHostEnvironment webHostEnvironment, Component.Interface.Session.ISession session, IOptions<AppSetting> appsetting, IOptions<IPAddress> ipAddress)
            : base(session, appsetting, ipAddress)
        {
            _adminService = adminService;
            _webHostEnvironment = webHostEnvironment;
            _configuration = configuration;
        }

        [HttpPost("AddNewEmployee")]
        public async Task AddNewEmployee([FromForm] AddNewEmployeeRequest request)
        {
            if (request.AvatarImage.Length > 0)
            {
                Guid guid = Guid.NewGuid();
                ////string path = _webHostEnvironment.WebRootPath + "\\uploads\\";
                ////string path = $"{_configuration.GetValue<string>("AppSetting:AvatarRootPathToStoreImage")}";
                string path = _appSetting.AvatarRootPathToStoreImage;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                request.Picture = $"{guid}_{request.AvatarImage.FileName}";

                using (FileStream fileStream = System.IO.File.Create(path + guid + request.AvatarImage.FileName))
                {
                    request.AvatarImage.CopyTo(fileStream);
                }
            }

            if (request.SignatureImage.Length > 0)
            {
                Guid guid = Guid.NewGuid();
                string path = _appSetting.AvatarRootPathToStoreImage;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                request.Signature = $"{guid}_{request.SignatureImage.FileName}";

                using (FileStream fileStream = System.IO.File.Create(path + guid + request.SignatureImage.FileName))
                {
                    request.SignatureImage.CopyTo(fileStream);
                }
            }

            ////request.Password = "6G94qKPK8LYNjnTllCqm2G3BUM08AzOK7yW30tfjrMc=";
            await _adminService.AddNewEmployee(await CreateServiceRequest(request));
        }

        [HttpGet("GetAllEmployeeLimit")]
        public async Task<ServiceSearchResponse<GetAllEmployeeLimitsResponse>> GetAllEmployeeLimits([FromQuery] GetAllEmployeeLimitsRequest request)
        {
            ////var serviceRequest = await CreateServiceRequest(request);
            return await _adminService.GetAllEmployeeLimits(request);
        }

        [HttpGet("GetEmployeeByEmployeeId")]
        public async Task<List<GetEmployeeByEmployeeIdResponse>> GetEmployeeByEmployeeId([FromQuery] GetEmployeeByEmployeeIdRequest request)
        {
            return await _adminService.GetEmployeeByEmployeeId(request);
        }

        [HttpPost("UpdateEmployeeLimit")]
        public async Task UpdateEmployeeLimit(UpdateEmployeeLimitByIdRequest updateEmployeeLimitByIdRequest)
        {
            await _adminService.UpdateEmployeeLimit(updateEmployeeLimitByIdRequest);
        }

        [HttpPost("ChangeEmployeeStatus")]
        public async Task ChangeEmployeeStatus(ChangeEmployeeStatusRequest request)
        {
            await _adminService.ChangeEmployeeStatus(CreateServiceRequest(request));
        }

        [HttpGet("CheckIfEmployeeIdExists")]
        public async Task<CheckIfEmployeeIdExistsResponse> CheckIfEmployeeIdExists([FromQuery] CheckIfEmployeeIdExistsRequest checkIfEmployeeIdExistsRequest)
        {
            return await _adminService.CheckIfEmployeeIdExists(checkIfEmployeeIdExistsRequest);
        }

        [HttpGet("CheckIfEmailAddressExists")]
        public async Task<CheckIfEmailAddressExistsResponse> CheckIfEmailAddressExists([FromQuery] CheckIfEmailAddressExistsRequest request)
        {
            return await _adminService.CheckIfEmailAddressExists(request);
        }

        [HttpGet("GetDocumentApprovals")]
        public async Task<List<GetDocumentApprovalListResponse>> GetDocumentApprovals()
        {
            return await _adminService.GetDocumentApprovals();
        }

        [HttpPost("UpdateDocumentApprovalList")]
        public async Task UpdateDocumentApprovalList(List<UpdateDocumentApprovalListRequest> request)
        {
            await _adminService.UpdateDocumentApprovalList(request);
        }

        [HttpGet("GetUserRights")]
        public async Task<List<GetUserRightsResponse>> GetUserRights()
        {
            return await _adminService.GetUserRights(await CreateServiceRequest());
        }

        [HttpGet("GetRoles")]
        public async Task<List<GetRolesResponse>> GetRoles()
        {
            return await _adminService.GetRoles();
        }

        [HttpGet("GetRightsAccessByRoleId")]
        public async Task<List<GetRightsAccessByRoleIdResponse>> GetRightsAccessByRoleId([FQuery] GetRightsAccessByRoleIdRequest request)
        {
            return await _adminService.GetRightsAccessByRoleId(request);
        }

        [HttpPost("UpdateRightsAccessByRoleId")]
        public async Task UpdateRightsAccessByRoleId(UpdateRightsAccessByRoleIdRequest request)
        {
            await _adminService.UpdateRightsAccessByRoleId(request);
        }
    }
}
