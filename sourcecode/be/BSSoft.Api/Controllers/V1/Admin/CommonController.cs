﻿namespace SSWhite.Api.Controllers.V1.Admin
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Service.Interface.Common;

    [Route("/v1.0/admin/common")]
    [ApiController]
    public class CommonController : BaseController
    {
        private readonly ICommonService _commonService;

        public CommonController(ICommonService commonService, ISession session, IOptions<AppSetting> appsetting, IOptions<IPAddress> ipAddress)
            : base(session, appsetting, ipAddress)
        {
            _commonService = commonService;
        }

        [HttpGet("GetOrganization")]
        public async Task<List<GetOrganizationResponse>> GetOrganization()
        {
            return await _commonService.GetOrganization();
        }

        [HttpGet("GetEmployeeJobDescription")]
        public async Task<List<GetEmployeeJobDescriptionResponse>> GetEmployeeJobDescription()
        {
            return await _commonService.GetEmployeeJobDescription();
        }

        [HttpGet("GetReportingUnder")]
        public async Task<List<GetReportingUnderResponse>> GetReportingUnder()
        {
            return await _commonService.GetReportingUnder();
        }

        [HttpGet("GetDepartmentByDepartmentSupervisorName")]
        public async Task<List<GetDepartmentByDepartmentSupervisorNameResponse>> GetDepartmentByDepartmentSupervisorName([FQuery] GetDepartmentByDepartmentSupervisorNameRequest getDepartmentByDepartmentSupervisorNameRequest)
        {
            return await _commonService.GetDepartmentByDepartmentSupervisorName(getDepartmentByDepartmentSupervisorNameRequest);
        }
    }
}
