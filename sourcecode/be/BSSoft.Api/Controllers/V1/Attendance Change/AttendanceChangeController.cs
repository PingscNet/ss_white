﻿namespace SSWhite.Api.Controllers.V1.AttendanceChange
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.AttendanceChange;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.Domain.Response.AttendanceChange;
    using SSWhite.Service.Interface.AttendanceChange;

    [Route("v1.0/[controller]")]
    [ApiController]
    public class AttendanceChangeController : BaseController
    {
        private readonly IAttendanceChangeService _attendanceChangeService;

        private readonly AppSetting _appSetting;

        public AttendanceChangeController(IAttendanceChangeService attendanceChangeService, IOptions<AppSetting> appSetting)
        {
            _attendanceChangeService = attendanceChangeService;
            _appSetting = appSetting.Value;
        }

        [HttpPost("CreateAttendanceFto")]
        public async Task<CreateAttendanceFtoResponse> CreateAttendanceFto(CreateAttendanceFtoRequest request)
        {
            return await _attendanceChangeService.CreateAttendanceFto(await CreateServiceRequest(request));
        }

        [HttpPost("ApproveOrDenyFto")]
        public async Task ApproveOrDenyFto(ApproveOrDenyFtoRequest request)
        {
            await _attendanceChangeService.ApproveOrDenyFto(await CreateServiceRequest(request));
        }

        [HttpPost("GetDepartmentFtoByUserId")]
        public async Task<List<GetDepartmentFtoByUserIdResponse>> GetDepartmentFtoByUserId(GetDepartmentFtoByUserIdRequest request)
        {
            var response = await _attendanceChangeService.GetDepartmentFtoByUserId(await CreateServiceRequest(request));
            return response;
        }

        [HttpGet("GetAttendanceFtoById")]
        public async Task<GetAttendanceFtoByIdResponse> GetAttendanceFtoById([FQuery] GetAttendanceFtoByIdRequest request)
        {
            var response = await _attendanceChangeService.GetAttendanceFtoById(await CreateServiceRequest(request));
            return response;
        }

        [HttpPost("GetAllDepartmentOrUserFtoByUserId")]
        public async Task<ServiceSearchResponse<GetAllDepartmentOrUserFtoByUserIdResponse>> GetAllDepartmentOrUserFtoByUserId(ServiceSearchRequest<GetAllDepartmentOrUserFtoByUserIdRequest> request)
        {
            await SetSessionDetails(request);
            var response = await _attendanceChangeService.GetAllDepartmentOrUserFtoByUserId(request);
            return response;
        }
    }
}
