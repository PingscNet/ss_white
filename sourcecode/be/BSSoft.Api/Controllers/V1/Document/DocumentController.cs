﻿namespace SSWhite.Api.Controllers.V1.Document
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using NReco.PdfGenerator;
    using SSWhite.Api.Common;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Document;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.Domain.Response.Document;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;
    using SSWhite.Service.Interface.Document;

    [Route("v1.0/[controller]")]
    [ApiController]
    public class DocumentController : BaseController
    {
        private readonly IDocumentService _documentService;
        private readonly AppSetting _appSetting;

        public DocumentController(IDocumentService documentService, IOptions<AppSetting> appSetting)
        {
            _documentService = documentService;
            _appSetting = appSetting.Value;
        }

        [HttpPost("AddOrEditDocument")]
        public async Task<AddOrEditDocumentResponse> AddOrEditDocument([FromForm] AddOrEditDocumentRequest request)
        {
            if (request.AttachmentImage?.Count > 0)
            {
                int i = 0;
                foreach (var item in request.AttachmentImage)
                {
                    if (item.Length > 0)
                    {
                        var epoAttachmentMst = new EpoAttachmentMst();

                        Guid guid = Guid.NewGuid();
                        string path = _appSetting.DocumentAttachmentRootPathImage;

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        var attachmentPath = $"{guid}_{item.FileName}";
                        request.Attachment[i].AttachmentPath = attachmentPath;

                        using (FileStream fileStream = System.IO.File.Create(path + guid + "_" + item.FileName))
                        {
                            item.CopyTo(fileStream);
                        }

                        i++;
                    }
                }
            }

            return await _documentService.AddOrEditDocument(await CreateServiceRequest(request));
        }

        [HttpPost("ApproveDocument")]
        public async Task<IActionResult> ApproveDocument(ApproveDocumentRequest request)
        {
            var data = await _documentService.ApproveDocument(await CreateServiceRequest(request));
            if (data.Result == -1)
            {
                return StatusCode(403);
            }
            else
            {
                return Ok();
            }
        }

        [HttpPost("GetAllDocuments")]
        public async Task<ServiceSearchResponse<GetAllDocumentsResponse>> GetAllDocuments(ServiceSearchRequest<GetAllDocumentsRequest> request)
        {
            await SetSessionDetails(request);
            var response = await _documentService.GetAllDocuments(request);
            return response;
        }

        [HttpGet("GetAssociateDocuments")]
        public async Task<List<GetAssociateDocumentsResponse>> GetAssociateDocuments()
        {
            var response = await _documentService.GetAssociateDocuments();
            return response;
        }

        [HttpGet("GetDocumentById")]
        public async Task<GetDocumentByIdResponse> GetDocumentById([FQuery] GetDocumentByIdRequest request)
        {
            var response = await _documentService.GetDocumentById(await CreateServiceRequest(request));
            return response;
        }

        [HttpGet("GetDocumentsDropdownAndUserDetails")]
        public async Task<GetDocumentsDropdownAndUserDetailsResponse> GetDocumentsDropdownAndUserDetails()
        {
            var response = await _documentService.GetDocumentsDropdownAndUserDetails(await CreateServiceRequest());
            return response;
        }

        [HttpGet("GetEmployeeByDepartment")]
        public async Task<List<GetEmployeeByDepartmentResponse>> GetEmployeeByDepartment([FQuery] GetEmployeeByDepartmentRequest request)
        {
            var response = await _documentService.GetEmployeeByDepartment(request);
            return response;
        }

        [HttpGet("DownloadPdf")]
        [ProducesResponseType(typeof(byte[]), StatusCodes.Status200OK)]
        public async Task<byte[]> DownloadPdf([FromQuery] GetDocumentByIdRequest request)
        {
            var response = await _documentService.GetDocumentById(await CreateServiceRequest(request));
            var dropDownsAndUserDetails = await _documentService.GetDocumentsDropdownAndUserDetails(await CreateServiceRequest());

            string htmlContent;
            if (response.DocumentTypeValue == "ECN")
            {
                htmlContent = TemplateGenerator.GenerateEcnDocumentHeaderTemplate(response, dropDownsAndUserDetails);
            }
            else
            {
                htmlContent = TemplateGenerator.GenerateDocumentHeaderTemplate(response, dropDownsAndUserDetails);
            }

            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var margins = new PageMargins() { Top = 10, Bottom = 30, Left = 5, Right = 5 };
            htmlToPdf.CustomWkHtmlArgs = "  --print-media-type ";
            htmlToPdf.Margins = margins;
            ////htmlToPdf.Margins.Left = 5;
            ////htmlToPdf.Margins.Right = 5;
            htmlToPdf.Orientation = NReco.PdfGenerator.PageOrientation.Portrait;
            htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            ////htmlToPdf.PageHeaderHtml = TemplateGenerator.GenerateEpoHeaderDetails(epoDetails);
            htmlToPdf.PageFooterHtml = TemplateGenerator.GenerateDocumentFooterTemplate(response);
            var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);
            return pdfBytes;
        }
    }
}
