﻿namespace SSWhite.Api.Controllers.V1.Master.Brand
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Master.Brand;
    using SSWhite.Domain.Response.Master.Brand;
    using SSWhite.ResourceFile;
    using SSWhite.Service.Interface.Master.Brand;

    [Route("/v1.0/brand/")]
    public class BrandController : BaseController
    {
        private readonly IBrandService _brandService;

        public BrandController(IBrandService brandService)
        {
            _brandService = brandService;
        }

        [HttpPost("search")]
        public async Task<ServiceSearchResponse<GetAllBrandsResponse>> GetAllBrands([FromBody] ServiceSearchRequest<GetAllBrands> request)
        {
            await SetSessionDetails(request);
            return await _brandService.GetAllBrands(request);
        }

        [HttpPost("add")]
        public async Task<string> AddBrand([FromBody] AddBrand request)
        {
            var response = await _brandService.AddBrand(await CreateServiceRequest(request));
            return response.Data == -1 ? Validations.BrandAlreadyExists : string.Format(Messages.Successfully, Labels.Brand, Labels.Added);
        }

        [HttpPost("edit")]
        public async Task<string> EditBrand([FromBody] EditBrand request)
        {
            var response = await _brandService.EditBrand(await CreateServiceRequest(request));
            return response.Data == -1 ? Validations.BrandAlreadyExists : string.Format(Messages.Successfully, Labels.Brand, Labels.Updated);
        }

        [HttpGet("detail")]
        public async Task<GetBrandByIdResponse> GetBrandById([FQuery] RequestEntity request)
        {
            return (await _brandService.GetBrandById(await CreateServiceRequest(request.Id))).Data;
        }

        [HttpGet("changeStatus")]
        public async Task ChangeBrandStatus(RequestEntity request)
        {
            await _brandService.ChangeBrandStatus(await CreateServiceRequest(request.Id));
        }

        [HttpPost("delete")]
        public async Task DeleteBrand([FromBody] RequestEntity request)
        {
            await _brandService.DeleteBrand(await CreateServiceRequest(request.Id));
        }
    }
}
