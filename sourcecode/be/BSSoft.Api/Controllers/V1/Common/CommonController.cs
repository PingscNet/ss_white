﻿namespace SSWhite.Api.Controllers.V1.Common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Routing;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Domain.Request.Common;
    using SSWhite.Domain.Response.Common;
    using SSWhite.Service.Interface.Common;

    [Route("/v1.0/common/")]
    public class CommonController : BaseController
    {
        private readonly ICommonService _commonService;

        public CommonController(ICommonService commonService)
        {
            _commonService = commonService;
        }

        ////[HttpGet("companies")]
        ////public async Task<List<GetEntitiesResponse>> GetCompanies(DropDownRequest request)
        ////{
        ////    return (await _commonService.GetAllEntities(await CreateServiceRequest(request))).DataList;
        ////}

        ////[HttpGet("roles")]
        ////public async Task<List<DropDownResponse>> GetRoles(DropDownRequest request)
        ////{
        ////    return (await _commonService.GetAllRoles(await CreateServiceRequest(request))).DataList;
        ////}
    }
}
