﻿namespace SSWhite.Api.Controllers.Base
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Extensions;
    using SSWhite.Core.Request;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Response.Account;

    public class BaseController : ControllerBase
    {
        protected static AppSetting _appSetting;
        private static ISession _session;
        private readonly IPAddress _ipAddress;

        public BaseController()
        {
        }

        public BaseController(ISession session, IOptions<AppSetting> appSetting, IOptions<IPAddress> ipAddress)
        {
            _session = session;
            _appSetting = appSetting.Value;
            _ipAddress = ipAddress.Value;
        }

        /// <summary>
        /// This Method Cast ViewModel Request to RequestViewModel.<T> request.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <returns>Object of RequestViewModel</returns>
        protected async Task<ServiceRequest<T>> CreateServiceRequest<T>()
        {
            return new ServiceRequest<T>()
            {
                Session = await GetSessionDetails(),
                Data = Activator.CreateInstance<T>()
            };
        }

        /// <summary>
        /// This Method Cast ViewModel Request to RequestViewModel.<T> request.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <returns>Object of RequestViewModel</returns>
        protected async Task<ServiceRequest> CreateServiceRequest()
        {
            return new ServiceRequest()
            {
                Session = await GetSessionDetails(),
            };
        }

        protected async Task<ServiceRequest<T>> CreateServiceRequest<T>(T data)
        {
            var request = new ServiceRequest<T>()
            {
                Session = await GetSessionDetails(),
                Data = Activator.CreateInstance<T>()
            };
            request.Data = data;
            return request;
        }

        /// <summary>
        /// This method is used to set Session Details for ServiceSearchRequest.
        /// </summary>
        protected async Task SetSessionDetails<T>(ServiceSearchRequest<T> request)
        {
            request.Session = await GetSessionDetails();
            HttpContext.Items["Data"] = request.Data;
        }

        /// <summary>
        /// This method is used to cast Session Details from Session Array to Session Entity.
        /// </summary>
        protected async Task<SessionDetail> GetSessionDetails()
        {
            _session ??= (ISession)HttpContext.RequestServices.GetService(typeof(ISession));
            return _appSetting.IsRedisCacheSession ? await _session.GetSessionDetails() : _session.GetInMemorySessionDetails();
        }

        protected async Task<(string, int)> SetSessionDetails(LogInResponse response)
        {
            string authToken;
            SessionDetail sessionDetail;
            PrepareAuthTokenAndSession(response, out authToken, out sessionDetail);
            await _session.SetSessionDetails(sessionDetail, authToken);
            return (authToken, 59);
        }

        protected (string, int) SetInMemorySessionDetails(LogInResponse response)
        {
            string authToken;
            SessionDetail sessionDetail;
            PrepareAuthTokenAndSession(response, out authToken, out sessionDetail);
            _session.SetInMemorySessionDetails(sessionDetail, authToken);
            return (authToken, 1440);
        }

        protected async Task<(string, int?)> CheckAndSetSessionDetails(LogInResponse response)
        {

            ////(var authToken, var expiringInMinutes) = await _session.GetSessionDetails(response.UserName);

            ////if (authToken.IsNullOrWhiteSpace() || expiringInMinutes <= 0)
            ////{
            ////    return await SetSessionDetails(response);
            ////}
            ////else
            ////{
            ////    return (authToken, expiringInMinutes);
            ////}
            return await SetSessionDetails(response);
        }

        protected (string, int?) CheckAndSetInMemomrySessionDetails(LogInResponse response)
        {
            ////(var authToken, var expiringInMinutes) = _session.GetInMemorySessionDetails(response.UserName);

            ////if (authToken.IsNullOrWhiteSpace() || expiringInMinutes <= 0)
            ////{
            ////    return SetInMemorySessionDetails(response);
            ////}
            ////else
            ////{
            ////    return (authToken, expiringInMinutes);
            ////}
            return SetInMemorySessionDetails(response);
        }

        protected async Task DeleteSession()
        {
            _session ??= (ISession)HttpContext.RequestServices.GetService(typeof(ISession));
            if (_appSetting.IsRedisCacheSession)
            {
                await _session.DeleteSession();
            }
            else
            {
                _session.DeleteInMemorySession();
            }
        }

        private void PrepareAuthTokenAndSession(LogInResponse response, out string authToken, out SessionDetail sessionDetail)
        {
            authToken = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Encrypt();
            sessionDetail = new SessionDetail()
            {
                SubscriberId = response.SubscriberId,
                UserId = response.UserId,
                UserName = response.UserName,
                UserType = response.UserType,
                IsPasswordReset = response.IsPasswordReset,
                CurrentEntityId = response.Entities.FirstOrDefault()?.Id,
                CurrentFinancialYear = Convert.ToInt32(CommonMethods.GetFinancialYearList().First()),
                Entities = response.Entities,
                Rights = response.Rights,
                AppSetting = _appSetting,
                IpAddress = _ipAddress.RequestIpAddress,
                CreatedDate = DateTime.Now
            };
        }
    }
}
