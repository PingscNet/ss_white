﻿namespace SSWhite.Api.Common
{
    using System;
    using System.Linq;
    using SSWhite.Core.Enums;
    using SSWhite.Domain.Enums;
    using SSWhite.Domain.Response.Document;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;

    public static class TemplateGenerator
    {
        public static string GenerateEPODetailsTemplate(GetEpoDetailsByEpoIdResponse epoDetails)
        {
            var purpose = "<html><body><div class=\"MuiGrid-root css-vj1n65-MuiGrid-root\" style=\"margin-top: 1%;\">\r\n        <div class=\"MuiBox-root css-1l4w6pd\">\r\n            <table border=\"0\" style=\"width:100%;\">\r\n                <tr>\r\n                    <td style=\"width:1%\">\r\n                        <p class=\"MuiTypography-root MuiTypography-body1 css-ndaqpv-MuiTypography-root\">Purpose:</p>\r\n                    </td>\r\n                    <td style=\"width:99%\">\r\n                        <p class=\"MuiTypography-root MuiTypography-body1 css-ahj2mt-MuiTypography-root\">{Purpose}</p>\r\n                    </td>\r\n                </tr>\r\n            </table>\r\n        </div>\r\n    </div>\r\n    <!--</div>\r\n    </div>-->\r\n    <!--</div>\r\n    </div>-->\r\n</body>\r\n</html>";
            purpose = purpose.Replace("{Purpose}", epoDetails.EpoBasicDetailsResponse.PurposeOfPurchase);
            string htmlFileContent = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"EPOItemTableHeader.html"))
            {
                htmlFileContent = file.ReadToEnd();
            }

            htmlFileContent = string.Concat(purpose, htmlFileContent);

            string lineItemFileContent = string.Empty;
            var isIndianCurr = epoDetails.EpoBasicDetailsResponse.CurrencyType == (int)CurrencyType.Ruppee;
            foreach (var item in epoDetails.EpoLineItemsList)
            {
                string subLineItemFileContent = string.Empty;
                using (System.IO.StreamReader file = new System.IO.StreamReader(@"EPOItemTableHTML.html"))
                {
                    subLineItemFileContent = file.ReadToEnd();
                }

                var unitPrice = isIndianCurr ? $"₹ {item.UnitPrice}" : $"${item.UnitPrice}";

                var extCost = isIndianCurr ? $"₹ {item.ExtendedCost}" : $"${(item.UnitPrice * item.Qty) - item.TotalCostDiscount}";
                var description = isIndianCurr ? $"{item.ProductDescription} <br/> Gst Rate: {item.Gst} %" : $"{item.ProductDescription}";
                subLineItemFileContent = subLineItemFileContent.Replace("{ITEM}", item.LineNo.ToString())
                                                         .Replace("{QUANTITY}", item.Qty.ToString())
                                                         .Replace("{DESCRIPTION}", description)
                                                         .Replace("{UM}", item.Uom == 5 ? item.UomOther : item.UOMName)
                                                         .Replace("{UNIT PRICE}", unitPrice)
                                                         .Replace("{DIS}", isIndianCurr ? item.TotalCostDiscount.ToString() : item.TotalCostDiscount.ToString())
                                                         .Replace("{IGST}", isIndianCurr ? item.IgstValue.ToString() : null)
                                                         .Replace("{CGST}", isIndianCurr ? item.CgstValue.ToString() : null)
                                                         .Replace("{SGST}", isIndianCurr ? item.SgstValue.ToString() : null)
                                                         .Replace("{EXTCOST}", extCost);

                lineItemFileContent = string.Concat(lineItemFileContent, subLineItemFileContent);
            }

            lineItemFileContent += "</tbody></table>";
            htmlFileContent = string.Concat(htmlFileContent, lineItemFileContent);

            string purposeFileContent = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"EPOPurchaseNote.html"))
            {
                purposeFileContent = file.ReadToEnd();
            }

            htmlFileContent = string.Concat(htmlFileContent, purposeFileContent);
            return htmlFileContent;
        }

        public static string GenerateEpoFooterDetails(GetEpoDetailsByEpoIdResponse epoDetails)
        {
            var isIndianCurr = epoDetails.EpoBasicDetailsResponse.CurrencyType == (int)CurrencyType.Ruppee;
            string purposeFileContent = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"EPOPurchaseTotal.html"))
            {
                purposeFileContent = file.ReadToEnd();
            }

            var grandTotal = isIndianCurr ? $"₹ {epoDetails.EpoBasicDetailsResponse.GrandTotal}"
                                          : $"$ {epoDetails.EpoBasicDetailsResponse.SubTotal - epoDetails.EpoLineItemsList.Sum(x => x.TotalCostDiscount).GetValueOrDefault()}";

            var subTotal = isIndianCurr ? $"₹ {epoDetails.EpoBasicDetailsResponse.SubTotal}"
                                        : $"$ {epoDetails.EpoBasicDetailsResponse.SubTotal - epoDetails.EpoLineItemsList.Sum(x => x.TotalCostDiscount).GetValueOrDefault()}";

            purposeFileContent = purposeFileContent.Replace("{CreatedBy}", epoDetails.EpoBasicDetailsResponse.PersonName)
                              .Replace("{ApprovedBy}", epoDetails.EpoBasicDetailsResponse.Status == 3 ? "*****PURCHASE ORDER IS VOID*****" : epoDetails.ApprovedBY)
                              .Replace("{SubTotal}", subTotal)
                              .Replace("{TotalTax}", isIndianCurr ? (epoDetails.EpoBasicDetailsResponse.TotalIgst
                              + epoDetails.EpoBasicDetailsResponse.TotalCgst
                              + epoDetails.EpoBasicDetailsResponse.TotalSgst).ToString() : "0.00")
                              .Replace("{GrandTotal}", grandTotal);
            return purposeFileContent;
        }

        public static string GenerateEpoHeaderDetails(GetEpoDetailsByEpoIdResponse epoDetails)
        {
            string htmlFileContent;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"PrintHTML.html"))
            {
                htmlFileContent = file.ReadToEnd();
            }

            var orderDate = epoDetails.EpoBasicDetailsResponse.ModifiedDate == null
                                             ? epoDetails.EpoBasicDetailsResponse.CreatedDate.ToString("dd-MM-yyyy")
                                             : $"{epoDetails.EpoBasicDetailsResponse.CreatedDate:dd-MM-yyyy} &nbsp; <b>Revised Date:</b> {epoDetails.EpoBasicDetailsResponse.ModifiedDate?.ToString("dd-MM-yyyy")}";
            htmlFileContent = htmlFileContent.Replace("{Purchase No}", epoDetails.EpoBasicDetailsResponse.Id.ToString())
                                             .Replace("{Vendor Name}", epoDetails.EpoBasicDetailsResponse.Vendor)
                                             .Replace("{Street1}", epoDetails.EpoBasicDetailsResponse.Street1)
                                             .Replace("{Street2}", epoDetails.EpoBasicDetailsResponse.Street2)
                                             .Replace("{City}", epoDetails.EpoBasicDetailsResponse.City)
                                             .Replace("{State}", epoDetails.EpoBasicDetailsResponse.State)
                                             .Replace("{Country}", epoDetails.EpoBasicDetailsResponse.CountryName?.ToUpper())
                                             .Replace("{Order Date}", orderDate)
                                             .Replace("{Vendor Id}", epoDetails.EpoBasicDetailsResponse.VendorId)
                                             .Replace("{CreatedBy}", epoDetails.EpoBasicDetailsResponse.PersonName)
                                             .Replace("{EPONo}", epoDetails.EpoBasicDetailsResponse.Id.ToString())
                                             .Replace("{Terms}", epoDetails.EpoBasicDetailsResponse.DaysOfPaymentPayWhenBilled);
            return htmlFileContent;
        }

        public static string GenerateDocumentHeaderTemplate(GetDocumentByIdResponse documentDetails, GetDocumentsDropdownAndUserDetailsResponse dropDownsAndUserDetails)
        {
            string htmlFileContent = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"DocumentHeader.html"))
            {
                htmlFileContent = file.ReadToEnd();
            }

            var approvalStatus = Enum.GetName(typeof(ApprovalType), documentDetails.ApprovalStatus);

            htmlFileContent = htmlFileContent.Replace("{Title}", documentDetails.DocumentTitle)
                                             .Replace("{Document Id}", documentDetails.DocumentId)
                                             .Replace("{DocumentType}", documentDetails.DocumentFullForm)
                                             .Replace("{Revision}", documentDetails.Rev)
                                             .Replace("{Status}", approvalStatus)
                                             .Replace("{DocumentManager}", dropDownsAndUserDetails.DocumentPersonDetail?.ReportingUnder)
                                             .Replace("{DocumentEditedBy}", documentDetails.CreatedByName)
                                             .Replace("{OriginalCreationDate}", documentDetails.CreatedDate.ToString("dd-MM-yyyy"))
                                             .Replace("{ImplementationDate}", documentDetails.CreatedDate.ToString("dd-MM-yyyy"));

            htmlFileContent = PreparePurposeContent(documentDetails, htmlFileContent);
            htmlFileContent = PrepareCommonTabContent(documentDetails, htmlFileContent);
            htmlFileContent = PrepareAssociatedDocument(documentDetails, htmlFileContent);
            htmlFileContent = PrepareAttachmentDocument(documentDetails, htmlFileContent);
            htmlFileContent = PrepareRevisionHistory(documentDetails, htmlFileContent);
            htmlFileContent = PrepareElectronicNotificationList(documentDetails, htmlFileContent);
            htmlFileContent = PrepareApprovals(documentDetails, htmlFileContent);

            return htmlFileContent;
        }

        public static string GenerateEcnDocumentHeaderTemplate(GetDocumentByIdResponse documentDetails, GetDocumentsDropdownAndUserDetailsResponse dropDownsAndUserDetails)
        {
            string htmlFileContent = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"DocumentHeader.html"))
            {
                htmlFileContent = file.ReadToEnd();
            }

            var approvalStatus = Enum.GetName(typeof(ApprovalType), documentDetails.ApprovalStatus);


            htmlFileContent = htmlFileContent.Replace("{Title}", documentDetails.DocumentTitle)
                                             .Replace("{Document Id}", documentDetails.DocumentId)
                                             .Replace("{DocumentType}", documentDetails.DocumentFullForm)
                                             .Replace("{Revision}", documentDetails.Rev)
                                             .Replace("{Status}", approvalStatus)
                                             .Replace("{DocumentManager}", dropDownsAndUserDetails.DocumentPersonDetail?.ReportingUnder)
                                             .Replace("{DocumentEditedBy}", documentDetails.CreatedByName)
                                             .Replace("{OriginalCreationDate}", documentDetails.CreatedDate.ToString("dd-MM-yyyy"))
                                             .Replace("{ImplementationDate}", documentDetails.CreatedDate.ToString("dd-MM-yyyy"));

            htmlFileContent = PrepareEcnTabContent(documentDetails, htmlFileContent);
            htmlFileContent = PrepareEcnContent(documentDetails, htmlFileContent);
            htmlFileContent = PrepareAssociatedDocument(documentDetails, htmlFileContent);
            htmlFileContent = PrepareAttachmentDocument(documentDetails, htmlFileContent);
            htmlFileContent = PrepareRevisionHistory(documentDetails, htmlFileContent);
            htmlFileContent = PrepareElectronicNotificationList(documentDetails, htmlFileContent);
            htmlFileContent = PrepareApprovals(documentDetails, htmlFileContent);
            return htmlFileContent;
        }

        public static string PreparePurposeContent(GetDocumentByIdResponse documentDetails, string htmlFileContent)
        {
            string purposeContent = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"DocumentPurposeHTML.html"))
            {
                purposeContent = file.ReadToEnd();
            }

            purposeContent = purposeContent.Replace("{Purpose}", documentDetails.Purpose)
                                             .Replace("{Scope}", documentDetails.Scope)
                                             .Replace("{Responsibility}", documentDetails.Responsibility)
                                             .Replace("{Definitions}", documentDetails.Defination);

            htmlFileContent = string.Concat(htmlFileContent, purposeContent);
            return htmlFileContent;
        }

        public static string PrepareEcnContent(GetDocumentByIdResponse documentDetails, string htmlFileContent)
        {
            string ecnContentHTML = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"EcnDocumentContentHTML.html"))
            {
                ecnContentHTML = file.ReadToEnd();
            }

            ecnContentHTML = ecnContentHTML.Replace("{DescriptionOfChange}", documentDetails.DescriptionOfChange)
                                         .Replace("{ReasonForChange}", documentDetails.ReasonForChange)
                                         .Replace("{ImplementationDateInstructions}", documentDetails.InstructionForEcnImplementation)
                                         .Replace("{EcnWillChange}", documentDetails.EcnChangeValue);

            htmlFileContent = string.Concat(htmlFileContent, ecnContentHTML);

            return htmlFileContent;
        }

        public static string PrepareEcnTabContent(GetDocumentByIdResponse documentDetails, string htmlFileContent)
        {
            string ecnDocumentItemTableHeaderHTML = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"EcnItemTableHeaderHTML.html"))
            {
                ecnDocumentItemTableHeaderHTML = file.ReadToEnd();
            }

            ecnDocumentItemTableHeaderHTML = ecnDocumentItemTableHeaderHTML.Replace("{PartDescription}", documentDetails.PartDescriptionName)
                                                                           .Replace("{PartNumber}", documentDetails.PartNumber)
                                                                           .Replace("{DrawingNumber}", documentDetails.DrawingNumber)
                                                                           .Replace("{CustomerPartNo}", documentDetails.CustomerPartNumber)
                                                                           .Replace("{CurrentRev}", documentDetails.CurrentRev)
                                                                           .Replace("{NewRev}", documentDetails.NewRev);

            htmlFileContent = string.Concat(htmlFileContent, ecnDocumentItemTableHeaderHTML);

            return htmlFileContent;
        }

        public static string GenerateDocumentFooterTemplate(GetDocumentByIdResponse documentDetails)
        {
            string htmlFileContent = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"DocumentFooterHTML.html"))
            {
                htmlFileContent = file.ReadToEnd();
            }

            htmlFileContent = htmlFileContent.Replace("{Date}", documentDetails.ApprovalNotification?.OrderByDescending(x => x.ApprovalTime).FirstOrDefault()?.ApprovalTime?.ToString("dd-MM-yyyy"))
                                             .Replace("{currentDate}", DateTime.Now.ToString("dd-MM-yyyy"))
                                             .Replace("{DocumentId}", documentDetails.DocumentId)
                                             .Replace("{Rev}", documentDetails.Rev);

            return htmlFileContent;
        }

        private static string PrepareAssociatedDocument(GetDocumentByIdResponse documentDetails, string htmlFileContent)
        {
            string associatedDocumentItemTableHeaderHTML = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"AssociatedDocumentItemTableHeaderHTML.html"))
            {
                associatedDocumentItemTableHeaderHTML = file.ReadToEnd();
            }

            associatedDocumentItemTableHeaderHTML = string.Concat(htmlFileContent, associatedDocumentItemTableHeaderHTML);

            string associatedDocumentItemFileContent = string.Empty;
            foreach (var item in documentDetails.AssociatedDocument)
            {
                string subAssociatedDocumentItemFileContent = string.Empty;
                using (System.IO.StreamReader file = new System.IO.StreamReader(@"AssociatedDocumentItemTableHTML.html"))
                {
                    subAssociatedDocumentItemFileContent = file.ReadToEnd();
                }

                subAssociatedDocumentItemFileContent = subAssociatedDocumentItemFileContent.Replace("{DocumentId}", item.AssociatedDocumentId)
                                                         .Replace("{Rev}", item.Rev)
                                                         .Replace("{Status}", item.Status?.ToString());

                associatedDocumentItemFileContent = string.Concat(associatedDocumentItemFileContent, subAssociatedDocumentItemFileContent);
            }

            associatedDocumentItemFileContent += "</tbody></table>";
            htmlFileContent = string.Concat(associatedDocumentItemTableHeaderHTML, associatedDocumentItemFileContent);
            return htmlFileContent;
        }

        private static string PrepareAttachmentDocument(GetDocumentByIdResponse documentDetails, string htmlFileContent)
        {
            string attachedDocumentItemTableHeaderHTML = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"AttachedDocumentItemTableHeaderHTML.html"))
            {
                attachedDocumentItemTableHeaderHTML = file.ReadToEnd();
            }

            attachedDocumentItemTableHeaderHTML = string.Concat(htmlFileContent, attachedDocumentItemTableHeaderHTML);

            string attachedDocumentItemFileContent = string.Empty;
            foreach (var item in documentDetails.Attachment)
            {
                string subAssociatedDocumentItemFileContent = string.Empty;
                using (System.IO.StreamReader file = new System.IO.StreamReader(@"AttachedDocumentItemTableHTML.html"))
                {
                    subAssociatedDocumentItemFileContent = file.ReadToEnd();
                }

                subAssociatedDocumentItemFileContent = subAssociatedDocumentItemFileContent.Replace("{Filename}", item.LocalFileName)
                                                         .Replace("{AttachmentName}", item.AttachmentPath);

                attachedDocumentItemFileContent = string.Concat(attachedDocumentItemFileContent, subAssociatedDocumentItemFileContent);
            }

            attachedDocumentItemFileContent += "</tbody></table>";
            htmlFileContent = string.Concat(attachedDocumentItemTableHeaderHTML, attachedDocumentItemFileContent);
            return htmlFileContent;
        }

        private static string PrepareRevisionHistory(GetDocumentByIdResponse documentDetails, string htmlFileContent)
        {
            string revisionHistoryItemTableHeaderHTML = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"RevisionHistoryItemTableHeaderHTML.html"))
            {
                revisionHistoryItemTableHeaderHTML = file.ReadToEnd();
            }

            revisionHistoryItemTableHeaderHTML = string.Concat(htmlFileContent, revisionHistoryItemTableHeaderHTML);

            string revisionHistoryItemFileContent = string.Empty;
            foreach (var item in documentDetails.RevisionLog)
            {
                string subRevisionHistoryItemFileContent = string.Empty;
                using (System.IO.StreamReader file = new System.IO.StreamReader(@"RevisionHistoryItemTableHTML.html"))
                {
                    subRevisionHistoryItemFileContent = file.ReadToEnd();
                }

                subRevisionHistoryItemFileContent = subRevisionHistoryItemFileContent.Replace("{Rev}", item.Rev)
                                                         .Replace("{Sec/Para Changed}", item.SectionParaChanged)
                                                         .Replace("{Change Made}", item.ChangeMade)
                                                         .Replace("{Date}", documentDetails.DocumentTypeValue != "ECN" ? item.ModifiedDate?.ToString("dd-MM-yyyy") : " ")
                                                         .Replace("{Changed By}", documentDetails.DocumentTypeValue != "ECN" ? item.ModifiedBy.ToString() : " ");

                revisionHistoryItemFileContent = string.Concat(revisionHistoryItemFileContent, subRevisionHistoryItemFileContent);

                if (documentDetails.DocumentTypeValue == "ECN")
                {
                    break;
                }
            }

            revisionHistoryItemFileContent += "</tbody></table>";
            htmlFileContent = string.Concat(revisionHistoryItemTableHeaderHTML, revisionHistoryItemFileContent);
            return htmlFileContent;
        }

        private static string PrepareElectronicNotificationList(GetDocumentByIdResponse documentDetails, string htmlFileContent)
        {
            string electronicNotificationListHTML = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"ElectronicNotificationListHTML.html"))
            {
                electronicNotificationListHTML = file.ReadToEnd();
            }

            string name = string.Empty;

            foreach (var item in documentDetails.DocumentNotification)
            {
                name += item.Name + ",";
            }

            electronicNotificationListHTML = electronicNotificationListHTML.Replace("{Electronic Notification List}", name?.Trim(','));
            htmlFileContent = string.Concat(htmlFileContent, electronicNotificationListHTML);

            return htmlFileContent;
        }

        private static string PrepareApprovals(GetDocumentByIdResponse documentDetails, string htmlFileContent)
        {
            string approvalItemTableHeaderHTML = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"ApprovalItemTableHeaderHTML.html"))
            {
                approvalItemTableHeaderHTML = file.ReadToEnd();
            }

            approvalItemTableHeaderHTML = string.Concat(htmlFileContent, approvalItemTableHeaderHTML);

            string approvalItemFileContent = string.Empty;
            foreach (var item in documentDetails.ApprovalNotification)
            {
                string subApprovalItemFileContent = string.Empty;
                using (System.IO.StreamReader file = new System.IO.StreamReader(@"ApprovalItemTableHTML.html"))
                {
                    subApprovalItemFileContent = file.ReadToEnd();
                }

                subApprovalItemFileContent = subApprovalItemFileContent.Replace("{Name}", item.Name)
                                                         .Replace("{Approved}", item.IsApproved?.ToString())
                                                         .Replace("{Timestamp}", item.ApprovalTime?.ToString("dd-MM-yyyy"));

                approvalItemFileContent = string.Concat(approvalItemFileContent, subApprovalItemFileContent);
            }

            approvalItemFileContent += "</tbody></table>";
            htmlFileContent = string.Concat(approvalItemTableHeaderHTML, approvalItemFileContent);
            return htmlFileContent;
        }

        private static string PrepareCommonTabContent(GetDocumentByIdResponse documentDetails, string htmlFileContent)
        {
            string commonTabHTML = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"CommonTabHTML.html"))
            {
                commonTabHTML = file.ReadToEnd();
            }

            string tabName = string.Empty;
            string tabContent = string.Empty;

            if (documentDetails.DocumentTypeValue == "FCD")
            {
                tabName = "Form";
                tabContent = documentDetails.Form;
            }
            else if (documentDetails.DocumentTypeValue == "SOP" || documentDetails.DocumentTypeValue == "LIT")
            {
                tabName = "Notes";
                tabContent = documentDetails.Notes;
            }
            else if (documentDetails.DocumentTypeValue == "WI")
            {
                tabName = "Work Instruction";
                tabContent = documentDetails.WorkStation;
            }

            commonTabHTML = commonTabHTML.Replace("{Tab Name}", tabName)
                                         .Replace("{Tab Content}", tabContent);

            htmlFileContent = string.Concat(htmlFileContent, commonTabHTML);

            return htmlFileContent;
        }
    }
}
