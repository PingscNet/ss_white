﻿namespace SSWhite.Api.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
    using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
    using Microsoft.Extensions.Logging;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Common;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Extensions;
    using SSWhite.Core.Utilities;

    public class CustomModelBinder : ComplexTypeModelBinder
    {
        private readonly IDictionary<ModelMetadata, IModelBinder> _propertyBinders;

        public CustomModelBinder(IDictionary<ModelMetadata, IModelBinder> propertyBinders, ILoggerFactory loggerFactory, bool allowValidatingTopLevelNodes)
            : base(propertyBinders, loggerFactory, allowValidatingTopLevelNodes)
        {
            _propertyBinders = propertyBinders;
        }

        protected override Task BindProperty(ModelBindingContext bindingContext)
        {
            var baseTypeName = bindingContext.ActionContext.ActionDescriptor.Parameters[0].ParameterType.BaseType.Name;

            string value;
            if (baseTypeName == AppConstant.SERVICE_REQUEST || baseTypeName == AppConstant.SERVICE_SEARCH_REQUEST)
            {
                if (bindingContext.FieldName == AppConstant.SESSION)
                {
                    bindingContext.Result = ModelBindingResult.Success(SessionGetterSetter.Get());
                    return Task.CompletedTask;
                }

                value = bindingContext.ValueProvider.GetValue($"{AppConstant.DATA}.{bindingContext.FieldName}").FirstValue;
            }
            else if (!bindingContext.ModelName.IsNullOrWhiteSpace())
            {
                value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName).FirstValue;
            }
            else
            {
                value = bindingContext.ValueProvider.GetValue(bindingContext.FieldName).FirstValue;
            }

            if (!value.IsNullOrWhiteSpace())
            {
                if (bindingContext.ModelType == typeof(string))
                {
                    bindingContext.Result = ModelBindingResult.Success(value.Trim());

                    if (((DefaultModelMetadata)bindingContext.ModelMetadata).Attributes.PropertyAttributes.OfType<UpperCaseAttribute>().Any())
                    {
                        bindingContext.Result = ModelBindingResult.Success(value.Trim().ToUpper());
                    }

                    return Task.CompletedTask;
                }

                if (((DefaultModelMetadata)bindingContext.ModelMetadata).Attributes.PropertyAttributes.OfType<DecryptAttribute>().Any())
                {
                    bindingContext.Result = ModelBindingResult.Success(Convert.ToInt32(EncryptDecrypt.Decrypt(value, null)));
                    return Task.CompletedTask;
                }
            }

            return base.BindProperty(bindingContext);
        }

        protected override void SetProperty(ModelBindingContext bindingContext, string modelName, ModelMetadata propertyMetadata, ModelBindingResult result)
        {
            base.SetProperty(bindingContext, modelName, propertyMetadata, result);
        }
    }
}
