﻿namespace SSWhite.Api.Common
{
    using System;
    using System.Linq;
    using System.Reflection;
    using Microsoft.OpenApi.Models;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using Swashbuckle.AspNetCore.SwaggerGen;

    public class SwaggerParameterFilter : IParameterFilter
    {
        public void Apply(OpenApiParameter parameter, ParameterFilterContext context)
        {
            if (context.PropertyInfo != null)
            {
                var internalAttribute = context.PropertyInfo.GetCustomAttribute<Internal>();

                if (internalAttribute == null)
                {
                    var jsonConverterAttribute = context.PropertyInfo.GetCustomAttribute<JsonConverterAttribute>();

                    if (jsonConverterAttribute?.ConverterType == typeof(EncryptPropertyConverter))
                    {
                        if (parameter.Schema.Items != null)
                        {
                            parameter.Schema.Items.Type = "string";
                        }
                        else
                        {
                            parameter.Schema.Type = "string";
                        }
                    }
                }
                ////else
                ////{
                ////    parameter.Schema.Extensions.Add(,)
                ////}
            }
        }
    }
}
