namespace SSWhite.Api
{
    using System.Globalization;
    using System.IO;
    using FluentValidation.AspNetCore;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Options;
    using Microsoft.OpenApi.Models;
    using SSWhite.Api.Common;
    using SSWhite.Component.Common;
    using SSWhite.Component.Implementation.Cache;
    using SSWhite.Component.Interface.Cache;
    using SSWhite.Component.Middleware;
    using SSWhite.Core.Common;
    using SSWhite.Core.Common.Application;
    using SSWhite.Logger.Interface.ExceptionLogger;
    using StackExchange.Redis.Extensions.Core.Configuration;
    using StackExchange.Redis.Extensions.Newtonsoft;
    using Swashbuckle.AspNetCore.SwaggerUI;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var currentCultureInfo = new CultureInfo("en-IN");
            currentCultureInfo.DateTimeFormat.FullDateTimePattern = DateFormat.DATE_FORMAT;
            currentCultureInfo.DateTimeFormat.ShortDatePattern = DateFormat.DATE_FORMAT;
            currentCultureInfo.DateTimeFormat.ShortTimePattern = DateFormat.TIME_FORMAT;
            System.Threading.Thread.CurrentThread.CurrentCulture = currentCultureInfo;
            services.AddDistributedMemoryCache();
            services.AddSession();
            var context = new CustomAssemblyLoadContext();
            context.LoadUnmanagedLibrary(Path.Combine(Directory.GetCurrentDirectory(), "libwkhtmltox.dll"));
            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1.0", new OpenApiInfo()
                {
                });
                x.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "auth-token",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    In = ParameterLocation.Header,
                });
                x.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                          new string[] { }
                    }
                });
                x.SchemaFilter<SwaggerSchemaFilter>();
                x.ParameterFilter<SwaggerParameterFilter>();
            });
            services.AddMvc(x =>
            {
                x.EnableEndpointRouting = false;
                x.Filters.Add(typeof(ValidatorActionFilter));
                ////x.ModelBinderProviders.Add(new CustomBinderProvider());
                x.ValueProviderFactories.Add(new FValueProviderFactory());
            })
            .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_3_0)
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.DateFormatString = DateFormat.DATE_TIME_FORMAT;
            })
            .AddJsonOptions(options =>
            {
                ////options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
            })
            .AddFluentValidation(x =>
            {
                x.RegisterValidatorsFromAssemblyContaining<Service.Validator.Account.LogInRequestValidator>();
                x.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IExceptionLogger, Logger.Implementation.ExceptionLogger.ExceptionLogger>();
            services.AddScoped<ICache, CacheService>();
            services.Configure<AppSetting>(Configuration.GetSection("AppSetting"));
            services.Configure<IPAddress>(options =>
            {
                options.RequestIpAddress = ConfigureIpAddress.GetRequestIpAddress(services);
            });
            services.AddStackExchangeRedisExtensions<NewtonsoftSerializer>(x =>
            {
                return Configuration.GetSection("Redis").Get<RedisConfiguration>();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(x => x
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader().SetIsOriginAllowed(hostName => true));
            app.UseSession();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            new ConfigureMiddleware().Configure(app.ApplicationServices.GetRequiredService<IOptions<AppSetting>>(), app, app.ApplicationServices.GetRequiredService<ICache>());
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseStaticFiles();
            app.UseSwagger();
            app.UseSwaggerUI(x =>
            {
                x.DefaultModelsExpandDepth(-1);
                x.SwaggerEndpoint("./v1.0/swagger.json", "v1.0");
                x.InjectStylesheet("/swagger-ui/swagger.css");
                x.DocExpansion(DocExpansion.None);
            });
        }
    }
}
