﻿namespace SSWhite.Domain.Request.AttendanceChange
{
    public class ApproveOrDenyFtoRequest
    {
        public int? FtoId { get; set; }

        public bool? IsApprove { get; set; }

        public string Note { get; set; }
    }
}
