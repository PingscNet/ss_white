﻿namespace SSWhite.Domain.Request.AttendanceChange
{
    public class GetAllDepartmentOrUserFtoByUserIdRequest
    {
        public bool? OnlyFutureDate { get; set; }

        public bool? OnlyUsersData { get; set; }

        public bool? ApproveFTO { get; set; }
    }
}
