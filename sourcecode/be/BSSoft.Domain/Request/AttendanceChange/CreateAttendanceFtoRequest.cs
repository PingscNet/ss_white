﻿namespace SSWhite.Domain.Request.AttendanceChange
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Domain.Enums;

    public class CreateAttendanceFtoRequest
    {
        public string EmployeeID { get; set; }

        public string EmployeeName { get; set; }

        public DateTime? StartDate { get; set; }

        public DayType StartDateDayType { get; set; }

        public DateTime? EndDate { get; set; }

        public DayType EndDateDayType { get; set; }

        public int LeaveTypeID { get; set; }

        public int? OtherLeaveTypeId { get; set; }

        public string EmployeeComment { get; set; }

        public float NumberOfDays { get; set; }
    }
}
