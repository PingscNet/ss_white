﻿namespace SSWhite.Domain.Request.AttendanceChange
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Domain.Enums;

    public class GetAttendanceFtoByIdRequest
    {
        public int FtoId { get; set; }
    }
}
