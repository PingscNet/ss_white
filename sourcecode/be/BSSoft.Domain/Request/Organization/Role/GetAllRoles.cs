﻿namespace SSWhite.Domain.Request.Organization.Role
{
    using SSWhite.Core.Enums;

    public class GetAllRoles
    {
        public StatusType? Status { get; set; }
    }
}
