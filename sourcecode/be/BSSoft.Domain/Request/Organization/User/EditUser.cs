﻿namespace SSWhite.Domain.Request.Organization.User
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Common.Organization.User;

    public class EditUser : User
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }
    }
}
