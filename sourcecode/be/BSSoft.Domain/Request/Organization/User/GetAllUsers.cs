﻿namespace SSWhite.Domain.Request.Organization.User
{
    using SSWhite.Core.Enums;

    public class GetAllUsers
    {
        public StatusType? Status { get; set; }
    }
}
