﻿namespace SSWhite.Domain.Request.Master.SubUnit
{
    using SSWhite.Core.Enums;

    public class GetAllSubUnits
    {
        public StatusType? Status { get; set; }
    }
}
