﻿namespace SSWhite.Domain.Request.Master.Site
{
    using SSWhite.Core.Enums;

    public class GetAllSites
    {
        public StatusType? Status { get; set; }
    }
}
