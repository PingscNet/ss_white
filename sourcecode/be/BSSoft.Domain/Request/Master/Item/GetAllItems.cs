﻿namespace SSWhite.Domain.Request.Master.Item
{
    using SSWhite.Core.Enums;

    public class GetAllItems
    {
        public StatusType? Status { get; set; }

        public int? ItemGroupId { get; set; }

        public int? UnitId { get; set; }

        public int? ItemCategoryId { get; set; }

        public int? CrateId { get; set; }
    }
}
