﻿namespace SSWhite.Domain.Request.Master.ItemSubGroup
{
    using SSWhite.Core.Enums;

    public class GetAllItemSubGroups
    {
        public int? ItemGroupId { get; set; }

        public StatusType? Status { get; set; }
    }
}
