﻿namespace SSWhite.Domain.Request.Master.City
{
    using SSWhite.Core.Enums;

    public class GetAllCities
    {
        public int? CountryId { get; set; }

        public int? StateId { get; set; }

        public StatusType? Status { get; set; }
    }
}
