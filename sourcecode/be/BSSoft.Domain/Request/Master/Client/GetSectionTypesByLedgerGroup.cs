﻿namespace SSWhite.Domain.Request.Master.Client
{
    public class GetSectionTypesByLedgerGroup
    {
        public int? LedgerGroupId { get; set; }
    }
}
