﻿namespace SSWhite.Domain.Request.Master.Client
{
    using SSWhite.Core.Enums;

    public class GetAllClients
    {
        public StatusType? Status { get; set; }

        public int? CountryId { get; set; }

        public int? StateId { get; set; }

        public int? CityId { get; set; }

        public int? LedgerGroupId { get; set; }

        public int? RegionId { get; set; }
    }
}
