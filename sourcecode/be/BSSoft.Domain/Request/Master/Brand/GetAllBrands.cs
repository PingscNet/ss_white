﻿namespace SSWhite.Domain.Request.Master.Brand
{
    using SSWhite.Core.Enums;

    public class GetAllBrands
    {
        public StatusType? Status { get; set; }
    }
}
