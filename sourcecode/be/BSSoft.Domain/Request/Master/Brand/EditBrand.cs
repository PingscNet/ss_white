﻿namespace SSWhite.Domain.Request.Master.Brand
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Common.Master.Brand;

    public class EditBrand : Brand
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }
    }
}
