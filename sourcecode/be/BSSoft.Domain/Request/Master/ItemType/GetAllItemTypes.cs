﻿namespace SSWhite.Domain.Request.Master.ItemTypes
{
    using SSWhite.Core.Enums;

    public class GetAllItemTypes
    {
        public StatusType? Status { get; set; }
    }
}
