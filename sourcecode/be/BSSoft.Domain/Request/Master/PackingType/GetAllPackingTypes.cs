﻿namespace SSWhite.Domain.Request.Master.PackingType
{
    using SSWhite.Core.Enums;

    public class GetAllPackingTypes
    {
        public StatusType? Status { get; set; }
    }
}
