﻿namespace SSWhite.Domain.Request.Master.Region
{
    using SSWhite.Core.Enums;

    public class GetAllRegions
    {
        public StatusType? Status { get; set; }
    }
}
