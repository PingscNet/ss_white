﻿namespace SSWhite.Domain.Request.Master.Uqc
{
    using SSWhite.Core.Enums;

    public class GetAllUqcs
    {
        public int? SubUnitId { get; set; }

        public int? PackingTypeId { get; set; }

        public StatusType? Status { get; set; }
    }
}
