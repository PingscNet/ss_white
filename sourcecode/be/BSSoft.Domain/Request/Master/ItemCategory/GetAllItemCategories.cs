﻿namespace SSWhite.Domain.Request.Master.ItemCategory
{
    using SSWhite.Core.Enums;

    public class GetAllItemCategories
    {
        public StatusType? Status { get; set; }
    }
}
