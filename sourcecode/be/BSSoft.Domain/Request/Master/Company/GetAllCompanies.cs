﻿namespace SSWhite.Domain.Request.Master.Company
{
    using SSWhite.Core.Enums;

    public class GetAllCompanies
    {
        public StatusType? Status { get; set; }
    }
}
