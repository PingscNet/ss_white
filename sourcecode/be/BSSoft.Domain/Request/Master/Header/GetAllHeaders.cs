﻿namespace SSWhite.Domain.Request.Master.Header
{
    using SSWhite.Core.Enums;

    public class GetAllHeaders
    {
        public StatusType? Status { get; set; }
    }
}
