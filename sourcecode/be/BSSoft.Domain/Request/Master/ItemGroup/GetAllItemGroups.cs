﻿namespace SSWhite.Domain.Request.Master.ItemGroup
{
    using SSWhite.Core.Enums;

    public class GetAllItemGroups
    {
        public StatusType? Status { get; set; }
    }
}
