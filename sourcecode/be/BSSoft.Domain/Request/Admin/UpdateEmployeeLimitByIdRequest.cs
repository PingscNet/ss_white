﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UpdateEmployeeLimitByIdRequest
    {
        public int LimitId { get; set; }

        public decimal Limit { get; set; }
    }
}
