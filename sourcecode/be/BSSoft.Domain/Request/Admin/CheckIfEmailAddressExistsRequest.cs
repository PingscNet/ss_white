﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class CheckIfEmailAddressExistsRequest
    {
        public string EmailAddress { get; set; }
    }
}
