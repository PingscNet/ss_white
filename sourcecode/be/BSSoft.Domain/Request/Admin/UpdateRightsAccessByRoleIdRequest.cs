﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UpdateRightsAccessByRoleIdRequest
    {
        public int? RoleId { get; set; }

        public List<RoleRightsTypeList> RoleRightsTypeList { get; set; }
    }
}
