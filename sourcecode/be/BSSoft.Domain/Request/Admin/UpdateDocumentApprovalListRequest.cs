﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class UpdateDocumentApprovalListRequest
    {
        public int Id { get; set; }

        public bool Approvalfordocument { get; set; }
    }
}
