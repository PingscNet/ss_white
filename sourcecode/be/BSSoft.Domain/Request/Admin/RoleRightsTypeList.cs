﻿namespace SSWhite.Domain.Request.Admin
{
    public class RoleRightsTypeList
    {
        public string UniqueCode { get; set; }

        public bool IsAccess { get; set; }
    }
}
