﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.Admin
{
    public class GetEmployeeByEmployeeIdRequest
    {
        public string EmployeeId { get; set; }
    }
}
