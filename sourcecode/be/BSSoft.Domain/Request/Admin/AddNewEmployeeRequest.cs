﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class AddNewEmployeeRequest
    {
        public int Id { get; set; }

        public string PersonID { get; set; }

        public string Organization { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Gender { get; set; }

        public DateTime DOB { get; set; }

        public DateTime DOJ { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string BloodGroup { get; set; }

        public string EmailAddress { get; set; }

        public string EmpPosition { get; set; }

        public string ReportingUnder { get; set; }

        public string Department { get; set; }

        public string Signature { get; set; }

        public string Picture { get; set; }

        public string Password { get; set; }

        public string PhoneNo { get; set; }

        public int Role { get; set; }

        [Internal]
        public int? CreatedBy { get; set; }

        public IFormFile AvatarImage { get; set; }

        public IFormFile SignatureImage { get; set; }
    }
}
