﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class CheckIfEmployeeIdExistsRequest
    {
        public string EmployeeId { get; set; }
    }
}
