﻿namespace SSWhite.Domain.Request.Account
{
    using SSWhite.Core.Attributes;

    public class SendForgetPasswordEmailRequest
    {
        public string UserLogin { get; set; }
    }
}
