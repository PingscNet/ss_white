﻿namespace SSWhite.Domain.Request.Account
{
    using SSWhite.Core.Attributes;

    public class IsEmailValidRequest
    {
        public string EmailAddress { get; set; }
    }
}
