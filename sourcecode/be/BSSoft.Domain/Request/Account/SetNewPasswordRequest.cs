﻿namespace SSWhite.Domain.Request.Account
{
    using SSWhite.Core.Attributes;

    public class SetNewPasswordRequest
    {
        public string EmailAddress { get; set; }

        public string Password { get; set; }
    }
}
