﻿namespace SSWhite.Domain.Request.Document
{
    public class DocumentNotification
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int UserId { get; set; }

        public string PersonId { get; set; }
    }
}
