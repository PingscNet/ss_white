﻿namespace SSWhite.Domain.Request.Document
{
    public class GetAllDocumentsRequest
    {
        public int? DocumentType { get; set; }

        public int? DocumentStageStatus { get; set; }

        public bool? ApprovalData { get; set; }

        public bool? IsSendForApproval { get; set; }
    }
}
