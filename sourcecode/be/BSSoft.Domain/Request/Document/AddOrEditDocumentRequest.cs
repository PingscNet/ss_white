﻿namespace SSWhite.Domain.Request.Document
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Domain.Enums.Document;

    public class AddOrEditDocumentRequest
    {
        public int? Id { get; set; }

        public string DocumentTitle { get; set; }

        public string DocumentId { get; set; }

        public int DocumentType { get; set; }

        public string Rev { get; set; }

        public int? EcnNumber { get; set; }

        public string EcnTitle { get; set; }

        public DateTime? EcnDate { get; set; }

        public bool? CustomerApprovalRequired { get; set; }

        public bool? IsSendForApproval { get; set; }

        public bool? CustomerNotificationRequired { get; set; }

        public string PartDescriptionName { get; set; }

        public string PartNumber { get; set; }

        public string DrawingNumber { get; set; }

        public string CustomerPartNumber { get; set; }

        public string CurrentRev { get; set; }

        public string NewRev { get; set; }

        public string DescriptionOfChange { get; set; }

        public string ReasonForChange { get; set; }

        public string InstructionForEcnImplementation { get; set; }

        public string EcnChange { get; set; }

        public string Purpose { get; set; }

        public string Scope { get; set; }

        public string Responsibility { get; set; }

        public string Defination { get; set; }

        public string Form { get; set; }

        public string Notes { get; set; }

        public string WorkStation { get; set; }

        public bool? AttachmentApproval { get; set; }

        public string? SectionParaChanged { get; set; }

        public string? ChangeMade { get; set; }

        public int? TrainingRequired { get; set; }

        public List<IFormFile> AttachmentImage { get; set; }

        public List<DocumentNotification> DocumentNotification { get; set; }

        public List<ApprovalNotification> ApprovalNotification { get; set; }

        public List<Attachment> Attachment { get; set; }

        public List<AssociatedDocument> AssociatedDocument { get; set; }

        public List<int> DocumentAttachmentDeleteIds { get; set; }
    }
}
