﻿namespace SSWhite.Domain.Request.Document
{
    public class Attachment
    {
        public int? Id { get; set; }

        public string LocalFileName { get; set; }

        public string Title { get; set; }

        public string AttachmentPath { get; set; }
    }
}
