﻿namespace SSWhite.Domain.Request.Document.Purchase
{
    using SSWhite.Domain.Enums.Document;

    public class GetAllPurchaseDocuments : Document
    {
        public VoucherType? VoucherType { get; set; }
    }
}
