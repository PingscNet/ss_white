﻿namespace SSWhite.Domain.Request.Document
{
    public class GetEmployeeByDepartmentRequest
    {
        public string DepartmentName { get; set; }
    }
}
