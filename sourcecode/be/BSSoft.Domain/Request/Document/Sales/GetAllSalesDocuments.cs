﻿namespace SSWhite.Domain.Request.Document.Sales
{
    using SSWhite.Domain.Enums.Document;

    public class GetAllSalesDocuments : Document
    {
        public VoucherType? VoucherType { get; set; }
    }
}
