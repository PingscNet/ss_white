﻿namespace SSWhite.Domain.Request.Document
{
    public class AssociatedDocument
    {
        public int? Id { get; set; }

        public string AssociatedDocumentId { get; set; }

        public string Rev { get; set; }

        public string Title { get; set; }

        public int? ApprovalStatus { get; set; }

        public int? Status { get; set; }
    }
}
