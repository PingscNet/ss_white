﻿using System;

namespace SSWhite.Domain.Request.Document
{
    public class ApprovalNotification
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int UserId { get; set; }

        public string PersonId { get; set; }

        public DateTime? ApprovalTime { get; set; }

        public bool? IsApproved { get; set; }
    }
}
