﻿namespace SSWhite.Domain.Request.Document.Payment
{
    using System;
    using SSWhite.Core.Enums;
    using SSWhite.Domain.Enums.Document;

    public class GetAllPayments : Document
    {
        public DocumentType? PaymentType { get; set; }
    }
}
