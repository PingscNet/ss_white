﻿namespace SSWhite.Domain.Request.Document
{
    using System;
    using SSWhite.Core.Enums;

    public class Document
    {
        public int? CompanyId { get; set; }

        public int? ClientId { get; set; }

        public int? Month { get; set; }

        public int? FinancialYear { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public StatusType? Status { get; set; }
    }
}
