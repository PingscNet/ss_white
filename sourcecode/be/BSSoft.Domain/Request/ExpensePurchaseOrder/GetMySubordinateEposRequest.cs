﻿namespace SSWhite.Domain.Request.ExpensePurchaseOrder
{
    public class GetMySubordinateEposRequest
    {
        public int? ApprovalType { get; set; }
    }
}
