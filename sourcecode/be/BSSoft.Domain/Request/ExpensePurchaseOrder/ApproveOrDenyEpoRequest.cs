﻿namespace SSWhite.Domain.Request.ExpensePurchaseOrder
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class ApproveOrDenyEpoRequest
    {
        public int EpoId { get; set; }

        public bool? IsApprove { get; set; }
    }
}
