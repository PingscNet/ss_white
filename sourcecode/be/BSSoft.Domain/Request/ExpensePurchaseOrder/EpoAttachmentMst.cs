﻿namespace SSWhite.Domain.Request.ExpensePurchaseOrder
{
    using System;
    using System.Linq;

    public class EpoAttachmentMst
    {
        public int Id { get; set; }

        public string AttachedFileName { get; set; }

        public bool? Approval { get; set; }

        public DateTime AttachmentDate { get; set; }

        public string FileName => AttachedFileName.Split("_").FirstOrDefault();

        public string AttachmentImageName => AttachedFileName.Split("_").LastOrDefault();
    }
}