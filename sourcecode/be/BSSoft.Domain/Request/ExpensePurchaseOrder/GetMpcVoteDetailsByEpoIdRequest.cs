﻿namespace SSWhite.Domain.Request.ExpensePurchaseOrder
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class GetMpcVoteDetailsByEpoIdRequest
    {
        public int EpoId { get; set; }
    }
}
