﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.ExpensePurchaseOrder
{
    public class GetAllVendorOrEmployeeDetailsByNameRequest
    {
        public string Name { get; set; }

        public bool? IsEmployee { get; set; }
    }
}
