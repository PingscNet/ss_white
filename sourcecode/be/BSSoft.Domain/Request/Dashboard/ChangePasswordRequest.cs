﻿namespace SSWhite.Domain.Request.Dashboard
{
    using SSWhite.Core.Attributes;

    public class ChangePasswordRequest
    {
        public string CurrentPassword { get; set; }

        public string NewPassword { get; set; }

        public string VerifyNewPassword { get; set; }
    }
}
