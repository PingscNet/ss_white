﻿namespace SSWhite.Domain.Request.Common
{
    public class GetSiteByUserId
    {
        public int? UserId { get; set; }
    }
}
