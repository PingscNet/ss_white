﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetAllStates
    {
        public int? CountryId { get; set; }

        public StatusType? Status { get; set; }
    }
}
