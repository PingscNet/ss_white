﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetHeaderDetailByClientId
    {
        public int ClientId { get; set; }

        public ModuleType ModuleType { get; set; }
    }
}
