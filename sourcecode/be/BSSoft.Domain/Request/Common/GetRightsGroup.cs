﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetRightsGroup
    {
        public StatusType? Status { get; set; }
    }
}
