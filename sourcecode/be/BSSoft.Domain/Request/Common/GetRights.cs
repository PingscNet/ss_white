﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetRights
    {
        public StatusType? Status { get; set; }
    }
}
