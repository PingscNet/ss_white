﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;
    using SSWhite.Domain.Enums.Document;

    public class GetNumberConfiguration
    {
        public int? EntityId { get; set; }

        public int? FinancialYear { get; set; }

        public DocumentType? DocumentType { get; set; }

        public ModuleType? ModuleType { get; set; }

        public StatusType? Status { get; set; }
    }
}
