﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetSitesForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
