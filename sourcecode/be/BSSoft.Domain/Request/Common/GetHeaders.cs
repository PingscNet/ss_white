﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetHeaders
    {
        public StatusType? Status { get; set; }
    }
}
