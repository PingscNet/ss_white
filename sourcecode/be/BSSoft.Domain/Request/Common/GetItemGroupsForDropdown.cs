﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetItemGroupsForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
