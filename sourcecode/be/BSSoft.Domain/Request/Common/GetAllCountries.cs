﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetAllCountries
    {
        public StatusType? Status { get; set; }
    }
}
