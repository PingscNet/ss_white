﻿namespace SSWhite.Domain.Request.Common
{
    public class CheckIfLotNumberAssigned
    {
        public int LotNumber { get; set; }
    }
}
