﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetItemRates
    {
        public StatusType? Status { get; set; }
    }
}
