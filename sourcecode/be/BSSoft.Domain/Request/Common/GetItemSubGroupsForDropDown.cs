﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetItemSubGroupsForDropDown
    {
        public StatusType? Status { get; set; }
    }
}
