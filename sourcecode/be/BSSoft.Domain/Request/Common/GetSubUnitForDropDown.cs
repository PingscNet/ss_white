﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetSubUnitForDropDown
    {
        public StatusType? Status { get; set; }
    }
}
