﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetRegionsForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
