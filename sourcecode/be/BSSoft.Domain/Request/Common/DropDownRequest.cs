﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class DropDownRequest
    {
        public DropDownRequest()
        {
            Status = StatusType.Active;
        }

        public int? Id { get; set; }

        public StatusType? Status { get; set; }
    }
}
