﻿namespace SSWhite.Domain.Request.Common
{
    using System.Collections.Generic;
    using SSWhite.Domain.Enums.Master.LedgerGroup;

    public class RefreshClient
    {
        public List<LedgerAccountType> AccountTypes { get; set; }

        public string PartialView { get; set; }
    }
}
