﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetRolesForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
