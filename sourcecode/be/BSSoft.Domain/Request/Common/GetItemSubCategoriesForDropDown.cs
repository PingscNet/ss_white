﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetItemSubCategoriesForDropDown
    {
        public StatusType? Status { get; set; }
    }
}
