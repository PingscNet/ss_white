﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetLedgerGroupForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
