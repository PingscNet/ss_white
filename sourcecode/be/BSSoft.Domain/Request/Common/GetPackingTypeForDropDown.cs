﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetPackingTypeForDropDown
    {
        public StatusType? Status { get; set; }
    }
}
