﻿namespace SSWhite.Domain.Request.Report.InwardSlip
{
    using System;

    public class GetAllInwardSlipsForReport
    {
        public int? ItemId { get; set; }

        public int? SiteId { get; set; }

        public int? ClientId { get; set; }

        public int? FinancialYear { get; set; }

        public int? Month { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}
