﻿namespace SSWhite.Domain.Request.TimeAndAttendance
{
    using System;
    using SSWhite.Core.Attributes;

    public class InsertInOutDetailsRequest
    {
        [Internal]
        public int UserId { get; set; }

        public int PunchType { get; set; }

        public int InOutStatus { get; set; }
    }
}
