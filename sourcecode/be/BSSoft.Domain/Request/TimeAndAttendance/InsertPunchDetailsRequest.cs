﻿namespace SSWhite.Domain.Request.TimeAndAttendance
{
    using SSWhite.Core.Attributes;

    public class InsertPunchDetailsRequest
    {
        [Internal]
        public long EmployeeId { get; set; }

        public short InOutStatus { get; set; }

        public short PunchType { get; set; }
    }
}
