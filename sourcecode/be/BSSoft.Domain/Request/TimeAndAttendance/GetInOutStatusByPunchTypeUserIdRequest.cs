﻿namespace SSWhite.Domain.Request.TimeAndAttendance
{
    using System;
    using SSWhite.Core.Attributes;

    public class GetInOutStatusByPunchTypeUserIdRequest
    {
        [Internal]
        public int UserId { get; set; }

        public int AttendancePunchTypeId { get; set; }
    }
}
