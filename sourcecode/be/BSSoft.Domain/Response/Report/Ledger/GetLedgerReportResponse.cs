﻿namespace SSWhite.Domain.Response.Report.Ledger
{
    using System.Collections.Generic;
    using SSWhite.Domain.Common.Report.Ledger;

    public class GetLedgerReportResponse
    {
        public GetLedgerReportResponse()
        {
            Ledger = new Ledger();
        }

        public Ledger Ledger { get; set; }

        public List<LedgerDetail> LedgerDetails { get; set; }
    }
}
