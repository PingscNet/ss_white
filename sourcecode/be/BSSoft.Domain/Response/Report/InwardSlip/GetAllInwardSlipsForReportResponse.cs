﻿namespace SSWhite.Domain.Response.Report.InwardSlip
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using SSWhite.ResourceFile;

    public class GetAllInwardSlipsForReportResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.SlipNo))]
        public string SlipNumber { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.Date))]
        public DateTime SlipDate { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.CompanyName))]
        public string ClientName { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.Material))]
        public string ItemName { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.RecQty))]
        public decimal ReceivedQuantity { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.BillQty))]
        public decimal BillQuantity { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.Unit))]
        public string Uqc { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.VehicleNo))]
        public string VehicleNumber { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.WeightBridgeSlipNo))]
        public string WeightBridgeSlipNumber { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.Remarks))]
        public string Remarks { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.SupplyDocNo))]
        public string SupplyDocumentNumber { get; set; }

        [ExportMapping(ExcelName = nameof(Headers.LrDate))]
        public DateTime? LrDate { get; set; }
    }
}
