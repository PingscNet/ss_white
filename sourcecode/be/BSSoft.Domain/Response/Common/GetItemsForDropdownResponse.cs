﻿namespace SSWhite.Domain.Response.Common
{
    public class GetItemsForDropdownResponse : DropDownResponse
    {
        public decimal IgstRate { get; set; }

        public decimal CgstRate { get; set; }

        public decimal SgstRate { get; set; }

        public decimal CessRate { get; set; }

        public int UnitId { get; set; }

        public int TaxId { get; set; }

        public int CrateId { get; set; }
    }
}
