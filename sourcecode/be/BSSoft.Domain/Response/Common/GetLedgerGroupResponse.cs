﻿namespace SSWhite.Domain.Response.Common
{
    using System.Collections.Generic;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums.Master.LedgerGroup;

    public class GetLedgerGroupResponse : DropDownResponse
    {
        public string Sections { get; set; }

        public List<LedgerSectionType> SectionTypes => Sections?.DeserializeJson<List<LedgerSectionType>>();
    }
}
