﻿namespace SSWhite.Domain.Response.Common
{
    public class GetTaxesForDropdownResponse : DropDownResponse
    {
        public decimal IgstRate { get; set; }

        public decimal CgstRate { get; set; }

        public decimal SgstRate { get; set; }

        public decimal CessRate { get; set; }
    }
}
