﻿namespace SSWhite.Domain.Response.Common
{
    using System;
    using SSWhite.Core.Enums;

    public class GetItemRatesResponse
    {
        public int ItemGroupId { get; set; }

        public int ItemId { get; set; }

        public int SiteId { get; set; }

        public decimal Rate { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public StatusType Status { get; set; }
    }
}
