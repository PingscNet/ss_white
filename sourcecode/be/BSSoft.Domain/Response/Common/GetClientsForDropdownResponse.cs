﻿namespace SSWhite.Domain.Response.Common
{
    public class GetClientsForDropdownResponse : DropDownResponse
    {
        public string Gstin { get; set; }

        public string StateCode { get; set; }
    }
}
