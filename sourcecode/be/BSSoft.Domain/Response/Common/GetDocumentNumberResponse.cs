﻿namespace SSWhite.Domain.Response.Common
{
    using SSWhite.Core.Enums;
    using SSWhite.Domain.Enums.Document;

    public class GetDocumentNumberResponse
    {
        public int Id { get; set; }

        public int? EntityId { get; set; }

        public DocumentType? DocumentType { get; set; }

        public ModuleType? ModuleType { get; set; }

        public int? FinancialYear { get; set; }

        public int Number { get; set; }

        public string Prefix { get; set; }

        public string Suffix { get; set; }

        public StatusType Status { get; set; }
    }
}
