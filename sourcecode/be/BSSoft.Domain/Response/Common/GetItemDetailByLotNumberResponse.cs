﻿namespace SSWhite.Domain.Response.Common
{
    public class GetItemDetailByLotNumberResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? Weight { get; set; }
    }
}
