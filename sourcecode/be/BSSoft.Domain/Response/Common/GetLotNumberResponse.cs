﻿namespace SSWhite.Domain.Response.Common
{
    public class GetLotNumberResponse : DropDownResponse
    {
        public string LotNumber { get; set; }

        public string ItemLotNumber => $"{LotNumber} - {Name}";
    }
}
