﻿namespace SSWhite.Domain.Response.AttendanceChange
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Domain.Enums;

    public class GetAttendanceFtoByIdResponse
    {
        public int Id { get; set; }

        public string EmployeeID { get; set; }

        public string EmployeeName { get; set; }

        public DateTime StartDate { get; set; }

        public DayType StartDateDayType { get; set; }

        public DateTime EndDate { get; set; }

        public DayType EndDateDayType { get; set; }

        public int LeaveTypeID { get; set; }

        public int OtherLeaveTypeId { get; set; }

        public string EmployeeComment { get; set; }

        public string SupervisorNotes { get; set; }

        public string HRNotes { get; set; }

        public float NumberOfDays { get; set; }

        public int SupervisorApproval { get; set; }

        public int HRApproval { get; set; }
    }
}
