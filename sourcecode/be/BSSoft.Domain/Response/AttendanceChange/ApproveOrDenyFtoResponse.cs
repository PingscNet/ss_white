﻿namespace SSWhite.Domain.Response.AttendanceChange
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Domain.Enums;

    public class ApproveOrDenyFtoResponse
    {
        public int FtoId { get; set; }

        public DateTime StartDate { get; set; }

        public DayType StartDateDayType { get; set; }

        public DateTime EndDate { get; set; }

        public DayType EndDateDayType { get; set; }

        public string LeaveTypeName { get; set; }

        public string CreatedBy { get; set; }

        public string UserEmailAddress { get; set; }

        public string HrEmailAddress { get; set; }
    }
}
