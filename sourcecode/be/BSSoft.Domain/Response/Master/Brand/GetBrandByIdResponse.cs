﻿namespace SSWhite.Domain.Response.Master.Brand
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Common.Master.Brand;

    public class GetBrandByIdResponse : Brand
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }
    }
}
