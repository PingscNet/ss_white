﻿namespace SSWhite.Domain.Response.Master.Client
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllClientsResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string CompanyName { get; set; }

        public string LedgerGroup { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string MobileNumber { get; set; }

        public string Region { get; set; }

        public string EmailAddress { get; set; }

        public StatusType Status { get; set; }
    }
}
