﻿namespace SSWhite.Domain.Response.Master.Tax
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllTaxesResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal? IgstRate { get; set; }

        public decimal? CgstRate { get; set; }

        public decimal? SgstRate { get; set; }

        public decimal? CessRate { get; set; }

        public StatusType Status { get; set; }
    }
}
