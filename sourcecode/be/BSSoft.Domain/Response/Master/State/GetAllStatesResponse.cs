﻿namespace SSWhite.Domain.Response.Master.State
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllStatesResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public StatusType Status { get; set; }
    }
}
