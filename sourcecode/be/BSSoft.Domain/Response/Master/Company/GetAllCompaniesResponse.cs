﻿namespace SSWhite.Domain.Response.Master.Company
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllCompaniesResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Gstin { get; set; }

        public string ContactPerson { get; set; }

        public string ContactNumber { get; set; }

        public string Address { get; set; }

        public string EmailAddress { get; set; }

        public StatusType Status { get; set; }
    }
}
