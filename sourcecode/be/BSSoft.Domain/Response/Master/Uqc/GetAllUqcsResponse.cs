﻿namespace SSWhite.Domain.Response.Master.Uqc
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllUqcsResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string SubUnit { get; set; }

        public string PackingType { get; set; }

        public decimal? ConversionUnit { get; set; }

        public StatusType? Status { get; set; }
    }
}
