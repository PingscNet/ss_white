﻿namespace SSWhite.Domain.Response.Master.ItemCategory
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllItemCategoriesResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public StatusType? Status { get; set; }
    }
}
