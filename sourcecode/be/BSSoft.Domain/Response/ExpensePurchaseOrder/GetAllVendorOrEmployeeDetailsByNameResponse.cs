﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    public class GetAllVendorOrEmployeeDetailsByNameResponse
    {
        public string Name { get; set; }

        public string Id { get; set; }

        public string Street1 { get; set; }

        public string Street2 { get; set; }

        public string Email { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public int Country { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Zip { get; set; }

        public string GstNo { get; set; }
    }
}
