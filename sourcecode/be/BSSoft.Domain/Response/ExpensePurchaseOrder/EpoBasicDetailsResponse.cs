﻿namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Newtonsoft.Json;
    using SSWhite.Core.Common;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Enums;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;

    public class EpoBasicDetailsResponse
    {
        public int? Id { get; set; }

        public bool IsEmployee { get; set; }

        public string EmployeeVendor { get; set; }

        public string EmployeeId { get; set; }

        public string Vendor { get; set; }

        public string VendorId { get; set; }

        public string Street1 { get; set; }

        public string Street2 { get; set; }

        public int Country { get; set; }

        public string State { get; set; }

        public string City { get; set; }

        public string Zip { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string GstNo { get; set; }

        public string Email { get; set; }

        public string PurposeOfPurchase { get; set; }

        public int EpoType { get; set; }

        public int ReceiptMethod { get; set; }

        public string EpoPurchaseStatus { get; set; }

        public int PaymentType { get; set; }

        public string PurchaseNotes { get; set; }

        public string InternalNotes { get; set; }

        public decimal SubTotal { get; set; }

        public decimal GrandTotal { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]
        public DateTime? DateOfPaymentCheckRequestOrCashAdvance { get; set; }

        public string DaysOfPaymentPayWhenBilled { get; set; }

        public int CurrencyType { get; set; }

        public bool? IsAttachmentPending { get; set; }

        public decimal DollarPrice { get; set; }

        public decimal TotalSgst { get; set; }

        public decimal TotalCgst { get; set; }

        public decimal TotalIgst { get; set; }

        public bool IsWitinGujarat { get; set; }

        public int SelfApproval { get; set; }

        public int ManagerApproval { get; set; }

        public int MPCApproval { get; set; }

        public int MPCHigherApproval { get; set; }

        public int EmpLimitNew { get; set; }

        public string PersonName { get; set; }

        public int Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CountryName { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string ModifiedBy { get; set; }
    }
}
