﻿namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    public class GetAuthorizedLimitByUserIdResponse
    {
        public decimal EmpLimitNew { get; set; }

        public decimal DollarPrice { get; set; }

        public decimal EmpLimitNewDollar { get; set; }
    }
}
