﻿namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class CurrencyRateResponse
    {
        public Motd Motd { get; set; }

        public bool Success { get; set; }

        [JsonProperty("_Base")]
        public string Base { get; set; }

        public string Date { get; set; }

        public Rates Rates { get; set; }
    }

    public class Motd
    {
        public string Msg { get; set; }

        public string Url { get; set; }
    }

    public class Rates
    {
        public decimal INR { get; set; }
    }
}
