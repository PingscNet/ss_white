﻿namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetMyApprovalForEpoResponse
    {
        public int Id { get; set; }

        public string Vendor { get; set; }

        public string CreatedBy { get; set; }

        public decimal GrandTotal { get; set; }

        public string PurposeOfPurchase { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
