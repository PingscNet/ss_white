﻿namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetAllEPODropDownsResponse
    {
        public List<EmployeeDropDown> EmployeeDropDown { get; set; }

        public List<CountryDropDown> CountryDropDown { get; set; }

        public List<EpoTypeDropDown> EpoTypeDropDown { get; set; }

        public List<EPORefDropDown> EpoDeptDropDown { get; set; }

        public List<EPORefDropDown> EpoProductDropDown { get; set; }

        public List<EPORefDropDown> EpoAccountNoDropDown { get; set; }

        public List<UomDropDown> UomDropDown { get; set; }

        public List<TaxDropDown> TaxDropDown { get; set; }
    }

    public class EmployeeDropDown
    {
        public string PersonID { get; set; }

        public string PersonName { get; set; }

        public static implicit operator List<object>(EmployeeDropDown v)
        {
            throw new NotImplementedException();
        }
    }

    public class CountryDropDown
    {
        public int ID { get; set; }

        public string CountryName { get; set; }
    }

    public class EpoTypeDropDown
    {
        public int ID { get; set; }

        public string Name { get; set; }
    }

    public class EPORefDropDown
    {
        public int ID { get; set; }

        public string RefName { get; set; }
    }

    public class UomDropDown
    {
        public int ID { get; set; }

        public string UOMName { get; set; }
    }

    public class TaxDropDown
    {
        public int ID { get; set; }

        public decimal Rate { get; set; }
    }
}
