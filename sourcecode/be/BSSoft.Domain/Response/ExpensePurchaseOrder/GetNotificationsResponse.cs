﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    public class GetNotificationsResponse
    {
        public int Id { get; set; }

        public int ModuleType { get; set; }

        public int UserId { get; set; }

        public int DocumentId { get; set; }

        public string DocumentNo { get; set; }

        public int SuperiorIds { get; set; }

        public int ActionType { get; set; }

        public string Message { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

    }
}
