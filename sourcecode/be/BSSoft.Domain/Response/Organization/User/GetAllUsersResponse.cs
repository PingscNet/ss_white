﻿namespace SSWhite.Domain.Response.Organization.User
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Utilities;

    public class GetAllUsersResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        [JsonIgnore]
        public string FirstName { get; set; }

        [JsonIgnore]
        public string LastName { get; set; }

        public string Name => $"{FirstName} {LastName}";

        public string UserName { get; set; }

        [JsonIgnore]
        public UserType UserType { get; set; }

        public string EmailAddress { get; set; }

        public string MobileNumber { get; set; }

        public StatusType Status { get; set; }

        public string StatusTypeString => CommonMethods.GetDisplayValue<StatusType>(Status);
    }
}
