﻿namespace SSWhite.Domain.Response.TimeAndAttendance
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Common;
    using SSWhite.Core.Enums;

    public class GetInOutDetailsByUserIdResponse
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        ////public double TotalHours { get; set; }

        public DateTime PunchTime { get; set; }

        public int InOutStatus { get; set; }

        public int PunchType { get; set; }
    }
}
