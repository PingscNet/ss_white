﻿namespace SSWhite.Domain.Response.TimeAndAttendance
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Core.Common;
    using SSWhite.Core.Enums;

    public class GetInOutDetailsByUserId
    {
        public DateTime Date { get; set; }

        public string FirstPunch { get; set; }

        public string LastPunch { get; set; }

        public string PunchType { get; set; }

        public double TotalHours { get; set; }
    }
}
