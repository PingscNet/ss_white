﻿namespace SSWhite.Domain.Response.TimeAndAttendance
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Core.Common;
    using SSWhite.Core.Enums;
    using SSWhite.Domain.Enums.Master.PunchType;

    public class GetInOutDetailsByUserIdFinalResponse
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public DateTime InPunchTime { get; set; }

        public DateTime OutPunchTime { get; set; }

        public int TotalHours { get; set; }
        
        public string PunchType { get; set; }
    }
}
