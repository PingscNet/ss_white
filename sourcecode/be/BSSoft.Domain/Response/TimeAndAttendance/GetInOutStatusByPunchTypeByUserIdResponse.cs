﻿namespace SSWhite.Domain.Response.TimeAndAttendance
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Core.Common;
    using SSWhite.Core.Enums;

    public class GetInOutStatusByPunchTypeByUserIdResponse
    {
        public int AttendanceInOutStatusId { get; set; }
    }
}
