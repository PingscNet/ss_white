﻿namespace SSWhite.Domain.Response.Account
{
    using System.Collections.Generic;
    using SSWhite.Core.Common;
    using SSWhite.Core.Enums;

    public class LogInResponse
    {
        public LogInResponse()
        {
            Entities = new List<UserEntity>();
            Rights = new List<UserRight>();
        }

        public int SubscriberId { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }

        public UserType UserType { get; set; }

        public bool IsPasswordReset { get; set; }

        public List<UserEntity> Entities { get; set; }

        public List<UserRight> Rights { get; set; }
    }
}
