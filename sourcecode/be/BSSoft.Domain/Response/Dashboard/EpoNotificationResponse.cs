﻿namespace SSWhite.Domain.Response.Dashboard
{
    public class EpoNotificationResponse
    {
        public int EpoId { get; set; }

        public bool IsAttachmentPending { get; set; }
    }
}
