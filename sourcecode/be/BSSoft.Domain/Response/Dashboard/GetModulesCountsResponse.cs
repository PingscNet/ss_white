﻿using System.Collections.Generic;

namespace SSWhite.Domain.Response.Dashboard
{
    public class GetModulesCountsResponse
    {

        public int MpcVoteForEpoCount { get; set; }

        public int EpoApprovalCount { get; set; }

        public int FtoApprovalCount { get; set; }

        public int DocumentApprovalCount { get; set; }
    }
}
