﻿using System.Collections.Generic;

namespace SSWhite.Domain.Response.Dashboard
{
    public class UserNotificationResponse
    {
        public List<EpoNotificationResponse> EpoNotificationResponse { get; set; }

        public List<FtoNotificationResponse> FtoNotificationResponse { get; set; }
    }
}
