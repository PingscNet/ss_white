﻿namespace SSWhite.Domain.Response.Dashboard
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Core.Common;
    using SSWhite.Core.Enums;

    public class UserDetail
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public UserType UserType { get; set; }

        public string REMARKS { get; set; }

        public StatusType Status { get; set; }

        public string EmailAddress { get; set; }

        public string Language1 { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Department { get; set; }

        public DateTime? DateOfJoining { get; set; }

        public string PhoneNo { get; set; }

        public string PinCode { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public DateTime PunchTime { get; set; }

        public string InOutStatus { get; set; }

        public string Picture { get; set; }

        public string PersonId { get; set; }

        public bool IsPictureExists { get; set; }
    }
}
