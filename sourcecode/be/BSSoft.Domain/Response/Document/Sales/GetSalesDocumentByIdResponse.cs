﻿namespace SSWhite.Domain.Response.Document.Sales
{
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Common.Document.Sales;

    public class GetSalesDocumentByIdResponse : SalesDocument
    {
        public string ClientName { get; set; }

        public string VoucherTypeString => CommonMethods.GetDisplayValue(VoucherType);
    }
}
