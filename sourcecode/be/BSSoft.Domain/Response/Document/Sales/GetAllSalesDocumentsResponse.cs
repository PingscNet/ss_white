﻿namespace SSWhite.Domain.Response.Document.Sales
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums.Document;

    public class GetAllSalesDocumentsResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string VoucherNumber { get; set; }

        public DateTime Date { get; set; }

        public string ClientName { get; set; }

        public VoucherType VoucherType { get; set; }

        public string ReceivedBy { get; set; }

        public decimal? AdvanceAmount { get; set; }

        public decimal TotalAmount { get; set; }

        public StatusType? Status { get; set; }

        public string VoucherTypeString => CommonMethods.GetDisplayValue(VoucherType);
    }
}
