﻿namespace SSWhite.Domain.Response.Document.Payment
{
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Common.Document.Payment;
    using SSWhite.Domain.Enums.Document;

    public class GetPaymentByIdResponse : Payment
    {
        public string ClientName { get; set; }

        public string DayBook { get; set; }

        public string Bank { get; set; }

        public string PaymentTypeString => CommonMethods.GetDisplayValue<DocumentType?>(PaymentType);
    }
}
