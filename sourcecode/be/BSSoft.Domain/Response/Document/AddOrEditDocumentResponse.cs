﻿using SSWhite.Domain.Enums.Document;

namespace SSWhite.Domain.Response.Document
{
    public class AddOrEditDocumentResponse
    {
        public string Id { get; set; }

        public int DocumentType { get; set; }

        public string DocumentId { get; set; }

        public string DocumentTitle { get; set; }

        public string NotificationEmailIds { get; set; }

        public string ApprovalEmailIds { get; set; }

        public string CreatedBy { get; set; }
    }
}
