﻿namespace SSWhite.Domain.Response.Document.JournalVoucher
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllJournalVouchersResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string VoucherNumber { get; set; }

        public DateTime Date { get; set; }

        public string CreditedClientName { get; set; }

        public string DebitedClientName { get; set; }

        public decimal TotalAmount { get; set; }

        public string Remarks { get; set; }

        public StatusType? Status { get; set; }
    }
}
