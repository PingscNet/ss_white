﻿namespace SSWhite.Domain.Response.Document
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Domain.Request.Document;

    public class GetDocumentsDropdownAndUserDetailsResponse
    {
        public List<DocumentEcnChange> DocumentEcnChange { get; set; }

        public List<string> DepartmentName { get; set; }

        public List<DocumentApprovalPerson> DocumentApprovalPerson { get; set; }

        public DocumentPersonDetail DocumentPersonDetail { get; set; }
    }
}