﻿namespace SSWhite.Domain.Response.Document.Purchase
{
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Common.Document.Purchase;

    public class GetPurchaseDocumentByIdResponse : PurchaseDocument
    {
        public string ClientName { get; set; }

        public string VoucherTypeString => CommonMethods.GetDisplayValue(VoucherType);
    }
}
