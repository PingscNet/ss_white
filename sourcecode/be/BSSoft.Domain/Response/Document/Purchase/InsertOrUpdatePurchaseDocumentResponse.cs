﻿namespace SSWhite.Domain.Response.Document.Purchase
{
    public class InsertOrUpdatePurchaseDocumentResponse
    {
        public int ReturnValue { get; set; }

        public int LotNumber { get; set; }

        public decimal Weight { get; set; }

        public decimal Quantity { get; set; }
    }
}
