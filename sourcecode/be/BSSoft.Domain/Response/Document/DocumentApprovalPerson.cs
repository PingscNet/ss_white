﻿namespace SSWhite.Domain.Response.Document
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Domain.Request.Document;

    public class DocumentApprovalPerson
    {
        public int Id { get; set; }

        public string PersonName { get; set; }
    }
}