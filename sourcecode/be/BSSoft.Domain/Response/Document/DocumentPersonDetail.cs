﻿namespace SSWhite.Domain.Response.Document
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Domain.Request.Document;

    public class DocumentPersonDetail
    {
        public string PersonName { get; set; }

        public string ReportingUnder { get; set; }
    }
}