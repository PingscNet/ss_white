﻿namespace SSWhite.Domain.Response.Document
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Common;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Enums.Document;
    using SSWhite.Domain.Request.Document;

    public class GetDocumentByIdResponse
    {
        public int Id { get; set; }

        public string DocumentTitle { get; set; }

        public string DocumentId { get; set; }

        public int DocumentType { get; set; }

        public string Rev { get; set; }

        public int? EcnNumber { get; set; }

        public string EcnTitle { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]
        public DateTime? EcnDate { get; set; }

        public bool? CustomerApprovalRequired { get; set; }

        public bool? CustomerNotificationRequired { get; set; }

        public string PartDescriptionName { get; set; }

        public string PartNumber { get; set; }

        public string DrawingNumber { get; set; }

        public string CustomerPartNumber { get; set; }

        public string CurrentRev { get; set; }

        public string NewRev { get; set; }

        public string DescriptionOfChange { get; set; }

        public string ReasonForChange { get; set; }

        public string InstructionForEcnImplementation { get; set; }

        public string EcnChange { get; set; }

        public string EcnChangeValue { get; set; }

        public string Purpose { get; set; }

        public string Scope { get; set; }

        public string Responsibility { get; set; }

        public string Defination { get; set; }

        public string Form { get; set; }

        public string Notes { get; set; }

        public string WorkStation { get; set; }

        public bool? AttachmentApproval { get; set; }

        public int? ApprovalStatus { get; set; }

        public int? ModifiedBy { get; set; }

        public int? AwaitingApprovalStatus { get; set; }

        public List<Attachment> Attachment { get; set; }

        public List<DocumentNotification> DocumentNotification { get; set; }

        public List<ApprovalNotification> ApprovalNotification { get; set; }

        public List<AssociatedDocument> AssociatedDocument { get; set; }

        public List<RevisionLog> RevisionLog { get; set; }

        public string DocumentTypeValue { get; set; }

        public string DocumentFullForm { get; set; }

        public string CreatedByName { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}