﻿namespace SSWhite.Domain.Response.Document.Receipt
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums.Document;

    public class GetAllReceiptsResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string ReceiptNumber { get; set; }

        public DateTime Date { get; set; }

        public string DayBook { get; set; }

        public string ClientName { get; set; }

        public DocumentType ReceiptType { get; set; }

        public decimal ReceivedAmount { get; set; }

        public decimal TotalAmount { get; set; }

        public StatusType? Status { get; set; }

        public string ReceiptTypeString => CommonMethods.GetDisplayValue(ReceiptType);
    }
}
