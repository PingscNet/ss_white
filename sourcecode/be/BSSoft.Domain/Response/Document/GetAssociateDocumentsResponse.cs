﻿namespace SSWhite.Domain.Response.Document
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAssociateDocumentsResponse
    {
        public int Id { get; set; }

        public string DocumentId { get; set; }

        public string Documenttitle { get; set; }

        public string Rev { get; set; }

        public string PersonName { get; set; }

        public DateTime CreatedDate { get; set; }

        [JsonConverter(typeof(EnumStringConverter))]
        public ApprovalType? ApprovalStatus { get; set; }

        public string DocumentType { get; set; }
    }
}