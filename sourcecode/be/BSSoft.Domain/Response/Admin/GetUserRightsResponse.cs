﻿namespace SSWhite.Domain.Response.Admin
{
    public class GetUserRightsResponse
    {
        public string UniqueCode { get; set; }

        public bool IsAccess { get; set; }
    }
}
