﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using SSWhite.Core.Common;
using SSWhite.Core.Converters;
namespace SSWhite.Domain.Response.Admin
{
    public class GetEmployeeByEmployeeIdResponse
    {
        public int Id { get; set; }

        public string EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Organization { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]

        public string DOB { get; set; }

        public int Gender { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string PhoneNo { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]

        public DateTime DOJ { get; set; }

        public string BloodGroup { get; set; }

        public string EmailAddress { get; set; }

        public string ReportingUnder { get; set; }

        public string Department { get; set; }

        public string EmpPosition { get; set; }

        public string RoleId { get; set; }

        public string AvatarImage { get; set; }

        public string SignatureImage { get; set; }

    }
}
