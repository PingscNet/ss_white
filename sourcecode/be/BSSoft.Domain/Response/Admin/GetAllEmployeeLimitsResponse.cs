﻿namespace SSWhite.Domain.Response.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetAllEmployeeLimitsResponse
    {
        public int Id { get; set; }

        public string EmployeeId { get; set; }

        public int EmpLimitNew { get; set; }

        public string PersonName { get; set; }

        public string ReportingUnder { get; set; }

        public string EmpDept { get; set; }

        public int Status { get; set; }
    }
}
