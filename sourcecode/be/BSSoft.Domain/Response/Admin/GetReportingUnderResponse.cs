﻿namespace SSWhite.Domain.Response.Admin
{
    public class GetReportingUnderResponse
    {
        public int Id { get; set; }

        public string DepartmentSupervisorName { get; set; }
    }
}
