﻿namespace SSWhite.Domain.Response.Admin
{
    public class CheckIfEmailAddressExistsResponse
    {
        public string Message { get; set; }
    }
}
