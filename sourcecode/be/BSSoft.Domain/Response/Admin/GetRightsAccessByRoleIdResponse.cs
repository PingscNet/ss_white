﻿namespace SSWhite.Domain.Response.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetRightsAccessByRoleIdResponse
    {
        public string UniqueCode { get; set; }

        public bool IsAccess { get; set; }
    }
}
