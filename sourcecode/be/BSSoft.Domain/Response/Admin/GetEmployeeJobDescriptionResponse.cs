﻿namespace SSWhite.Domain.Response.Admin
{
    public class GetEmployeeJobDescriptionResponse
    {
        public int Id { get; set; }

        public string DepartmentName { get; set; }
    }
}
