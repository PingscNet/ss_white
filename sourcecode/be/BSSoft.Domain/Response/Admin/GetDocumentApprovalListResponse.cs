﻿namespace SSWhite.Domain.Response.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetDocumentApprovalListResponse
    {
        public int Id { get; set; }

        public string PersonName { get; set; }

        public bool Approvalfordocument { get; set; }
    }
}
