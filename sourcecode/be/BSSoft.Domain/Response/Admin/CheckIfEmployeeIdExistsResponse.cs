﻿namespace SSWhite.Domain.Response.Admin
{
    public class CheckIfEmployeeIdExistsResponse
    {
        public string Message { get; set; }
    }
}
