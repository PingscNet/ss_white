﻿namespace SSWhite.Domain.Common.Master.Item
{
    public class ItemUnit
    {
        public int? UnitId { get; set; }

        public string Unit { get; set; }

        public decimal Labour { get; set; }
    }
}
