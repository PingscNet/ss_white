﻿namespace SSWhite.Domain.Common.Master.Item
{
    public class ItemCrate
    {
        public int? CrateId { get; set; }

        public string Crate { get; set; }

        public decimal Deposit { get; set; }
    }
}
