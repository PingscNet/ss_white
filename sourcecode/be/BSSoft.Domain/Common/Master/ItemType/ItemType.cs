﻿namespace SSWhite.Domain.Common.Master.ItemType
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class ItemType
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        [UpperCase]
        public string Name { get; set; }
    }
}
