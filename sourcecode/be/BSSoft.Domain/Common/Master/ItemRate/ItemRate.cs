﻿namespace SSWhite.Domain.Common.Master.ItemRate
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class ItemRate
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public int? ItemGroupId { get; set; }

        public int? ItemId { get; set; }

        public decimal? Rate { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}
