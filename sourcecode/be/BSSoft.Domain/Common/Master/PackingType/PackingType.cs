﻿namespace SSWhite.Domain.Common.Master.PackingType
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class PackingType
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public string Name { get; set; }
    }
}
