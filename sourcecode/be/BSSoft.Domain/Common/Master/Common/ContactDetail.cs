﻿namespace SSWhite.Domain.Common.Master.Common
{
    public class ContactDetail
    {
        public string Name { get; set; }

        public string MobileNumber { get; set; }

        public string Phone { get; set; }

        public string EmailAddress { get; set; }
    }
}
