﻿namespace SSWhite.Domain.Common.Master.Common
{
    public class BankDetail
    {
        public string Name { get; set; }

        public string AccountNumber { get; set; }

        public string Branch { get; set; }

        public string IfscCode { get; set; }

        public bool IsDefault { get; set; }
    }
}
