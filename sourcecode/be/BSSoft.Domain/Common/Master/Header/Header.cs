﻿namespace SSWhite.Domain.Common.Master.Header
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class Header
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public string Name { get; set; }

        public List<HeaderDetail> HeaderDetails { get; set; }
    }
}
