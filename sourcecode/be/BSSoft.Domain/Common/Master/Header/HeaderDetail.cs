﻿namespace SSWhite.Domain.Common.Master.Header
{
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums;

    public class HeaderDetail
    {
        public int? ModuleId { get; set; }

        public CalculationType? CalculationType { get; set; }

        public string Module { get; set; }

        public string CalculationTypeString => CommonMethods.GetDisplayValue(CalculationType);
    }
}
