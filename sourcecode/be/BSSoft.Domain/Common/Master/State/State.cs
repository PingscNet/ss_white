﻿namespace SSWhite.Domain.Common.Master.State
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class State
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public int CountryId { get; set; }

        public string Name { get; set; }
    }
}
