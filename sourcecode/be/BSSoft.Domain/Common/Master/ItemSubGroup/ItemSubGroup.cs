﻿namespace SSWhite.Domain.Common.Master.ItemSubGroup
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class ItemSubGroup
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public int? ItemGroupId { get; set; }

        public string Name { get; set; }
    }
}
