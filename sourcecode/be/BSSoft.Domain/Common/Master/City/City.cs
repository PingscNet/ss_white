﻿namespace SSWhite.Domain.Common.Master.City
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class City
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        [UpperCase]
        public string Name { get; set; }

        public int CountryId { get; set; }

        public int StateId { get; set; }
    }
}
