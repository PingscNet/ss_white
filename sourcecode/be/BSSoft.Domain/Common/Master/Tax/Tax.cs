﻿namespace SSWhite.Domain.Common.Master.Tax
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class Tax
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        [UpperCase]
        public string Name { get; set; }

        public decimal? IgstRate { get; set; }

        public decimal? CgstRate { get; set; }

        public decimal? SgstRate { get; set; }

        public decimal? CessRate { get; set; }
    }
}
