﻿namespace SSWhite.Domain.Common.Document.Receipt
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Enums.Document;

    public class Receipt
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public int? CompanyId { get; set; }

        public int? ClientId { get; set; }

        public int? DayBookId { get; set; }

        public string ReceiptNumber { get; set; }

        public DateTime? Date { get; set; }

        public DocumentType? ReceiptType { get; set; }

        public int Month { get; set; }

        public int FinancialYear { get; set; }

        public decimal? ReceivedAmount { get; set; }

        public decimal? TotalAmount { get; set; }

        public int? BankId { get; set; }

        public string ChequeNumber { get; set; }

        public string Branch { get; set; }

        public bool IsReturnedCheque { get; set; }

        public DateTime? ChequeReturnedDate { get; set; }

        public string Remarks { get; set; }

        public List<DocumentHeader> ReceiptHeaders { get; set; }

        public string Suffix { get; set; }

        public string Prefix { get; set; }
    }
}
