﻿namespace SSWhite.Domain.Common.Report.Ledger
{
    using System;
    using System.Collections.Generic;

    public class Ledger
    {
        public string CompanyName { get; set; }

        public string ClientName { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public List<LedgerDetail> LedgerDetails { get; set; }
    }
}
