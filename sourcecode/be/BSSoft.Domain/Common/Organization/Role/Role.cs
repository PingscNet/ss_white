﻿namespace SSWhite.Domain.Common.Organization.Role
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class Role
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public List<int> RightsIds { get; set; }

        public List<RightsGroup> RightsGroups { get; set; }

        public List<Right> Rights { get; set; }
    }
}
