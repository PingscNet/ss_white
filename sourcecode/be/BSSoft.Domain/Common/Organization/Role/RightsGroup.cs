﻿namespace SSWhite.Domain.Common.Organization.Role
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class RightsGroup
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public string Name { get; set; }

        public int DisplayOrder { get; set; }

        public string Acryonym { get; set; }
    }
}
