﻿namespace SSWhite.Domain.Common.Session
{
    public class AppData
    {
        public int CurrentEntityId { get; set; }

        public int CurrentFinancialYear { get; set; }
    }
}
