﻿namespace SSWhite.Domain.Enums.Document
{
    public enum CalculatedByType : short
    {
        Rs = 1,
        Perc = 2
    }
}
