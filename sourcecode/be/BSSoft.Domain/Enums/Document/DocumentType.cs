﻿namespace SSWhite.Domain.Enums.Document
{
    using System.ComponentModel.DataAnnotations;
    using System.Security.AccessControl;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Common;
    using SSWhite.ResourceFile;

    public enum DocumentType : short
    {
        Sales = 1,
        Purchase = 2,

        [Display(Name = nameof(EnumLabels.Cash), ResourceType = typeof(EnumLabels))]
        [Group(GroupConstant.RECEIPT)]
        CashReceipt = 3,

        [Display(Name = nameof(EnumLabels.Bank), ResourceType = typeof(EnumLabels))]
        [Group(GroupConstant.RECEIPT)]
        BankReceipt = 4,

        [Display(Name = nameof(EnumLabels.Cash), ResourceType = typeof(EnumLabels))]
        [Group(GroupConstant.PAYMENT)]
        CashPayment = 5,

        [Display(Name = nameof(EnumLabels.Bank), ResourceType = typeof(EnumLabels))]
        [Group(GroupConstant.PAYMENT)]
        BankPayment = 6,

        [Display(Name = nameof(EnumLabels.JournalVoucher), ResourceType = typeof(EnumLabels))]
        JournalVoucher = 7
    }
}
