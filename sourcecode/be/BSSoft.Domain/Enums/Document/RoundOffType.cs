﻿namespace SSWhite.Domain.Enums.Document
{
    public enum RoundOffType : short
    {
        Plus = 1,
        Minus = 2
    }
}
