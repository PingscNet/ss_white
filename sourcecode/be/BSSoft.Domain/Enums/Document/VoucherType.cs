﻿namespace SSWhite.Domain.Enums.Document
{
    public enum VoucherType : short
    {
        Commission = 1,
        Trading = 2
    }
}
