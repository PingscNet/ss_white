﻿namespace SSWhite.Domain.Enums.Document
{
    using System.ComponentModel.DataAnnotations;
    using SSWhite.ResourceFile;

    public enum DocumentStatus : short
    {
        [Display(Name = nameof(EnumLabels.YetNotApproved), ResourceType = typeof(EnumLabels))]
        YetNotApproved = 1,
        [Display(Name = nameof(EnumLabels.Approved), ResourceType = typeof(EnumLabels))]
        Approved = 2,
        [Display(Name = nameof(EnumLabels.Deleted), ResourceType = typeof(EnumLabels))]
        Deleted = 3
    }
}
