﻿namespace SSWhite.Domain.Enums.Master.LedgerGroup
{
    using System.ComponentModel.DataAnnotations;
    using SSWhite.ResourceFile;

    public enum LedgerGroupType : short
    {
        [Display(Name = nameof(EnumLabels.BalanceSheet), ResourceType = typeof(EnumLabels))]
        BalanceSheet = 1,
        [Display(Name = nameof(EnumLabels.ProfitAndLoss), ResourceType = typeof(EnumLabels))]
        ProfitAndLoss = 2,
        [Display(Name = nameof(EnumLabels.TradingAccount), ResourceType = typeof(EnumLabels))]
        TradingAccount = 3
    }
}
