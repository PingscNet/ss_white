﻿namespace SSWhite.Domain.Enums.Master.PunchType
{
    public enum PunchType : short
    {
        //PunchIn = 1,
        //PunchOut = 2,
        //BussinessIn = 3,
        //BusinessOut = 4,
        //InForPersnoalBusiness = 5,
        //OutForPersnoalBusiness = 6

        Normal = 1,
        Lunch = 2,
        Business = 3,
        PersonalBusiness = 4
    }
}
