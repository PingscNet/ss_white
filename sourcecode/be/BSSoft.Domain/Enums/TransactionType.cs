﻿namespace SSWhite.Domain.Enums
{
    using System.ComponentModel.DataAnnotations;

    public enum TransactionType : short
    {
        [Display(Name = "Cr")]
        Credit = 1,

        [Display(Name = "Dr")]
        Debit = 2
    }
}
