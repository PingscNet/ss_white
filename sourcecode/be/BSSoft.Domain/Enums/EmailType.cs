﻿namespace SSWhite.Domain.Enums
{
    public enum EmailType : short
    {
        EPOMailToManager = 1,
        EPOMailToSelfForApproval = 2,
        EPOMailCreatedToSelf = 3,
        EmailIdsForFtoCreatedNotification = 4,
        EmailIdForFtoApproval = 5,
        EmailIdForFtoApproved = 6,
        EmailIdsForFtoApprovedNotification = 7,
        DocumentNotification = 8,
        DocumentApproval = 9,
        DocumentApprovedNotification = 10
    }
}
