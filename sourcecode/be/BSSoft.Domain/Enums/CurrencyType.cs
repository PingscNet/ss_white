﻿namespace SSWhite.Domain.Enums
{
    using System.ComponentModel.DataAnnotations;
    using SSWhite.ResourceFile;

    public enum CurrencyType : short
    {
        Ruppee = 1,
        Dollar = 2
    }
}
