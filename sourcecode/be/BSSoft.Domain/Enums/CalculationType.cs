﻿namespace SSWhite.Domain.Enums
{
    using System.ComponentModel.DataAnnotations;
    using SSWhite.ResourceFile;

    public enum CalculationType : short
    {
        [Display(Name = nameof(EnumLabels.PlusSymbol), ResourceType = typeof(EnumLabels))]
        Plus = 1,

        [Display(Name = nameof(EnumLabels.MinusSymbol), ResourceType = typeof(EnumLabels))]
        Minus = 2
    }
}
