﻿namespace SSWhite.Service.Interface.AttendanceChange
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.AttendanceChange;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.Domain.Response.AttendanceChange;

    public interface IAttendanceChangeService
    {
        Task<CreateAttendanceFtoResponse> CreateAttendanceFto(ServiceRequest<CreateAttendanceFtoRequest> request);

        Task ApproveOrDenyFto(ServiceRequest<ApproveOrDenyFtoRequest> request);

        Task<List<GetDepartmentFtoByUserIdResponse>> GetDepartmentFtoByUserId(ServiceRequest<GetDepartmentFtoByUserIdRequest> request);

        Task<GetAttendanceFtoByIdResponse> GetAttendanceFtoById(ServiceRequest<GetAttendanceFtoByIdRequest> request);

        Task<ServiceSearchResponse<GetAllDepartmentOrUserFtoByUserIdResponse>> GetAllDepartmentOrUserFtoByUserId(ServiceSearchRequest<GetAllDepartmentOrUserFtoByUserIdRequest> request);
    }
}
