﻿namespace SSWhite.Service.Interface.Master.Region
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Region;
    using SSWhite.Domain.Request.Master.Region;
    using SSWhite.Domain.Response.Master.Region;

    public interface IRegionService
    {
        Task<ServiceSearchResponse<GetAllRegionsResponse>> GetAllRegions(ServiceSearchRequest<GetAllRegions> request);

        Task<ServiceResponse<int>> AddEditRegion(ServiceRequest<Region> request);

        Task ChangeRegionStatus(ServiceRequest<int> request);

        Task<ServiceResponse<Region>> GetRegionById(ServiceRequest<int> request);

        Task DeleteRegion(ServiceRequest<int> request);
    }
}
