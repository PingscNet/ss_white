﻿namespace SSWhite.Service.Interface.Master.Site
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Site;
    using SSWhite.Domain.Request.Master.Site;
    using SSWhite.Domain.Response.Master.Site;

    public interface ISiteService
    {
        Task<ServiceSearchResponse<GetAllSitesResponse>> GetAllSites(ServiceSearchRequest<GetAllSites> request);

        Task<ServiceResponse<int>> AddEditSite(ServiceRequest<Site> request);

        Task ChangeSiteStatus(ServiceRequest<int> request);

        Task<ServiceResponse<Site>> GetSiteById(ServiceRequest<int> request);

        Task DeleteSite(ServiceRequest<int> request);
    }
}
