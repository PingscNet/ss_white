﻿namespace SSWhite.Service.Interface.Master.Country
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Country;
    using SSWhite.Domain.Request.Master.Country;
    using SSWhite.Domain.Response.Master.Country;

    public interface ICountryService
    {
        Task<ServiceSearchResponse<GetAllCountriesResponse>> GetAllCountries(ServiceSearchRequest<GetAllCountries> request);

        Task<ServiceResponse<int>> AddEditCountry(ServiceRequest<Country> request);

        Task ChangeCountryStatus(ServiceRequest<int> request);

        Task<ServiceResponse<Country>> GetCountryById(ServiceRequest<int> request);

        Task DeleteCountry(ServiceRequest<int> request);
    }
}
