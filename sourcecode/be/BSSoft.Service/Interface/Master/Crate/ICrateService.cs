﻿namespace SSWhite.Service.Interface.Master.Crate
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Crate;
    using SSWhite.Domain.Request.Master.Crate;
    using SSWhite.Domain.Response.Master.Crate;

    public interface ICrateService
    {
        Task<ServiceSearchResponse<GetAllCratesResponse>> GetAllCrates(ServiceSearchRequest<GetAllCrates> request);

        Task<ServiceResponse<int>> AddEditCrate(ServiceRequest<Crate> request);

        Task ChangeCrateStatus(ServiceRequest<int> request);

        Task<ServiceResponse<Crate>> GetCrateById(ServiceRequest<int> request);

        Task DeleteCrate(ServiceRequest<int> request);
    }
}
