﻿namespace SSWhite.Service.Interface.Master.Brand
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Master.Brand;
    using SSWhite.Domain.Response.Master.Brand;

    public interface IBrandService
    {
        Task<ServiceSearchResponse<GetAllBrandsResponse>> GetAllBrands(ServiceSearchRequest<GetAllBrands> request);

        Task<ServiceResponse<int>> AddBrand(ServiceRequest<AddBrand> request);

        Task ChangeBrandStatus(ServiceRequest<int> request);

        Task<ServiceResponse<GetBrandByIdResponse>> GetBrandById(ServiceRequest<int> request);

        Task DeleteBrand(ServiceRequest<int> request);

        Task<ServiceResponse<int>> EditBrand(ServiceRequest<EditBrand> request);
    }
}
