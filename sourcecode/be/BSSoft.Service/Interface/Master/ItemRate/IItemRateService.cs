﻿namespace SSWhite.Service.Interface.Master.ItemRate
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemRate;
    using SSWhite.Domain.Request.Master.ItemRate;
    using SSWhite.Domain.Response.Master.ItemRate;

    public interface IItemRateService
    {
        Task<ServiceSearchResponse<GetAllItemRatesResponse>> GetAllItemRates(ServiceSearchRequest<GetAllItemRates> request);

        Task<ServiceResponse<int>> AddEditItemRate(ServiceRequest<ItemRate> request);

        Task ChangeItemRateStatus(ServiceRequest<int> request);

        Task<ServiceResponse<ItemRate>> GetItemRateById(ServiceRequest<int> request);

        Task DeleteItemRate(ServiceRequest<int> request);
    }
}
