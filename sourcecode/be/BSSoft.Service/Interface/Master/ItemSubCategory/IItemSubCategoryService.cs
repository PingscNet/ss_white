﻿namespace SSWhite.Service.Interface.Master.ItemSubCategory
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemSubCategory;
    using SSWhite.Domain.Request.Master.ItemSubCategory;
    using SSWhite.Domain.Response.Master.ItemSubCategory;

    public interface IItemSubCategoryService
    {
        Task<ServiceSearchResponse<GetAllItemSubCategoriesResponse>> GetAllItemSubCategories(ServiceSearchRequest<GetAllItemSubCategories> request);

        Task<ServiceResponse<int>> AddEditItemSubCategory(ServiceRequest<ItemSubCategory> request);

        Task ChangeItemSubCategoryStatus(ServiceRequest<int> request);

        Task<ServiceResponse<ItemSubCategory>> GetItemSubCategoryById(ServiceRequest<int> request);

        Task DeleteItemSubCategory(ServiceRequest<int> request);
    }
}
