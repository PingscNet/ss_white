﻿namespace SSWhite.Service.Interface.Master.LedgerGroup
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.LedgerGroup;
    using SSWhite.Domain.Request.Master.LedgerGroup;
    using SSWhite.Domain.Response.Master.LedgerGroup;

    public interface ILedgerGroupService
    {
        Task<ServiceSearchResponse<GetAllLedgerGroupsResponse>> GetAllLedgerGroups(ServiceSearchRequest<GetAllLedgerGroups> request);

        Task<ServiceResponse<int>> AddEditLedgerGroup(ServiceRequest<LedgerGroup> request);

        Task ChangeLedgerGroupStatus(ServiceRequest<int> request);

        Task<ServiceResponse<LedgerGroup>> GetLedgerGroupById(ServiceRequest<int> request);

        Task DeleteLedgerGroup(ServiceRequest<int> request);
    }
}
