﻿namespace SSWhite.Service.Interface.Master.Company
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Company;
    using SSWhite.Domain.Request.Master.Company;
    using SSWhite.Domain.Response.Master.Company;

    public interface ICompanyService
    {
        Task<ServiceSearchResponse<GetAllCompaniesResponse>> GetAllCompanies(ServiceSearchRequest<GetAllCompanies> request);

        Task<ServiceResponse<int>> AddEditCompany(ServiceRequest<Company> request);

        Task ChangeCompanyStatus(ServiceRequest<int> request);

        Task<ServiceResponse<GetCompanyByIdResponse>> GetCompanyById(ServiceRequest<int> request);

        Task DeleteCompany(ServiceRequest<int> request);
    }
}
