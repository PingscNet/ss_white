﻿namespace SSWhite.Service.Interface.Master.Header
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Header;
    using SSWhite.Domain.Request.Master.Header;
    using SSWhite.Domain.Response.Master.Header;

    public interface IHeaderService
    {
        Task<ServiceSearchResponse<GetAllHeadersResponse>> GetAllHeaders(ServiceSearchRequest<GetAllHeaders> request);

        Task<ServiceResponse<int>> AddEditHeader(ServiceRequest<Header> request);

        Task ChangeHeaderStatus(ServiceRequest<int> request);

        Task<ServiceResponse<Header>> GetHeaderById(ServiceRequest<int> request);

        Task DeleteHeader(ServiceRequest<int> request);
    }
}
