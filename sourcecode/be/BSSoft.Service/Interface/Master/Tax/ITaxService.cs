﻿namespace SSWhite.Service.Interface.Master.Tax
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Tax;
    using SSWhite.Domain.Request.Master.Tax;
    using SSWhite.Domain.Response.Master.Tax;

    public interface ITaxService
    {
        Task<ServiceSearchResponse<GetAllTaxesResponse>> GetAllTaxes(ServiceSearchRequest<GetAllTaxes> request);

        Task<ServiceResponse<int>> AddEditTax(ServiceRequest<Tax> request);

        Task ChangeTaxStatus(ServiceRequest<int> request);

        Task<ServiceResponse<Tax>> GetTaxById(ServiceRequest<int> request);

        Task DeleteTax(ServiceRequest<int> request);
    }
}
