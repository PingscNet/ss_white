﻿namespace SSWhite.Service.Interface.Organization.User
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Organization.User;
    using SSWhite.Domain.Response.Organization.User;

    public interface IUserService
    {
        Task<ServiceSearchResponse<GetAllUsersResponse>> GetAllUsers(ServiceSearchRequest<GetAllUsers> request);

        Task<ServiceResponse<int>> AddUser(ServiceRequest<AddUser> request);

        Task ChangeUserStatus(ServiceRequest<int> request);

        Task<ServiceResponse<GetUserByIdResponse>> GetUserById(ServiceRequest<int> request);

        Task DeleteUser(ServiceRequest<int> request);

        Task<ServiceResponse<int>> EditUser(ServiceRequest<EditUser> request);
    }
}
