﻿namespace SSWhite.Service.Interface.Document
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Document;
    using SSWhite.Domain.Response.Document;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;

    public interface IDocumentService
    {
        Task<AddOrEditDocumentResponse> AddOrEditDocument(ServiceRequest<AddOrEditDocumentRequest> request);

        Task<ApproveDocumentResponse> ApproveDocument(ServiceRequest<ApproveDocumentRequest> request);

        Task<ServiceSearchResponse<GetAllDocumentsResponse>> GetAllDocuments(ServiceSearchRequest<GetAllDocumentsRequest> request);

        Task<List<GetAssociateDocumentsResponse>> GetAssociateDocuments();

        Task<GetDocumentByIdResponse> GetDocumentById(ServiceRequest<GetDocumentByIdRequest> request);

        Task<GetDocumentsDropdownAndUserDetailsResponse> GetDocumentsDropdownAndUserDetails(ServiceRequest request);

        Task<List<GetEmployeeByDepartmentResponse>> GetEmployeeByDepartment(GetEmployeeByDepartmentRequest request);
    }
}
