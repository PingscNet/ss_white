﻿namespace SSWhite.Service.Interface.Document.Receipt
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Document.Receipt;
    using SSWhite.Domain.Request.Document.Receipt;
    using SSWhite.Domain.Response.Document.Receipt;

    public interface IReceiptService
    {
        Task<ServiceSearchResponse<GetAllReceiptsResponse>> GetAllReceipts(ServiceSearchRequest<GetAllReceipts> request);

        Task<ServiceResponse<int>> AddEditReceipt(ServiceRequest<Receipt> request);

        Task ChangeReceiptStatus(ServiceRequest<int> request);

        Task<ServiceResponse<Receipt>> GetReceiptById(ServiceRequest<int> request);

        Task DeleteReceipt(ServiceRequest<int> request);
    }
}
