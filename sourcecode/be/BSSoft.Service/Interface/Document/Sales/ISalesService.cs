﻿namespace SSWhite.Service.Interface.Document.Sales
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Document.Sales;
    using SSWhite.Domain.Request.Document.Sales;
    using SSWhite.Domain.Response.Document.Sales;

    public interface ISalesService
    {
        Task<ServiceSearchResponse<GetAllSalesDocumentsResponse>> GetAllSalesDocuments(ServiceSearchRequest<GetAllSalesDocuments> request);

        Task<ServiceResponse<List<InsertOrUpdateSalesDocumentResponse>>> AddEditSalesDocument(ServiceRequest<SalesDocument> request);

        Task ChangeSalesDocumentStatus(ServiceRequest<int> request);

        Task<ServiceResponse<SalesDocument>> GetSalesDocumentById(ServiceRequest<int> request);

        Task DeleteSalesDocument(ServiceRequest<int> request);

        Task<ServiceSearchResponse<GetAllSalesDocumentsResponse>> GetAllPendingRateSalesDocuments(ServiceSearchRequest<GetAllSalesDocuments> request);
    }
}
