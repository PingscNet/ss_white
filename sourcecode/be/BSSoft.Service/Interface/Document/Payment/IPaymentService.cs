﻿namespace SSWhite.Service.Interface.Document.Payment
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Document.Payment;
    using SSWhite.Domain.Request.Document.Payment;
    using SSWhite.Domain.Response.Document.Payment;

    public interface IPaymentService
    {
        Task<ServiceSearchResponse<GetAllPaymentsResponse>> GetAllPayments(ServiceSearchRequest<GetAllPayments> request);

        Task<ServiceResponse<int>> AddEditPayment(ServiceRequest<Payment> request);

        Task ChangePaymentStatus(ServiceRequest<int> request);

        Task<ServiceResponse<Payment>> GetPaymentById(ServiceRequest<int> request);

        Task DeletePayment(ServiceRequest<int> request);
    }
}
