﻿namespace SSWhite.Service.Interface.TimeAndAttendance
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.TimeAndAttendance;
    using SSWhite.Domain.Response.Account;
    using SSWhite.Domain.Response.Dashboard;
    using SSWhite.Domain.Response.TimeAndAttendance;

    public interface ITimeAndAttendanceService
    {
        Task InsertInOutDetailsForLunch(InsertInOutDetailsRequest insertInOutDetailsRequest);

        Task<List<GetInOutDetailsByUserIdFinalResponse>> GetInOutDetailsByUserId(ServiceRequest<GetInOutDetailsByUserIdRequest> request);

        Task<List<GetUsersInOutStatusResponse>> GetUsersInOutStatus();

        Task<GetInOutStatusByPunchTypeByUserIdResponse> GetInOutStatusByPunchTypeUserId(ServiceRequest<GetInOutStatusByPunchTypeUserIdRequest> request);

        Task InsertPunchDetails(ServiceRequest<InsertPunchDetailsRequest> insertPunchDetailsRequest);
    }
}
