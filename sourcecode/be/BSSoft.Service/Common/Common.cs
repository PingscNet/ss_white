﻿namespace SSWhite.Service.Common
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using Newtonsoft.Json;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;

    public static class Common
    {
        public static decimal GetDollarValue()
        {

            string apiUrl = "https://api.exchangerate.host/latest?base=USD&symbols=INR";
            CurrencyRateResponse currencyRateResponse = new CurrencyRateResponse();
            HttpClient client = new HttpClient();
            HttpResponseMessage httpResponseMessage = client.GetAsync(apiUrl).Result;
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                currencyRateResponse = JsonConvert.DeserializeObject<CurrencyRateResponse>(httpResponseMessage.Content.ReadAsStringAsync().Result);
                return currencyRateResponse.Rates.INR;
            }

            return currencyRateResponse.Rates.INR = 77.00m;
        }
    }
}
