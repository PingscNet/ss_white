﻿namespace SSWhite.Service.Implementation.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Interface.Admin;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;
    using SSWhite.Service.Interface.Admin;

    public class AdminService : IAdminService
    {
        private readonly IAdminRepository _adminRepository;
        private readonly ISendMailService _sendMailService;
        private readonly AppSetting _appSetting;

        public AdminService(IAdminRepository adminRepository, ISendMailService sendMailService, IOptions<AppSetting> appSetting)
        {
            _adminRepository = adminRepository;
            _sendMailService = sendMailService;
            _appSetting = appSetting.Value;
        }

        public async Task AddNewEmployee(ServiceRequest<AddNewEmployeeRequest> request)
        {
            try
            {
                var response = await _adminRepository.AddNewEmployee(request);

                if (response != null)
                {
                    await _sendMailService.SendMailAsync(PrepareMailForNewUser(response));
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllEmployeeLimitsResponse>> GetAllEmployeeLimits(GetAllEmployeeLimitsRequest request)
        {
            return await _adminRepository.GetAllEmployeeLimits(request);
        }

        public async Task<List<GetEmployeeByEmployeeIdResponse>> GetEmployeeByEmployeeId(GetEmployeeByEmployeeIdRequest request)
        {
            return await _adminRepository.GetEmployeeByEmployeeId(request);
        }

        public async Task UpdateEmployeeLimit(UpdateEmployeeLimitByIdRequest updateEmployeeLimitByIdRequest)
        {
            await _adminRepository.UpdateEmployeeLimit(updateEmployeeLimitByIdRequest);
        }

        public async Task ChangeEmployeeStatus(Task<ServiceRequest<ChangeEmployeeStatusRequest>> request)
        {
            await _adminRepository.ChangeEmployeeStatus(request);
        }

        public async Task<CheckIfEmployeeIdExistsResponse> CheckIfEmployeeIdExists(CheckIfEmployeeIdExistsRequest checkIfEmployeeIdExistsRequest)
        {
            return await _adminRepository.CheckIfEmployeeIdExists(checkIfEmployeeIdExistsRequest);
        }

        public async Task<CheckIfEmailAddressExistsResponse> CheckIfEmailAddressExists(CheckIfEmailAddressExistsRequest request)
        {
            return await _adminRepository.CheckIfEmailAddressExists(request);
        }

        public async Task<List<GetDocumentApprovalListResponse>> GetDocumentApprovals()
        {
            return await _adminRepository.GetDocumentApprovals();
        }

        public async Task UpdateDocumentApprovalList(List<UpdateDocumentApprovalListRequest> request)
        {
            await _adminRepository.UpdateDocumentApprovalList(request);
        }

        public async Task<List<GetUserRightsResponse>> GetUserRights(ServiceRequest request)
        {
            try
            {
                return await _adminRepository.GetUserRights(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetRolesResponse>> GetRoles()
        {
            try
            {
                return await _adminRepository.GetRoles();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetRightsAccessByRoleIdResponse>> GetRightsAccessByRoleId(GetRightsAccessByRoleIdRequest request)
        {
            try
            {
                return await _adminRepository.GetRightsAccessByRoleId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task UpdateRightsAccessByRoleId(UpdateRightsAccessByRoleIdRequest request)
        {
            try
            {
                await _adminRepository.UpdateRightsAccessByRoleId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private SendMail PrepareMailForNewUser(AddNewEmployeeResponse response)
        {
            var subject = string.Empty;
            var email = string.Empty;
            var body = string.Empty;
            var isHtml = true;

            subject = "*** New User Created #  - ";
            email = response.EmailAddress;
            body = $"<span style = 'font-family:Arial;font-size:10pt'>" +
                $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'>" +
                ////$" <p><strong>DocumentId:</strong>  { response.DocumentId }" +
                $"<br /><strong> UserId :</strong> {response.Loginid}" +
                $" <br /> <strong>EmailAddress: </strong> {response.EmailAddress}" +
                $" <br /> <strong>Password: </strong> {_appSetting.CreateUserPassword}" +
                $"<br /> <strong>Change the password from dashboard </strong> </p> <br/>" +
                $" </span>";

            isHtml = true;

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email
            };
        }
    }
}
