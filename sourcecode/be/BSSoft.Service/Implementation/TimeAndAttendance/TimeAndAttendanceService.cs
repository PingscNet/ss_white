﻿namespace SSWhite.Service.Implementation.TimeAndAttendance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Data.Interface.TimeAndAttendance;
    using SSWhite.Domain.Enums.Master.PunchType;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.TimeAndAttendance;
    using SSWhite.Domain.Response.Account;
    using SSWhite.Domain.Response.Dashboard;
    using SSWhite.Domain.Response.TimeAndAttendance;
    using SSWhite.Service.Interface.Account;
    using SSWhite.Service.Interface.Dashboard;
    using SSWhite.Service.Interface.TimeAndAttendance;

    public class TimeAndAttendanceService : ITimeAndAttendanceService
    {
        private readonly ITimeAndAttendanceRepository _timeAndAttendanceRepository;
        private readonly IConfiguration _configuration;

        public TimeAndAttendanceService(ITimeAndAttendanceRepository timeAndAttendanceRepository, IConfiguration configuration)
        {
            _timeAndAttendanceRepository = timeAndAttendanceRepository;
            _configuration = configuration;
        }

        public async Task InsertInOutDetailsForLunch(InsertInOutDetailsRequest insertInOutDetailsRequest)
        {
            ////var response = new ServiceResponse<LogInResponse>();
            try
            {
                await _timeAndAttendanceRepository.InsertInOutDetailsForLunch(insertInOutDetailsRequest);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetInOutDetailsByUserIdFinalResponse>> GetInOutDetailsByUserId(ServiceRequest<GetInOutDetailsByUserIdRequest> request)
        {
            ////var response = new ServiceResponse<LogInResponse>();
            try
            {
                var result = await _timeAndAttendanceRepository.GetInOutDetailsByUserId(request);
                List<GetInOutDetailsByUserIdFinalResponse> resultFinal = new List<GetInOutDetailsByUserIdFinalResponse>();
                var dates = result.Select(x => x.Date).Distinct();
                int i = 1;

                foreach (var dateItem in dates)
                {
                    var res = result.Select(x => x).Where(x => x.Date == dateItem);

                    var punchType = res.Select(x => x.PunchType).FirstOrDefault();
                    var intime = res.Where(y => y.InOutStatus == 1).Select(x => x.PunchTime).FirstOrDefault();
                    var outtime = res.Where(y => y.InOutStatus == 2).Select(x => x.PunchTime).FirstOrDefault();
                    resultFinal.Add(
                        new GetInOutDetailsByUserIdFinalResponse
                        {
                            Id = i,
                            Date = res.Select(x => x.Date).FirstOrDefault(),
                            InPunchTime = intime,
                            OutPunchTime = outtime,
                            TotalHours = (int)outtime.Subtract(intime).TotalHours,
                            PunchType = Enum.GetName(typeof(PunchType), punchType)
                });
                    i++;
                }

                return resultFinal;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetUsersInOutStatusResponse>> GetUsersInOutStatus()
        {
            ////var response = new ServiceResponse<LogInResponse>();
            try
            {
                var result = await _timeAndAttendanceRepository.GetUsersInOutStatus();

                foreach (var item in result)
                {
                    item.Picture = $"{_configuration.GetValue<string>("AppSetting:AvatarRootPath")}{item.Picture}";
                }

                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetInOutStatusByPunchTypeByUserIdResponse> GetInOutStatusByPunchTypeUserId(ServiceRequest<GetInOutStatusByPunchTypeUserIdRequest> request)
        {
            ////var response = new ServiceResponse<LogInResponse>();
            try
            {
                return await _timeAndAttendanceRepository.GetInOutStatusByPunchTypeUserId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task InsertPunchDetails(ServiceRequest<InsertPunchDetailsRequest> insertPunchDetailsRequest)
        {
            await _timeAndAttendanceRepository.InsertPunchDetails(insertPunchDetailsRequest);
        }
    }
}
