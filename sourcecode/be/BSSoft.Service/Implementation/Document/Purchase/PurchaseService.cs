﻿namespace SSWhite.Service.Implementation.Document.Purchase
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Document.Purchase;
    using SSWhite.Domain.Common.Document.Purchase;
    using SSWhite.Domain.Request.Document.Purchase;
    using SSWhite.Domain.Response.Document.Purchase;
    using SSWhite.Service.Interface.Document.Purchase;

    public class PurchaseService : IPurchaseService
    {
        private readonly IPurchaseRepository _purchaseRepository;

        public PurchaseService(IPurchaseRepository purchaseRepository)
        {
            _purchaseRepository = purchaseRepository;
        }

        public async Task<ServiceResponse<List<InsertOrUpdatePurchaseDocumentResponse>>> AddEditPurchaseDocument(ServiceRequest<PurchaseDocument> request)
        {
            var response = new ServiceResponse<List<InsertOrUpdatePurchaseDocumentResponse>>();
            try
            {
                response.Data = await _purchaseRepository.AddEditPurchaseDocument(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangePurchaseDocumentStatus(ServiceRequest<int> request)
        {
            try
            {
                await _purchaseRepository.ChangePurchaseDocumentStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceResponse<int>> DeletePurchaseDocument(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _purchaseRepository.DeletePurchaseDocument(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllPurchaseDocumentsResponse>> GetAllPendingRatePurchaseDocuments(ServiceSearchRequest<GetAllPurchaseDocuments> request)
        {
            var response = new ServiceSearchResponse<GetAllPurchaseDocumentsResponse>();
            try
            {
                response = await _purchaseRepository.GetAllPendingRatePurchaseDocuments(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllPurchaseDocumentsResponse>> GetAllPurchaseDocuments(ServiceSearchRequest<GetAllPurchaseDocuments> request)
        {
            var response = new ServiceSearchResponse<GetAllPurchaseDocumentsResponse>();
            try
            {
                response = await _purchaseRepository.GetAllPurchaseDocuments(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<PurchaseDocument>> GetPurchaseDocumentById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<PurchaseDocument>();
            try
            {
                response.Data = await _purchaseRepository.GetPurchaseDocumentById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
