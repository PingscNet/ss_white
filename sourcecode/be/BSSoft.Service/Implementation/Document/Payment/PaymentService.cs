﻿namespace SSWhite.Service.Implementation.Document.Payment
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Document.Payment;
    using SSWhite.Domain.Common.Document.Payment;
    using SSWhite.Domain.Request.Document.Payment;
    using SSWhite.Domain.Response.Document.Payment;
    using SSWhite.Service.Interface.Document.Payment;

    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;

        public PaymentService(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;
        }

        public async Task<ServiceResponse<int>> AddEditPayment(ServiceRequest<Payment> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _paymentRepository.AddEditPayment(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangePaymentStatus(ServiceRequest<int> request)
        {
            try
            {
                await _paymentRepository.ChangePaymentStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeletePayment(ServiceRequest<int> request)
        {
            try
            {
                await _paymentRepository.DeletePayment(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllPaymentsResponse>> GetAllPayments(ServiceSearchRequest<GetAllPayments> request)
        {
            var response = new ServiceSearchResponse<GetAllPaymentsResponse>();
            try
            {
                response = await _paymentRepository.GetAllPayments(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Payment>> GetPaymentById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Payment>();
            try
            {
                response.Data = await _paymentRepository.GetPaymentById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
