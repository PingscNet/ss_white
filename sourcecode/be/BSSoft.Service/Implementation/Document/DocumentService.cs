﻿namespace SSWhite.Service.Implementation.Document
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Document;
    using SSWhite.Domain.Enums;
    using SSWhite.Domain.Request.Document;
    using SSWhite.Domain.Response.Document;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;
    using SSWhite.Service.Interface.Document;

    public class DocumentService : IDocumentService
    {
        private readonly IDocumentRepository _documentRepository;
        private readonly IConfiguration _configuration;
        private readonly ISendMailService _sendMailService;
        private readonly AppSetting _appSetting;

        public DocumentService(IDocumentRepository documentRepository, IConfiguration configuration, ISendMailService sendMailService, IOptions<AppSetting> appSetting)
        {
            _documentRepository = documentRepository;
            _configuration = configuration;
            _sendMailService = sendMailService;
            _appSetting = appSetting.Value;
        }

        public async Task<AddOrEditDocumentResponse> AddOrEditDocument(ServiceRequest<AddOrEditDocumentRequest> request)
        {
            try
            {
                var response = await _documentRepository.AddOrEditDocument(request);

                if (response.NotificationEmailIds != null && request.Data.IsSendForApproval == true)
                {
                    await _sendMailService.SendMailAsync(PrepareMailForApproval(response, EmailType.DocumentNotification));
                }
                if (response.ApprovalEmailIds != null && request.Data.IsSendForApproval == true)
                {
                    await _sendMailService.SendMailAsync(PrepareMailForApproval(response, EmailType.DocumentApproval));
                }

                return response;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ApproveDocumentResponse> ApproveDocument(ServiceRequest<ApproveDocumentRequest> request)
        {
            try
            {
                var response = await _documentRepository.ApproveDocument(request);
                var userName = request.Session.UserName;
                if (response != null && response.EmailAddress != null && response.Result != -1)
                {
                    await _sendMailService.SendMailAsync(PrepareMailAfterApproval(response, EmailType.DocumentApprovedNotification, request.Data.ApprovalType));
                }

                return response;
                //else if(response.Result == -1)
                //{
                //    response.Message = "Can not approve the document because you are unauthenticated";
                //}
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllDocumentsResponse>> GetAllDocuments(ServiceSearchRequest<GetAllDocumentsRequest> request)
        {
            try
            {
                return await _documentRepository.GetAllDocuments(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetAssociateDocumentsResponse>> GetAssociateDocuments()
        {
            try
            {
                return await _documentRepository.GetAssociateDocuments();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetDocumentByIdResponse> GetDocumentById(ServiceRequest<GetDocumentByIdRequest> request)
        {
            try
            {
                var result = await _documentRepository.GetDocumentById(request);
                foreach (var item in result.Attachment)
                {
                    item.AttachmentPath = $"{_appSetting.DocumentAttachmentPathToGetImage}{item.AttachmentPath}";
                }

                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetDocumentsDropdownAndUserDetailsResponse> GetDocumentsDropdownAndUserDetails(ServiceRequest request)
        {
            try
            {
                var result = await _documentRepository.GetDocumentsDropdownAndUserDetails(request);
                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetEmployeeByDepartmentResponse>> GetEmployeeByDepartment(GetEmployeeByDepartmentRequest request)
        {
            try
            {
                var result = await _documentRepository.GetEmployeeByDepartment(request);
                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private SendMail PrepareMailForApproval(AddOrEditDocumentResponse response, EmailType emailType)
        {
            var subject = string.Empty;
            var email = string.Empty;
            var body = string.Empty;
            var isHtml = true;
            switch (emailType)
            {
                case EmailType.DocumentApproval:
                    subject = "*** Document #  - " + response.DocumentId + " - Created By " + response.CreatedBy + " Submitted For Approval";
                    email = response.ApprovalEmailIds;
                    body = $"<span style = 'font-family:Arial;font-size:10pt'>" +
                        $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'> <p><strong>DocumentId:</strong>  { response.DocumentId }" +
                        $"<br /><strong> Created By :</strong> {response.CreatedBy}" +
                        $" <br /> <strong>Document Title: </strong> {response.DocumentTitle}" +
                        $"<br /> <strong> Awaiting Approval </strong> </p> <br/><a href={_appSetting.DocumentApprovalLink}{response.Id}?case=Approval&documentType={response.DocumentType}  > Click here to approve </span>";
                    isHtml = true;
                    break;
                case EmailType.DocumentNotification:
                    subject = "*** Document #  - " + response.DocumentId + " - Created By " + response.CreatedBy;
                    email = response.NotificationEmailIds;
                    body = $"<span style = 'font-family:Arial;font-size:10pt'>" +
                        $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'> <p><strong>DocumentId:</strong>  { response.DocumentId }" +
                        $"<br /><strong> Created By :</strong> {response.CreatedBy}" +
                        $" <br /> <strong>Document Title: </strong> {response.DocumentTitle}" +
                        $" </span>";
                    isHtml = true;
                    break;
            }

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email
            };
        }

        private SendMail PrepareMailAfterApproval(ApproveDocumentResponse response, EmailType emailType, ApprovalType? approvalType)
        {
            var approvalTypeName = Enum.GetName(typeof(ApprovalType), approvalType);

            var subject = string.Empty;
            var email = string.Empty;
            var body = string.Empty;
            var isHtml = true;
            switch (emailType)
            {
                case EmailType.DocumentApprovedNotification:
                    subject = "*** Document #  - " + response.DocumentId + " : " + approvalTypeName + " By " + response.ApprovedPerson;
                    email = response.EmailAddress;
                    body = $"<span style = 'font-family:Arial;font-size:10pt'>" +
                        $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'> <p><strong>DocumentId:</strong>  { response.DocumentId }" +
                        $"<br /><strong> Approved By :</strong> {response.ApprovedPerson}" +
                        $" <br /> <strong>Document Title: </strong> {response.DocumentTitle}" +
                        //$"<br /> <strong> Awaiting Approval </strong> </p> <br/><a href=http://18.215.233.17/time-attendance/building > Click here to approve" +
                        $" </span>";
                    isHtml = true;
                    break;
            }

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email
            };
        }
    }
}
