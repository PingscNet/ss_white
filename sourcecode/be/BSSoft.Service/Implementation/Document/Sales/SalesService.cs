﻿namespace SSWhite.Service.Implementation.Document.Sales
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Document.Sales;
    using SSWhite.Domain.Common.Document.Sales;
    using SSWhite.Domain.Request.Document.Sales;
    using SSWhite.Domain.Response.Document.Sales;
    using SSWhite.Service.Interface.Document.Sales;

    public class SalesService : ISalesService
    {
        private readonly ISalesRepository _salesRepository;

        public SalesService(ISalesRepository salesRepository)
        {
            _salesRepository = salesRepository;
        }

        public async Task<ServiceResponse<List<InsertOrUpdateSalesDocumentResponse>>> AddEditSalesDocument(ServiceRequest<SalesDocument> request)
        {
            var response = new ServiceResponse<List<InsertOrUpdateSalesDocumentResponse>>();
            try
            {
                response.Data = await _salesRepository.AddEditSalesDocument(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeSalesDocumentStatus(ServiceRequest<int> request)
        {
            try
            {
                await _salesRepository.ChangeSalesDocumentStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteSalesDocument(ServiceRequest<int> request)
        {
            try
            {
                await _salesRepository.DeleteSalesDocument(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllSalesDocumentsResponse>> GetAllPendingRateSalesDocuments(ServiceSearchRequest<GetAllSalesDocuments> request)
        {
            var response = new ServiceSearchResponse<GetAllSalesDocumentsResponse>();
            try
            {
                response = await _salesRepository.GetAllPendingRateSalesDocuments(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllSalesDocumentsResponse>> GetAllSalesDocuments(ServiceSearchRequest<GetAllSalesDocuments> request)
        {
            var response = new ServiceSearchResponse<GetAllSalesDocumentsResponse>();
            try
            {
                response = await _salesRepository.GetAllSalesDocuments(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<SalesDocument>> GetSalesDocumentById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<SalesDocument>();
            try
            {
                response.Data = await _salesRepository.GetSalesDocumentById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
