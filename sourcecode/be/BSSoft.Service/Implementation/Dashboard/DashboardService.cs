﻿namespace SSWhite.Service.Implementation.Account
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.Domain.Response.Account;
    using SSWhite.Domain.Response.Dashboard;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;
    using SSWhite.Service.Interface.Account;
    using SSWhite.Service.Interface.Dashboard;

    public class DashboardService : IDashboardService
    {
        private readonly IDashboardRepository _dashboardRepository;
        private readonly IConfiguration _configuration;

        public DashboardService(IDashboardRepository dashboardRepository, IConfiguration configuration)
        {
            _dashboardRepository = dashboardRepository;
            _configuration = configuration;
        }

        public async Task<List<GetSSWhiteTeamDetailsResponse>> GetSSWhiteTeamDetails()
        {
            try
            {
                var result = await _dashboardRepository.GetSSWhiteTeamDetails();
                foreach (var item in result)
                {
                    item.Image = $"{_configuration.GetValue<string>("AppSetting:AvatarRootPath")}{item.Image}";
                }

                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<UserDetail> GetUserDetailsById(int id)
        {
            ////var response = new ServiceResponse<LogInResponse>();
            try
            {
                var result = await _dashboardRepository.GetUserDetailsById(id);
                result.Picture = $"{_configuration.GetValue<string>("AppSetting:AvatarRootPath")}{result.Picture}";
                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<UserNotificationResponse> UserNotification(ServiceRequest request)
        {
            try
            {
                var response = await _dashboardRepository.UserNotification(request);
                return response;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetNotificationsResponse>> GetNotifications(ServiceRequest request)
        {
            try
            {
                var response = await _dashboardRepository.GetNotifications(request);
                return response;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetModulesCountsResponse> GetModulesCounts(ServiceRequest request)
        {
            try
            {
                var response = await _dashboardRepository.GetModulesCounts(request);
                return response;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<bool> UpdateWorkingStatus(ServiceRequest<UpdateWorkingStatusRequest> request)
        {
            try
            {
                return await _dashboardRepository.UpdateWorkingStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<int> ChangePassword(ServiceRequest<ChangePasswordRequest> request)
        {
            try
            {
                return await _dashboardRepository.ChangePassword(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
