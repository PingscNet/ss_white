﻿namespace SSWhite.Service.Implementation.Organization.User
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Organization.User;
    using SSWhite.Domain.Common.Organization.User;
    using SSWhite.Domain.Request.Organization.User;
    using SSWhite.Domain.Response.Organization.User;
    using SSWhite.Service.Interface.Organization.User;

    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<ServiceResponse<int>> AddUser(ServiceRequest<AddUser> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _userRepository.AddUser(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeUserStatus(ServiceRequest<int> request)
        {
            try
            {
                await _userRepository.ChangeUserStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteUser(ServiceRequest<int> request)
        {
            try
            {
                await _userRepository.DeleteUser(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceResponse<int>> EditUser(ServiceRequest<EditUser> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _userRepository.EditUser(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllUsersResponse>> GetAllUsers(ServiceSearchRequest<GetAllUsers> request)
        {
            var response = new ServiceSearchResponse<GetAllUsersResponse>();
            try
            {
                response = await _userRepository.GetAllUsers(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<GetUserByIdResponse>> GetUserById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<GetUserByIdResponse>();
            try
            {
                response.Data = await _userRepository.GetUserById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
