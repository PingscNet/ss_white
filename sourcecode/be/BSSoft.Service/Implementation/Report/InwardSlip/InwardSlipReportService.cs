﻿namespace SSWhite.Service.Implementation.Report.InwardSlip
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Report.InwardSlip;
    using SSWhite.Domain.Request.Report.InwardSlip;
    using SSWhite.Domain.Response.Report.InwardSlip;
    using SSWhite.Service.Interface.Report.InwardSlip;

    public class InwardSlipReportService : IInwardSlipReportService
    {
        private readonly IInwardSlipReportRepository _inwardSlipRepository;

        public InwardSlipReportService(IInwardSlipReportRepository inwardSlipRepository)
        {
            _inwardSlipRepository = inwardSlipRepository;
        }

        public async Task<ServiceSearchResponse<GetAllInwardSlipsForReportResponse>> GetAllInwardSlipsForReport(ServiceSearchRequest<GetAllInwardSlipsForReport> request)
        {
            var response = new ServiceSearchResponse<GetAllInwardSlipsForReportResponse>();
            try
            {
                response = await _inwardSlipRepository.GetAllInwardSlipsForReport(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
