﻿namespace SSWhite.Service.Implementation.Report.Ledger
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Report.Ledger;
    using SSWhite.Domain.Request.Report.LedgerReport;
    using SSWhite.Domain.Response.Report.Ledger;
    using SSWhite.Service.Interface.Report.Ledger;

    public class LedgerReportService : ILedgerReportService
    {
        private readonly ILedgerReportRepository _ledgerReportRepository;

        public LedgerReportService(ILedgerReportRepository inwardSlipRepository)
        {
            _ledgerReportRepository = inwardSlipRepository;
        }

        public async Task<ServiceResponse<GetLedgerReportResponse>> GetLedgerReport(ServiceRequest<GetLedgerReport> request)
        {
            var response = new ServiceResponse<GetLedgerReportResponse>();
            try
            {
                response = await _ledgerReportRepository.GetLedgerReport(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
