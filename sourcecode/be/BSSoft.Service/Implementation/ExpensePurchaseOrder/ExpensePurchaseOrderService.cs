﻿namespace SSWhite.Service.Implementation.ExpensePurchaseOrder
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Mail;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Admin;
    using SSWhite.Data.Interface.ExpensePurchaseOrder;
    using SSWhite.Domain.Enums;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;
    using SSWhite.Service.Common;
    using SSWhite.Service.Interface.ExpensePurchaseOrder;

    public class ExpensePurchaseOrderService : IExpensePurchaseOrderService
    {
        private readonly IExpensePurchaseOrderRepository _expensePurchaseOrderRepository;
        private readonly IConfiguration _configuration;
        private readonly ISendMailService _sendMailService;
        private readonly AppSetting _appSetting;

        public ExpensePurchaseOrderService(IExpensePurchaseOrderRepository expensePurchaseOrderRepository, IConfiguration configuration, ISendMailService sendMailService, IOptions<AppSetting> appSetting)
        {
            _expensePurchaseOrderRepository = expensePurchaseOrderRepository;
            _configuration = configuration;
            _sendMailService = sendMailService;
            _appSetting = appSetting.Value;
        }

        public async Task<GetAllEPODropDownsResponse> GetAllEPODropDowns()
        {
            try
            {
                return await _expensePurchaseOrderRepository.GetAllEPODropDowns();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetAuthorizedLimitByUserIdResponse> GetAuthorizedLimitByUserId(ServiceRequest request)
        {
            try
            {
                var response = await _expensePurchaseOrderRepository.GetAuthorizedLimitByUserId(request);

                response.DollarPrice = Common.GetDollarValue();
                response.EmpLimitNewDollar = response.EmpLimitNew / response.DollarPrice;
                return response;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<AddOrEditResponse> AddOrEditEpo(ServiceRequest<AddOrEditEpoRequest> request)
        {
            try
            {
                var response = await _expensePurchaseOrderRepository.AddOrEditEpo(request);

                if (response.ApprovalEmailIds != null)
                {
                    await _sendMailService.SendMailAsync(PrepareMailForApproval(response, EmailType.EPOMailToManager));
                    await _sendMailService.SendMailAsync(PrepareMailForApproval(response, EmailType.EPOMailToSelfForApproval));
                }
                else
                {
                    await _sendMailService.SendMailAsync(PrepareMailForApproval(response, EmailType.EPOMailCreatedToSelf));
                }

                return response;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetEpoDetailsByEpoIdResponse> GetEpoDetailsByEpoId(ServiceRequest<GetEpoDetailsByEpoIdRequest> request)
        {
            try
            {
                var result = await _expensePurchaseOrderRepository.GetEpoDetailsByEpoId(request);

                if (result.EpoBasicDetailsResponse?.EmployeeId != null)
                {
                    result.EpoBasicDetailsResponse.IsEmployee = true;
                }

                foreach (var item in result.EpoAttachmentMst)
                {
                    ////item.AttachedFileName = $"{_configuration.GetValue<string>("AppSetting:AttachmentRootPathToGetImage")}{item.AttachedFileName}";
                    item.AttachedFileName = $"{_appSetting.AttachmentRootPathToGetImage}{item.AttachedFileName}";
                }

                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllEposResponse>> GetAllEpos(ServiceSearchRequest<GetAllEposRequest> request)
        {
            try
            {
                return await _expensePurchaseOrderRepository.GetAllEpos(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task VoidEpoByUserId(ServiceRequest<GetEpoDetailsByEpoIdRequest> request)
        {
            try
            {
                await _expensePurchaseOrderRepository.VoidEpoByUserId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task ApproveOrDenyEpo(ServiceRequest<ApproveOrDenyEpoRequest> request)
        {
            try
            {
                await _expensePurchaseOrderRepository.ApproveOrDenyEpo(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetMpcVoteForEpoResponse>> GetMpcVoteForEpo(ServiceSearchRequest<GetMpcVoteForEpoRequest> request)
        {
            try
            {
                return await _expensePurchaseOrderRepository.GetMpcVoteForEpo(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllMpcVoteForEpoResponse>> GetAllMpcVoteForEpo(ServiceSearchRequest<GetAllMpcVoteForEpoRequest> request)
        {
            try
            {
                return await _expensePurchaseOrderRepository.GetAllMpcVoteForEpo(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetMySubordinateEposResponse>> GetMySubordinateEpos(ServiceSearchRequest<GetMySubordinateEposRequest> request)

        {
            try
            {
                return await _expensePurchaseOrderRepository.GetMySubordinateEpos(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetAllVendorOrEmployeeDetailsByNameResponse>> GetAllVendorOrEmployeeDetailsByName(GetAllVendorOrEmployeeDetailsByNameRequest request)
        {
            try
            {
                return await _expensePurchaseOrderRepository.GetAllVendorOrEmployeeDetailsByName(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetMyApprovalForEpoResponse>> GetMyApprovalForEpo(ServiceSearchRequest<GetMyApprovalForEpoRequest> request)
        {
            try
            {
                return await _expensePurchaseOrderRepository.GetMyApprovalForEpo(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetEpoModulesCountsResponse> GetEpoModulesCounts(ServiceRequest request)
        {
            try
            {
                return await _expensePurchaseOrderRepository.GetEpoModulesCounts(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetMpcVoteDetailsByEpoIdResponse>> GetMpcVoteDetailsByEpoId(GetMpcVoteDetailsByEpoIdRequest request)
        {
            try
            {
                return await _expensePurchaseOrderRepository.GetMpcVoteDetailsByEpoId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private SendMail PrepareMailForApproval(AddOrEditResponse response, EmailType ePOMailToManager)
        {
            var subject = string.Empty;
            var email = string.Empty;
            var body = string.Empty;
            var isHtml = true;
            switch (ePOMailToManager)
            {
                case EmailType.EPOMailToManager:
                    subject = "*** EPO # " + response.EpoID + " - " + response.CreatedBy + " Submitted For Approval";
                    email = response.ApprovalEmailIds;
                    body = $"<span style = 'font-family:Arial;font-size:10pt'><p><strong> {response.EpoApprovalMessage} " +
                        $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'> <p><strong>EPO Number:</strong>  {response.EpoID}" +
                        $"<br /><strong> Created By :</strong> {response.CreatedBy}" +
                        $" <br /> <strong>EPO Approx. Amount: </strong> {response.GrandTotal}" +
                        $"<br /><strong>EPO Purpose: </strong> {response.PurposeOfPurchase}" +
                        $"<br /><strong>Currency Type: </strong> {response.CurrencyType}" +
                        $"<br /> <strong> Awaiting Approval: </strong> {response.AwaitingForApproval}</p> <br/><a href={_appSetting.EpoApprovalLink}{response.EpoID}?case=needApproval > Click here to approve </span>";
                    isHtml = true;
                    break;
                case EmailType.EPOMailToSelfForApproval:
                    subject = $"*** EPO # {response.EpoID} - {response.CreatedBy} - Created. Pending for Approval";
                    email = response.PersonEmailId;
                    body = $"<span style = 'font-family:Arial;font-size:10pt'>" +
                           $"<p><strong>{response.EpoApprovalMessage}</strong></p>" +
                           $"<hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'>" +
                           $"<p><strong>EPO Number:</strong>{response.EpoID}<br /><strong> Created By :</strong>{response.CreatedBy}<br /> " +
                           $"<strong>EPO Approx. Amount: </strong>{response.GrandTotal}<br />" +
                           $"<strong>EPO Purpose: </strong>{response.PurposeOfPurchase}<br />" +
                           $"<br /><strong>Currency Type: </strong> {response.CurrencyType}" +
                           $"<strong> Awaiting Approval: </strong>{response.AwaitingForApproval} </p> </span>";
                    isHtml = true;
                    break;
                case EmailType.EPOMailCreatedToSelf:
                    subject = "*** EPO # " + response.EpoID + " - " + response.CreatedBy + " Self Approved";
                    email = response.PersonEmailId;
                    body = $"<span style = 'font-family:Arial;font-size:10pt'><p><strong>{response.EpoApprovalMessage}" +
                        $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'> " +
                        $"<p><strong>EPO Number:</strong>  {response.EpoID}" +
                        $"<br /><strong> Created By :</strong> {response.CreatedBy}" +
                        $" <br /> <strong>EPO Approx. Amount: </strong> {response.GrandTotal}" +
                        $"<br /><strong>EPO Purpose: </strong> {response.PurposeOfPurchase}" +
                        $"<br /><strong>Currency Type: </strong> {response.CurrencyType}" +
                        $"</p> </span>";
                    isHtml = true;
                    break;
            }

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email
            };
        }
    }
}
