﻿namespace SSWhite.Service.Implementation.Common
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Data.Interface.Common;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Service.Interface.Common;

    public class CommonService : ICommonService
    {
        private readonly ICommonRepository _commonRepository;

        public CommonService(ICommonRepository commonRepository)
        {
            _commonRepository = commonRepository;
        }

        public async Task<List<GetDepartmentByDepartmentSupervisorNameResponse>> GetDepartmentByDepartmentSupervisorName(GetDepartmentByDepartmentSupervisorNameRequest getDepartmentByDepartmentSupervisorNameRequest)
        {
            try
            {
                return await _commonRepository.GetDepartmentByDepartmentSupervisorName(getDepartmentByDepartmentSupervisorNameRequest);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetEmployeeJobDescriptionResponse>> GetEmployeeJobDescription()
        {
            try
            {
                return await _commonRepository.GetEmployeeJobDescription();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetReportingUnderResponse>> GetReportingUnder()
        {
            try
            {
                return await _commonRepository.GetReportingUnder();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetOrganizationResponse>> GetOrganization()
        {
            try
            {
                return await _commonRepository.GetOrganization();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
