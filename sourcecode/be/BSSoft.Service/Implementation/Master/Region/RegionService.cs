﻿namespace SSWhite.Service.Implementation.Master.Region
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Region;
    using SSWhite.Domain.Common.Master.Region;
    using SSWhite.Domain.Request.Master.Region;
    using SSWhite.Domain.Response.Master.Region;
    using SSWhite.Service.Interface.Master.Region;

    public class RegionService : IRegionService
    {
        private readonly IRegionRepository _regionRepository;

        public RegionService(IRegionRepository regionRepository)
        {
            _regionRepository = regionRepository;
        }

        public async Task<ServiceResponse<int>> AddEditRegion(ServiceRequest<Region> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _regionRepository.AddEditRegion(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeRegionStatus(ServiceRequest<int> request)
        {
            try
            {
                await _regionRepository.ChangeRegionStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteRegion(ServiceRequest<int> request)
        {
            try
            {
                await _regionRepository.DeleteRegion(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllRegionsResponse>> GetAllRegions(ServiceSearchRequest<GetAllRegions> request)
        {
            var response = new ServiceSearchResponse<GetAllRegionsResponse>();
            try
            {
                response = await _regionRepository.GetAllRegions(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Region>> GetRegionById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Region>();
            try
            {
                response.Data = await _regionRepository.GetRegionById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
