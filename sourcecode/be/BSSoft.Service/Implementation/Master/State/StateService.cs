﻿namespace SSWhite.Service.Implementation.Master.State
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.State;
    using SSWhite.Domain.Common.Master.State;
    using SSWhite.Domain.Request.Master.State;
    using SSWhite.Domain.Response.Master.State;
    using SSWhite.Service.Interface.Master.State;

    public class StateService : IStateService
    {
        private readonly IStateRepository _stateRepository;

        public StateService(IStateRepository stateRepository)
        {
            _stateRepository = stateRepository;
        }

        public async Task<ServiceResponse<int>> AddEditState(ServiceRequest<State> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _stateRepository.AddEditState(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeStateStatus(ServiceRequest<int> request)
        {
            try
            {
                await _stateRepository.ChangeStateStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteState(ServiceRequest<int> request)
        {
            try
            {
                await _stateRepository.DeleteState(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllStatesResponse>> GetAllStates(ServiceSearchRequest<GetAllStates> request)
        {
            var response = new ServiceSearchResponse<GetAllStatesResponse>();
            try
            {
                response = await _stateRepository.GetAllStates(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<State>> GetStateById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<State>();
            try
            {
                response.Data = await _stateRepository.GetStateById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
