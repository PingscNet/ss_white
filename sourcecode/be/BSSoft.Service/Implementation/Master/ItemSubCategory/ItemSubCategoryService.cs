﻿namespace SSWhite.Service.Implementation.Master.ItemSubCategory
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.ItemSubCategory;
    using SSWhite.Domain.Common.Master.ItemSubCategory;
    using SSWhite.Domain.Request.Master.ItemSubCategory;
    using SSWhite.Domain.Response.Master.ItemSubCategory;
    using SSWhite.Service.Interface.Master.ItemSubCategory;

    public class ItemSubCategoryService : IItemSubCategoryService
    {
        private readonly IItemSubCategoryRepository _itemSubCategoryRepository;

        public ItemSubCategoryService(IItemSubCategoryRepository itemSubCategoryRepository)
        {
            _itemSubCategoryRepository = itemSubCategoryRepository;
        }

        public async Task<ServiceResponse<int>> AddEditItemSubCategory(ServiceRequest<ItemSubCategory> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _itemSubCategoryRepository.AddEditItemSubCategory(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeItemSubCategoryStatus(ServiceRequest<int> request)
        {
            try
            {
                await _itemSubCategoryRepository.ChangeItemSubCategoryStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteItemSubCategory(ServiceRequest<int> request)
        {
            try
            {
                await _itemSubCategoryRepository.DeleteItemSubCategory(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllItemSubCategoriesResponse>> GetAllItemSubCategories(ServiceSearchRequest<GetAllItemSubCategories> request)
        {
            var response = new ServiceSearchResponse<GetAllItemSubCategoriesResponse>();
            try
            {
                response = await _itemSubCategoryRepository.GetAllItemSubCategories(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<ItemSubCategory>> GetItemSubCategoryById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<ItemSubCategory>();
            try
            {
                response.Data = await _itemSubCategoryRepository.GetItemSubCategoryById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
