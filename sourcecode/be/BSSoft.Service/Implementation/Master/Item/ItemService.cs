﻿namespace SSWhite.Service.Implementation.Master.Item
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Item;
    using SSWhite.Domain.Common.Master.Item;
    using SSWhite.Domain.Request.Master.Item;
    using SSWhite.Domain.Response.Master.Item;
    using SSWhite.Service.Interface.Master.Item;

    public class ItemService : IItemService
    {
        private readonly IItemRepository _itemRepository;

        public ItemService(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<ServiceResponse<int>> AddEditItem(ServiceRequest<Item> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _itemRepository.AddEditItem(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeItemStatus(ServiceRequest<int> request)
        {
            try
            {
                await _itemRepository.ChangeItemStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteItem(ServiceRequest<int> request)
        {
            try
            {
                await _itemRepository.DeleteItem(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllItemsResponse>> GetAllItems(ServiceSearchRequest<GetAllItems> request)
        {
            var response = new ServiceSearchResponse<GetAllItemsResponse>();
            try
            {
                response = await _itemRepository.GetAllItems(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Item>> GetItemById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Item>();
            try
            {
                response.Data = await _itemRepository.GetItemById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
