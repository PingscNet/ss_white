﻿namespace SSWhite.Service.Implementation.Master.Client
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Client;
    using SSWhite.Domain.Common.Master.Client;
    using SSWhite.Domain.Request.Master.Client;
    using SSWhite.Domain.Response.Master.Client;
    using SSWhite.Service.Interface.Master.Client;

    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;

        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public async Task<ServiceResponse<int>> AddEditClient(ServiceRequest<Client> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _clientRepository.AddEditClient(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeClientStatus(ServiceRequest<int> request)
        {
            try
            {
                await _clientRepository.ChangeClientStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteClient(ServiceRequest<int> request)
        {
            try
            {
                await _clientRepository.DeleteClient(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllClientsResponse>> GetAllClients(ServiceSearchRequest<GetAllClients> request)
        {
            var response = new ServiceSearchResponse<GetAllClientsResponse>();
            try
            {
                response = await _clientRepository.GetAllClients(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Client>> GetClientById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Client>();
            try
            {
                response.Data = await _clientRepository.GetClientById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<ClientOpeningBalance>> GetClientOpeningBalanceDetails(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<ClientOpeningBalance>();
            try
            {
                response.DataList = await _clientRepository.GetClientOpeningBalanceDetails(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<int>> AddEditOpeningBalnce(ServiceRequest<ClientOpeningBalance> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _clientRepository.AddEditOpeningBalnce(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
