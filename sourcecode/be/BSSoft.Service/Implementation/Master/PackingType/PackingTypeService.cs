﻿namespace SSWhite.Service.Implementation.Master.PackingType
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.PackingType;
    using SSWhite.Domain.Common.Master.PackingType;
    using SSWhite.Domain.Request.Master.PackingType;
    using SSWhite.Domain.Response.Master.PackingType;
    using SSWhite.Service.Interface.Master.PackingType;

    public class PackingTypeService : IPackingTypeService
    {
        private readonly IPackingTypeRepository _packingTypeRepository;

        public PackingTypeService(IPackingTypeRepository packingTypeRepository)
        {
            _packingTypeRepository = packingTypeRepository;
        }

        public async Task<ServiceResponse<int>> AddEditPackingType(ServiceRequest<PackingType> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _packingTypeRepository.AddEditPackingType(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangePackingTypeStatus(ServiceRequest<int> request)
        {
            try
            {
                await _packingTypeRepository.ChangePackingTypeStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeletePackingType(ServiceRequest<int> request)
        {
            try
            {
                await _packingTypeRepository.DeletePackingType(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllPackingTypesResponse>> GetAllPackingTypes(ServiceSearchRequest<GetAllPackingTypes> request)
        {
            var response = new ServiceSearchResponse<GetAllPackingTypesResponse>();
            try
            {
                response = await _packingTypeRepository.GetAllPackingTypes(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<PackingType>> GetPackingTypeById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<PackingType>();
            try
            {
                response.Data = await _packingTypeRepository.GetPackingTypeById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
