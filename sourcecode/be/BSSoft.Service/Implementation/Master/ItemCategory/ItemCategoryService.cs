﻿namespace SSWhite.Service.Implementation.Master.ItemCategory
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.ItemCategory;
    using SSWhite.Domain.Common.Master.ItemCategory;
    using SSWhite.Domain.Request.Master.ItemCategory;
    using SSWhite.Domain.Response.Master.ItemCategory;
    using SSWhite.Service.Interface.Master.ItemCategory;

    public class ItemCategoryService : IItemCategoryService
    {
        private readonly IItemCategoryRepository _itemCategoryRepository;

        public ItemCategoryService(IItemCategoryRepository itemCategoryRepository)
        {
            _itemCategoryRepository = itemCategoryRepository;
        }

        public async Task<ServiceResponse<int>> AddEditItemCategory(ServiceRequest<ItemCategory> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _itemCategoryRepository.AddEditItemCategory(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeItemCategoryStatus(ServiceRequest<int> request)
        {
            try
            {
                await _itemCategoryRepository.ChangeItemCategoryStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteItemCategory(ServiceRequest<int> request)
        {
            try
            {
                await _itemCategoryRepository.DeleteItemCategory(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllItemCategoriesResponse>> GetAllItemCategories(ServiceSearchRequest<GetAllItemCategories> request)
        {
            var response = new ServiceSearchResponse<GetAllItemCategoriesResponse>();
            try
            {
                response = await _itemCategoryRepository.GetAllItemCategories(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<ItemCategory>> GetItemCategoryById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<ItemCategory>();
            try
            {
                response.Data = await _itemCategoryRepository.GetItemCategoryById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
