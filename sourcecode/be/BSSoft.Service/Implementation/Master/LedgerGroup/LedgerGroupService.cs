﻿namespace SSWhite.Service.Implementation.Master.LedgerGroup
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Interface.Master.LedgerGroup;
    using SSWhite.Domain.Common.Master.LedgerGroup;
    using SSWhite.Domain.Enums.Master.LedgerGroup;
    using SSWhite.Domain.Request.Master.LedgerGroup;
    using SSWhite.Domain.Response.Master.LedgerGroup;
    using SSWhite.Service.Interface.Master.LedgerGroup;

    public class LedgerGroupService : ILedgerGroupService
    {
        private readonly ILedgerGroupRepository _ledgerGroupRepository;

        public LedgerGroupService(ILedgerGroupRepository ledgerGroupRepository)
        {
            _ledgerGroupRepository = ledgerGroupRepository;
        }

        public async Task<ServiceResponse<int>> AddEditLedgerGroup(ServiceRequest<LedgerGroup> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _ledgerGroupRepository.AddEditLedgerGroup(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeLedgerGroupStatus(ServiceRequest<int> request)
        {
            try
            {
                await _ledgerGroupRepository.ChangeLedgerGroupStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteLedgerGroup(ServiceRequest<int> request)
        {
            try
            {
                await _ledgerGroupRepository.DeleteLedgerGroup(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllLedgerGroupsResponse>> GetAllLedgerGroups(ServiceSearchRequest<GetAllLedgerGroups> request)
        {
            var response = new ServiceSearchResponse<GetAllLedgerGroupsResponse>();
            try
            {
                response = await _ledgerGroupRepository.GetAllLedgerGroups(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<LedgerGroup>> GetLedgerGroupById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<LedgerGroup>();
            try
            {
                response.Data = await _ledgerGroupRepository.GetLedgerGroupById(request);
                response.Data.SectionTypes = response.Data.Sections?.DeserializeJson<List<LedgerSectionType>>();
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
