﻿namespace SSWhite.Service.Implementation.Master.City
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.City;
    using SSWhite.Domain.Common.Master.City;
    using SSWhite.Domain.Request.Master.City;
    using SSWhite.Domain.Response.Master.City;
    using SSWhite.Service.Interface.Master.City;

    public class CityService : ICityService
    {
        private readonly ICityRepository _cityRepository;

        public CityService(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }

        public async Task<ServiceResponse<int>> AddEditCity(ServiceRequest<City> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _cityRepository.AddEditCity(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeCityStatus(ServiceRequest<int> request)
        {
            try
            {
                await _cityRepository.ChangeCityStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteCity(ServiceRequest<int> request)
        {
            try
            {
                await _cityRepository.DeleteCity(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllCitiesResponse>> GetAllCities(ServiceSearchRequest<GetAllCities> request)
        {
            var response = new ServiceSearchResponse<GetAllCitiesResponse>();
            try
            {
                response = await _cityRepository.GetAllCities(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<City>> GetCityById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<City>();
            try
            {
                response.Data = await _cityRepository.GetCityById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
