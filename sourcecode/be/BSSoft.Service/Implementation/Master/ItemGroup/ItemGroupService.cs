﻿namespace SSWhite.Service.Implementation.Master.ItemGroup
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.ItemGroup;
    using SSWhite.Domain.Common.Master.ItemGroup;
    using SSWhite.Domain.Request.Master.ItemGroup;
    using SSWhite.Domain.Response.Master.ItemGroup;
    using SSWhite.Service.Interface.Master.ItemGroup;

    public class ItemGroupService : IItemGroupService
    {
        private readonly IItemGroupRepository _itemGroupRepository;

        public ItemGroupService(IItemGroupRepository itemGroupRepository)
        {
            _itemGroupRepository = itemGroupRepository;
        }

        public async Task<ServiceResponse<int>> AddEditItemGroup(ServiceRequest<ItemGroup> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _itemGroupRepository.AddEditItemGroup(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeItemGroupStatus(ServiceRequest<int> request)
        {
            try
            {
                await _itemGroupRepository.ChangeItemGroupStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteItemGroup(ServiceRequest<int> request)
        {
            try
            {
                await _itemGroupRepository.DeleteItemGroup(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllItemGroupsResponse>> GetAllItemGroups(ServiceSearchRequest<GetAllItemGroups> request)
        {
            var response = new ServiceSearchResponse<GetAllItemGroupsResponse>();
            try
            {
                response = await _itemGroupRepository.GetAllItemGroups(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<ItemGroup>> GetItemGroupById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<ItemGroup>();
            try
            {
                response.Data = await _itemGroupRepository.GetItemGroupById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
