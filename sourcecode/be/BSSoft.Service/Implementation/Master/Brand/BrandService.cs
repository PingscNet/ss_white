﻿namespace SSWhite.Service.Implementation.Master.Brand
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Brand;
    using SSWhite.Domain.Request.Master.Brand;
    using SSWhite.Domain.Response.Master.Brand;
    using SSWhite.Service.Interface.Master.Brand;

    public class BrandService : IBrandService
    {
        private readonly IBrandRepository _brandRepository;

        public BrandService(IBrandRepository brandRepository)
        {
            _brandRepository = brandRepository;
        }

        public async Task<ServiceResponse<int>> AddBrand(ServiceRequest<AddBrand> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _brandRepository.AddBrand(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeBrandStatus(ServiceRequest<int> request)
        {
            try
            {
                await _brandRepository.ChangeBrandStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteBrand(ServiceRequest<int> request)
        {
            try
            {
                await _brandRepository.DeleteBrand(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceResponse<int>> EditBrand(ServiceRequest<EditBrand> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _brandRepository.EditBrand(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllBrandsResponse>> GetAllBrands(ServiceSearchRequest<GetAllBrands> request)
        {
            var response = new ServiceSearchResponse<GetAllBrandsResponse>();
            try
            {
                response = await _brandRepository.GetAllBrands(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<GetBrandByIdResponse>> GetBrandById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<GetBrandByIdResponse>();
            try
            {
                response.Data = await _brandRepository.GetBrandById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
