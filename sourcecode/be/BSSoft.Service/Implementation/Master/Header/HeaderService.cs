﻿namespace SSWhite.Service.Implementation.Master.Header
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Header;
    using SSWhite.Domain.Common.Master.Header;
    using SSWhite.Domain.Request.Master.Header;
    using SSWhite.Domain.Response.Master.Header;
    using SSWhite.Service.Interface.Master.Header;

    public class HeaderService : IHeaderService
    {
        private readonly IHeaderRepository _headerRepository;

        public HeaderService(IHeaderRepository headerRepository)
        {
            _headerRepository = headerRepository;
        }

        public async Task<ServiceResponse<int>> AddEditHeader(ServiceRequest<Header> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _headerRepository.AddEditHeader(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeHeaderStatus(ServiceRequest<int> request)
        {
            try
            {
                await _headerRepository.ChangeHeaderStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteHeader(ServiceRequest<int> request)
        {
            try
            {
                await _headerRepository.DeleteHeader(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllHeadersResponse>> GetAllHeaders(ServiceSearchRequest<GetAllHeaders> request)
        {
            var response = new ServiceSearchResponse<GetAllHeadersResponse>();
            try
            {
                response = await _headerRepository.GetAllHeaders(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Header>> GetHeaderById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Header>();
            try
            {
                response.Data = await _headerRepository.GetHeaderById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
