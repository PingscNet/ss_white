﻿namespace SSWhite.Service.Implementation.Master.Uqc
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Uqc;
    using SSWhite.Domain.Common.Master.Uqc;
    using SSWhite.Domain.Request.Master.Uqc;
    using SSWhite.Domain.Response.Master.Uqc;
    using SSWhite.Service.Interface.Master.Uqc;

    public class UqcService : IUqcService
    {
        private readonly IUqcRepository _uqcRepository;

        public UqcService(IUqcRepository uqcTypeRepository)
        {
            _uqcRepository = uqcTypeRepository;
        }

        public async Task<ServiceResponse<int>> AddEditUqc(ServiceRequest<Uqc> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _uqcRepository.AddEditUqc(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeUqcStatus(ServiceRequest<int> request)
        {
            try
            {
                await _uqcRepository.ChangeUqcStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteUqc(ServiceRequest<int> request)
        {
            try
            {
                await _uqcRepository.DeleteUqc(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllUqcsResponse>> GetAllUqcs(ServiceSearchRequest<GetAllUqcs> request)
        {
            var response = new ServiceSearchResponse<GetAllUqcsResponse>();
            try
            {
                response = await _uqcRepository.GetAllUqcs(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Uqc>> GetUqcById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Uqc>();
            try
            {
                response.Data = await _uqcRepository.GetUqcById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
