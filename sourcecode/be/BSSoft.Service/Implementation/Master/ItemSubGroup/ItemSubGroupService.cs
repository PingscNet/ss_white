﻿namespace SSWhite.Service.Implementation.Master.ItemSubGroup
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.ItemSubGroup;
    using SSWhite.Domain.Common.Master.ItemSubGroup;
    using SSWhite.Domain.Request.Master.ItemSubGroup;
    using SSWhite.Domain.Response.Master.ItemSubGroup;
    using SSWhite.Service.Interface.Master.ItemSubGroup;

    public class ItemSubGroupService : IItemSubGroupService
    {
        private readonly IItemSubGroupRepository _itemSubGroupRepository;

        public ItemSubGroupService(IItemSubGroupRepository itemSubGroupRepository)
        {
            _itemSubGroupRepository = itemSubGroupRepository;
        }

        public async Task<ServiceResponse<int>> AddEditItemSubGroup(ServiceRequest<ItemSubGroup> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _itemSubGroupRepository.AddEditItemSubGroup(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeItemSubGroupStatus(ServiceRequest<int> request)
        {
            try
            {
                await _itemSubGroupRepository.ChangeItemSubGroupStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteItemSubGroup(ServiceRequest<int> request)
        {
            try
            {
                await _itemSubGroupRepository.DeleteItemSubGroup(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllItemSubGroupsResponse>> GetAllItemSubGroups(ServiceSearchRequest<GetAllItemSubGroups> request)
        {
            var response = new ServiceSearchResponse<GetAllItemSubGroupsResponse>();
            try
            {
                response = await _itemSubGroupRepository.GetAllItemSubGroups(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<ItemSubGroup>> GetItemSubGroupById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<ItemSubGroup>();
            try
            {
                response.Data = await _itemSubGroupRepository.GetItemSubGroupById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
