﻿namespace SSWhite.Service.Implementation.Account
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Response.Account;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;
    using SSWhite.Service.Interface.Account;

    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly ISendMailService _sendMailService;
        private readonly AppSetting _appSetting;

        public AccountService(IAccountRepository accountRepository, ISendMailService sendMailService, IOptions<AppSetting> appSetting)
        {
            _accountRepository = accountRepository;
            _sendMailService = sendMailService;
            _appSetting = appSetting.Value;
        }

        public async Task<ServiceResponse<LogInResponse>> ValidateUserForLogIn(ServiceRequest<LogInRequest> request)
        {
            var response = new ServiceResponse<LogInResponse>();
            try
            {
                response.Data = await _accountRepository.ValidateUserForLogIn(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task SendForgetPasswordEmail(SendForgetPasswordEmailRequest request)
        {
            try
            {
                var response = await _accountRepository.SendForgetPasswordEmail(request);

                if (response != null)
                {
                    await _sendMailService.SendMailAsync(PrepareMailForForgetEmailLink(response));
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<string> IsEmailValid(IsEmailValidRequest request)
        {
            try
            {
                try
                {
                    request.EmailAddress = request.EmailAddress.Base64Decode();
                    var s = request.EmailAddress.Substring(request.EmailAddress.LastIndexOf('#') + 1);
                    ////var s = date.Substring('#',);
                    DateTime tempDate = DateTime.Parse(s);

                    if (tempDate < DateTime.Now)
                    {
                        return "Invalid";
                    }

                    request.EmailAddress = request.EmailAddress.Substring(0, request.EmailAddress.LastIndexOf('#'));
                }
                catch (Exception exception)
                {
                    return "Invalid";
                }

                return await _accountRepository.IsEmailValid(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task SetNewPassword(SetNewPasswordRequest request)
        {
            try
            {
                try
                {
                    request.EmailAddress = request.EmailAddress.Base64Decode();
                    request.EmailAddress = request.EmailAddress.Substring(0, request.EmailAddress.LastIndexOf('#'));
                }
                catch (Exception exception)
                {
                    throw exception;
                }

                var response = await _accountRepository.SetNewPassword(request);

                if (response != null)
                {
                    await _sendMailService.SendMailAsync(PrepareMailForChangePassword(response));
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private SendMail PrepareMailForForgetEmailLink(string response)
        {
            var expiryDate = DateTime.Now.AddDays(1).ToString();
            var urlString = response + "#" + expiryDate;
            var subject = string.Empty;
            var email = string.Empty;
            var body = string.Empty;
            var isHtml = true;

            subject = "*** Forget Password Link #  - ";
            email = response;
            body = $"<span style = 'font-family:Arial;font-size:10pt'>" +
                $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'>" +
                ////$" <p><strong>DocumentId:</strong>  { response.DocumentId }" +
                $"<br /><strong> Click below Link to change password</strong> " +
                 $"<br /> <strong>  </strong> </p> <br/><a href={_appSetting.ForgetPasswordLink}{urlString.Base64Encode()}> Click here to change password " +
                 $"</span>";

            isHtml = true;

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email
            };
        }

        private SendMail PrepareMailForChangePassword(string response)
        {
            var subject = string.Empty;
            var email = string.Empty;
            var body = string.Empty;
            var isHtml = true;

            subject = "*** Password Change #  - ";
            email = response;
            body = $"<span style = 'font-family:Arial;font-size:10pt'>" +
                $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'>" +
                $"<br /><strong> Your password hase been successfully changed </strong> " +
                 $"</span>";

            isHtml = true;

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email
            };
        }
    }
}
