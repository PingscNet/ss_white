﻿namespace SSWhite.Service.Validator.Report.Ledger
{
    using FluentValidation;
    using SSWhite.Domain.Request.Report.LedgerReport;
    using SSWhite.ResourceFile;

    public class GetLedgerReportValidator : AbstractValidator<GetLedgerReport>
    {
        public GetLedgerReportValidator()
        {
            RuleFor(x => x.ClientId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Client))
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.IsRequired, Labels.Client));

            RuleFor(x => x.FromDate)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.FromDate));

            RuleFor(x => x.ToDate)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.ToDate));

            RuleFor(x => x.ToDate)
                .GreaterThanOrEqualTo(x => x.FromDate).WithMessage(Validations.ToDateLessThanFromDate);
        }
    }
}
