﻿namespace SSWhite.Service.Validator.Account
{
    using System;
    using FluentValidation;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.TimeAndAttendance;
    using SSWhite.ResourceFile;

    public class GetInOutStatusByPunchTypeUserIdRequestValidator : AbstractValidator<GetInOutStatusByPunchTypeUserIdRequest>
    {
        public GetInOutStatusByPunchTypeUserIdRequestValidator()
        {
            ////RuleFor(x => x.UserId)
            ////    .NotEmpty().WithMessage("UserId Is Required");

            RuleFor(x => x.AttendancePunchTypeId)
                .NotEmpty().WithMessage("AttendancePunchTypeId Is Required");
        }
    }
}
