﻿namespace SSWhite.Service.Validator.Account
{
    using FluentValidation;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.TimeAndAttendance;
    using SSWhite.ResourceFile;

    public class InsertInOutDetailsRequestValidator : AbstractValidator<InsertInOutDetailsRequest>
    {
        public InsertInOutDetailsRequestValidator()
        {
            RuleFor(x => x.PunchType)
                .NotEmpty().WithMessage("PunchType Is Required");

            RuleFor(x => x.InOutStatus)
                .NotEmpty().WithMessage("InOutStatus Is Required");
        }
    }
}
