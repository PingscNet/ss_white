﻿namespace SSWhite.Service.Validator.ExpensePurchaseOrder
{
    using FluentValidation;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;

    public class GetMpcVoteDetailsByEpoIdRequestValidator : AbstractValidator<GetMpcVoteDetailsByEpoIdRequest>
    {
        public GetMpcVoteDetailsByEpoIdRequestValidator()
        {
            RuleFor(x => x.EpoId)
                .NotEmpty().WithMessage("EpoId Is Required");
        }
    }
}
