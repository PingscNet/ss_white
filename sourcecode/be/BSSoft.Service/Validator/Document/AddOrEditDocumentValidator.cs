﻿namespace SSWhite.Service.Validator.Document
{
    using FluentValidation;
    using SSWhite.Domain.Request.Document;

    public class AddOrEditDocumentValidator : AbstractValidator<AddOrEditDocumentRequest>
    {
        public AddOrEditDocumentValidator()
        {
            RuleFor(x => x.DocumentType)
                .NotEmpty().WithMessage("DocumentType Is Required");

            RuleFor(x => x.Rev)
                .NotNull().WithMessage("Rev Is Required");

            RuleFor(x => x.DocumentTitle)
                .NotNull().WithMessage("Document Title Is Required");

            ////RuleFor(x => x.EcnTitle)
            ////    .NotEmpty().WithMessage("EcnTitle Is Required");

            ////RuleFor(x => x.AttachmentApproval)
            ////    .NotNull().WithMessage("AttachmentApproval Is Required");
        }
    }
}
