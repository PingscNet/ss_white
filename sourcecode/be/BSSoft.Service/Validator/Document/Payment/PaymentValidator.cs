﻿namespace SSWhite.Service.Validator.Document.Payment
{
    using FluentValidation;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Common.Document.Payment;
    using SSWhite.Domain.Enums.Document;
    using SSWhite.ResourceFile;

    public class PaymentValidator : AbstractValidator<Payment>
    {
        public PaymentValidator()
        {
            RuleFor(x => x.PaymentNumber)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.PaymentNumber));

            RuleFor(x => x.Date)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Date));

            When(x => x.Date != null, () =>
            {
                RuleFor(x => x.Date)
                    .Custom((context, arguments) =>
                    {
                        if (context.Value.ToFinancialYear() != SessionGetterSetter.Get().CurrentFinancialYear)
                        {
                            arguments.AddFailure(nameof(Payment.Date), string.Format(Validations.DateNotInSelectedFinancialYear));
                        }
                    });
            });

            When(x => x.PaymentType == DocumentType.BankPayment, () =>
            {
                RuleFor(x => x.BankId)
                    .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Bank))
                    .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Bank));

                RuleFor(x => x.ChequeNumber)
                    .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.ChequeNo));

                RuleFor(x => x.Branch)
                    .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Branch));

                RuleFor(x => x.ChequeReturnedDate)
                    .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.ReturnDate))
                    .When(x => x.IsReturnedCheque);

                RuleFor(x => x.ChequeReturnedDate)
                    .Must(x => x == null).WithMessage(Validations.ChequeReturnedDateMustBeEmpty)
                    .When(x => !x.IsReturnedCheque);
            });

            RuleFor(x => x.PaymentType)
                 .IsInEnum().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.PaymentType));
        }
    }
}
