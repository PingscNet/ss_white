﻿namespace SSWhite.Service.Validator.Document
{
    using FluentValidation;
    using SSWhite.Domain.Request.Document;

    public class GetDocumentByIdValidator : AbstractValidator<GetDocumentByIdRequest>
    {
        public GetDocumentByIdValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty().WithMessage("EpoId Is Required");
        }
    }
}
