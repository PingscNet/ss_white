﻿namespace SSWhite.Service.Validator.Document.Receipt
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FluentValidation;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Common.Document.Receipt;
    using SSWhite.Domain.Enums.Document;
    using SSWhite.ResourceFile;

    public class ReceiptValidator : AbstractValidator<Receipt>
    {
        public ReceiptValidator()
        {
            RuleFor(x => x.ReceiptNumber)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.ReceiptNumber));

            RuleFor(x => x.Date)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Date));

            When(x => x.Date != null, () =>
            {
                RuleFor(x => x.Date)
                    .Custom((context, arguments) =>
                    {
                        if (context.Value.ToFinancialYear() != SessionGetterSetter.Get().CurrentFinancialYear)
                        {
                            arguments.AddFailure(nameof(Receipt.Date), string.Format(Validations.DateNotInSelectedFinancialYear));
                        }
                    });
            });

            When(x => x.ReceiptType == DocumentType.BankReceipt, () =>
            {
                RuleFor(x => x.BankId)
                    .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Bank))
                    .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Bank));

                RuleFor(x => x.ChequeNumber)
                    .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.ChequeNo));

                RuleFor(x => x.Branch)
                    .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Branch));

                RuleFor(x => x.ChequeReturnedDate)
                    .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.ReturnDate))
                    .When(x => x.IsReturnedCheque);

                RuleFor(x => x.ChequeReturnedDate)
                    .Must(x => x == null).WithMessage(Validations.ChequeReturnedDateMustBeEmpty)
                    .When(x => !x.IsReturnedCheque);
            });

            RuleFor(x => x.ReceiptType)
                 .IsInEnum().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.ReceiptType));
        }
    }
}
