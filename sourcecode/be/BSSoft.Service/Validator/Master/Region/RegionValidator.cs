﻿namespace SSWhite.Service.Validator.Master.Region
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.Region;
    using SSWhite.ResourceFile;

    public class RegionValidator : AbstractValidator<Region>
    {
        public RegionValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 50));
        }
    }
}
