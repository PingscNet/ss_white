﻿namespace SSWhite.Service.Validator.Master.Client
{
    using FluentValidation;
    using SSWhite.Core.Common;
    using SSWhite.Core.Extensions;
    using SSWhite.Domain.Common.Master.Client;
    using SSWhite.ResourceFile;

    public class ClientValidator : AbstractValidator<Client>
    {
        public ClientValidator()
        {
            RuleFor(x => x.CompanyName)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.CompanyName))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.CompanyName, 100));

            RuleFor(x => x.Code)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Code));

            RuleFor(x => x.Gstin)
                .Length(15).WithMessage(string.Format(Validations.ExactLength, Labels.GSTNo, 15))
                .When(x => !x.Gstin.IsNullOrWhiteSpace());

            RuleFor(x => x.Pan)
                .Length(10).WithMessage(string.Format(Validations.ExactLength, Labels.Pan, 10))
                .When(x => !x.Pan.IsNullOrWhiteSpace());

            RuleFor(x => x.EmailAddress)
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Email, 100))
                .Matches(RegexExpression.EMAIL).WithMessage(string.Format(Validations.Invalid, Labels.Email))
                .When(x => !x.EmailAddress.IsNullOrWhiteSpace());

            RuleFor(x => x.LedgerGroupId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.LedgerGroup))
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.LedgerGroup));

            RuleFor(x => x.TransactionType)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.TransactionType))
                .IsInEnum().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.TransactionType))
                .When(x => x.OpeningBalance != null);
        }
    }
}
