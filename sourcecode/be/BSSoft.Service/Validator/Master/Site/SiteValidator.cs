﻿namespace SSWhite.Service.Validator.Master.Site
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.Site;
    using SSWhite.ResourceFile;

    public class SiteValidator : AbstractValidator<Site>
    {
        public SiteValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));

            RuleFor(x => x.Address)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Address))
                .MaximumLength(500).WithMessage(string.Format(Validations.MaxLength, Labels.Address, 500));

            RuleFor(x => x.CityId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.City))
                .GreaterThan(0).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.City));

            RuleFor(x => x.StateId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.State))
                .GreaterThan(0).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.State));
        }
    }
}
