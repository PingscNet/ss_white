﻿namespace SSWhite.Service.Validator.Master.Brand
{
    using FluentValidation;
    using SSWhite.Domain.Request.Master.Brand;
    using SSWhite.ResourceFile;

    public class EditBrandValidator : AbstractValidator<EditBrand>
    {
        public EditBrandValidator()
        {
            Include(new BrandValidator());

            RuleFor(x => x.Id)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.Id))
                .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, Labels.Id, 0));
        }
    }
}
