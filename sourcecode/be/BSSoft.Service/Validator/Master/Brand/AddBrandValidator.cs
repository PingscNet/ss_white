﻿namespace SSWhite.Service.Validator.Master.Brand
{
    using FluentValidation;
    using SSWhite.Domain.Request.Master.Brand;

    public class AddBrandValidator : AbstractValidator<AddBrand>
    {
        public AddBrandValidator()
        {
            Include(new BrandValidator());
        }
    }
}
