﻿namespace SSWhite.Service.Validator.Master.Crate
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.Crate;
    using SSWhite.ResourceFile;

    public class CrateValidator : AbstractValidator<Crate>
    {
        public CrateValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));
        }
    }
}
