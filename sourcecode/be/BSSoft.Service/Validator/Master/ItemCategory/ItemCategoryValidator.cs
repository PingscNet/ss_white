﻿namespace SSWhite.Service.Validator.Master.ItemCategory
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.ItemCategory;
    using SSWhite.ResourceFile;

    public class ItemCategoryValidator : AbstractValidator<ItemCategory>
    {
        public ItemCategoryValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 50));
        }
    }
}
