﻿namespace SSWhite.Service.Validator.Master.Item
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.Item;
    using SSWhite.ResourceFile;

    public class ItemValidator : AbstractValidator<Item>
    {
        public ItemValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));

            RuleFor(x => x.Code)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Code))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Code, 50));

            RuleFor(x => x.Remarks)
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Code, 50))
                .When(x => !string.IsNullOrEmpty(x.Remarks));

            RuleFor(x => x.HsnOrSac)
                .MaximumLength(8).WithMessage(string.Format(Validations.MaxLength, Labels.HsnCode, 8))
                .When(x => !string.IsNullOrEmpty(x.HsnOrSac));

            RuleFor(x => x.ItemGroupId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Group));

            RuleFor(x => x.ItemSubGroupId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.SubGroup));

            RuleFor(x => x.ItemCategoryId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Category));

            RuleFor(x => x.ItemSubCategoryId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.SubCategory));
        }
    }
}
