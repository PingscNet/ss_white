﻿namespace SSWhite.Service.Validator.Master.SubUnit
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.SubUnit;
    using SSWhite.ResourceFile;

    public class SubUnitValidator : AbstractValidator<SubUnit>
    {
        public SubUnitValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));
        }
    }
}
