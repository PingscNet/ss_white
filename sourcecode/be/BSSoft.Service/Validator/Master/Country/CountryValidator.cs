﻿namespace SSWhite.Service.Validator.Master.Country
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.Country;
    using SSWhite.ResourceFile;

    public class CountryValidator : AbstractValidator<Country>
    {
        public CountryValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 50));
        }
    }
}
