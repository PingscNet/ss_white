﻿namespace SSWhite.Service.Validator.Master.Tax
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.Tax;
    using SSWhite.ResourceFile;

    public class TaxValidator : AbstractValidator<Tax>
    {
        public TaxValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 50));

            RuleFor(x => x.IgstRate)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.IgstPercentage));

            RuleFor(x => x.CgstRate)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.CgstPercentage));

            RuleFor(x => x.SgstRate)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.SgstPercentage));
        }
    }
}
