﻿namespace SSWhite.Service.Validator.Master.PackingType
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.PackingType;
    using SSWhite.ResourceFile;

    public class PackingTypeValidator : AbstractValidator<PackingType>
    {
        public PackingTypeValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));
        }
    }
}
