﻿namespace SSWhite.Service.Validator.Master.ItemSubGroup
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.ItemSubGroup;
    using SSWhite.ResourceFile;

    public class ItemSubGroupValidator : AbstractValidator<ItemSubGroup>
    {
        public ItemSubGroupValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 50));

            RuleFor(x => x.ItemGroupId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.ItemGroup))
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.ItemGroup));
        }
    }
}
