﻿namespace SSWhite.Service.Validator.Master.ItemSubCategory
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.ItemSubCategory;
    using SSWhite.ResourceFile;

    public class ItemSubCategoryValidator : AbstractValidator<ItemSubCategory>
    {
        public ItemSubCategoryValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 50));

            RuleFor(x => x.ItemCategoryId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.ItemCategory))
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.ItemCategory));
        }
    }
}
