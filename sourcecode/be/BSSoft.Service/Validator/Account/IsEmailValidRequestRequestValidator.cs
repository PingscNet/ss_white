﻿namespace SSWhite.Service.Validator.Account
{
    using FluentValidation;
    using SSWhite.Domain.Request.Account;
    using SSWhite.ResourceFile;

    public class IsEmailValidRequestRequestValidator : AbstractValidator<IsEmailValidRequest>
    {
        public IsEmailValidRequestRequestValidator()
        {
            RuleFor(x => x.EmailAddress)
                .NotEmpty().WithMessage("EmailAddress is required");
        }
    }
}
