﻿namespace SSWhite.Service.Validator.Account
{
    using FluentValidation;
    using SSWhite.Domain.Request.Account;
    using SSWhite.ResourceFile;

    public class LogInRequestValidator : AbstractValidator<LogInRequest>
    {
        public LogInRequestValidator()
        {
            RuleFor(x => x.UserName)
                .NotEmpty().WithMessage(Validations.UserNameIsRequired);

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage(Validations.PasswordIsRequired);
        }
    }
}
