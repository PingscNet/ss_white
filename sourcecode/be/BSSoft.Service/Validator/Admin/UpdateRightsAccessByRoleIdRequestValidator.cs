﻿namespace SSWhite.Service.Validator.Admin
{
    using System;
    using FluentValidation;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.ResourceFile;

    public class UpdateRightsAccessByRoleIdRequestValidator : AbstractValidator<UpdateRightsAccessByRoleIdRequest>
    {
        public UpdateRightsAccessByRoleIdRequestValidator()
        {
            RuleFor(x => x.RoleId)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.RoleId));
        }
    }
}
