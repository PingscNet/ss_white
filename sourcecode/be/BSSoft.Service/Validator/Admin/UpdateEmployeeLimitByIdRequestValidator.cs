﻿namespace SSWhite.Service.Validator.Account
{
    using System;
    using FluentValidation;
    using SSWhite.Domain.Request.Admin;

    public class UpdateEmployeeLimitByIdRequestValidator : AbstractValidator<UpdateEmployeeLimitByIdRequest>
    {
        public UpdateEmployeeLimitByIdRequestValidator()
        {
            RuleFor(x => x.LimitId)
                .NotEmpty().WithMessage("Limit Id Is Required");

            //RuleFor(x => x.Limit)
            //    .NotEmpty().WithMessage("Limit Is Required");
        }
    }
}
