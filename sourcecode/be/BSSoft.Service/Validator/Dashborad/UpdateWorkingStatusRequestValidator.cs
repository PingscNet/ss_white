﻿namespace SSWhite.Service.Validator.Dashboard
{
    using FluentValidation;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.ResourceFile;

    public class UpdateWorkingStatusRequestValidator : AbstractValidator<UpdateWorkingStatusRequest>
    {
        public UpdateWorkingStatusRequestValidator()
        {
            RuleFor(x => x.Status)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.Status));
        }
    }
}
