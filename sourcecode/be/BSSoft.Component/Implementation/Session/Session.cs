﻿namespace SSWhite.Component.Implementation.Session
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    using SSWhite.Component.Interface.Cache;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Extensions;
    using StackExchange.Redis.Extensions.Core.Abstractions;
    using ISession = SSWhite.Component.Interface.Session.ISession;

    public class Session : ISession
    {
        private readonly IRedisCacheClient _redisCacheClient;
        private readonly IHttpContextAccessor _httpContext;
        private readonly ICache _cache;

        public Session(IRedisCacheClient redisCacheClient, IHttpContextAccessor httpContext, ICache cache)
        {
            _redisCacheClient = redisCacheClient;
            _httpContext = httpContext;
            _cache = cache;
        }

        public async Task<SessionDetail> GetSessionDetails()
        {
            var authToken = _httpContext.HttpContext.Request.Headers["auth-token"].ToString();
            return await _redisCacheClient.GetDbFromConfiguration().GetAsync<SessionDetail>(authToken);
        }

        public async Task SetSessionDetails(SessionDetail data, string authToken)
        {
            await _redisCacheClient.GetDbFromConfiguration().AddAsync(authToken, data, TimeSpan.FromMinutes(59.00));
            await _redisCacheClient.GetDbFromConfiguration().AddAsync($"{_httpContext.HttpContext.Request.Host.Value}_{data.UserName}", authToken);
            SessionGetterSetter.Set(data);
        }

        public async Task<(string, int?)> GetSessionDetails(string userName)
        {
            var authToken = await _redisCacheClient.GetDbFromConfiguration().GetAsync<string>($"{_httpContext.HttpContext.Request.Host.Value}_{userName}");
            if (!authToken.IsNullOrWhiteSpace())
            {
                var sessionDetail = await _redisCacheClient.GetDbFromConfiguration().GetAsync<SessionDetail>(authToken);
                var expiringInMinutes = 0;

                if (sessionDetail != null)
                {
                    expiringInMinutes = (sessionDetail.CreatedDate.AddMinutes(59) - DateTime.Now).Minutes;
                    if (expiringInMinutes <= 0)
                    {
                        return (null, null);
                    }
                }

                return (authToken, expiringInMinutes);
            }
            else
            {
                return (null, null);
            }
        }

        public async Task DeleteSession()
        {
            var authToken = _httpContext.HttpContext.Request.Headers["auth-token"].ToString();
            var session = await _redisCacheClient.GetDbFromConfiguration().GetAsync<SessionDetail>(authToken);
            await _redisCacheClient.GetDbFromConfiguration().RemoveAsync(authToken);
            var appSession = await _redisCacheClient.GetDbFromConfiguration().GetAsync<string>($"{_httpContext.HttpContext.Request.Host.Value}_{session.UserName}");

            if (appSession == authToken)
            {
                await _redisCacheClient.GetDbFromConfiguration().RemoveAsync($"{_httpContext.HttpContext.Request.Host.Value}_{session.UserName}");
            }
        }

        public void SetInMemorySessionDetails(SessionDetail data, string authToken)
        {
            var expiry = DateTime.Now.AddMinutes(1440);
            _cache.SetData(authToken, JsonConvert.SerializeObject(data), expiry);
            _cache.SetData($"{_httpContext.HttpContext.Request.Host.Value}_{data.UserName}", authToken, expiry);
            ////_httpContext.HttpContext.Session.SetString(authToken, );
            ////_httpContext.HttpContext.Session.SetString($"{_httpContext.HttpContext.Request.Host.Value}_{data.UserName}", authToken);
            SessionGetterSetter.Set(data);
        }

        public SessionDetail GetInMemorySessionDetails()
        {
            var authToken = _httpContext.HttpContext.Request.Headers["auth-token"].ToString();
            if (authToken.IsNullOrWhiteSpace())
            {
                return null;

            }
            else
            {
                return GetSessionFromAuthToken(authToken);
            }
        }

        public void DeleteInMemorySession()
        {
            var authToken = _httpContext.HttpContext.Request.Headers["auth-token"].ToString();
            var session = GetSessionFromAuthToken(authToken);
            _cache.RemoveData(authToken);
            var appSession = _cache.GetData<string>($"{_httpContext.HttpContext.Request.Host.Value}_{session.UserName}");

            if (appSession == authToken)
            {
                _cache.RemoveData($"{_httpContext.HttpContext.Request.Host.Value}_{session.UserName}");
            }
        }

        public (string, int?) GetInMemorySessionDetails(string userName)
        {
            var authToken = _cache.GetData<string>($"{_httpContext.HttpContext.Request.Host.Value}_{userName}");
            if (!authToken.IsNullOrWhiteSpace())
            {
                var sessionDetail = GetSessionFromAuthToken(authToken);
                var expiringInMinutes = 0;

                if (sessionDetail != null)
                {
                    expiringInMinutes = (sessionDetail.CreatedDate.AddMinutes(59) - DateTime.Now).Minutes;
                    if (expiringInMinutes <= 0)
                    {
                        return (null, null);
                    }
                }

                return (authToken, expiringInMinutes);
            }
            else
            {
                return (null, null);
            }
        }

        private SessionDetail GetSessionFromAuthToken(string authToken)
        {
            return JsonConvert.DeserializeObject<SessionDetail>(_cache.GetData<string>(authToken));
        }
    }
}
