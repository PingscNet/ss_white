﻿namespace SSWhite.Component.Attributes
{
    using System;
    using Microsoft.AspNetCore.Mvc;

    [AttributeUsage(AttributeTargets.Parameter)]
    public class FQueryAttribute : FromQueryAttribute
    {
    }
}
