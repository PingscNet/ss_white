﻿namespace SSWhite.Component.Common
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.Filters;
    using SSWhite.Core.Response.Error;

    public class ValidatorActionFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new ValidationFailedResult(context.ModelState);
                return;
            }

            await next();
        }
    }
}
