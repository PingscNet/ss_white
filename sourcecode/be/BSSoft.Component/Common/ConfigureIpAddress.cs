﻿namespace SSWhite.Component.Common
{
    using System.Linq;
    using System.Net;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.DependencyInjection;

    public static class ConfigureIpAddress
    {
        public static string GetRequestIpAddress(IServiceCollection services)
        {
            var context = services.BuildServiceProvider().GetRequiredService<IHttpContextAccessor>();
            if (context.HttpContext.Request.Headers["X-Forwaded-For"].Any())
            {
                return context.HttpContext.Request.Headers["X-Forwaded-For"].First();
            }
            else
            {
                return context.HttpContext.Connection.RemoteIpAddress?.ToString() == "::1" ? Dns.GetHostEntry(Dns.GetHostName()).AddressList[1].ToString() : context.HttpContext.Connection.RemoteIpAddress?.ToString();
            }
        }
    }
}
