﻿namespace SSWhite.Component.Middleware
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.Options;
    using SSWhite.Component.Interface.Cache;
    using SSWhite.Core.Common.Application;

    public class ConfigureMiddleware
    {
        public void Configure(IOptions<AppSetting> appSetting, IApplicationBuilder app, ICache cache)
        {
            if (appSetting.Value.IsLogRequest)
            {
                app.UseMiddleware<RequestMiddleware>();
            }

            app.UseMiddleware<ExceptionMiddleware>();
            app.UseMiddleware<AuthenticateMiddleware>(appSetting, cache);
        }
    }
}
