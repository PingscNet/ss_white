﻿namespace SSWhite.Component.Middleware
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Request;
    using SSWhite.Core.Utilities;
    using SSWhite.Logger.Interface.ExceptionLogger;
    using StackExchange.Redis.Extensions.Core.Abstractions;

    public class ExceptionMiddleware
    {
        private static IExceptionLogger _exceptionLogger;
        private static IRedisCacheClient _redisCacheClient;
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception appException)
            {
                try
                {
                    await LogException(appException, httpContext);
                    throw appException;
                }
                catch (Exception exception)
                {
                    _exceptionLogger = (IExceptionLogger)(_exceptionLogger ?? httpContext.RequestServices.GetService(typeof(IExceptionLogger)));
                    await _exceptionLogger.LogToFile(appException, exception);
                    throw appException;
                }
            }
        }

        private async Task LogException(Exception exception, HttpContext httpContext)
        {
            _exceptionLogger = (IExceptionLogger)(_exceptionLogger ?? httpContext.RequestServices.GetService(typeof(IExceptionLogger)));
            _redisCacheClient = (IRedisCacheClient)(_redisCacheClient ?? httpContext.RequestServices.GetService(typeof(IRedisCacheClient)));
            var request = new ServiceRequest<object>()
            {
                ////Session = await _redisCacheClient.GetDbFromConfiguration().GetAsync<SessionDetail>(httpContext.Request.Headers["auth-token"].ToString()),
                Session = new SessionDetail(),
                Data = httpContext.Items["Data"].SerializeObject()
            };
            await _exceptionLogger.LogToDatabase(request, exception);
        }
    }
}
