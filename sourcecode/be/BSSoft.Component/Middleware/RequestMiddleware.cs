﻿namespace SSWhite.Component.Middleware
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Request;
    using SSWhite.Core.Utilities;
    using SSWhite.Logger.Interface.ExceptionLogger;
    using SSWhite.Logger.Interface.RequestLogger;
    using SSWhite.Logger.Request.Request;
    using StackExchange.Redis.Extensions.Core.Abstractions;

    public class RequestMiddleware
    {
        private static IRequestLogger _requestLogger;
        private static IRedisCacheClient _redisCacheClient;
        private readonly RequestDelegate _next;

        public RequestMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
                httpContext.Items["RequestId"] = Guid.NewGuid();
                var logRequest = new LogRequest()
                {
                    RequestId = (Guid)httpContext.Items["RequestId"],
                    Data = JsonConvert.SerializeObject(httpContext.Items["Data"]),
                    FileName = httpContext.Request.Path,
                    Method = httpContext.Request.Method
                };
                _requestLogger = (IRequestLogger)(_requestLogger ?? httpContext.RequestServices.GetService(typeof(IRequestLogger)));
                _redisCacheClient = (IRedisCacheClient)(_redisCacheClient ?? httpContext.RequestServices.GetService(typeof(IRedisCacheClient)));
                var request = (IRequestLogger)(_requestLogger ?? httpContext.RequestServices.GetService(typeof(IRequestLogger)));
                var serviceRequest = new ServiceRequest<LogRequest>()
                {
                    ////Session = await _redisCacheClient.GetDbFromConfiguration().GetAsync<SessionDetail>(httpContext.Request.Headers["auth-token"].ToString()),
                    Session = new SessionDetail(),
                    Data = logRequest
                };
                await _requestLogger.LogToDatabase(serviceRequest);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
