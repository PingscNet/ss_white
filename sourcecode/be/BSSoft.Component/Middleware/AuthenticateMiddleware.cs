﻿namespace SSWhite.Component.Middleware
{
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using SSWhite.Component.Interface.Cache;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Extensions;
    using StackExchange.Redis.Extensions.Core.Abstractions;

    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class AuthenticateMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IOptions<AppSetting> appSetting;
        private readonly ICache _cache;
        private readonly AppSetting _appSetting;

        public AuthenticateMiddleware(RequestDelegate next, IOptions<AppSetting> appSetting, ICache cache)
        {
            _next = next;
            this.appSetting = appSetting;
            _cache = cache;
            _appSetting = appSetting.Value;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            string sourceType = httpContext.Request.Headers["source-type"].ToString();
            if (httpContext.Request.Path.Value.Contains("swagger"))
            {
                await _next(httpContext);
            }
            else if (httpContext.Request.Path.Value.Contains("token"))
            {
                await _next(httpContext);
            }
            else if (httpContext.Request.Path.Value.Contains("ForgetPassword"))
            {
                await _next(httpContext);
            }
            else if (httpContext.Request.Path.Value.Contains("IsEmailValid"))
            {
                await _next(httpContext);
            }
            else if (httpContext.Request.Path.Value.Contains("SetNewPassword"))
            {
                await _next(httpContext);
            }
            else
            {
                if (httpContext.Request.Headers["referer"].ToString().Contains("swagger"))
                {
                    sourceType = httpContext.Request.Headers["source-type"] = SourceType.Swagger.ToString();
                }

                var authToken = httpContext.Request.Headers["auth-token"].ToString();

                if (string.IsNullOrEmpty(authToken))
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    return;
                }
                else
                {
                    if (_appSetting.IsRedisCacheSession)
                    {
                        var redis = (IRedisCacheClient)httpContext.RequestServices.GetService(typeof(IRedisCacheClient));
                        var session = await redis.GetDbFromConfiguration().GetAsync<SessionDetail>(authToken);

                        if (session == null)
                        {
                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            return;
                        }
                        else
                        {
                            var userAuth = await redis.GetDbFromConfiguration().GetAsync<string>($"{httpContext.Request.Host.Value}_{session.UserName}");

                            if (authToken != userAuth)
                            {
                                await redis.GetDbFromConfiguration().RemoveAsync(authToken);
                                httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                                return;
                            }
                            else
                            {
                                await _next(httpContext);
                            }
                        }
                    }
                    else
                    {
                        var value = _cache.GetData<string>(authToken);
                        if (value == null)
                        {
                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            return;
                        }

                        var session = JsonConvert.DeserializeObject<SessionDetail>(value);

                        if (session == null)
                        {
                            httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            return;
                        }
                        else
                        {
                            ////var userAuth = _cache.GetData<string>($"{httpContext.Request.Host.Value}_{session.UserName}");

                            ////if (authToken != userAuth)
                            ////{
                            ////    _cache.RemoveData(authToken);
                            ////    httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            ////    return;
                            ////}
                            ////else
                            ////{
                            await _next(httpContext);
                            ////}
                        }
                    }
                }
            }
        }
    }
}
