﻿namespace SSWhite.Notification.Interface
{
    using System.Threading.Tasks;
    using SSWhite.Notification.Model;

    public interface ISendMailService
    {
        public Task SendMailAsync(SendMail sendMail);
    }
}
