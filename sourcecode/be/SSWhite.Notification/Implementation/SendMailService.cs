﻿namespace SSWhite.Notification.Implementation
{
    using System.Net.Mail;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Utilities;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;

    public class SendMailService : ISendMailService
    {

        private readonly AppSetting _appSetting;

        public SendMailService(IOptions<AppSetting> appSetting)
        {
            _appSetting = appSetting.Value;
        }

        public Task SendMailAsync(SendMail sendMail)
        {
            //sendMail.ToEmails = "forams@sixsigmatechnosoft.com";
            var message = new MailMessage();
            var toEmails = sendMail.ToEmails.ToString().Split(',');
            message.From = new MailAddress(_appSetting.NotificationCredentials.From);
            message.Subject = sendMail.Subject;
            GetMailingName(toEmails, message.To);

            message.BodyEncoding = Encoding.UTF8;
            message.Body = sendMail.Body;
            message.IsBodyHtml = sendMail.IsHtml;
            message.Priority = MailPriority.High;
            var smpt = new SmtpClient
            {
                Host = _appSetting.NotificationCredentials.Host,
                Port = _appSetting.NotificationCredentials.Port,
                EnableSsl = true,
                Credentials = new System.Net.NetworkCredential(_appSetting.NotificationCredentials.UserName, EncryptDecrypt.Decrypt(_appSetting.NotificationCredentials.Password))
            };

            smpt.SendMailAsync(message);
            return Task.CompletedTask;
        }

        private static void GetMailingName(string[] emails, MailAddressCollection mailAddresses)
        {
            foreach (string multiemailid in emails)
            {
                mailAddresses.Add(new MailAddress(multiemailid));
            }
        }
    }
}
