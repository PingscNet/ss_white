﻿namespace SSWhite.Data.Interface.Document.Payment
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Document.Payment;
    using SSWhite.Domain.Request.Document.Payment;
    using SSWhite.Domain.Response.Document.Payment;

    public interface IPaymentRepository
    {
        Task<ServiceSearchResponse<GetAllPaymentsResponse>> GetAllPayments(ServiceSearchRequest<GetAllPayments> request);

        Task<int> AddEditPayment(ServiceRequest<Payment> request);

        Task ChangePaymentStatus(ServiceRequest<int> request);

        Task<GetPaymentByIdResponse> GetPaymentById(ServiceRequest<int> request);

        Task DeletePayment(ServiceRequest<int> request);
    }
}
