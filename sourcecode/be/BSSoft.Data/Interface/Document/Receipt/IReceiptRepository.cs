﻿namespace SSWhite.Data.Interface.Document.Receipt
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Document.Receipt;
    using SSWhite.Domain.Request.Document.Receipt;
    using SSWhite.Domain.Response.Document.Receipt;

    public interface IReceiptRepository
    {
        Task<ServiceSearchResponse<GetAllReceiptsResponse>> GetAllReceipts(ServiceSearchRequest<GetAllReceipts> request);

        Task<int> AddEditReceipt(ServiceRequest<Receipt> request);

        Task ChangeReceiptStatus(ServiceRequest<int> request);

        Task<GetReceiptByIdResponse> GetReceiptById(ServiceRequest<int> request);

        Task DeleteReceipt(ServiceRequest<int> request);
    }
}
