﻿namespace SSWhite.Data.Interface.Document.Sales
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Document.Sales;
    using SSWhite.Domain.Request.Document.Sales;
    using SSWhite.Domain.Response.Document.Sales;

    public interface ISalesRepository
    {
        Task<ServiceSearchResponse<GetAllSalesDocumentsResponse>> GetAllSalesDocuments(ServiceSearchRequest<GetAllSalesDocuments> request);

        Task<List<InsertOrUpdateSalesDocumentResponse>> AddEditSalesDocument(ServiceRequest<SalesDocument> request);

        Task ChangeSalesDocumentStatus(ServiceRequest<int> request);

        Task<GetSalesDocumentByIdResponse> GetSalesDocumentById(ServiceRequest<int> request);

        Task DeleteSalesDocument(ServiceRequest<int> request);

        Task<ServiceSearchResponse<GetAllSalesDocumentsResponse>> GetAllPendingRateSalesDocuments(ServiceSearchRequest<GetAllSalesDocuments> request);
    }
}
