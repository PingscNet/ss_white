﻿namespace SSWhite.Data.Interface.Document.Purchase
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Document.Purchase;
    using SSWhite.Domain.Request.Document.Purchase;
    using SSWhite.Domain.Response.Document.Purchase;

    public interface IPurchaseRepository
    {
        Task<ServiceSearchResponse<GetAllPurchaseDocumentsResponse>> GetAllPurchaseDocuments(ServiceSearchRequest<GetAllPurchaseDocuments> request);

        Task<List<InsertOrUpdatePurchaseDocumentResponse>> AddEditPurchaseDocument(ServiceRequest<PurchaseDocument> request);

        Task ChangePurchaseDocumentStatus(ServiceRequest<int> request);

        Task<GetPurchaseDocumentByIdResponse> GetPurchaseDocumentById(ServiceRequest<int> request);

        Task<int> DeletePurchaseDocument(ServiceRequest<int> request);

        Task<ServiceSearchResponse<GetAllPurchaseDocumentsResponse>> GetAllPendingRatePurchaseDocuments(ServiceSearchRequest<GetAllPurchaseDocuments> request);
    }
}
