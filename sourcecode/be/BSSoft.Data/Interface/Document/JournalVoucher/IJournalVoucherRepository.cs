﻿namespace SSWhite.Data.Interface.Document.JournalVoucher
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Document.JournalVoucher;
    using SSWhite.Domain.Request.Document.JournalVoucher;
    using SSWhite.Domain.Response.Document.JournalVoucher;

    public interface IJournalVoucherRepository
    {
        Task<ServiceSearchResponse<GetAllJournalVouchersResponse>> GetAllJournalVouchers(ServiceSearchRequest<GetAllJournalVouchers> request);

        Task<int> AddEditJournalVoucher(ServiceRequest<JournalVoucher> request);

        Task ChangeJournalVoucherStatus(ServiceRequest<int> request);

        Task<GetJournalVoucherByIdResponse> GetJournalVoucherById(ServiceRequest<int> request);

        Task DeleteJournalVoucher(ServiceRequest<int> request);
    }
}
