﻿namespace SSWhite.Data.Interface.Account
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.Domain.Response.Dashboard;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;

    public interface IDashboardRepository
    {
        Task<List<GetSSWhiteTeamDetailsResponse>> GetSSWhiteTeamDetails();

        Task<UserDetail> GetUserDetailsById(int id);

        Task<UserNotificationResponse> UserNotification(ServiceRequest request);

        Task<List<GetNotificationsResponse>> GetNotifications(ServiceRequest request);

        Task<GetModulesCountsResponse> GetModulesCounts(ServiceRequest request);

        Task<bool> UpdateWorkingStatus(ServiceRequest<UpdateWorkingStatusRequest> request);

        Task<int> ChangePassword(ServiceRequest<ChangePasswordRequest> request);
    }
}
