﻿namespace SSWhite.Data.Interface.Report.Ledger
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Report.LedgerReport;
    using SSWhite.Domain.Response.Report.Ledger;

    public interface ILedgerReportRepository
    {
        Task<ServiceResponse<GetLedgerReportResponse>> GetLedgerReport(ServiceRequest<GetLedgerReport> request);
    }
}
