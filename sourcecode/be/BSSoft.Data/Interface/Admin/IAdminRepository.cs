﻿namespace SSWhite.Data.Interface.Admin
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;

    public interface IAdminRepository
    {
        Task<List<GetDocumentApprovalListResponse>> GetDocumentApprovals();

        Task<ServiceSearchResponse<GetAllEmployeeLimitsResponse>> GetAllEmployeeLimits(GetAllEmployeeLimitsRequest request);

        Task<List<GetEmployeeByEmployeeIdResponse>> GetEmployeeByEmployeeId(GetEmployeeByEmployeeIdRequest request);

        Task UpdateEmployeeLimit(UpdateEmployeeLimitByIdRequest updateEmployeeLimitByIdRequest);

        Task ChangeEmployeeStatus(Task<ServiceRequest<ChangeEmployeeStatusRequest>> request);

        Task<AddNewEmployeeResponse> AddNewEmployee(ServiceRequest<AddNewEmployeeRequest> request);

        Task<CheckIfEmployeeIdExistsResponse> CheckIfEmployeeIdExists(CheckIfEmployeeIdExistsRequest checkIfEmployeeIdExistsRequest);

        Task<CheckIfEmailAddressExistsResponse> CheckIfEmailAddressExists(CheckIfEmailAddressExistsRequest request);

        Task UpdateDocumentApprovalList(List<UpdateDocumentApprovalListRequest> request);

        Task<List<GetUserRightsResponse>> GetUserRights(ServiceRequest request);

        Task<List<GetRolesResponse>> GetRoles();

        Task<List<GetRightsAccessByRoleIdResponse>> GetRightsAccessByRoleId(GetRightsAccessByRoleIdRequest request);

        Task UpdateRightsAccessByRoleId(UpdateRightsAccessByRoleIdRequest request);
    }
}
