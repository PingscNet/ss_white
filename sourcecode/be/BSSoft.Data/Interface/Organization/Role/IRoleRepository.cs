﻿namespace SSWhite.Data.Interface.Organization.Role
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Organization.Role;
    using SSWhite.Domain.Request.Organization.Role;
    using SSWhite.Domain.Response.Organization.Role;

    public interface IRoleRepository
    {
        Task<ServiceSearchResponse<GetAllRolesResponse>> GetAllRoles(ServiceSearchRequest<GetAllRoles> request);

        Task<int> AddEditRole(ServiceRequest<Role> request);

        Task ChangeRoleStatus(ServiceRequest<int> request);

        Task<GetRoleByIdResponse> GetRoleById(ServiceRequest<int> request);

        Task DeleteRole(ServiceRequest<int> request);
    }
}
