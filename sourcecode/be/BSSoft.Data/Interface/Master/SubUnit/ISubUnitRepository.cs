﻿namespace SSWhite.Data.Interface.Master.SubUnit
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.SubUnit;
    using SSWhite.Domain.Request.Master.SubUnit;
    using SSWhite.Domain.Response.Master.SubUnit;

    public interface ISubUnitRepository
    {
        Task<ServiceSearchResponse<GetAllSubUnitsResponse>> GetAllSubUnits(ServiceSearchRequest<GetAllSubUnits> request);

        Task<int> AddEditSubUnit(ServiceRequest<SubUnit> request);

        Task ChangeSubUnitStatus(ServiceRequest<int> request);

        Task<GetSubUnitByIdResponse> GetSubUnitById(ServiceRequest<int> request);

        Task DeleteSubUnit(ServiceRequest<int> request);
    }
}
