﻿namespace SSWhite.Data.Interface.Master.LedgerGroup
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.LedgerGroup;
    using SSWhite.Domain.Request.Master.LedgerGroup;
    using SSWhite.Domain.Response.Master.LedgerGroup;

    public interface ILedgerGroupRepository
    {
        Task<ServiceSearchResponse<GetAllLedgerGroupsResponse>> GetAllLedgerGroups(ServiceSearchRequest<GetAllLedgerGroups> request);

        Task<int> AddEditLedgerGroup(ServiceRequest<LedgerGroup> request);

        Task ChangeLedgerGroupStatus(ServiceRequest<int> request);

        Task<GetLedgerGroupByIdResponse> GetLedgerGroupById(ServiceRequest<int> request);

        Task DeleteLedgerGroup(ServiceRequest<int> request);
    }
}
