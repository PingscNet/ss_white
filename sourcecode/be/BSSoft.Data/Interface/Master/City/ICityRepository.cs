﻿namespace SSWhite.Data.Interface.Master.City
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.City;
    using SSWhite.Domain.Request.Master.City;
    using SSWhite.Domain.Response.Master.City;

    public interface ICityRepository
    {
        Task<ServiceSearchResponse<GetAllCitiesResponse>> GetAllCities(ServiceSearchRequest<GetAllCities> request);

        Task<int> AddEditCity(ServiceRequest<City> request);

        Task ChangeCityStatus(ServiceRequest<int> request);

        Task<GetCityByIdResponse> GetCityById(ServiceRequest<int> request);

        Task DeleteCity(ServiceRequest<int> request);
    }
}
