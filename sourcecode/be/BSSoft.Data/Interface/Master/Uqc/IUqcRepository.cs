﻿namespace SSWhite.Data.Interface.Master.Uqc
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Uqc;
    using SSWhite.Domain.Request.Master.Uqc;
    using SSWhite.Domain.Response.Master.Uqc;

    public interface IUqcRepository
    {
        Task<ServiceSearchResponse<GetAllUqcsResponse>> GetAllUqcs(ServiceSearchRequest<GetAllUqcs> request);

        Task<int> AddEditUqc(ServiceRequest<Uqc> request);

        Task ChangeUqcStatus(ServiceRequest<int> request);

        Task<GetUqcByIdResponse> GetUqcById(ServiceRequest<int> request);

        Task DeleteUqc(ServiceRequest<int> request);
    }
}
