﻿namespace SSWhite.Data.Interface.Master.Tax
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Tax;
    using SSWhite.Domain.Request.Master.Tax;
    using SSWhite.Domain.Response.Master.Tax;

    public interface ITaxRepository
    {
        Task<ServiceSearchResponse<GetAllTaxesResponse>> GetAllTaxes(ServiceSearchRequest<GetAllTaxes> request);

        Task<int> AddEditTax(ServiceRequest<Tax> request);

        Task ChangeTaxStatus(ServiceRequest<int> request);

        Task<GetTaxByIdResponse> GetTaxById(ServiceRequest<int> request);

        Task DeleteTax(ServiceRequest<int> request);
    }
}
