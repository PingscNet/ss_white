﻿namespace SSWhite.Data.Interface.Master.Item
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Item;
    using SSWhite.Domain.Request.Master.Item;
    using SSWhite.Domain.Response.Master.Item;

    public interface IItemRepository
    {
        Task<ServiceSearchResponse<GetAllItemsResponse>> GetAllItems(ServiceSearchRequest<GetAllItems> request);

        Task<int> AddEditItem(ServiceRequest<Item> request);

        Task ChangeItemStatus(ServiceRequest<int> request);

        Task<GetItemByIdResponse> GetItemById(ServiceRequest<int> request);

        Task DeleteItem(ServiceRequest<int> request);
    }
}
