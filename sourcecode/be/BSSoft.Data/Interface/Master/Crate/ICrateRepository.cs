﻿namespace SSWhite.Data.Interface.Master.Crate
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Crate;
    using SSWhite.Domain.Request.Master.Crate;
    using SSWhite.Domain.Response.Master.Crate;

    public interface ICrateRepository
    {
        Task<ServiceSearchResponse<GetAllCratesResponse>> GetAllCrates(ServiceSearchRequest<GetAllCrates> request);

        Task<int> AddEditCrate(ServiceRequest<Crate> request);

        Task ChangeCrateStatus(ServiceRequest<int> request);

        Task<GetCrateByIdResponse> GetCrateById(ServiceRequest<int> request);

        Task DeleteCrate(ServiceRequest<int> request);
    }
}
