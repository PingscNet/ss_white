﻿namespace SSWhite.Data.Interface.Master.ItemGroup
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemGroup;
    using SSWhite.Domain.Request.Master.ItemGroup;
    using SSWhite.Domain.Response.Master.ItemGroup;

    public interface IItemGroupRepository
    {
        Task<ServiceSearchResponse<GetAllItemGroupsResponse>> GetAllItemGroups(ServiceSearchRequest<GetAllItemGroups> request);

        Task<int> AddEditItemGroup(ServiceRequest<ItemGroup> request);

        Task ChangeItemGroupStatus(ServiceRequest<int> request);

        Task<GetItemGroupByIdResponse> GetItemGroupById(ServiceRequest<int> request);

        Task DeleteItemGroup(ServiceRequest<int> request);
    }
}
