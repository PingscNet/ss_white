﻿namespace SSWhite.Data.Interface.Master.Header
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Header;
    using SSWhite.Domain.Request.Master.Header;
    using SSWhite.Domain.Response.Master.Header;

    public interface IHeaderRepository
    {
        Task<ServiceSearchResponse<GetAllHeadersResponse>> GetAllHeaders(ServiceSearchRequest<GetAllHeaders> request);

        Task<int> AddEditHeader(ServiceRequest<Header> request);

        Task ChangeHeaderStatus(ServiceRequest<int> request);

        Task<GetHeaderByIdResponse> GetHeaderById(ServiceRequest<int> request);

        Task DeleteHeader(ServiceRequest<int> request);
    }
}
