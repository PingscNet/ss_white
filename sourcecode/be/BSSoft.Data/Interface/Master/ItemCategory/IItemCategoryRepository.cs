﻿namespace SSWhite.Data.Interface.Master.ItemCategory
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemCategory;
    using SSWhite.Domain.Request.Master.ItemCategory;
    using SSWhite.Domain.Response.Master.ItemCategory;

    public interface IItemCategoryRepository
    {
        Task<ServiceSearchResponse<GetAllItemCategoriesResponse>> GetAllItemCategories(ServiceSearchRequest<GetAllItemCategories> request);

        Task<int> AddEditItemCategory(ServiceRequest<ItemCategory> request);

        Task ChangeItemCategoryStatus(ServiceRequest<int> request);

        Task<GetItemCategoryByIdResponse> GetItemCategoryById(ServiceRequest<int> request);

        Task DeleteItemCategory(ServiceRequest<int> request);
    }
}
