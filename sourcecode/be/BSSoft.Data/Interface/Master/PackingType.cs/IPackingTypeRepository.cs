﻿namespace SSWhite.Data.Interface.Master.PackingType
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.PackingType;
    using SSWhite.Domain.Request.Master.PackingType;
    using SSWhite.Domain.Response.Master.PackingType;

    public interface IPackingTypeRepository
    {
        Task<ServiceSearchResponse<GetAllPackingTypesResponse>> GetAllPackingTypes(ServiceSearchRequest<GetAllPackingTypes> request);

        Task<int> AddEditPackingType(ServiceRequest<PackingType> request);

        Task ChangePackingTypeStatus(ServiceRequest<int> request);

        Task<GetPackingTypeByIdResponse> GetPackingTypeById(ServiceRequest<int> request);

        Task DeletePackingType(ServiceRequest<int> request);
    }
}
