﻿namespace SSWhite.Data.Interface.Master.Company
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Company;
    using SSWhite.Domain.Request.Master.Company;
    using SSWhite.Domain.Response.Master.Company;

    public interface ICompanyRepository
    {
        Task<ServiceSearchResponse<GetAllCompaniesResponse>> GetAllCompanies(ServiceSearchRequest<GetAllCompanies> request);

        Task<int> AddEditCompany(ServiceRequest<Company> request);

        Task ChangeCompanyStatus(ServiceRequest<int> request);

        Task<GetCompanyByIdResponse> GetCompanyById(ServiceRequest<int> request);

        Task DeleteCompany(ServiceRequest<int> request);
    }
}
