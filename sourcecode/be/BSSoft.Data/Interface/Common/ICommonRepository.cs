﻿namespace SSWhite.Data.Interface.Common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Domain.Common.Document;
    using SSWhite.Domain.Common.Master.Client;
    using SSWhite.Domain.Common.Organization.Role;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.Common;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.Common;

    public interface ICommonRepository
    {
        Task<List<GetOrganizationResponse>> GetOrganization();

        Task<List<GetEmployeeJobDescriptionResponse>> GetEmployeeJobDescription();

        Task<List<GetReportingUnderResponse>> GetReportingUnder();

        Task<List<GetDepartmentByDepartmentSupervisorNameResponse>> GetDepartmentByDepartmentSupervisorName(GetDepartmentByDepartmentSupervisorNameRequest getDepartmentByDepartmentSupervisorNameRequest);
    }
}
