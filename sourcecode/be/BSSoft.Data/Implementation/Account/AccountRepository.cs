﻿namespace SSWhite.Data.Implementation.Account
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Response.Account;

    public class AccountRepository : IAccountRepository
    {
        private readonly IBaseRepository _baseRepository;
        private readonly IPAddress _ipAddress;

        public AccountRepository(IBaseRepository baseRepository, IOptions<IPAddress> ipAddress)
        {
            _baseRepository = baseRepository;
            _ipAddress = ipAddress.Value;
        }

        public async Task<LogInResponse> ValidateUserForLogIn(ServiceRequest<LogInRequest> request)
        {
            var paramters = new DynamicParameters();
            paramters.Add("UserName", request.Data.UserName, DbType.String);
            paramters.Add("Password", request.Data.Password.GenerateSHA256Hash(), DbType.String);
            ////paramters.Add("IpAddress", _ipAddress.RequestIpAddress, DbType.String);
            paramters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            ////paramters.Add("UserTypeAdmin", UserType.Admin, DbType.Int16);

            var response = new LogInResponse();
            var entities = new List<UserEntity>();
            var rights = new List<UserRight>();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.MASTER_VALIDATE_USER_FOR_LOGIN, paramters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstOrDefaultAsync<LogInResponse>();
                ////entities = (await multi.ReadAsync<UserEntity>()).ToList();
                ////rights = (await multi.ReadAsync<UserRight>()).ToList();
            }

            if (response != null)
            {
                response.Entities = entities;
                response.Rights = rights;
            }

            return response;
        }

        public async Task<string> SendForgetPasswordEmail(SendForgetPasswordEmailRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserLogin", request.UserLogin, DbType.String);
            parameters.Add("StatusTypeActive", value: StatusType.Active, DbType.Int32);

            return await _baseRepository.QueryFirstOrDefaultAsync<string>(StoredProcedures.EMP_SEND_FORGET_PASSWORD_EMAIL, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<string> IsEmailValid(IsEmailValidRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("EmailAddress", request.EmailAddress, DbType.String);
            parameters.Add("StatusTypeActive", value: StatusType.Active, DbType.Int32);

            return await _baseRepository.QueryFirstOrDefaultAsync<string>(StoredProcedures.EMP_IS_EMAIL_VALID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<string> SetNewPassword(SetNewPasswordRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("EmailAddress", request.EmailAddress, DbType.String);
            parameters.Add("Password", Generator.EncryptPassword(request.Password), DbType.String);
            parameters.Add("StatusTypeActive", value: StatusType.Active, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);

            return await _baseRepository.QueryFirstOrDefaultAsync<string>(StoredProcedures.EMP_SET_NEW_PASSWORD, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
