﻿namespace SSWhite.Data.Implementation.Master.Client
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Master.Client;
    using SSWhite.Domain.Common.Master.Client;
    using SSWhite.Domain.Common.Master.Common;
    using SSWhite.Domain.Request.Master.Client;
    using SSWhite.Domain.Response.Master.Client;

    public class ClientRepository : IClientRepository
    {
        private readonly IBaseRepository _baseRepository;

        public ClientRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddEditClient(ServiceRequest<Client> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("FinancialYear", CommonMethods.ToFinancialYear(DateTime.Now), DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("CompanyName", request.Data.CompanyName, DbType.String);
            parameters.Add("Code", $"{request.Data.Prefix}{request.Data.Code}{request.Data.Suffix}", DbType.String);
            parameters.Add("Gstin", request.Data.Gstin, DbType.String);
            parameters.Add("Pan", request.Data.Pan, DbType.String);
            parameters.Add("Address", request.Data.Address, DbType.String);
            parameters.Add("CityId", request.Data.CityId, DbType.Int32);
            parameters.Add("StateId", request.Data.StateId, DbType.Int32);
            parameters.Add("CountryId", request.Data.CountryId, DbType.Int32);
            parameters.Add("EmailAddress", request.Data.EmailAddress, DbType.String);
            parameters.Add("MobileNumber", request.Data.MobileNumber, DbType.String);
            parameters.Add("ContactNumber", request.Data.ContactNumber, DbType.String);
            parameters.Add("Website", request.Data.Website, DbType.String);
            parameters.Add("CreditLimit", request.Data.CreditLimit, DbType.Decimal);
            parameters.Add("OpeningBalance", request.Data.OpeningBalance, DbType.Decimal);
            parameters.Add("TransactionType", request.Data.TransactionType, DbType.Int16);
            parameters.Add("LedgerGroupId", request.Data.LedgerGroupId, DbType.Int32);
            parameters.Add("RegionId", request.Data.RegionId, DbType.Int32);
            parameters.Add("ActualLastNumber", request.Data.ActualLastNumber, DbType.Int32);

            if (request.Data.ClientHeaderDetails?.Any() == true)
            {
                var headerDetails = (from item in request.Data.ClientHeaderDetails
                                     select new
                                     {
                                         item.HeaderId,
                                         item.ModuleId,
                                         item.Value
                                     }).ToList();
                parameters.Add("ClientHeaderDetails", headerDetails.ToDataTable(), DbType.Object);
            }

            if (request.Data.BankDetails?.Any() == true)
            {
                parameters.Add("BankDetails", request.Data.BankDetails.ToDataTable(), DbType.Object);
            }

            if (request.Data.ContactDetails?.Any() == true)
            {
                parameters.Add("ContactDetails", request.Data.ContactDetails.ToDataTable(), DbType.Object);
            }

            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.Date);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ModuleTypeClient", ModuleType.Client, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);
            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_INSERT_OR_UPDATE_CLIENT, parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("ReturnValue");
        }

        public async Task ChangeClientStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_CHANGE_CLIENT_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteClient(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("StatusTypeDeleted", StatusType.Deleted, DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_DELETE_CLIENT, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllClientsResponse>> GetAllClients(ServiceSearchRequest<GetAllClients> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("CountryId", request.Data.CountryId, DbType.Int32);
            parameters.Add("StateId", request.Data.StateId, DbType.Int32);
            parameters.Add("CityId", request.Data.CityId, DbType.Int32);
            parameters.Add("LedgerGroupId", request.Data.LedgerGroupId, DbType.Int32);
            parameters.Add("RegionId", request.Data.RegionId, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllClientsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllClientsResponse>(StoredProcedures.MASTER_GET_ALL_CLIENTS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetClientByIdResponse> GetClientById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);
            var response = new GetClientByIdResponse();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.MASTER_GET_CLIENT_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstOrDefaultAsync<GetClientByIdResponse>();
                response.BankDetails = (await multi.ReadAsync<BankDetail>()).ToList();
                response.ContactDetails = (await multi.ReadAsync<ContactDetail>()).ToList();
                response.ClientHeaderDetails = (await multi.ReadAsync<ClientHeaderDetail>()).ToList();
            }

            response.BankDetails = response.BankDetails.Any() ? response.BankDetails : null;
            response.ContactDetails = response.ContactDetails.Any() ? response.ContactDetails : null;
            response.ClientHeaderDetails = response.ClientHeaderDetails.Any() ? response.ClientHeaderDetails : null;
            return response;
        }

        public async Task<List<ClientOpeningBalance>> GetClientOpeningBalanceDetails(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@ClientId", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);

            return (await _baseRepository.QueryAsync<ClientOpeningBalance>(StoredProcedures.MASTER_GET_CLIENT_OPENING_BALANCE_DETAILS, parameters, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<int> AddEditOpeningBalnce(ServiceRequest<ClientOpeningBalance> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("FinancialYear", request.Data.FinancialYear, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("Balance", request.Data.Balance, DbType.Decimal);
            parameters.Add("TransactionType", request.Data.TransactionType, DbType.Int16);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.Date);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);
            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_INSERT_OR_UPDATE_OPENING_BALANCE, parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("ReturnValue");
        }
    }
}
