﻿namespace SSWhite.Data.Implementation.Master.ItemRate
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Master.ItemRate;
    using SSWhite.Domain.Common.Master.ItemRate;
    using SSWhite.Domain.Request.Master.ItemRate;
    using SSWhite.Domain.Response.Master.ItemRate;

    public class ItemRateRepository : IItemRateRepository
    {
        private readonly IBaseRepository _baseRepository;

        public ItemRateRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddEditItemRate(ServiceRequest<ItemRate> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ItemGroupId", request.Data.ItemGroupId, DbType.Int32);
            parameters.Add("ItemId", request.Data.ItemId, DbType.Int32);
            parameters.Add("SiteId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("Rate", request.Data.Rate, DbType.Decimal);
            parameters.Add("FromDate", request.Data.FromDate ?? DateTime.Now, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);

            object p = await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_INSERT_OR_UPDATE_ITEM_RATE, parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("ReturnValue");
        }

        public async Task ChangeItemRateStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_CHANGE_ITEM_RATE_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteItemRate(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("StatusTypeDeleted", StatusType.Deleted, DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_DELETE_ITEM_RATE, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllItemRatesResponse>> GetAllItemRates(ServiceSearchRequest<GetAllItemRates> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("ItemId", request.Data.ItemId, DbType.Int32);
            parameters.Add("ItemGroupId", request.Data.ItemGroupId, DbType.Int32);
            parameters.Add("SiteId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllItemRatesResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllItemRatesResponse>(StoredProcedures.MASTER_GET_ALL_ITEM_RATES, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetItemRateByIdResponse> GetItemRateById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);

            return await _baseRepository.QueryFirstAsync<GetItemRateByIdResponse>(StoredProcedures.MASTER_GET_ITEM_RATE_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
