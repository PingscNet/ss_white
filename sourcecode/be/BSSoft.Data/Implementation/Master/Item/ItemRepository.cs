﻿namespace SSWhite.Data.Implementation.Master.Item
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Master.Item;
    using SSWhite.Domain.Common.Master.Item;
    using SSWhite.Domain.Request.Master.Item;
    using SSWhite.Domain.Response.Master.Item;

    public class ItemRepository : IItemRepository
    {
        private readonly IBaseRepository _baseRepository;

        public ItemRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddEditItem(ServiceRequest<Item> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("Code", request.Data.Code, DbType.String);
            parameters.Add("HsnOrSac", request.Data.HsnOrSac, DbType.String);
            parameters.Add("ItemGroupId", request.Data.ItemGroupId, DbType.Int32);
            parameters.Add("ItemSubGroupId", request.Data.ItemSubGroupId, DbType.Int32);
            parameters.Add("ItemCategoryId", request.Data.ItemCategoryId, DbType.Int32);
            parameters.Add("ItemSubCategoryId", request.Data.ItemSubCategoryId, DbType.Int32);
            parameters.Add("Remarks", request.Data.Remarks, DbType.String);
            parameters.Add("Prefix", request.Data.Prefix, DbType.String);
            parameters.Add("Suffix", request.Data.Suffix, DbType.String);

            if (request.Data.ItemUnits?.Any() == true)
            {
                var units = (from unit in request.Data.ItemUnits
                             select new
                             {
                                 unit.UnitId,
                                 unit.Labour
                             }).ToList();

                parameters.Add("ItemUnits", units.ToDataTable(), DbType.Object);
            }

            if (request.Data.ItemCrates?.Any() == true)
            {
                var crates = (from crate in request.Data.ItemCrates
                              select new
                              {
                                  crate.CrateId,
                                  crate.Deposit
                              }).ToList();

                parameters.Add("ItemCrates", crates.ToDataTable(), DbType.Object);
            }

            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ModuleTypeItem", ModuleType.Item, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_INSERT_OR_UPDATE_ITEM, parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("ReturnValue");
        }

        public async Task ChangeItemStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_CHANGE_ITEM_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteItem(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("StatusTypeDeleted", StatusType.Deleted, DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_DELETE_ITEM, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllItemsResponse>> GetAllItems(ServiceSearchRequest<GetAllItems> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("ItemGroupId", request.Data.ItemGroupId, DbType.Int32);
            parameters.Add("ItemCategoryId", request.Data.ItemCategoryId, DbType.Int32);
            parameters.Add("UnitId", request.Data.UnitId, DbType.Int32);
            parameters.Add("CrateId", request.Data.CrateId, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllItemsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllItemsResponse>(StoredProcedures.MASTER_GET_ALL_ITEMS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetItemByIdResponse> GetItemById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);

            var response = new GetItemByIdResponse();
            var itemUnits = new List<ItemUnit>();
            var itemCrates = new List<ItemCrate>();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.MASTER_GET_ITEM_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstAsync<GetItemByIdResponse>();
                itemUnits = (await multi.ReadAsync<ItemUnit>()).ToList();
                itemCrates = (await multi.ReadAsync<ItemCrate>()).ToList();
            }

            if (itemUnits?.Any() == true)
            {
                response.ItemUnits = itemUnits;
            }

            if (itemCrates?.Any() == true)
            {
                response.ItemCrates = itemCrates;
            }

            return response;
        }
    }
}
