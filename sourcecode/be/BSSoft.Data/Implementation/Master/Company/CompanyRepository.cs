﻿namespace SSWhite.Data.Implementation.Master.Company
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Master.Company;
    using SSWhite.Domain.Common.Master.Common;
    using SSWhite.Domain.Common.Master.Company;
    using SSWhite.Domain.Request.Master.Company;
    using SSWhite.Domain.Response.Master.Company;

    public class CompanyRepository : ICompanyRepository
    {
        private readonly IBaseRepository _baseRepository;

        public CompanyRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddEditCompany(ServiceRequest<Company> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("Gstin", request.Data.Gstin, DbType.String);
            parameters.Add("Pan", request.Data.Pan, DbType.String);
            parameters.Add("Address", request.Data.Address, DbType.String);
            parameters.Add("CountryId", request.Data.CountryId, DbType.Int32);
            parameters.Add("StateId", request.Data.StateId, DbType.Int32);
            parameters.Add("CityId", request.Data.CityId, DbType.Int32);
            parameters.Add("Pincode", request.Data.Pincode, DbType.String);
            parameters.Add("EmailAddress", request.Data.EmailAddress, DbType.String);
            parameters.Add("ContactPerson", request.Data.ContactPerson, DbType.String);
            parameters.Add("ContactNumber", request.Data.ContactNumber, DbType.String);
            parameters.Add("Website", request.Data.Website, DbType.String);
            parameters.Add("OtherLicense", request.Data.OtherLicense, DbType.String);
            parameters.Add("TagLine", request.Data.TagLine, DbType.String);
            parameters.Add("TermsAndConditions", request.Data.TermsAndConditions, DbType.String);
            parameters.Add("LogoPath", request.Data.LogoPath, DbType.String);
            parameters.Add("BankDetails", request.Data.BankDetails.ToDataTable(), DbType.Object);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.Date);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);
            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_INSERT_OR_UPDATE_COMPANY, parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("ReturnValue");
        }

        public async Task ChangeCompanyStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_CHANGE_COMPANY_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteCompany(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("StatusTypeDeleted", StatusType.Deleted, DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_DELETE_COMPANY, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllCompaniesResponse>> GetAllCompanies(ServiceSearchRequest<GetAllCompanies> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllCompaniesResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllCompaniesResponse>(StoredProcedures.MASTER_GET_ALL_COMPANIES, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetCompanyByIdResponse> GetCompanyById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);

            var response = new GetCompanyByIdResponse();
            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.MASTER_GET_COMPANY_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstAsync<GetCompanyByIdResponse>();
                response.BankDetails = (await multi.ReadAsync<BankDetail>()).ToList();
            }

            return response;
        }
    }
}
