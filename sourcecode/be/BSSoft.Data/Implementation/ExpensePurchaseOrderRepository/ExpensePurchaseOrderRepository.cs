﻿namespace SSWhite.Data.Implementation.ExpensePurchaseOrder
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.ExpensePurchaseOrder;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;

    public class ExpensePurchaseOrderRepository : IExpensePurchaseOrderRepository
    {
        private readonly IBaseRepository _baseRepository;

        public ExpensePurchaseOrderRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<GetAllEPODropDownsResponse> GetAllEPODropDowns()
        {
            var getAllEPODropDownsResponse = new GetAllEPODropDownsResponse();
            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.EMP_GET_ALL_EPO_DROPDOWNS, commandType: CommandType.StoredProcedure))
            {
                getAllEPODropDownsResponse.EmployeeDropDown = (await multi.ReadAsync<EmployeeDropDown>()).ToList();
                getAllEPODropDownsResponse.CountryDropDown = (await multi.ReadAsync<CountryDropDown>()).ToList();
                getAllEPODropDownsResponse.EpoTypeDropDown = (await multi.ReadAsync<EpoTypeDropDown>()).ToList();
                getAllEPODropDownsResponse.EpoDeptDropDown = (await multi.ReadAsync<EPORefDropDown>()).ToList();
                getAllEPODropDownsResponse.EpoProductDropDown = (await multi.ReadAsync<EPORefDropDown>()).ToList();
                getAllEPODropDownsResponse.EpoAccountNoDropDown = (await multi.ReadAsync<EPORefDropDown>()).ToList();
                getAllEPODropDownsResponse.UomDropDown = (await multi.ReadAsync<UomDropDown>()).ToList();
                getAllEPODropDownsResponse.TaxDropDown = (await multi.ReadAsync<TaxDropDown>()).ToList();
            }

            return getAllEPODropDownsResponse;
        }

        public async Task<GetAuthorizedLimitByUserIdResponse> GetAuthorizedLimitByUserId(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int16);

            var result = await _baseRepository.QueryFirstAsync<GetAuthorizedLimitByUserIdResponse>(StoredProcedures.EMP_GET_AUTHORIZED_LIMIT_BY_USER_ID, parameters, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<AddOrEditResponse> AddOrEditEpo(ServiceRequest<AddOrEditEpoRequest> request)
        {
            var epoLineItemsList = (from item in request.Data.EpoLineItemsList
                                    select new
                                    {
                                        item.Id,
                                        item.LineNo,
                                        item.ProductDescription,
                                        item.ProductGroup,
                                        item.Qty,
                                        item.UnitPrice,
                                        item.Uom,
                                        item.UomOther,
                                        item.Department,
                                        item.AccountNo,
                                        item.Gujarat,
                                        item.Tax,
                                        item.Discount,
                                        item.UnitPriceQty,
                                        item.QtyUnitPriceTax,
                                        item.TotalCostDiscount,
                                        item.Gst,
                                        item.Cgst,
                                        item.Sgst,
                                        item.Igst,
                                        item.ExtendedCost,
                                        item.Purchase,
                                        item.DueDate,
                                        item.CgstValue,
                                        item.SgstValue,
                                        item.TaxValue,
                                        item.IgstValue,
                                        item.CurrencyType,
                                    }).ToList();

            var epoAttachmentMst = (from item in request.Data.EpoAttachmentMst
                                    select new
                                    {
                                        item.AttachedFileName,
                                        item.Approval
                                    }).ToList();

            if (request.Data.EpoAttachmentDeleteIds == null)
            {
                request.Data.EpoAttachmentDeleteIds = new List<int>();
            }

            var epoAttachmentDeleteIds = (from item in request.Data.EpoAttachmentDeleteIds
                                          select new
                                          {
                                              item
                                          }).ToList();

            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("EmployeeVendor", request.Data.IsEmployee == true ? request.Data.EmployeeVendor : null, DbType.String);
            parameters.Add("EmployeeId", request.Data.IsEmployee == true ? request.Data.EmployeeId : null, DbType.String);
            parameters.Add("Vendor", request.Data.IsEmployee == false ? request.Data.Vendor : null, DbType.String);
            parameters.Add("VendorId", request.Data.IsEmployee == false ? request.Data.VendorId : null, DbType.String);
            parameters.Add("Street1", request.Data.Street1, DbType.String);
            parameters.Add("Street2", request.Data.Street2, DbType.String);
            parameters.Add("Country", request.Data.Country, DbType.Int16);
            parameters.Add("State", request.Data.State, DbType.String);
            parameters.Add("City", request.Data.City, DbType.String);
            parameters.Add("Zip", request.Data.Zip, DbType.String);
            parameters.Add("Phone", request.Data.Phone, DbType.String);
            parameters.Add("Fax", request.Data.Fax, DbType.String);
            parameters.Add("GstNo", request.Data.GstNo, DbType.String);
            parameters.Add("Email", request.Data.Email, DbType.String);
            parameters.Add("PurposeOfPurchase", request.Data.PurposeOfPurchase, DbType.String);
            parameters.Add("EpoType", request.Data.EpoType, DbType.Int16);
            parameters.Add("ReceiptMethod", request.Data.ReceiptMethod, DbType.Int16);
            parameters.Add("EpoPurchaseStatus", request.Data.EpoPurchaseStatus, DbType.String);
            parameters.Add("PaymentType", request.Data.PaymentType, DbType.Int16);
            parameters.Add("PurchaseNotes", request.Data.PurchaseNotes, DbType.String);
            parameters.Add("InternalNotes", request.Data.InternalNotes, DbType.String);
            parameters.Add("SubTotal", request.Data.SubTotal, DbType.Decimal);
            parameters.Add("GrandTotal", request.Data.GrandTotal, DbType.Decimal);
            parameters.Add("DateOfPaymentCheckRequestOrCashAdvance", request.Data.DateOfPaymentCheckRequestOrCashAdvance, DbType.DateTime);
            parameters.Add("DaysOfPaymentPayWhenBilled", request.Data.DaysOfPaymentPayWhenBilled, DbType.String);
            parameters.Add("CurrencyType", request.Data.CurrencyType, DbType.Int32);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("IsAttachmentPending", request.Data.IsAttachmentPending, DbType.Boolean);
            parameters.Add("Currentprice", request.Data.GrandTotal, DbType.Decimal);
            parameters.Add("DollarPrice", request.Data.DollarPrice, DbType.Decimal);
            parameters.Add("TotalSgst", request.Data.TotalSgst, DbType.Decimal);
            parameters.Add("TotalCgst", request.Data.TotalCgst, DbType.Decimal);
            parameters.Add("TotalIgst", request.Data.TotalIgst, DbType.Decimal);
            parameters.Add("IsWitinGujarat", request.Data.IsWitinGujarat, DbType.Boolean);
            parameters.Add("ApprovalTypeApproved", ApprovalType.Approved, DbType.Int16);
            parameters.Add("ApprovalTypePending", ApprovalType.Pending, DbType.Int16);
            parameters.Add("ApprovalTypeSelfApproved", ApprovalType.SelfApproved, DbType.Int16);
            parameters.Add("ApprovalTypeManagerApproval", ApprovalType.ManagerApproval, DbType.Int16);
            parameters.Add("ApprovalTypeHigherManagerApproval", ApprovalType.HigherManagerApproval, DbType.Int16);
            parameters.Add("ApprovalTypeMPCApproval", ApprovalType.MPCApproval, DbType.Int16);
            parameters.Add("ApprovalTypeHigherAuthorityApprovalAndMPCApproval", ApprovalType.HigherAuthorityApprovalAndMPCApproval, DbType.Int16);
            parameters.Add("EpoLineItemsList", epoLineItemsList.ToDataTable(), DbType.Object);
            parameters.Add("EpoAttachmentMstList", epoAttachmentMst.ToDataTable(), DbType.Object);
            parameters.Add("EpoAttachmentDeleteIds", epoAttachmentDeleteIds.ToDataTable(), DbType.Object);

            var list = (await _baseRepository.QueryAsync<AddOrEditResponse>(StoredProcedures.EMP_ADD_OR_EDIT_EPO, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();
            return list;
        }

        public async Task<GetEpoDetailsByEpoIdResponse> GetEpoDetailsByEpoId(ServiceRequest<GetEpoDetailsByEpoIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("EpoNo", request.Data.EpoNo, DbType.String);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("ApprovalTypeApproved", ApprovalType.Approved, DbType.Int16);

            var response = new GetEpoDetailsByEpoIdResponse();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.EMP_GET_EPO_DETAILS_BY_EPOID, parameters, commandType: CommandType.StoredProcedure))
            {
                response.EpoBasicDetailsResponse = await multi.ReadFirstOrDefaultAsync<EpoBasicDetailsResponse>();
                response.EpoLineItemsList = (await multi.ReadAsync<EpoLineItemsList>()).ToList();
                response.EpoAttachmentMst = (await multi.ReadAsync<EpoAttachmentMst>()).ToList();
                response.CanEdit = await multi.ReadFirstOrDefaultAsync<bool?>();
                response.ApprovedBY = await multi.ReadFirstOrDefaultAsync<string>();
            }

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllEposResponse>> GetAllEpos(ServiceSearchRequest<GetAllEposRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Data.IsMyEpos == true ? request.Session.UserId : (int?)null, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("ApprovalType", request.Data.ApprovalType, DbType.Int32);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetAllEposResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllEposResponse>(StoredProcedures.EMP_GET_ALL_EPOS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task VoidEpoByUserId(ServiceRequest<GetEpoDetailsByEpoIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("EpoId", request.Data.EpoNo, DbType.Int32);
            parameters.Add("StatusTypeDeleted", value: StatusType.Deleted, DbType.Int32);

            await _baseRepository.ExecuteAsync(StoredProcedures.EMP_VOID_EPO_BY_USERID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task ApproveOrDenyEpo(ServiceRequest<ApproveOrDenyEpoRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("EpoId", request.Data.EpoId, DbType.Int32);
            parameters.Add("IsApprove", request.Data.IsApprove, DbType.Boolean);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("ApprovalTypeApproved", ApprovalType.Approved, DbType.Int16);
            parameters.Add("ApprovalTypeDenied", ApprovalType.Denied, DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.EMP_APPROVE_OR_DENY_EPO, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetMpcVoteForEpoResponse>> GetMpcVoteForEpo(ServiceSearchRequest<GetMpcVoteForEpoRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetMpcVoteForEpoResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetMpcVoteForEpoResponse>(StoredProcedures.EMP_GET_MPC_VOTE_FOR_EPO, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllMpcVoteForEpoResponse>> GetAllMpcVoteForEpo(ServiceSearchRequest<GetAllMpcVoteForEpoRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetAllMpcVoteForEpoResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllMpcVoteForEpoResponse>(StoredProcedures.EMP_GET_ALL_MPC_VOTE_FOR_EPO, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<ServiceSearchResponse<GetMySubordinateEposResponse>> GetMySubordinateEpos(ServiceSearchRequest<GetMySubordinateEposRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("ApprovalType", request.Data.ApprovalType, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetMySubordinateEposResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetMySubordinateEposResponse>(StoredProcedures.EMP_GET_MY_SUBORDINATE_EPOS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<ServiceSearchResponse<GetMyApprovalForEpoResponse>> GetMyApprovalForEpo(ServiceSearchRequest<GetMyApprovalForEpoRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            parameters.Add("ApprovalTypeApproved", ApprovalType.Approved, DbType.Int16);
            var response = new ServiceSearchResponse<GetMyApprovalForEpoResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetMyApprovalForEpoResponse>(StoredProcedures.EMP_GET_MY_APPROVAL_FOR_EPO, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetEpoModulesCountsResponse> GetEpoModulesCounts(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);

            return await _baseRepository.QueryFirstOrDefaultAsync<GetEpoModulesCountsResponse>(StoredProcedures.EMP_GET_EPO_MODULES_COUNTS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetAllVendorOrEmployeeDetailsByNameResponse>> GetAllVendorOrEmployeeDetailsByName(GetAllVendorOrEmployeeDetailsByNameRequest request)
        {

            var parameters = new DynamicParameters();
            parameters.Add("Name", request.Name, DbType.String);
            parameters.Add("IsEmployee", request.IsEmployee, DbType.Boolean);

            return (await _baseRepository.QueryAsync<GetAllVendorOrEmployeeDetailsByNameResponse>(StoredProcedures.EMP_GET_ALL_VENDOR_OR_EMPLOYEE_DETAILS_BY_NAME, parameters, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetMpcVoteDetailsByEpoIdResponse>> GetMpcVoteDetailsByEpoId(GetMpcVoteDetailsByEpoIdRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("EpoId", request.EpoId, DbType.Int32);

            return (await _baseRepository.QueryAsync<GetMpcVoteDetailsByEpoIdResponse>(StoredProcedures.EMP_GET_MPC_VOTE_DETAILS_BY_EPOID, parameters, commandType: CommandType.StoredProcedure)).ToList();
        }

    }
}
