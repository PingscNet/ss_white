﻿namespace SSWhite.Data.Implementation.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Admin;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;

    public class AdminRepository : IAdminRepository
    {
        private readonly IBaseRepository _baseRepository;
        private readonly AppSetting _appSetting;

        public AdminRepository(IBaseRepository baseRepository, IOptions<AppSetting> appSetting)
        {
            _baseRepository = baseRepository;
            _appSetting = appSetting.Value;
        }

        public async Task<AddNewEmployeeResponse> AddNewEmployee(ServiceRequest<AddNewEmployeeRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int16);
            parameters.Add("PersonID", request.Data.PersonID, DbType.String);
            parameters.Add("Organization", request.Data.Organization, DbType.String);
            parameters.Add("FirstName", request.Data.FirstName, DbType.String);
            parameters.Add("LastName", request.Data.LastName, DbType.String);
            parameters.Add("Gender", request.Data.Gender, DbType.Int32);
            parameters.Add("DOB", request.Data.DOB, DbType.DateTime);
            parameters.Add("DOJ", request.Data.DOJ, DbType.DateTime);
            parameters.Add("DOI", DateTime.Now, DbType.DateTime);
            parameters.Add("Address1", request.Data.Address1, DbType.String);
            parameters.Add("Address2", request.Data.Address2, DbType.String);
            parameters.Add("BloodGroup", request.Data.BloodGroup, DbType.String);
            parameters.Add("EmailAddress", request.Data.EmailAddress, DbType.String);
            parameters.Add("EmpPosition", request.Data.EmpPosition, DbType.String);
            parameters.Add("ReportingUnder", request.Data.ReportingUnder, DbType.String);
            parameters.Add("Department", request.Data.Department, DbType.String);
            parameters.Add("Signature", request.Data.Signature, DbType.String);
            parameters.Add("Picture", request.Data.Picture, DbType.String);
            parameters.Add("Password", Generator.EncryptPassword(_appSetting.CreateUserPassword), DbType.String);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("PhoneNo", request.Data.PhoneNo, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("StatusActive", StatusType.Active, DbType.Int32);
            parameters.Add("Role", request.Data.Role, DbType.Int32);
            return (await _baseRepository.QueryAsync<AddNewEmployeeResponse>(StoredProcedures.EMP_ADD_NEW_EMPLOYEE, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();
        }

        public async Task<List<GetDocumentApprovalListResponse>> GetDocumentApprovals()
        {
            return (await _baseRepository.QueryAsync<GetDocumentApprovalListResponse>(StoredProcedures.EMPLOYEE_GET_DOCUMENT_APPROVAL_LIST, null, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<ServiceSearchResponse<GetAllEmployeeLimitsResponse>> GetAllEmployeeLimits(GetAllEmployeeLimitsRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("PId", 1, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetAllEmployeeLimitsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllEmployeeLimitsResponse>(StoredProcedures.EMPLOYEE_GET_ALL_EMPLOYEE_LIMIT, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<List<GetEmployeeByEmployeeIdResponse>> GetEmployeeByEmployeeId(GetEmployeeByEmployeeIdRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("PersonId", request.EmployeeId, DbType.String);

            return (await _baseRepository.QueryAsync<GetEmployeeByEmployeeIdResponse>(StoredProcedures.EMP_GET_EMPLOYEE_BY_EMPLOYEE_ID, parameters, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task UpdateEmployeeLimit(UpdateEmployeeLimitByIdRequest updateEmployeeLimitByIdRequest)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("Id", updateEmployeeLimitByIdRequest.LimitId, DbType.Int32);
            parameteres.Add("EmpLimitNew", updateEmployeeLimitByIdRequest.Limit, DbType.Decimal);
            await _baseRepository.ExecuteAsync(StoredProcedures.EMPLOYEE_UPDATE_EMPLOYEE_LIMIT, parameteres, commandType: CommandType.StoredProcedure);
        }

        public async Task ChangeEmployeeStatus(Task<ServiceRequest<ChangeEmployeeStatusRequest>> request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("EmployeeId", request.Result.Data.EmployeeId, DbType.String);
            parameteres.Add("UpdatedBy", request.Result.Session.UserId, DbType.Int16);
            parameteres.Add("UpdatedDate", DateTime.Now, DbType.DateTime);
            await _baseRepository.ExecuteAsync(StoredProcedures.EMP_CHANGE_EMPLOYEE_STATUS, parameteres, commandType: CommandType.StoredProcedure);
        }

        public async Task<CheckIfEmployeeIdExistsResponse> CheckIfEmployeeIdExists(CheckIfEmployeeIdExistsRequest checkIfEmployeeIdExistsRequest)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("EmployeeId", checkIfEmployeeIdExistsRequest.EmployeeId, DbType.String);
            return (await _baseRepository.QueryAsync<CheckIfEmployeeIdExistsResponse>(StoredProcedures.EMP_CHECK_IF_EMPLOYEEID_EXISTS, parameteres, commandType: CommandType.StoredProcedure)).FirstOrDefault();
        }

        public async Task<CheckIfEmailAddressExistsResponse> CheckIfEmailAddressExists(CheckIfEmailAddressExistsRequest request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("EmailAddress", request.EmailAddress, DbType.String);
            return (await _baseRepository.QueryAsync<CheckIfEmailAddressExistsResponse>(StoredProcedures.EMP_CHECK_IF_EMAIL_ADDRESS_EXISTS, parameteres, commandType: CommandType.StoredProcedure)).FirstOrDefault();
        }

        public async Task UpdateDocumentApprovalList(List<UpdateDocumentApprovalListRequest> request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("UpdateDocumentApprovalList", request.ToDataTable(), DbType.Object);

            await _baseRepository.ExecuteAsync(StoredProcedures.EMP_UPDATE_DOCUMENT_APPROVAL_LIST, parameteres, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetUserRightsResponse>> GetUserRights(ServiceRequest request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("UserId", request.Session.UserId, DbType.Int32);

            return (await _baseRepository.QueryAsync<GetUserRightsResponse>(StoredProcedures.EMP_GET_USER_RIGHTS, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetRolesResponse>> GetRoles()
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("StatusTypeActive", StatusType.Active, DbType.Int32);

            return (await _baseRepository.QueryAsync<GetRolesResponse>(StoredProcedures.EMP_GET_ROLES, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetRightsAccessByRoleIdResponse>> GetRightsAccessByRoleId(GetRightsAccessByRoleIdRequest request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("RoleId", request.RoleId, DbType.Int32);

            return (await _baseRepository.QueryAsync<GetRightsAccessByRoleIdResponse>(StoredProcedures.EMP_GET_RIGHTS_ACCESS_BY_ROLEID, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task UpdateRightsAccessByRoleId(UpdateRightsAccessByRoleIdRequest request)
        {
            var roleRightsTypeList = (from item in request.RoleRightsTypeList
                                      select new
                                      {
                                          item.UniqueCode,
                                          item.IsAccess
                                      }).ToList();

            var parameters = new DynamicParameters();
            parameters.Add("RoleId", request.RoleId, DbType.Int32);
            parameters.Add("RoleRightsTypeList", roleRightsTypeList.ToDataTable(), DbType.Object);

            await _baseRepository.ExecuteAsync(StoredProcedures.EMP_UPDATE_RIGHTS_ACCESS_BY_ROLEID, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
