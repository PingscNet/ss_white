﻿namespace SSWhite.Data.Implementation.Common
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Common;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;

    public class CommonRepository : ICommonRepository
    {
        private readonly IBaseRepository _baseRepository;

        public CommonRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<List<GetOrganizationResponse>> GetOrganization()
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("Status", StatusType.Active, DbType.Int32);
            return (await _baseRepository.QueryAsync<GetOrganizationResponse>(StoredProcedures.MASTER_GET_ORGANIZATION, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetEmployeeJobDescriptionResponse>> GetEmployeeJobDescription()
        {
            return (await _baseRepository.QueryAsync<GetEmployeeJobDescriptionResponse>(StoredProcedures.MASTER_GET_EMPLOYEE_JOB_DESCRIPTION, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetReportingUnderResponse>> GetReportingUnder()
        {
            return (await _baseRepository.QueryAsync<GetReportingUnderResponse>(StoredProcedures.MASTER_GET_REPORTING_UNDER, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetDepartmentByDepartmentSupervisorNameResponse>> GetDepartmentByDepartmentSupervisorName(GetDepartmentByDepartmentSupervisorNameRequest getDepartmentByDepartmentSupervisorNameRequest)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("DepartmentSupervisorName", getDepartmentByDepartmentSupervisorNameRequest.DepartmentSupervisorName, DbType.String);

            return (await _baseRepository.QueryAsync<GetDepartmentByDepartmentSupervisorNameResponse>(StoredProcedures.MASTER_GET_DEPARTMENT_BY_DEPARTMENT_SUPERVISOR_NAME, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }
    }
}
