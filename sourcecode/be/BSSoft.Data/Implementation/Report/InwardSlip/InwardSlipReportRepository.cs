﻿namespace SSWhite.Data.Implementation.Report.InwardSlip
{
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Report.InwardSlip;
    using SSWhite.Domain.Request.Report.InwardSlip;
    using SSWhite.Domain.Response.Report.InwardSlip;

    public class InwardSlipReportRepository : IInwardSlipReportRepository
    {
        private readonly IBaseRepository _baseRepository;

        public InwardSlipReportRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<ServiceSearchResponse<GetAllInwardSlipsForReportResponse>> GetAllInwardSlipsForReport(ServiceSearchRequest<GetAllInwardSlipsForReport> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("ItemId", request.Data.ItemId, DbType.Int32);
            parameters.Add("SiteId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("Month", request.Data.Month, DbType.Int16);
            parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("Status", StatusType.Active, DbType.Int16);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllInwardSlipsForReportResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllInwardSlipsForReportResponse>(StoredProcedures.DOCUMENT_GET_ALL_INWARD_SLIPS_FOR_REPORT, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }
    }
}
