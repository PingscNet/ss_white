﻿namespace SSWhite.Data.Implementation.Account
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.Domain.Response.Account;
    using SSWhite.Domain.Response.Dashboard;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;

    public class DashboardRepository : IDashboardRepository
    {
        private readonly IBaseRepository _baseRepository;
        private readonly IPAddress _ipAddress;

        public DashboardRepository(IBaseRepository baseRepository, IOptions<IPAddress> ipAddress)
        {
            _baseRepository = baseRepository;
            _ipAddress = ipAddress.Value;
        }

        public async Task<List<GetSSWhiteTeamDetailsResponse>> GetSSWhiteTeamDetails()
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("Status", StatusType.Active, DbType.Int32);

            return (await _baseRepository.QueryAsync<GetSSWhiteTeamDetailsResponse>(StoredProcedures.MASTER_GET_SSWHITE_TEAM_DETAILS, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<UserDetail> GetUserDetailsById(int id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Int16);

            var result = await _baseRepository.QueryFirstAsync<UserDetail>(StoredProcedures.DBO_GET_USER_DETAILS_BY_ID, parameters, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<UserNotificationResponse> UserNotification(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int16);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("ApprovalTypeApproved", ApprovalType.Approved, DbType.Int16);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);

            var userNotificationResponse = new UserNotificationResponse();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.EMP_USER_NOTIFICATION, parameters, commandType: CommandType.StoredProcedure))
            {
                userNotificationResponse.EpoNotificationResponse = (await multi.ReadAsync<EpoNotificationResponse>()).ToList();
                userNotificationResponse.FtoNotificationResponse = (await multi.ReadAsync<FtoNotificationResponse>()).ToList();
            }

            return userNotificationResponse;
        }

        public async Task<List<GetNotificationsResponse>> GetNotifications(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int16);

            return (await _baseRepository.QueryAsync<GetNotificationsResponse>(StoredProcedures.EMP_GET_NOTIFICATIONS, parameters, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<GetModulesCountsResponse> GetModulesCounts(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int16);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ApprovalTypeApproved", ApprovalType.Approved, DbType.Int16);

            var result = await _baseRepository.QueryFirstAsync<GetModulesCountsResponse>(StoredProcedures.EMP_GET_MODULES_COUNTS, parameters, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<bool> UpdateWorkingStatus(ServiceRequest<UpdateWorkingStatusRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("Status", request.Data.Status, DbType.Int32);
            parameters.Add("StatusTypeActive", value: StatusType.Active, DbType.Int32);
            parameters.Add("StatusTypeInActive", value: StatusType.InActive, DbType.Int32);

            return await _baseRepository.QueryFirstOrDefaultAsync<bool>(StoredProcedures.EMP_UPDATE_WORKING_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<int> ChangePassword(ServiceRequest<ChangePasswordRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("CurrentPassword", Generator.EncryptPassword(request.Data.CurrentPassword.Trim()), DbType.String);
            parameters.Add("NewPassword", Generator.EncryptPassword(request.Data.NewPassword.Trim()), DbType.String);
            ////parameters.Add("StatusTypeActive", value: StatusType.Active, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);

            return await _baseRepository.QueryFirstOrDefaultAsync<int>(StoredProcedures.EMP_CHANGE_PASSWORD, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
