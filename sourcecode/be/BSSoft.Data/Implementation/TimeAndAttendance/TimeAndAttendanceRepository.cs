﻿namespace SSWhite.Data.Implementation.Account
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.TimeAndAttendance;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.TimeAndAttendance;
    using SSWhite.Domain.Response.Account;
    using SSWhite.Domain.Response.Dashboard;
    using SSWhite.Domain.Response.TimeAndAttendance;

    public class TimeAndAttendanceRepository : ITimeAndAttendanceRepository
    {
        private readonly IBaseRepository _baseRepository;
        private readonly IPAddress _ipAddress;

        public TimeAndAttendanceRepository(IBaseRepository baseRepository, IOptions<IPAddress> ipAddress)
        {
            _baseRepository = baseRepository;
            _ipAddress = ipAddress.Value;
        }

        public async Task InsertInOutDetailsForLunch(InsertInOutDetailsRequest insertInOutDetailsRequest)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", insertInOutDetailsRequest.UserId, DbType.Int32);
            parameters.Add("AttendancePunchTypeId", insertInOutDetailsRequest.PunchType, DbType.Int32);
            parameters.Add("AttendanceInOutStatusId", insertInOutDetailsRequest.InOutStatus, DbType.String);
            parameters.Add("CurrentDate", DateTime.Now, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DBO_INSERT_IN_OUT_DETAILS_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetInOutDetailsByUserIdResponse>> GetInOutDetailsByUserId(ServiceRequest<GetInOutDetailsByUserIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("StartDate", request.Data.StartDate, DbType.DateTime);
            parameters.Add("EndDate", request.Data.EndDate, DbType.DateTime);
            var response = (await _baseRepository.QueryAsync<GetInOutDetailsByUserIdResponse>(StoredProcedures.DBO_GET_IN_OUT_DETAILS_BY_USER_ID, parameters, commandType: CommandType.StoredProcedure)).ToList();
            return response;
        }

        public async Task<List<GetUsersInOutStatusResponse>> GetUsersInOutStatus()
        {
            ////var parameters = new DynamicParameters();
            ////parameters.Add("UserId", getInOutDetailsByUserIdRequest.UserId, DbType.Int32);
            ////parameters.Add("StartDate", getInOutDetailsByUserIdRequest.StartDate, DbType.DateTime);
            ////parameters.Add("EndDate", getInOutDetailsByUserIdRequest.EndDate, DbType.DateTime);
            var response = (List<GetUsersInOutStatusResponse>)await _baseRepository.QueryAsync<GetUsersInOutStatusResponse>(StoredProcedures.DBO_GET_USERS_IN_OUT_STATUS, commandType: CommandType.StoredProcedure);
            return response;
        }

        public async Task<GetInOutStatusByPunchTypeByUserIdResponse> GetInOutStatusByPunchTypeUserId(ServiceRequest<GetInOutStatusByPunchTypeUserIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("AttendancePunchTypeId", request.Data.AttendancePunchTypeId, DbType.Int32);
            var response = await _baseRepository.QueryFirstAsync<GetInOutStatusByPunchTypeByUserIdResponse>(StoredProcedures.DBO_GET_IN_OUT_STATUS_BY_PUNCHTYPE, parameters, commandType: CommandType.StoredProcedure);
            return response;
        }

        public async Task InsertPunchDetails(ServiceRequest<InsertPunchDetailsRequest> insertPunchDetailsRequest)
        {
            var parameters = new DynamicParameters();
            parameters.Add("EmployeeId", insertPunchDetailsRequest.Session.UserId, DbType.Int32);
            parameters.Add("PunchDateTime", DateTime.Now, DbType.DateTime);
            parameters.Add("InOutStatus", insertPunchDetailsRequest.Data.InOutStatus, DbType.Int16);
            parameters.Add("PunchType", insertPunchDetailsRequest.Data.PunchType, DbType.Int16);
            parameters.Add("CreatedBy", insertPunchDetailsRequest.Session.UserId, DbType.Int32);
            await _baseRepository.QueryAsync<bool>(StoredProcedures.EMP_INSERT_PUNCH_DETAILS, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
