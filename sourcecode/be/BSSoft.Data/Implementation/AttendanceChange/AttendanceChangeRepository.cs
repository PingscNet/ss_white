﻿namespace SSWhite.Data.Implementation.AttendanceChange
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.AttendanceChange;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Domain.Request.AttendanceChange;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.Domain.Response.AttendanceChange;

    public class AttendanceChangeRepository : IAttendanceChangeRepository
    {
        private readonly IBaseRepository _baseRepository;

        public AttendanceChangeRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<CreateAttendanceFtoResponse> CreateAttendanceFto(ServiceRequest<CreateAttendanceFtoRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserID", request.Session.UserId, DbType.Int32);
            parameters.Add("EmployeeID", request.Data.EmployeeID, DbType.String);
            parameters.Add("EmployeeName", request.Data.EmployeeName, DbType.String);
            parameters.Add("StartDate", request.Data.StartDate, DbType.Date);
            parameters.Add("StartDateDayType", request.Data.StartDateDayType, DbType.Int16);
            parameters.Add("EndDate", request.Data.EndDate, DbType.Date);
            parameters.Add("EndDateDayType", request.Data.EndDateDayType, DbType.Int16);
            parameters.Add("LeaveTypeID", request.Data.LeaveTypeID, DbType.Int16);
            parameters.Add("OtherLeaveTypeId", request.Data.OtherLeaveTypeId, DbType.Int16);
            parameters.Add("EmployeeComment", request.Data.EmployeeComment, DbType.String);
            parameters.Add("ApprovalTypePending", ApprovalType.Pending, DbType.Int32);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("NumberOfDays", request.Data.NumberOfDays, DbType.Double);

            var result = (await _baseRepository.QueryAsync<CreateAttendanceFtoResponse>(StoredProcedures.EMP_CREATE_ATTENDANCE_FTO, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();
            return result;
        }

        public async Task<ApproveOrDenyFtoResponse> ApproveOrDenyFto(ServiceRequest<ApproveOrDenyFtoRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("FtoId", request.Data.FtoId, DbType.Int32);
            parameters.Add("Note", request.Data.Note, DbType.String);
            parameters.Add("IsApprove", request.Data.IsApprove, DbType.Boolean);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("ApprovalTypeApproved", ApprovalType.Approved, DbType.Int16);
            parameters.Add("ApprovalTypeDenied", ApprovalType.Denied, DbType.Int16);
            parameters.Add("ApprovalTypePending", ApprovalType.Pending, DbType.Int16);

            return (await _baseRepository.QueryAsync<ApproveOrDenyFtoResponse>(StoredProcedures.EMP_APPROVE_OR_DENY_FTO, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();
        }

        public async Task<List<GetDepartmentFtoByUserIdResponse>> GetDepartmentFtoByUserId(ServiceRequest<GetDepartmentFtoByUserIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("StartDate", request.Data.StartDate, DbType.Date);
            parameters.Add("EndDate", request.Data.EndDate, DbType.Date);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int32);

            return (await _baseRepository.QueryAsync<GetDepartmentFtoByUserIdResponse>(StoredProcedures.EMP_GET_DEPARTMENT_FTO_BY_USERID, parameters, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<GetAttendanceFtoByIdResponse> GetAttendanceFtoById(ServiceRequest<GetAttendanceFtoByIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("FtoId", request.Data.FtoId, DbType.Int32);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int32);

            return await _baseRepository.QueryFirstOrDefaultAsync<GetAttendanceFtoByIdResponse>(StoredProcedures.EMP_GET_ATTENDANCE_FTO_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllDepartmentOrUserFtoByUserIdResponse>> GetAllDepartmentOrUserFtoByUserId(ServiceSearchRequest<GetAllDepartmentOrUserFtoByUserIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int32);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("OnlyFutureDate", request.Data.OnlyFutureDate, DbType.Boolean);
            parameters.Add("OnlyUsersData", request.Data.OnlyUsersData, DbType.Boolean);
            parameters.Add("ApproveFTO", request.Data.ApproveFTO, DbType.Boolean);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllDepartmentOrUserFtoByUserIdResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllDepartmentOrUserFtoByUserIdResponse>(StoredProcedures.EMP_GET_ALL_DEPARTMENT_OR_USER_FTO_BY_USERID, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }
    }
}
