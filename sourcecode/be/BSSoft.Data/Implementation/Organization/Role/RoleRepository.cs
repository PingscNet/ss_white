﻿namespace SSWhite.Data.Implementation.Organization.Role
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Organization.Role;
    using SSWhite.Domain.Common.Organization.Role;
    using SSWhite.Domain.Request.Organization.Role;
    using SSWhite.Domain.Response.Organization.Role;

    public class RoleRepository : IRoleRepository
    {
        private readonly IBaseRepository _baseRepository;

        public RoleRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddEditRole(ServiceRequest<Role> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("Description", request.Data.Description, DbType.String);

            if (request.Data.RightsIds?.Any() == true)
            {
                parameters.Add("RightsIds", request.Data.RightsIds.ToDataTable(), DbType.Object);
            }

            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);

            await _baseRepository.ExecuteAsync(StoredProcedures.SUBSCRIBER_INSERT_OR_UPDATE_ROLE, parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("ReturnValue");
        }

        public async Task ChangeRoleStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);

            await _baseRepository.ExecuteAsync(StoredProcedures.SUBSCRIBER_CHANGE_ROLE_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteRole(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("StatusTypeDeleted", StatusType.Deleted, DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.SUBSCRIBER_DELETE_ROLE, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllRolesResponse>> GetAllRoles(ServiceSearchRequest<GetAllRoles> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllRolesResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllRolesResponse>(StoredProcedures.SUBSCRIBER_GET_ALL_ROLES, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetRoleByIdResponse> GetRoleById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);

            var response = new GetRoleByIdResponse();
            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.SUBSCRIBER_GET_ROLE_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadSingleAsync<GetRoleByIdResponse>();
                response.RightsIds = (await multi.ReadAsync<int>()).ToList();
            }

            return response;
        }
    }
}
