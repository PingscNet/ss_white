﻿namespace SSWhite.Data.Implementation.Document
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Document;
    using SSWhite.Domain.Enums.Document;
    using SSWhite.Domain.Request.Document;
    using SSWhite.Domain.Response.Document;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;

    public class DocumentRepository : IDocumentRepository
    {
        private readonly IBaseRepository _baseRepository;

        public DocumentRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<AddOrEditDocumentResponse> AddOrEditDocument(ServiceRequest<AddOrEditDocumentRequest> request)
        {


            if (request.Data.DocumentNotification == null)
            {
                request.Data.DocumentNotification = new List<DocumentNotification>();
            }

            var documentNotification = (from item in request.Data.DocumentNotification
                                        select new
                                        {
                                            item.Id,
                                            item.Name,
                                            item.UserId,
                                            item.PersonId
                                        }).ToList();

            if (request.Data.ApprovalNotification == null)
            {
                request.Data.ApprovalNotification = new List<ApprovalNotification>();
            }

            var approvalNotification = (from item in request.Data.ApprovalNotification
                                        select new
                                        {
                                            item.Id,
                                            item.Name,
                                            item.UserId,
                                            item.PersonId
                                        }).ToList();

            if (request.Data.AssociatedDocument == null)
            {
                request.Data.AssociatedDocument = new List<AssociatedDocument>();
            }

            var associatedDocument = (from item in request.Data.AssociatedDocument
                                      select new
                                      {
                                          item.Id,
                                          item.AssociatedDocumentId,
                                          item.Rev,
                                          item.Title
                                      }).ToList();

            if (request.Data.Attachment == null)
            {
                request.Data.Attachment = new List<Attachment>();
            }

            var attachment = (from item in request.Data.Attachment
                              select new
                              {
                                  item.Id,
                                  item.LocalFileName,
                                  item.Title,
                                  item.AttachmentPath
                              }).ToList();

            if (request.Data.DocumentAttachmentDeleteIds == null)
            {
                request.Data.DocumentAttachmentDeleteIds = new List<int>();
            }

            var documentAttachmentDeleteIds = (from item in request.Data.DocumentAttachmentDeleteIds
                                               select new
                                               {
                                                   item
                                               }).ToList();

            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("DocumentTitle", request.Data.DocumentTitle, DbType.String);
            parameters.Add("DocumentType", request.Data.DocumentType, DbType.Int16);
            parameters.Add("DocumentId", request.Data.DocumentId, DbType.String);
            parameters.Add("Rev", request.Data.Rev, DbType.String);
            parameters.Add("EcnNumber", request.Data.EcnNumber, DbType.Int32);
            parameters.Add("EcnTitle", request.Data.EcnTitle, DbType.String);
            parameters.Add("EcnDate", request.Data.EcnDate, DbType.DateTime);
            parameters.Add("CustomerApprovalRequired", request.Data.CustomerApprovalRequired, DbType.Boolean);
            parameters.Add("CustomerNotificationRequired", request.Data.CustomerNotificationRequired, DbType.Boolean);
            parameters.Add("PartDescriptionName", request.Data.PartDescriptionName, DbType.String);
            parameters.Add("PartNumber", request.Data.PartNumber, DbType.String);
            parameters.Add("DrawingNumber", request.Data.DrawingNumber, DbType.String);
            parameters.Add("CustomerPartNumber", request.Data.CustomerPartNumber, DbType.String);
            parameters.Add("CurrentRev", request.Data.CurrentRev, DbType.String);
            parameters.Add("NewRev", request.Data.NewRev, DbType.String);
            parameters.Add("DescriptionOfChange", request.Data.DescriptionOfChange, DbType.String);
            parameters.Add("ReasonForChange", request.Data.ReasonForChange, DbType.String);
            parameters.Add("InstructionForEcnImplementation", request.Data.InstructionForEcnImplementation, DbType.String);
            parameters.Add("EcnChange", request.Data.EcnChange, DbType.String);
            parameters.Add("Purpose", request.Data.Purpose, DbType.String);
            parameters.Add("Scope", request.Data.Scope, DbType.String);
            parameters.Add("Responsibility", request.Data.Responsibility, DbType.String);
            parameters.Add("Defination", request.Data.Defination, DbType.String);
            parameters.Add("Form", request.Data.Form, DbType.String);
            parameters.Add("Notes", request.Data.Notes, DbType.String);
            parameters.Add("WorkStation", request.Data.WorkStation, DbType.String);
            parameters.Add("AttachmentApproval", request.Data.AttachmentApproval, DbType.Boolean);
            parameters.Add("IsSendForApproval", request.Data.IsSendForApproval, DbType.Boolean);
            parameters.Add("SectionParaChanged", request.Data.SectionParaChanged, DbType.String);
            parameters.Add("ChangeMade", request.Data.ChangeMade, DbType.String);
            parameters.Add("TrainingRequired",request.Data.TrainingRequired , DbType.Int16);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("DocumentNotification", documentNotification.ToDataTable(), DbType.Object);
            parameters.Add("ApprovalNotification", approvalNotification.ToDataTable(), DbType.Object);
            parameters.Add("AssociatedDocument", associatedDocument.ToDataTable(), DbType.Object);
            parameters.Add("Attachment", attachment.ToDataTable(), DbType.Object);
            parameters.Add("ModifiedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("ModifiedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("DocumentAttachmentDeleteIds", documentAttachmentDeleteIds.ToDataTable(), DbType.Object);

            var list = (await _baseRepository.QueryAsync<AddOrEditDocumentResponse>(StoredProcedures.DOCUMENT_ADD_OR_EDIT_DOCUMENT, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();
            return list;
        }

        public async Task<ApproveDocumentResponse> ApproveDocument(ServiceRequest<ApproveDocumentRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ApprovalType", request.Data.ApprovalType, DbType.Int16);
            parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
            parameters.Add("Result", DbType.Int16, direction: ParameterDirection.Output);

            var response = (await _baseRepository.QueryAsync<ApproveDocumentResponse>(StoredProcedures.DOCUMENT_APPROVE_DOCUMENT, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();

            if (response == null)
            {
                var approveDocumentResponse = new ApproveDocumentResponse()
                {
                    Result = parameters.Get<int>("Result"),
                };
                return approveDocumentResponse;
            }

            return response;
         }

        public async Task<ServiceSearchResponse<GetAllDocumentsResponse>> GetAllDocuments(ServiceSearchRequest<GetAllDocumentsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int32);
            parameters.Add("DocumentType", request.Data.DocumentType, DbType.Int32);
            parameters.Add("DocumentStageStatus", request.Data.DocumentStageStatus, DbType.Int32);
            parameters.Add("ApprovalData", request.Data.ApprovalData, DbType.Boolean);
            parameters.Add("IsSendForApproval", request.Data.IsSendForApproval, DbType.Boolean);
            parameters.Add("ApprovalTypeApprovedAndReleased", ApprovalType.ApprovedAndReleased, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllDocumentsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllDocumentsResponse>(StoredProcedures.DOCUMENT_GET_ALL_DOCUMENTS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<List<GetAssociateDocumentsResponse>> GetAssociateDocuments()
        {
            var parameters = new DynamicParameters();
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);

            var result = (await _baseRepository.QueryAsync<GetAssociateDocumentsResponse>(StoredProcedures.DOCUMENT_GET_ASSOCIATE_DOCUMENTS, parameters, commandType: CommandType.StoredProcedure)).ToList();
            return result;
        }

        public async Task<GetDocumentByIdResponse> GetDocumentById(ServiceRequest<GetDocumentByIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int16);
            parameters.Add("DocumentId", request.Data.DocumentId, DbType.String);
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);

            var response = new GetDocumentByIdResponse();


            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.DOCUMENT_GET_DOCUMENT_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstOrDefaultAsync<GetDocumentByIdResponse>();

                response.RevisionLog = (await multi.ReadAsync<RevisionLog>()).ToList();

                foreach (var item in response.RevisionLog)
                {
                    item.TrainingRequired = Enum.GetName(typeof(TrainingRequired), Convert.ToInt16(item.TrainingRequired));
                }

                response.Attachment = (await multi.ReadAsync<Attachment>()).ToList();
                response.DocumentNotification = (await multi.ReadAsync<DocumentNotification>()).ToList();
                response.ApprovalNotification = (await multi.ReadAsync<ApprovalNotification>()).ToList();
                response.AssociatedDocument = (await multi.ReadAsync<AssociatedDocument>()).ToList();
            }
            //var x = Enum.GetName(typeof(TrainingRequired), short.Parse(response.TrainingRequired));
            return response;
        }

        public async Task<GetDocumentsDropdownAndUserDetailsResponse> GetDocumentsDropdownAndUserDetails(ServiceRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Session.UserId, DbType.Int32);

            var response = new GetDocumentsDropdownAndUserDetailsResponse();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.DOCUMENT_GET_DOCUMENTS_DROPDOWN_AND_USER_DETAILS, parameters, commandType: CommandType.StoredProcedure))
            {
                response.DocumentEcnChange = (await multi.ReadAsync<DocumentEcnChange>()).ToList();
                response.DepartmentName = (await multi.ReadAsync<string>()).ToList();
                response.DocumentApprovalPerson = (await multi.ReadAsync<DocumentApprovalPerson>()).ToList();
                response.DocumentPersonDetail = await multi.ReadFirstOrDefaultAsync<DocumentPersonDetail>();
            }

            return response;
        }

        public async Task<List<GetEmployeeByDepartmentResponse>> GetEmployeeByDepartment(GetEmployeeByDepartmentRequest request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("DepartmentName", request.DepartmentName, DbType.String);

            var result = (await _baseRepository.QueryAsync<GetEmployeeByDepartmentResponse>(StoredProcedures.DOCUMENT_GET_EMPLOYEE_BY_DEPARTMENT, parameters, commandType: CommandType.StoredProcedure)).ToList();
            return result;
        }
    }
}
