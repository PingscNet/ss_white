﻿namespace SSWhite.Data.Implementation.Document.Sales
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Document.Sales;
    using SSWhite.Domain.Common.Document;
    using SSWhite.Domain.Common.Document.Sales;
    using SSWhite.Domain.Enums.Document;
    using SSWhite.Domain.Request.Document.Sales;
    using SSWhite.Domain.Response.Document.Sales;

    public class SalesRepository : ISalesRepository
    {
        private readonly IBaseRepository _baseRepository;

        public SalesRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<List<InsertOrUpdateSalesDocumentResponse>> AddEditSalesDocument(ServiceRequest<SalesDocument> request)
        {
            var documentItems = (from item in request.Data.SalesDocumentItems
                                 select new
                                 {
                                     item.ItemId,
                                     item.UnitId,
                                     item.Description,
                                     item.Quantity,
                                     item.Weight,
                                     item.FreeQuantity,
                                     item.Rate,
                                     item.Amount,
                                     item.LotNumber
                                 }).ToList();

            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("VoucherNumber", request.Data.VoucherNumber, DbType.String);
            parameters.Add("Date", request.Data.Date, DbType.Date);
            parameters.Add("VoucherType", request.Data.VoucherType, DbType.Int16);
            parameters.Add("Month", request.Data.Date?.Month, DbType.Int16);
            parameters.Add("ReceivedBy", request.Data.ReceivedBy, DbType.String);
            parameters.Add("TotalAmount", request.Data.TotalAmount, DbType.Decimal);
            parameters.Add("AdvanceReceivedBy", request.Data.AdvanceReceivedBy, DbType.String);
            parameters.Add("AdvanceAmount", request.Data.AdvanceAmount, DbType.Decimal);
            parameters.Add("Remarks", request.Data.Remarks, DbType.String);
            parameters.Add("Prefix", request.Data.Prefix, DbType.String);
            parameters.Add("Suffix", request.Data.Suffix, DbType.String);
            parameters.Add("DocumentItems", documentItems.ToDataTable(), DbType.Object);

            if (request.Data.DocumentHeaders?.Any() == true)
            {
                var documentHeaders = (from item in request.Data.DocumentHeaders
                                       select new
                                       {
                                           item.HeaderId,
                                           CalculationType = (int)item.CalculationType,
                                           item.Percentage,
                                           item.Value
                                       }).ToList();
                parameters.Add("DocumentHeaders", documentHeaders.ToDataTable(), DbType.Object);
            }

            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("DocumentTypeSales", DocumentType.Sales, DbType.Int16);
            parameters.Add("DocumentTypePurchase", DocumentType.Purchase, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.Output);

            var list = (await _baseRepository.QueryAsync<InsertOrUpdateSalesDocumentResponse>(StoredProcedures.DOCUMENT_INSERT_OR_UPDATE_SALES_DOCUMENT, parameters, commandType: CommandType.StoredProcedure)).ToList();

            list = list.Any() ? list : new List<InsertOrUpdateSalesDocumentResponse>() { new InsertOrUpdateSalesDocumentResponse() { ReturnValue = parameters.Get<int>("ReturnValue") } };

            return list;
        }

        public async Task ChangeSalesDocumentStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DOCUMENT_CHANGE_SALES_DOCUMENT_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteSalesDocument(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("DocumentTypePurchase", DocumentType.Purchase, DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.DOCUMENT_DELETE_SALES_DOCUMENT, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllSalesDocumentsResponse>> GetAllPendingRateSalesDocuments(ServiceSearchRequest<GetAllSalesDocuments> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("VoucherType", request.Data.VoucherType, DbType.Int16);
            parameters.Add("Month", request.Data.Month, DbType.Int16);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllSalesDocumentsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllSalesDocumentsResponse>(StoredProcedures.DOCUMENT_GET_ALL_PENDING_RATE_SALES_DOCUMENTS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllSalesDocumentsResponse>> GetAllSalesDocuments(ServiceSearchRequest<GetAllSalesDocuments> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("VoucherType", request.Data.VoucherType, DbType.Int16);
            parameters.Add("Month", request.Data.Month, DbType.Int16);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllSalesDocumentsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllSalesDocumentsResponse>(StoredProcedures.DOCUMENT_GET_ALL_SALES_DOCUMENTS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetSalesDocumentByIdResponse> GetSalesDocumentById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);
            var response = new GetSalesDocumentByIdResponse();
            var documentItems = new List<SalesDocumentItem>();
            var documentHeaders = new List<DocumentHeader>();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.DOCUMENT_GET_SALES_DOCUMENT_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstAsync<GetSalesDocumentByIdResponse>();
                documentItems = (await multi.ReadAsync<SalesDocumentItem>()).ToList();
                documentHeaders = (await multi.ReadAsync<DocumentHeader>()).ToList();
            }

            if (response != null)
            {
                response.SalesDocumentItems = documentItems;
                response.DocumentHeaders = documentHeaders;
            }

            return response;
        }
    }
}
