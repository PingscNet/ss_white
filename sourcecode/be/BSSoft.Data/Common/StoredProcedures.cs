﻿namespace SSWhite.Data.Common
{
    public static class StoredProcedures
    {
        // Dashboard
        public const string MASTER_GET_SSWHITE_TEAM_DETAILS = "master.GetSSWhiteTeamDetails";
        public const string EMP_USER_NOTIFICATION = "emp.UserNotification";
        public const string EMP_GET_MODULES_COUNTS = "emp.GetModulesCounts";
        public const string EMP_UPDATE_WORKING_STATUS = "emp.UpdateWorkingStatus";
        public const string EMP_CHANGE_PASSWORD = "emp.ChangePassword";
        public const string EMP_GET_NOTIFICATIONS = "emp.GetNotifications";

        // Admin
        public const string MASTER_GET_ORGANIZATION = "master.GetOrganization";
        public const string MASTER_GET_DEPARTMENT_BY_DEPARTMENT_SUPERVISOR_NAME = "master.GetDepartmentByDepartmentSupervisorName";
        public const string MASTER_GET_REPORTING_UNDER = "master.GetReportingUnder";
        public const string MASTER_GET_EMPLOYEE_JOB_DESCRIPTION = "master.GetEmployeeJobDescription";
        public const string EMP_ADD_NEW_EMPLOYEE = "emp.AddNewEmployee";
        public const string EMPLOYEE_GET_ALL_EMPLOYEE_LIMIT = "emp.GetAllEmployeeLimit";
        public const string EMPLOYEE_UPDATE_EMPLOYEE_LIMIT = "emp.UpdateEmployeeLimit";
        public const string EMPLOYEE_GET_DOCUMENT_APPROVAL_LIST = "emp.GetDocumentApprovalList";
        public const string EMP_CHECK_IF_EMPLOYEEID_EXISTS = "emp.CheckIfEmployeeIdExists";
        public const string EMP_CHECK_IF_EMAIL_ADDRESS_EXISTS = "emp.CheckIfEmailAddressExists";
        public const string EMP_INSERT_PUNCH_DETAILS = "emp.InsertPunchDetails";
        public const string EMP_UPDATE_DOCUMENT_APPROVAL_LIST = "emp.UpdateDocumentApprovalList";
        public const string EMP_VOID_EPO_BY_USERID = "emp.VoidEpoByUserId";
        public const string EMP_APPROVE_OR_DENY_EPO = "emp.ApproveOrDenyEpo";
        public const string EMP_GET_USER_RIGHTS = "emp.GetUserRights";
        public const string EMP_GET_ROLES = "emp.GetRoles";
        public const string EMP_GET_RIGHTS_ACCESS_BY_ROLEID = "emp.GetRightsAccessByRoleId";
        public const string EMP_UPDATE_RIGHTS_ACCESS_BY_ROLEID = "emp.UpdateRightsAccessByRoleId";
        public const string EMP_SEND_FORGET_PASSWORD_EMAIL = "emp.SendForgetPasswordEmail";
        public const string EMP_IS_EMAIL_VALID = "emp.IsEmailValid";
        public const string EMP_SET_NEW_PASSWORD = "emp.SetNewPassword";
        public const string EMP_GET_EMPLOYEE_BY_EMPLOYEE_ID = "emp.GetEmployeeByEmployeeId";
        public const string EMP_CHANGE_EMPLOYEE_STATUS = "emp.ChangeEmployeeStatus";

        // EPO
        public const string EMP_GET_ALL_EPO_DROPDOWNS = "emp.GetAllEPODropDowns";
        public const string EMP_ADD_OR_EDIT_EPO = "emp.AddOrEditEpo";
        public const string EMP_GET_AUTHORIZED_LIMIT_BY_USER_ID = "emp.GetAuthorizedLimitByUserId";
        public const string EMP_GET_EPO_DETAILS_BY_EPOID = "emp.GetEpoDetailsByEpoId";
        public const string EMP_GET_ALL_EPOS = "emp.GetAllEpos";
        public const string EMP_GET_MPC_VOTE_FOR_EPO = "emp.GetMpcVoteForEpo";
        public const string EMP_GET_ALL_MPC_VOTE_FOR_EPO = "emp.GetAllMpcVoteForEpo";
        public const string EMP_GET_MY_SUBORDINATE_EPOS = "emp.GetMySubordinateEpos";
        public const string EMP_GET_MY_APPROVAL_FOR_EPO = "emp.GetMyApprovalForEpo";
        public const string EMP_GET_ALL_VENDOR_OR_EMPLOYEE_DETAILS_BY_NAME = "emp.GetAllVendorOrEmployeeDetailsByName";

        // Document
        public const string DOCUMENT_ADD_OR_EDIT_DOCUMENT = "document.AddOrEditDocument";
        public const string DOCUMENT_APPROVE_DOCUMENT = "document.ApproveDocument";
        public const string DOCUMENT_GET_ALL_DOCUMENTS = "document.GetAllDocuments";
        public const string DOCUMENT_GET_ASSOCIATE_DOCUMENTS = "document.GetAssociateDocuments";
        public const string DOCUMENT_GET_DOCUMENT_BY_ID = "document.GetDocumentById";
        public const string DOCUMENT_GET_DOCUMENTS_DROPDOWN_AND_USER_DETAILS = "document.GetDocumentsDropdownAndUserDetails";
        public const string DOCUMENT_GET_EMPLOYEE_BY_DEPARTMENT = "document.GetEmployeeByDepartment";

        // Attendance Change
        public const string EMP_CREATE_ATTENDANCE_FTO = "emp.CreateAttendanceFto";
        public const string EMP_APPROVE_OR_DENY_FTO = "emp.ApproveOrDenyFto";
        public const string EMP_GET_EPO_MODULES_COUNTS = "emp.GetEpoModulesCounts";
        public const string EMP_GET_DEPARTMENT_FTO_BY_USERID = "emp.GetDepartmentFtoByUserId";
        public const string EMP_GET_ATTENDANCE_FTO_BY_ID = "emp.GetAttendanceFtoById";
        public const string EMP_GET_ALL_DEPARTMENT_OR_USER_FTO_BY_USERID = "emp.GetAllDepartmentOrUserFtoByUserId";
        public const string EMP_GET_MPC_VOTE_DETAILS_BY_EPOID = "emp.GetMpcVoteDetailsByEpoId";

        // Subscriber Schema -> Account
        public const string SUBSCRIBER_VALIDATE_USER_FOR_LOGIN = "subscriber.ValidateUserForLogIn";
        public const string MASTER_VALIDATE_USER_FOR_LOGIN = "master.ValidateUserForLogIn";

        // Subscriber Schema -> User
        public const string SUBSCRIBER_GET_ALL_USERS = "subscriber.GetAllUsers";
        public const string SUBSCRIBER_INSERT_OR_UPDATE_USER = "subscriber.InsertOrUpdateUser";
        public const string SUBSCRIBER_CHANGE_USER_STATUS = "subscriber.ChangeUserStatus";
        public const string SUBSCRIBER_GET_USER_BY_ID = "subscriber.GetUserById";
        public const string SUBSCRIBER_DELETE_USER = "subscriber.DeleteUser";
        public const string DBO_GET_USER_DETAILS_BY_ID = "dbo.GetUserDetailsById";
        public const string DBO_INSERT_IN_OUT_DETAILS_BY_ID = "dbo.InsertInOutDetailsById";
        public const string DBO_GET_IN_OUT_DETAILS_BY_USER_ID = "dbo.GetInOutDetailsByUserId";
        public const string DBO_GET_USERS_IN_OUT_STATUS = "dbo.GetUsersInOutStatus";
        public const string DBO_GET_IN_OUT_STATUS_BY_PUNCHTYPE = "dbo.GetInOutStatusByPunchType";

        // Subscriber Schema -> Role
        public const string SUBSCRIBER_GET_ALL_ROLES = "subscriber.GetAllRoles";
        public const string SUBSCRIBER_INSERT_OR_UPDATE_ROLE = "subscriber.InsertOrUpdateRole";
        public const string SUBSCRIBER_CHANGE_ROLE_STATUS = "subscriber.ChangeRoleStatus";
        public const string SUBSCRIBER_GET_ROLE_BY_ID = "subscriber.GetRoleById";
        public const string SUBSCRIBER_DELETE_ROLE = "subscriber.DeleteRole";
        public const string SUBSCRIBER_GET_ROLES = "subscriber.GetRoles";

        // Subscriber Schema -> RightsGroup
        public const string SUBSCRIBER_GET_RIGHTS_GROUPS = "subscriber.GetRightsGroups";
        public const string SUBSCRIBER_GET_RIGHTS = "subscriber.GetRights";

        // Subscriber Schema -> Module
        public const string SUBSCRIBER_GET_MODULES = "subscriber.GetModules";

        // Master Schema -> Client
        public const string MASTER_GET_ALL_CLIENTS = "master.GetAllClients";
        public const string MASTER_INSERT_OR_UPDATE_CLIENT = "master.InsertOrUpdateClient";
        public const string MASTER_CHANGE_CLIENT_STATUS = "master.ChangeClientStatus";
        public const string MASTER_GET_CLIENT_BY_ID = "master.GetClientById";
        public const string MASTER_DELETE_CLIENT = "master.DeleteClient";
        public const string MASTER_GET_CLIENT_OPENING_BALANCE_DETAILS = "master.GetClientOpeningBalanceDetails";
        public const string MASTER_INSERT_OR_UPDATE_OPENING_BALANCE = "master.InsertOrUpdateOpeningBalance";

        // Master Schema -> Company
        public const string MASTER_GET_ALL_COMPANIES = "master.GetAllCompanies";
        public const string MASTER_INSERT_OR_UPDATE_COMPANY = "master.InsertOrUpdateCompany";
        public const string MASTER_CHANGE_COMPANY_STATUS = "master.ChangeCompanyStatus";
        public const string MASTER_GET_COMPANY_BY_ID = "master.GetCompanyById";
        public const string MASTER_DELETE_COMPANY = "master.DeleteCompany";
        public const string MASTER_GET_ENTITIES = "master.GetEntities";

        // Subscriber Schema -> Site
        public const string SUBSCRIBER_GET_ALL_SITES = "subscriber.GetAllSites";
        public const string SUBSCRIBER_INSERT_OR_UPDATE_SITE = "subscriber.InsertOrUpdateSite";
        public const string SUBSCRIBER_CHANGE_SITE_STATUS = "subscriber.ChangeSiteStatus";
        public const string SUBSCRIBER_GET_SITE_BY_ID = "subscriber.GetSiteById";
        public const string SUBSCRIBER_DELETE_SITE = "subscriber.DeleteSite";

        // Master Schema -> Tax
        public const string MASTER_GET_ALL_TAXES = "master.GetAllTaxes";
        public const string MASTER_INSERT_OR_UPDATE_TAXES = "master.InsertOrUpdateTax";
        public const string MASTER_CHANGE_TAX_STATUS = "master.ChangeTaxStatus";
        public const string MASTER_GET_TAX_BY_ID = "master.GetTaxById";
        public const string MASTER_DELETE_TAX = "master.DeleteTax";

        // Master Schema -> ItemGroup
        public const string MASTER_GET_ALL_ITEM_GROUPS = "master.GetAllItemGroups";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_GROUP = "master.InsertOrUpdateItemGroup";
        public const string MASTER_CHANGE_ITEM_GROUP_STATUS = "master.ChangeItemGroupStatus";
        public const string MASTER_GET_ITEM_GROUP_BY_ID = "master.GetItemGroupById";
        public const string MASTER_DELETE_ITEM_GROUP = "master.DeleteItemGroup";

        // Master Schema -> ItemSubGroup
        public const string MASTER_GET_ALL_ITEM_SUB_GROUPS = "master.GetAllItemSubGroups";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_SUB_GROUP = "master.InsertOrUpdateItemSubGroup";
        public const string MASTER_CHANGE_ITEM_SUB_GROUP_STATUS = "master.ChangeItemSubGroupStatus";
        public const string MASTER_GET_ITEM_SUB_GROUP_BY_ID = "master.GetItemSubGroupById";
        public const string MASTER_DELETE_ITEM_SUB_GROUP = "master.DeleteItemSubGroup";

        // Master Schema -> ItemCategory
        public const string MASTER_GET_ALL_ITEM_CATEGORIES = "master.GetAllItemCategories";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_CATEGORY = "master.InsertOrUpdateItemCategory";
        public const string MASTER_CHANGE_ITEM_CATEGORY_STATUS = "master.ChangeItemCategoryStatus";
        public const string MASTER_GET_ITEM_CATEGORY_BY_ID = "master.GetItemCategoryById";
        public const string MASTER_DELETE_ITEM_CATEGORY = "master.DeleteItemCategory";
        public const string MASTER_GET_ITEM_CATEGORIES = "master.GetItemCategories";

        // Master Schema -> ItemSubCategory
        public const string MASTER_GET_ALL_ITEM_SUB_CATEGORIES = "master.GetAllItemSubCategories";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_SUB_CATEGORY = "master.InsertOrUpdateItemSubCategory";
        public const string MASTER_CHANGE_ITEM_SUB_CATEGORY_STATUS = "master.ChangeItemSubCategoryStatus";
        public const string MASTER_GET_ITEM_SUB_CATEGORY_BY_ID = "master.GetItemSubCategoryById";
        public const string MASTER_DELETE_ITEM_SUB_CATEGORY = "master.DeleteItemSubCategory";

        // Master Schema -> ItemType
        public const string MASTER_GET_ALL_ITEM_TYPES = "master.GetAllItemTypes";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_TYPE = "master.InsertOrUpdateItemType";
        public const string MASTER_CHANGE_ITEM_TYPE_STATUS = "master.ChangeItemTypeStatus";
        public const string MASTER_GET_ITEM_TYPE_BY_ID = "master.GetItemTypeById";
        public const string MASTER_DELETE_ITEM_TYPE = "master.DeleteItemType";

        // Master Schema -> Uqc
        public const string MASTER_GET_ALL_UQCS = "master.GetAllUqcs";
        public const string MASTER_INSERT_OR_UPDATE_UQC = "master.InsertOrUpdateUqc";
        public const string MASTER_CHANGE_UQC_STATUS = "master.ChangeUqcStatus";
        public const string MASTER_GET_UQC_BY_ID = "master.GetUqcById";
        public const string MASTER_DELETE_UQC = "master.DeleteUqc";

        // Master Schema -> Item
        public const string MASTER_GET_ALL_ITEMS = "master.GetAllItems";
        public const string MASTER_INSERT_OR_UPDATE_ITEM = "master.InsertOrUpdateItem";
        public const string MASTER_CHANGE_ITEM_STATUS = "master.ChangeItemStatus";
        public const string MASTER_GET_ITEM_BY_ID = "master.GetItemById";
        public const string MASTER_DELETE_ITEM = "master.DeleteItem";
        public const string MASTER_GET_ITEM_SUB_GROUP_BY_GROUP_ID = "master.GetItemSubGroupByGroupId";
        public const string MASTER_GET_ITEM_SUB_CATEGORY_BY_CATEGORY_ID = "master.GetItemSubCategoryByCategoryId";

        // Master Schema -> ItemRate
        public const string MASTER_GET_ALL_ITEM_RATES = "master.GetAllItemRates";
        public const string MASTER_INSERT_OR_UPDATE_ITEM_RATE = "master.InsertOrUpdateItemRate";
        public const string MASTER_CHANGE_ITEM_RATE_STATUS = "master.ChangeItemRateStatus";
        public const string MASTER_GET_ITEM_RATE_BY_ID = "master.GetItemRateById";
        public const string MASTER_DELETE_ITEM_RATE = "master.DeleteItemRate";
        public const string MASTER_GET_ITEM_BY_GROUP_ID = "master.GetItemByGroupId";

        // Master Schema -> Crate
        public const string MASTER_GET_ALL_CRATES = "master.GetAllCrates";
        public const string MASTER_INSERT_OR_UPDATE_CRATE = "master.InsertOrUpdateCrate";
        public const string MASTER_CHANGE_CRATE_STATUS = "master.ChangeCrateStatus";
        public const string MASTER_GET_CRATE_BY_ID = "master.GetCrateById";
        public const string MASTER_DELETE_CRATE = "master.DeleteCrate";
        public const string MASTER_GET_CRATES = "master.GetCrates";

        // Master Schema -> State
        public const string APP_GET_ALL_STATES_FOR_MASTER = "app.GetAllStatesForMaster";
        public const string APP_INSERT_OR_UPDATE_STATE = "app.InsertOrUpdateState";
        public const string APP_CHANGE_STATE_STATUS = "app.ChangeStateStatus";
        public const string APP_GET_STATE_BY_ID = "app.GetStateById";
        public const string APP_DELETE_STATE = "app.DeleteState";

        // Master Schema -> City
        public const string APP_GET_ALL_CITIES = "app.GetAllCities";
        public const string APP_INSERT_OR_UPDATE_CITY = "app.InsertOrUpdateCity";
        public const string APP_CHANGE_CITY_STATUS = "app.ChangeCityStatus";
        public const string APP_GET_CITY_BY_ID = "app.GetCityById";
        public const string APP_DELETE_CITY = "app.DeleteCity";

        // Master Schema -> Headers
        public const string MASTER_GET_ALL_HEADERS = "master.GetAllHeaders";
        public const string MASTER_INSERT_OR_UPDATE_HEADER = "master.InsertOrUpdateHeader";
        public const string MASTER_CHANGE_HEADER_STATUS = "master.ChangeHeaderStatus";
        public const string MASTER_GET_HEADER_BY_ID = "master.GetHeaderById";
        public const string MASTER_DELETE_HEADER = "master.DeleteHeader";
        public const string MASTER_GET_HEADERS = "master.GetHeaders";
        public const string MASTER_GET_HEADER_BY_CLIENT_ID = "master.GetHeaderByClientId";

        // Application Schema
        public const string APP_GET_ALL_STATES = "app.GetAllStates";
        public const string APP_GET_CITY_BY_STATE_ID = "app.GetCityByStateId";

        // Master Schema -> Item Types
        public const string MASTER_GET_ITEM_TYPES = "master.GetItemTypes";
        public const string MASTER_GET_ITEM_GROUPS = "master.GetItemGroups";
        public const string MASTER_GET_UQCS = "master.GetUqcs";
        public const string MASTER_GET_TAXES = "master.GetTaxes";
        public const string MASTER_GET_ITEMS = "master.GetItems";
        public const string MASTER_GET_ITEM_RATES = "master.GetItemRates";
        public const string MASTER_GET_ITEM_SUB_GROUPS = "master.GetItemSubGroups";
        public const string MASTER_GET_ITEM_SUB_CATEGORIES = "master.GetItemSubCategories";
        public const string MASTER_GET_CRATES_BY_ITEM_ID = "master.GetCratesByItemId";
        public const string MASTER_GET_UNITS_BY_ITEM_ID = "master.GetUnitsByItemId";

        // Master Schema -> Regions
        public const string MASTER_GET_ALL_REGIONS = "master.GetAllRegions";
        public const string MASTER_INSERT_OR_UPDATE_REGION = "master.InsertOrUpdateRegion";
        public const string MASTER_CHANGE_REGION_STATUS = "master.ChangeRegionStatus";
        public const string MASTER_GET_REGION_BY_ID = "master.GetRegionById";
        public const string MASTER_DELETE_REGION = "master.DeleteRegion";
        public const string MASTER_GET_REGIONS = "master.GetRegions";

        // Master Schema -> LedgerGroups
        public const string MASTER_GET_LEDGER_GROUPS = "master.GetLedgerGroups";

        // Master Schema -> Countries
        public const string MASTER_GET_ALL_COUNTRIES = "master.GetAllCountries";
        public const string MASTER_INSERT_OR_UPDATE_COUNTRY = "master.InsertOrUpdateCountry";
        public const string MASTER_CHANGE_COUNTRY_STATUS = "master.ChangeCountryStatus";
        public const string MASTER_GET_COUNTRY_BY_ID = "master.GetCountryById";
        public const string MASTER_DELETE_COUNTRY = "master.DeleteCountry";
        public const string MASTER_GET_COUNTRIES = "master.GetCountries";

        // Master Schema -> PackingTypes
        public const string MASTER_GET_ALL_PACKING_TYPES = "master.GetAllPackingTypes";
        public const string MASTER_INSERT_OR_UPDATE_PACKING_TYPE = "master.InsertOrUpdatePackingType";
        public const string MASTER_CHANGE_PACKING_TYPE_STATUS = "master.ChangePackingTypeStatus";
        public const string MASTER_GET_PACKING_TYPE_BY_ID = "master.GetPackingTypeById";
        public const string MASTER_DELETE_PACKING_TYPE = "master.DeletePackingType";
        public const string MASTER_GET_PACKING_TYPES = "master.GetPackingTypes";

        // Master Schema -> SubUnit
        public const string MASTER_GET_ALL_SUB_UNITS = "master.GetAllSubUnits";
        public const string MASTER_INSERT_OR_UPDATE_SUB_UNIT = "master.InsertOrUpdateSubUnit";
        public const string MASTER_CHANGE_SUB_UNIT_STATUS = "master.ChangeSubUnitStatus";
        public const string MASTER_GET_SUB_UNIT_BY_ID = "master.GetSubUnitById";
        public const string MASTER_DELETE_SUB_UNIT = "master.DeleteSubUnit";
        public const string MASTER_GET_SUB_UNITS = "master.GetSubUnits";

        // Susbcriber Schema
        public const string MASTER_GET_CLIENTS = "master.GetClients";

        // Document Schema -> Purchase
        public const string DOCUMENT_GET_ALL_PURCHASE_DOCUMENTS = "document.GetAllPurchaseDocuments";
        public const string DOCUMENT_GET_ALL_PENDING_PURCHASE_DOCUMENTS = "document.GetAllPendingPurchaseDocuments";
        public const string DOCUMENT_INSERT_OR_UPDATE_PURCHASE_DOCUMENT = "document.InsertOrUpdatePurchaseDocument";
        public const string DOCUMENT_CHANGE_PURCHASE_DOCUMENT_STATUS = "document.ChangePurchaseDocumentStatus";
        public const string DOCUMENT_GET_PURCHASE_DOCUMENT_BY_ID = "document.GetPurchaseDocumentById";
        public const string DOCUMENT_DELETE_PURCHASE_DOCUMENT = "document.DeletePurchaseDocument";

        // Document Schema
        public const string DOCUMENT_GET_DOCUMENT_NUMBERS = "document.GetDocumentNumbers";

        // Subscriber Schema
        public const string SUBSCRIBER_GET_NUMBER_CONFIGURATION = "subscriber.GetNumberConfiguration";

        // Document Schema -> Report -> InwardSlip
        public const string DOCUMENT_GET_ALL_INWARD_SLIPS_FOR_REPORT = "document.GetAllInwardSlipsForReport";

        // Master Schema -> LedgerGroup
        public const string MASTER_GET_ALL_LEDGER_GROUPS = "master.GetAllLedgerGroups";
        public const string MASTER_INSERT_OR_UPDATE_LEDGER_GROUP = "master.InsertOrUpdateLedgerGroup";
        public const string MASTER_CHANGE_LEDGER_GROUP_STATUS = "master.ChangeLedgerGroupStatus";
        public const string MASTER_GET_LEDGER_GROUP_BY_ID = "master.GetLedgerGroupById";
        public const string MASTER_DELETE_LEDGER_GROUP = "master.DeleteLedgerGroup";

        // Master Schema -> Brand
        public const string MASTER_GET_ALL_BRANDS = "master.GetAllBrands";
        public const string MASTER_INSERT_OR_UPDATE_BRAND = "master.InsertOrUpdateBrand";
        public const string MASTER_CHANGE_BRAND_STATUS = "master.ChangeBrandStatus";
        public const string MASTER_GET_BRAND_BY_ID = "master.GetBrandById";
        public const string MASTER_DELETE_BRAND = "master.DeleteBrand";

        // Document Schema -> Sales
        public const string DOCUMENT_GET_ALL_SALES_DOCUMENTS = "document.GetAllSalesDocuments";
        public const string DOCUMENT_GET_ALL_PENDING_RATE_SALES_DOCUMENTS = "document.GetAllPendingRateSalesDocuments";
        public const string DOCUMENT_INSERT_OR_UPDATE_SALES_DOCUMENT = "document.InsertOrUpdateSalesDocument";
        public const string DOCUMENT_CHANGE_SALES_DOCUMENT_STATUS = "document.ChangeSalesDocumentStatus";
        public const string DOCUMENT_GET_SALES_DOCUMENT_BY_ID = "document.GetSalesDocumentById";
        public const string DOCUMENT_DELETE_SALES_DOCUMENT = "document.DeleteSalesDocument";

        // Document Schema -> Receipt
        public const string DOCUMENT_GET_ALL_RECEIPTS = "document.GetAllReceipts";
        public const string DOCUMENT_INSERT_OR_UPDATE_RECEIPT = "document.InsertOrUpdateReceipt";
        public const string DOCUMENT_CHANGE_RECEIPT_STATUS = "document.ChangeReceiptStatus";
        public const string DOCUMENT_GET_RECEIPT_BY_ID = "document.GetReceiptById";
        public const string DOCUMENT_DELETE_RECEIPT = "document.DeleteReceipt";

        // Document Schema -> Payment
        public const string DOCUMENT_GET_ALL_PAYMENTS = "document.GetAllPayments";
        public const string DOCUMENT_INSERT_OR_UPDATE_PAYMENT = "document.InsertOrUpdatePayment";
        public const string DOCUMENT_CHANGE_PAYMENT_STATUS = "document.ChangePaymentStatus";
        public const string DOCUMENT_GET_PAYMENT_BY_ID = "document.GetPaymentById";
        public const string DOCUMENT_DELETE_PAYMENT = "document.DeletePayment";

        // Document Schema -> Payment
        public const string DOCUMENT_GET_ALL_JOURNAL_VOUCHERS = "document.GetAllJournalVouchers";
        public const string DOCUMENT_INSERT_OR_UPDATE_JOURNAL_VOUCHER = "document.InsertOrUpdateJournalVoucher";
        public const string DOCUMENT_CHANGE_JOURNAL_VOUCHER_STATUS = "document.ChangeJournalVoucherStatus";
        public const string DOCUMENT_GET_JOURNAL_VOUCHER_BY_ID = "document.GetJournalVoucherById";
        public const string DOCUMENT_DELETE_JOURNAL_VOUCHER = "document.DeleteJournalVoucher";

        // Document Schema -> Common
        public const string DOCUMENT_GET_LOT_NUMBERS = "document.GetLotNumbers";
        public const string DOCUMENT_GET_ITEM_DETAIL_BY_LOT_NUMBER = "document.GetItemDetailByLotNumber";
        public const string DOCUMENT_CHECK_IF_LOT_NUMBER_IS_ASSIGNED = "document.CheckIfLotNumberIsAssigned";

        // Document Schema -> Report -> Ledger
        public const string DOCUMENT_GET_LEDGER_REPORT = "document.GetLedgerReport";
    }
}
