﻿namespace SSWhite.Data.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SSWhite.Domain.Enums.Document;

    public static class CommonCalls
    {
        public static List<int> GetDocumentTypeList()
        {
            var documentTypeList = new List<int>();
            var documentTypes = Enum.GetValues(typeof(DocumentType)).Cast<DocumentType>().ToList();

            foreach (var documentType in documentTypes)
            {
                documentTypeList.Add((int)documentType);
            }

            return documentTypeList;
        }
    }
}
